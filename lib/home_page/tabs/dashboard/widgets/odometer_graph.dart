import 'package:autonemogps/api_calls/gps_server_requests.dart';
import 'package:autonemogps/config/colors.dart';
import 'package:autonemogps/home_page/tabs/dashboard/bar_chart.dart';
import 'package:autonemogps/models/chart_key_value_model.dart';
import 'package:autonemogps/utils/logs.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/cupertino.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class OdometerGraph extends StatefulWidget {
  OdometerGraph({required Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return OdometerGraphState();
  }
}

class OdometerGraphState extends State<OdometerGraph> {
  List<charts.Series<ChartKeyValueModel, String>> odometerSeriesList = [];

  bool loading = false;
  bool hasNoData = false;

  @override
  void initState() {
    loadGraphData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (loading) {
      return SpinKitCircle(color: AppColors.primaryColor);
    }

    if (hasNoData) {
      return Container();
    }

    return BarGraph(odometerSeriesList, false);
  }

  void loadGraphData() {
    setState(() => loading = true);

    GPSServerRequests.getOdometerDashboardGraph().then((List odometerList) {
      setState(() => this.hasNoData = odometerList.length == 0);

      setState(() {
        List<ChartKeyValueModel> odometerData = [];

        odometerList.forEach((element) {
          try {
            String name = element['name'];
            int odometer = element['odometer'];
            ChartKeyValueModel chartKeyValueModel = new ChartKeyValueModel(name, odometer);
            odometerData.add(chartKeyValueModel);
          } catch (e) {
            writeErrorLogs('getOdometerDashboardGraph', e: e);
          }
        });

        charts.Series<ChartKeyValueModel, String> odometerSeriesValue = new charts.Series<ChartKeyValueModel, String>(
          id: 'Odometer (km, top 10)',
          colorFn: (_, __) => charts.MaterialPalette.red.shadeDefault,
          domainFn: (ChartKeyValueModel chartKeyValueModel, _) => chartKeyValueModel.key,
          measureFn: (ChartKeyValueModel chartKeyValueModel, _) => chartKeyValueModel.value,
          labelAccessorFn: (ChartKeyValueModel chartKeyValueModel, _) => chartKeyValueModel.value.toString(),
          data: odometerData,
        );

        odometerSeriesList = [odometerSeriesValue];
      });

      setState(() => loading = false);
    });
  }
}
