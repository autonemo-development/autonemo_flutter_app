import 'package:autonemogps/config/colors.dart';
import 'package:autonemogps/config/string_constants.dart';
import 'package:flutter/material.dart';

class PrivacyPolicyPage extends StatefulWidget {
  static const String page = 'privacy_policy_page';
  @override
  _PrivacyPolicyPageState createState() => _PrivacyPolicyPageState();
}

class _PrivacyPolicyPageState extends State<PrivacyPolicyPage> {
  String policy_text =
      'This Privacy Policy was last modified on April 4, 2017. $companyName (“$companyName,” “we,” or “us”) are committed to protecting your privacy. This Privacy Policy explains how our practices work regarding the collection, use, disclosure, and protection of information that is collected through our mobile app and our website (collectively, our “Service”), as well as the choices you make regarding the collection and use of information. Capitalized terms that are not defined in this Privacy Policy have the meaning given them in our Terms of Use.\n\n' +
          '$companyName is a GPS Tracking Service using GPS Tracking Devices installed in vehicles or used by the customer for monitoring their concerned Physical Assets. This App or application is used for Tracking the physical devices only, and the data collected through the physical device. Only the customers and or owners of these IOT devices and or assets will be able to use this service for their Asset Tracking purposes. This is the domain of the service of this Application.';

  String collect_and_use_text =
      'We collect the following types of information about you: Registration and Profile Information. When you create an account and during the use of the service, we may collect your personal information such as your username, first and last name, email address, mobile phone number. If you want to add another member to your account, we collect the member’s name, email address, and mobile phone number. We may also collect billing information when you sign up for the Paid Service. \n\n' +
          'After you set up your account, we may ask for other profile and demographic information, such as your organization name and details, like address, phone number and city.Geolocation. The Geolocation data will be collected from physical IOT or physical devices bought by the customer of this Application. We will collect Geolocation from the App only for showing the current location of the user in the map to show distance from a specific vehicle using $companyName Devices.\n\n' +
          'Driving Event Data. We collect sensory and motion data from your smartphone or mobile device, including information from the gyroscope, accelerometer, compass and Bluetooth, in order to calculate and detect driving events such as speeding, hard braking, crash detection and other events. We maintain sensory and motion data as long as is reasonable to provide the Service which is related to the IOT devices used for the Application.\n\n' +
          'Messages and User Content. We collect information that is stored or used throughout the GPS Tracking service. We use this information to operate, maintain, and provide the features and functionality of the Service and to communicate directly with the customer, such as to send the customer alerts on the movements of the (customer) user’s vehicles or assets which is only related to the concerned customer’s vehicles or assets. The customer or user can control the receipt of email and text alerts by visiting “Settings” within the mobile app or email. We may also send Service-related emails (e.g., account verification, order confirmations, change or updates to features of the Service, technical and security notices) related to the Service the customer has opted for.\n\n' +
          'Information we receive about you from others: Social Media. When you interact with our site through various social media, such as when you Like us on Facebook or post a comment to our Facebook page, we may receive information from the social network such as your profile information, profile picture, gender, user name, user ID associated with your social media account, age range, language, country, friends list, and any other information you permit the social network to share with third parties. The data we receive is dependent upon your privacy settings with the social network. You should always review, and if necessary, adjust your privacy settings on third-party websites and services before linking or connecting them to $companyName’s website or Service.\n\n' +
          'Information we collect automatically through the use of technology. When you visit our website or use our mobile app, we and the related technology providers which we are not directly connected to other than using their analytics software) may collect certain information about your computer or device through technology such as cookies, web beacons, log files, or other tracking/recording tools. The information we collect through the use of tracking technologies includes, but is not limited to, IP address, browser information, referring/exit pages and URLs, click stream data and information about how you interact with links on the website, mobile app, or Service, domain names, landing pages, page views, cookie data that allows us to uniquely identify your browser and track your browsing behavior on our site, mobile device type, mobile device IDs or other persistent identifiers, and location data collected from your mobile device. Some or all of this data may be combined with other personally identifying information described above. When you access our Service by or through a mobile device, we may receive or collect and store a unique identification numbers associated with your device or our mobile application (including, for example, a UDID, Unique ID for Advertisers (“IDFA”), Google Ad ID, or Windows Advertising ID or other identifier), mobile carrier, device type, model and manufacturer, mobile device operating system brand and model, phone number, and, depending on your mobile device settings, your geographical location data, including GPS coordinates (e.g. latitude and/or longitude), WiFi location or similar information regarding the location of your mobile device. We use this information to: Remember information so that you will not have to re-enter it during your visit or the next time you visit the site or mobile app Provide custom, personalized content and information, including advertising Track your location and movements Monitor the effectiveness of our Service and our marketing campaigns Monitor aggregate metrics such as total number of visitors, users, traffic, and demographic patterns Diagnose or fix technology problems reported by our Users or engineers that are associated with certain IP addresses or User IDs, and Automatically update our mobile application on your system and related devices.\n\n' +
          'We may also collect analytics data, or use third-party analytics tools, to help us measure traffic and usage trends for the Service. These tools collect information sent by your browser or mobile device, including the pages you visit, your use of third party applications, and other information that assists us in analyzing and improving the Service.';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: false,
        title: Text('Privacy Policy', style: TextStyle(color: Color(0xFF474747), fontWeight: FontWeight.w500, fontSize: 16)),
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Color(0xFF7E7E7E)),
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(15),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Padding(
                padding: const EdgeInsets.all(20.0),
                child: Text('Privacy Policy of $companyName', textAlign: TextAlign.center, style: TextStyle(color: AppColors.primaryColor, fontSize: 17, fontWeight: FontWeight.w500)),
              ),
              Container(
                padding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 7),
                decoration: BoxDecoration(
                  color: Color(0xFFF8F8F8),
                  borderRadius: BorderRadius.circular(4.0),
                ),
                child: Center(
                  child: Text('PLEASE READ THESE PRIVACY POLICY CAREFULLY', textAlign: TextAlign.center, style: TextStyle(color: Color(0xFF686868), fontSize: 14, fontWeight: FontWeight.w400)),
                ),
              ),
              SizedBox(height: 15),
              Text(policy_text, textAlign: TextAlign.justify, style: TextStyle(color: Color(0xFF3B3B3B), fontWeight: FontWeight.w300)),
              SizedBox(height: 15),
              Text('1. How We Collect And Use Information', style: TextStyle(color: Color(0xFF3B3B3B), fontWeight: FontWeight.w500, fontSize: 15)),
              SizedBox(height: 12),
              Text(collect_and_use_text, textAlign: TextAlign.justify, style: TextStyle(color: Color(0xFF3B3B3B), fontWeight: FontWeight.w300)),
              SizedBox(height: 12),
              InkWell(
                onTap: () => Navigator.pop(context),
                child: Container(
                  width: double.infinity,
                  padding: EdgeInsets.all(12.0),
                  decoration: BoxDecoration(color: AppColors.primaryColor, borderRadius: BorderRadius.circular(4.0)),
                  child: Center(
                    child: Text('Ok Got it', style: TextStyle(color: Colors.white, fontSize: 16, fontWeight: FontWeight.w500)),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
