import 'package:autonemogps/models/tracking_device_model.dart';
import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_gauges/gauges.dart';

class AdvanceSpeedometer extends StatelessWidget {
  final TrackingDevice trackingDevice;

  const AdvanceSpeedometer(this.trackingDevice);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 250,
      margin: EdgeInsets.only(right: 50, left: 50, top: 10),
      child: SfRadialGauge(axes: <RadialAxis>[
        RadialAxis(
          minimum: -20,
          maximum: 140,
          startAngle: 150,
          endAngle: 30,
          showAxisLine: false,
          showTicks: false,
          showFirstLabel: false,
          showLastLabel: false,
          showLabels: false,
          ranges: <GaugeRange>[
            GaugeRange(startValue: -20, endValue: -1, startWidth: 0.2, sizeUnit: GaugeSizeUnit.factor, endWidth: 0.2, color: Colors.white),
            GaugeRange(startValue: 0, endValue: 19, startWidth: 0.2, sizeUnit: GaugeSizeUnit.factor, endWidth: 0.2, color: const Color(0xFFCEEAAE)),
            GaugeRange(startValue: 20, endValue: 39, startWidth: 0.2, sizeUnit: GaugeSizeUnit.factor, endWidth: 0.2, color: const Color(0xFFE3F065)),
            GaugeRange(startValue: 40, endValue: 59, startWidth: 0.2, sizeUnit: GaugeSizeUnit.factor, endWidth: 0.2, color: const Color(0xFFFFF070)),
            GaugeRange(startValue: 60, endValue: 79, startWidth: 0.2, sizeUnit: GaugeSizeUnit.factor, endWidth: 0.2, color: const Color(0xFFFD904A)),
            GaugeRange(startValue: 80, endValue: 99, startWidth: 0.2, sizeUnit: GaugeSizeUnit.factor, endWidth: 0.2, color: const Color(0xFFEB5E5E)),
            GaugeRange(startValue: 100, endValue: 119, startWidth: 0.2, sizeUnit: GaugeSizeUnit.factor, endWidth: 0.2, color: const Color(0xFFE74A46)),
            GaugeRange(startValue: 120, endValue: 140, startWidth: 0.2, sizeUnit: GaugeSizeUnit.factor, endWidth: 0.2, color: Colors.white),
          ],
          pointers: <GaugePointer>[
            NeedlePointer(
              value: trackingDevice.speed!.toDouble(),
              needleStartWidth: 0.5,
              needleEndWidth: 2,
              enableAnimation: true,
              animationType: AnimationType.bounceOut,
              needleColor: Color(0xFF9A73DF),
              knobStyle: KnobStyle(color: Color(0xFF9A73DF)),
            )
          ],
          annotations: <GaugeAnnotation>[
            GaugeAnnotation(
              widget: Container(
                width: 100,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(child: Text('${trackingDevice.speed!}', style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500), textAlign: TextAlign.center)),
                    Container(margin: EdgeInsets.only(left: 5), child: Text('KM/H', style: TextStyle(fontSize: 12, fontWeight: FontWeight.w300), textAlign: TextAlign.center)),
                  ],
                ),
              ),
              angle: 90,
              positionFactor: 0.3,
            )
          ],
        ),
        RadialAxis(
          axisLineStyle: AxisLineStyle(thickness: 1),
          radiusFactor: 0.75,
          showTicks: false,
          showFirstLabel: false,
          showLastLabel: false,
          showLabels: false,
          minimum: -20,
          maximum: 120,
          startAngle: 150,
          endAngle: 30,
        ),
        RadialAxis(
          axisLineStyle: AxisLineStyle(thickness: 1),
          radiusFactor: 0.97,
          showTicks: false,
          showFirstLabel: false,
          showLastLabel: false,
          showLabels: false,
          minimum: -20,
          maximum: 120,
          startAngle: 150,
          endAngle: 30,
        )
      ]),
    );
  }
}
