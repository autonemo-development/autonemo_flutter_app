import 'package:autonemogps/config/helpers.dart';
import 'package:autonemogps/models/device_model.dart';
import 'package:autonemogps/models/sensor_model.dart';
import 'package:autonemogps/models/tracking_device_model.dart';
import 'package:flutter/material.dart';
import 'package:text_scroll/text_scroll.dart';

class TrackingPageLayoutPart3 extends StatefulWidget {
  final TrackingDevice currentTrackingDevice;
  final DeviceModel deviceModel;

  TrackingPageLayoutPart3(this.currentTrackingDevice, this.deviceModel);

  @override
  State<StatefulWidget> createState() {
    return TrackingPageLayoutPart3State();
  }
}

class TrackingPageLayoutPart3State extends State<TrackingPageLayoutPart3> {
  late TrackingDevice currentTrackingDevice;

  @override
  void initState() {
    currentTrackingDevice = widget.currentTrackingDevice;
    super.initState();
  }

  @override
  void didUpdateWidget(covariant TrackingPageLayoutPart3 oldWidget) {
    currentTrackingDevice = widget.currentTrackingDevice;
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(color: Colors.white),
      width: MediaQuery.of(context).size.width,
      child: SingleChildScrollView(scrollDirection: Axis.horizontal, child: Container(margin: EdgeInsets.only(left: 5), child: Row(children: children()))),
    );
  }

  List<Widget> children() {
    List<Sensor> sList = currentTrackingDevice.sensorList!;

    List<Sensor> sensorList = [];

    sensorList.addAll(getFirstThreeSensor(widget.currentTrackingDevice));

    for (int j = 0; j < sList.length; j++) {
      Sensor sensor = sList.elementAt(j);
      if (sensor.sensorName == 'AC' ||
          sensor.sensorName == 'Charging Status' ||
          sensor.sensorName!.toLowerCase().contains('fuel') ||
          sensor.sensorName!.toLowerCase().contains('battery') ||
          sensor.sensorName!.toLowerCase().contains('air')) {
        sensorList.add(sensor);
      }
    }

    return sensorList.map((sensor) => SensorItemWidget(sensor)).toList();
  }
}

class SensorItemWidget extends StatelessWidget {
  final Sensor sensorItem;

  SensorItemWidget(this.sensorItem);

  @override
  Widget build(BuildContext context) {
    String text = sensorItem.sensorValue.toString();
    return Container(
      margin: EdgeInsets.only(top: 10, left: 5, right: 5, bottom: 10),
      width: 60,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          TextScroll(
            sensorItem.sensorName!,
            style: TextStyle(color: Colors.black, fontSize: 10, fontWeight: FontWeight.w500),
            velocity: Velocity(pixelsPerSecond: Offset(50, 0)),
          ),
          Container(
              padding: EdgeInsets.all(7),
              height: 30,
              width: 30,
              margin: EdgeInsets.only(top: 5, bottom: 5),
              decoration: BoxDecoration(shape: BoxShape.circle, color: sensorItem.sensorLightColor),
              child: Image.asset(sensorItem.imageName!, color: sensorItem.sensorColor)),
          TextScroll(
            text,
            textAlign: TextAlign.center,
            style: TextStyle(color: Colors.black, fontSize: 10, fontWeight: FontWeight.w500),
            velocity: Velocity(pixelsPerSecond: Offset(50, 0)),
          )
        ],
      ),
    );
  }
}
