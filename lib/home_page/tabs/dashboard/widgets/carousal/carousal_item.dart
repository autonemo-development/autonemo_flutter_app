import 'package:autonemogps/config/helpers.dart';
import 'package:flutter/material.dart';

class CarousalItem extends StatelessWidget {
  final String assetImagePath;
  final String link;

  CarousalItem(this.assetImagePath, this.link);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        launchUrl(this.link);
      },
      child: Container(
        height: 120,
        margin: EdgeInsets.only(right: 5, left: 5),
        decoration: BoxDecoration(
          image: DecorationImage(fit: BoxFit.cover, image: AssetImage(this.assetImagePath)),
          borderRadius: BorderRadius.circular(16),
        ),
      ),
    );
  }
}
