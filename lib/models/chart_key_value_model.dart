class ChartKeyValueModel {
  final String key;
  final int value;

  ChartKeyValueModel(this.key, this.value);
}
