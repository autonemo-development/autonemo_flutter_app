import 'package:autonemogps/api_calls/gps_server_requests.dart';
import 'package:autonemogps/config/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

class FuelGraph extends StatefulWidget {
  final String selectedImei;

  final String selectedDevice;

  FuelGraph(this.selectedImei, this.selectedDevice, {required Key key}) : super(key: key);

  @override
  State<FuelGraph> createState() => FuelGraphState();
}

class FuelGraphState extends State<FuelGraph> {
  List<TimeSeriesSales> fuelGraphSeries = [];

  bool loading = false;
  bool hasNoData = false;

  @override
  void initState() {
    loadDevicesData();
    super.initState();
  }

  @override
  void didUpdateWidget(covariant FuelGraph oldWidget) {
    loadDevicesData();
    super.didUpdateWidget(oldWidget);
  }

  @override
  void didChangeDependencies() {
    loadDevicesData();
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    if (loading) {
      return SpinKitCircle(color: AppColors.primaryColor);
    }

    if (hasNoData) {
      return Container();
    }

    return Container(
      padding: EdgeInsets.all(10),
      margin: EdgeInsets.only(bottom: 16),
      height: 200,
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(16),
        boxShadow: [BoxShadow(color: AppColors.shadowMaterialColor[100]!, blurRadius: 5.0, spreadRadius: 1.0, offset: Offset(1.0, 2.0))],
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                child: Text.rich(
                  TextSpan(
                    children: [TextSpan(text: widget.selectedDevice + ' ( Fuel Chart (L) Today )', style: TextStyle(fontSize: 16, fontWeight: FontWeight.w600))],
                  ),
                ),
              ),
            ],
          ),
          SizedBox(width: 20),
          Divider(),
          Expanded(
            child: SfCartesianChart(
              primaryXAxis: DateTimeAxis(),
              series: <ChartSeries<TimeSeriesSales, DateTime>>[
                LineSeries(
                  dataSource: fuelGraphSeries,
                  xValueMapper: (TimeSeriesSales sale, _) => sale.time,
                  yValueMapper: (TimeSeriesSales sale, _) => sale.sales,
                  color: Colors.red,
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  void loadDevicesData() {
    setState(() => this.loading = true);

    GPSServerRequests.getFuelWidgetStatisticsData(widget.selectedImei).then((data) {
      setState(() => this.hasNoData = data.length == 0);

      List<TimeSeriesSales> seriesData = [];

      DateTime now = DateTime.now();

      data.forEach((element) {
        int element0 = element.elementAt(0);
        String element1 = element.elementAt(1);

        int unix = element0 - now.timeZoneOffset.inMilliseconds;

        double value = double.parse(element1);

        TimeSeriesSales timeSeriesSaleItem = new TimeSeriesSales(new DateTime.fromMillisecondsSinceEpoch(unix), value);

        seriesData.add(timeSeriesSaleItem);
      });

      setState(() => this.fuelGraphSeries = seriesData);
      setState(() => this.loading = false);
    });
  }
}

class TimeSeriesSales {
  final DateTime time;
  final double sales;

  TimeSeriesSales(this.time, this.sales);
}
