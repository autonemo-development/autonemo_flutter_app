import 'package:autonemogps/api_calls/gps_server_requests.dart';
import 'package:autonemogps/config/colors.dart';
import 'package:autonemogps/config/helpers.dart';
import 'package:autonemogps/config/string_constants.dart';
import 'package:autonemogps/models/maintenance.dart';
import 'package:autonemogps/screens/maintenance_page/add_maintenance_page.dart';
import 'package:autonemogps/screens/maintenance_page/add_maintence_gpswox_page.dart';
import 'package:autonemogps/storage/repository.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class MaintenanceDashboard extends StatefulWidget {
  MaintenanceDashboard({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return MaintenanceDashboardState();
  }
}

class MaintenanceDashboardState extends State {
  List<Map<String, dynamic>> dueCount = [];
  List<Map<String, dynamic>> overdueCount = [];
  List<Maintenance> maintenanceList = [];

  bool loading = false;
  bool hasNoData = false;

  @override
  void initState() {
    fetchMaintenanceData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (loading) {
      return SpinKitCircle(color: AppColors.primaryColor);
    }

    if (hasNoData) {
      return Container();
    }

    return Container(
      padding: EdgeInsets.all(10),
      margin: EdgeInsets.only(bottom: 16),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(16),
        boxShadow: [BoxShadow(color: AppColors.shadowMaterialColor[100]!, blurRadius: 5.0, spreadRadius: 1.0, offset: Offset(1.0, 2.0))],
      ),
      child: Column(mainAxisSize: MainAxisSize.min, crossAxisAlignment: CrossAxisAlignment.start, children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text('Maintenance Reminder', style: TextStyle(color: AppColors.headerTextColor)),
            InkWell(
              onTap: () => Navigator.push(context, MaterialPageRoute(builder: (context) => Repository.getServerType() == GPSWoxConstants.type ? AddMaintenanceGPSWoxPage() : AddMaintenancePage())),
              child: Row(children: [Icon(Icons.add_circle), SizedBox(width: 5), Text('Add Service')]),
            )
          ],
        ),
        SizedBox(width: 20),
        Divider(),
        DueAndOverDue(dueCount: dueCount, overdueCount: overdueCount),
        SizedBox(height: 5),
        _buildMaintenanceListContainer(),
      ]),
    );
  }

  Container _buildMaintenanceListContainer() {
    return Container(
      height: maintenanceList.length * 55,
      constraints: BoxConstraints(maxHeight: 250),
      child: ListView.builder(
        itemCount: maintenanceList.length,
        itemBuilder: (context, index) {
          Maintenance maintenance = maintenanceList.elementAt(index);
          return Container(
            decoration: BoxDecoration(color: Color(0xFFF8F8F8), borderRadius: BorderRadius.circular(10)),
            margin: EdgeInsets.only(top: 5),
            padding: EdgeInsets.all(10),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              mainAxisSize: MainAxisSize.max,
              children: [
                Expanded(
                  child: Container(
                    child: Row(
                      children: [
                        Container(width: 2, decoration: BoxDecoration(color: getBarColor(maintenance)), height: 20),
                        SizedBox(width: 5),
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(maintenance.name, style: TextStyle(fontSize: 12, fontWeight: FontWeight.w600)),
                              SizedBox(height: 2),
                              Text(maintenance.objectName, style: TextStyle(fontSize: 10)),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(width: 10),
                Expanded(
                  child: Container(
                    child: Row(
                      children: [
                        Container(
                          child: Image.asset('images/maintenance_odometer.png'),
                          width: 20,
                          height: 20,
                        ),
                        SizedBox(width: 5),
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(maintenance.odometer, style: TextStyle(fontSize: 12, fontWeight: FontWeight.w600)),
                              Text(getHtmlText(maintenance.odometerLeft), style: TextStyle(fontSize: 10, color: checkHasRedText(maintenance.odometerLeft))),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ),
                SizedBox(width: 10),
                Expanded(
                  child: Container(
                    child: Row(
                      children: [
                        Container(
                          child: Image.asset('images/maintenance_clock.png'),
                          width: 20,
                          height: 20,
                        ),
                        SizedBox(width: 5),
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(maintenance.days, style: TextStyle(fontSize: 12, fontWeight: FontWeight.w600)),
                              Text(getHtmlText(maintenance.daysLeft.replaceAll(' d', ' Days Left')), style: TextStyle(fontSize: 10, color: checkHasRedText(maintenance.daysLeft))),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          );
        },
      ),
    );
  }

  void fetchMaintenanceData() {
    setState(() => this.loading = true);

    if (Repository.getServerType() == GPSServerConstants.type) {
      GPSServerRequests.fetchDashboardMaintenance().then((value) {
        value.forEach((map) {
          Map<String, dynamic> maintenanceData = map as Map<String, dynamic>;
          if (maintenanceData['days_left_val'] < 0 || maintenanceData['engine_hours_left_val'] < 0 || maintenanceData['odometer_left_val'] < 0) {
            overdueCount.add(maintenanceData);
          } else {
            dueCount.add(maintenanceData);
          }
        });

        setState(() => this.loading = false);
      });

      GPSServerRequests.fetchMaintenances().then((value) {
        setState(() => this.hasNoData = value.length == 0);
        setState(() => maintenanceList = value);
        setState(() => this.loading = false);
      });
    }
  }

  getBarColor(Maintenance maintenance) {
    if (maintenance.odometerLeft.contains('red') || maintenance.daysLeft.contains('red')) {
      return Colors.red;
    }

    return Colors.orange;
  }
}

class DueAndOverDue extends StatelessWidget {
  const DueAndOverDue({Key? key, required this.dueCount, required this.overdueCount}) : super(key: key);

  final List<Map<String, dynamic>> dueCount;
  final List<Map<String, dynamic>> overdueCount;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Container(
          decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: Color(0xffF8F8F8)),
          constraints: BoxConstraints(minWidth: 150),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                padding: EdgeInsets.all(10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [Text('DUE'), Text(dueCount.length.toString(), style: TextStyle(fontSize: 25, fontWeight: FontWeight.w700))],
                ),
              ),
              Container(padding: EdgeInsets.all(5), height: 60, child: Image.asset('images/maintenance_due.png'))
            ],
          ),
        ),
        Container(
          decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: Color(0xffF8F8F8)),
          constraints: BoxConstraints(minWidth: 150),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                padding: EdgeInsets.all(10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [Text('OVERDUE'), Text(overdueCount.length.toString(), style: TextStyle(fontSize: 25, fontWeight: FontWeight.w700))],
                ),
              ),
              Container(padding: EdgeInsets.all(5), height: 60, child: Image.asset('images/maintenance_overdue.png'))
            ],
          ),
        ),
      ],
    );
  }
}
