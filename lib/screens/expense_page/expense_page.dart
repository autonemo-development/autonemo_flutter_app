import 'package:autonemogps/api_calls/gps_server_requests.dart';
import 'package:autonemogps/config/colors.dart';
import 'package:autonemogps/config/helpers.dart';
import 'package:autonemogps/models/expense.dart';
import 'package:autonemogps/screens/expense_page/add_expense_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class ExpensePage extends StatefulWidget {
  static const String page = 'expense_page';

  @override
  _ExpensePageState createState() => _ExpensePageState();
}

class _ExpensePageState extends State<ExpensePage> {
  bool _isDataLoading = true;
  List<Expense> expenseList = List<Expense>.empty(growable: true);
  String? error;

  @override
  void initState() {
    super.initState();
    fetchData();
  }

  fetchData() {
    GPSServerRequests.fetchExpense().then((list) {
      setState(() {
        _isDataLoading = false;
        if (list.length > 0) {
          expenseList = list;
        } else {
          error = 'No Expense';
        }
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
      child: Container(
        height: MediaQuery.of(context).size.height,
        decoration: BoxDecoration(
          gradient: LinearGradient(begin: Alignment.topCenter, end: Alignment.bottomCenter, colors: [
            Color(0x90DC0E18),
            Color(0x10DC0E18),
          ]),
        ),
        child: Column(
          children: [
            SizedBox(height: 10),
            Row(
              children: [
                IconButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    icon: Icon(
                      Icons.keyboard_backspace,
                      color: Colors.white,
                    )),
                Expanded(
                  child: Text(
                    'Expense',
                    style: TextStyle(color: Colors.white, fontSize: 24, fontWeight: FontWeight.bold),
                  ),
                ),
                IconButton(
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => AddExpensePage(
                              'false',
                            ),
                          )).then((value) {
                        if (value != null && value == true) {
                          setState(() {
                            _isDataLoading = true;
                          });
                          fetchData();
                        }
                      });
                    },
                    icon: Icon(
                      Icons.add,
                      color: Colors.white,
                    )),
              ],
            ),
            Expanded(
              child: _isDataLoading
                  ? SpinKitRing(
                      lineWidth: 3.0,
                      color: AppColors.primaryColor,
                      size: 35.0,
                    )
                  : error == null
                      ? Stack(children: [
                          Container(
                              child: ListView.separated(
                            itemCount: expenseList.length,
                            separatorBuilder: (context, index) => Divider(),
                            itemBuilder: (context, index) {
                              return ExpenseListItemWidget(
                                () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                        builder: (context) => AddExpensePage(
                                          expenseList.elementAt(index).id,
                                        ),
                                      )).then((value) {
                                    if (value != null && value == true) {
                                      setState(() {
                                        _isDataLoading = true;
                                      });
                                      fetchData();
                                    }
                                  });
                                },
                                () {
                                  showDialog(
                                      barrierDismissible: false,
                                      context: context,
                                      builder: (context) => Center(
                                              child: SpinKitRing(
                                            lineWidth: 3.0,
                                            color: AppColors.primaryColor,
                                            size: 35.0,
                                          )));
                                  GPSServerRequests.deleteExpenseById(expenseList.elementAt(index).id).then((list) {
                                    Navigator.pop(context);
                                    if (list[0] == 0) {
                                      showError('Something went wrong!!!', context);
                                    } else {
                                      expenseList.removeAt(index);
                                      setState(() {});
                                    }
                                  });
                                },
                                expenseList.elementAt(index),
                                index,
                              );
                            },
                          )),
                        ])
                      : Center(
                          child: Text(error!),
                        ),
            ),
          ],
        ),
      ),
    ));
  }

  showError(String error, BuildContext context) {
    showSnackBar(context, error, Colors.red);
  }
}

class ExpenseListItemWidget extends StatelessWidget {
  final Expense expense;
  final int position;
  final void Function() onEditClicked, onDeleteClicked;

  ExpenseListItemWidget(this.onEditClicked, this.onDeleteClicked, this.expense, this.position);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(15.0),
      margin: EdgeInsets.symmetric(horizontal: 16, vertical: 4.0),
      decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(16.0), boxShadow: [
        BoxShadow(
          color: AppColors.primaryMaterialColor[100]!,
          blurRadius: 5.0,
          spreadRadius: 1.0,
          offset: Offset(
            1.0, // Move to right 10  horizontally
            2.0, // Move to bottom 5 Vertically
          ),
        )
      ]),
      child: Row(
        children: [
          Expanded(
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          expense.name,
                          style: TextStyle(color: AppColors.headerTextColor, fontSize: 16, fontWeight: FontWeight.w700),
                        ),
                        Text(
                          expense.objectName,
                          style: TextStyle(color: AppColors.headerTextColor, fontSize: 12, fontWeight: FontWeight.w400),
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Text(
                          expense.date,
                          style: TextStyle(color: Color(0xFF474747), fontSize: 16, fontWeight: FontWeight.w500),
                        ),
                        Text(
                          expense.cost,
                          style: TextStyle(color: AppColors.headerTextColor, fontSize: 12, fontWeight: FontWeight.w400),
                        ),
                      ],
                    ),
                  ],
                ),
                SizedBox(
                  height: 15.0,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          children: [
                            Text(
                              'Quantity : ',
                              style: TextStyle(color: Color(0xFF7E7E7E), fontSize: 11, fontWeight: FontWeight.w500),
                            ),
                            Text(
                              expense.qty,
                              style: TextStyle(color: Color(0xFF474747), fontSize: 16, fontWeight: FontWeight.w500),
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            Text(
                              'Buyer : ',
                              style: TextStyle(color: Color(0xFF7E7E7E), fontSize: 11, fontWeight: FontWeight.w500),
                            ),
                            Text(
                              expense.buyer,
                              style: TextStyle(color: Color(0xFF474747), fontSize: 13, fontWeight: FontWeight.w500),
                            ),
                          ],
                        ),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Supplier',
                          style: TextStyle(color: Color(0xFF7E7E7E), fontSize: 11, fontWeight: FontWeight.w500),
                        ),
                        SizedBox(
                          height: 5.0,
                        ),
                        Text(
                          expense.supplier,
                          style: TextStyle(color: Color(0xFF474747), fontSize: 13, fontWeight: FontWeight.w500),
                        ),
                      ],
                    ),
                  ],
                )
              ],
            ),
          ),
          SizedBox(
            width: 16,
          ),
          Column(
            children: [
              InkWell(
                onTap: onEditClicked,
                child: Container(
                  height: 30,
                  width: 30,
                  padding: EdgeInsets.all(7.0),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(3.0),
                    color: AppColors.sensorLightActiveColor,
                  ),
                  child: Image.asset(
                    'images/edit.png',
                    color: AppColors.sensorActiveColor,
                  ),
                ),
              ),
              SizedBox(
                height: 16.0,
              ),
              InkWell(
                onTap: onDeleteClicked,
                child: Container(
                  height: 30,
                  width: 30,
                  padding: EdgeInsets.all(7.0),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(3.0),
                    color: Color(0xFFFFEEE4),
                  ),
                  child: Image.asset(
                    'images/cancel.png',
                    color: Color(0xFFD43333),
                  ),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}
