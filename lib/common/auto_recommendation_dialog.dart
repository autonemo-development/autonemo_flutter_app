import 'dart:math';

import 'package:animations/animations.dart';
import 'package:autonemogps/api_calls/gps_server_requests.dart';
import 'package:autonemogps/config/string_constants.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher_string.dart';

showAutoRecommendationDialog(BuildContext context) {
  GPSServerRequests.getClientData().then((value) {
    if (value[STATUS] == 1) {
      Map<String, dynamic> dataMap = value[DATA];
      List<dynamic> vehicleData = dataMap['vehicle_data'];
      Random random = Random();
      Map<String, dynamic> vehicleDataItem = vehicleData.elementAt(random.nextInt(vehicleData.length));
      showModal(context: context, builder: (context) => Dialog(backgroundColor: Colors.transparent, child: RecommendationDialogUi(vehicleDataItem)));
    }
  });
}

class RecommendationDialogUi extends StatefulWidget {
  final Map<String, dynamic> vehicleDataItem;

  RecommendationDialogUi(this.vehicleDataItem, {Key? key}) : super(key: key);

  @override
  State<RecommendationDialogUi> createState() => _RecommendationDialogUiState();
}

class _RecommendationDialogUiState extends State<RecommendationDialogUi> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: Colors.white),
      child: Stack(
        children: [
          Positioned(
            top: 0,
            right: 0,
            child: InkWell(
              onTap: () => Navigator.pop(context),
              child: Container(
                child: Icon(Icons.close, size: 20),
                decoration: BoxDecoration(color: Colors.grey, borderRadius: BorderRadius.circular(20)),
                padding: EdgeInsets.all(5),
                margin: EdgeInsets.all(10),
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.all(20),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  child: Column(
                    children: [
                      Container(child: Image.asset('images/engine_oil_recommendation_dashboard_icon.png'), padding: EdgeInsets.symmetric(horizontal: 20)),
                      Container(
                        padding: EdgeInsets.symmetric(horizontal: 20),
                        child: Text(
                          'Change Engine Oil on time with Genuine Products from our Online Shop',
                          style: TextStyle(fontSize: 18, color: Colors.black, fontWeight: FontWeight.w600),
                          textAlign: TextAlign.center,
                        ),
                      )
                    ],
                  ),
                ),
                Container(
                  decoration: BoxDecoration(color: Color(0xFFF8F8F8), borderRadius: BorderRadius.circular(10)),
                  margin: EdgeInsets.only(top: 5),
                  padding: EdgeInsets.all(10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Container(child: Image.asset('images/engine_oil_recommendation_dashboard_icon.png'), height: 25, width: 25),
                      SizedBox(width: 5),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(widget.vehicleDataItem['vehicle_model'], style: TextStyle(fontSize: 12, fontWeight: FontWeight.w600)),
                            SizedBox(height: 2),
                            Text(widget.vehicleDataItem['car_reg_number'], style: TextStyle(fontSize: 10)),
                          ],
                        ),
                      ),
                      SizedBox(width: 10),
                      if (widget.vehicleDataItem['engine_oil_grade'] != null)
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: [
                            Text(widget.vehicleDataItem['engine_oil_grade'], style: TextStyle(fontSize: 12, fontWeight: FontWeight.w600, color: Colors.red)),
                            Text('Oil Grade', style: TextStyle(fontSize: 10)),
                          ],
                        ),
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(top: 20),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      InkWell(
                        onTap: () => Navigator.pop(context),
                        child: Container(child: Text('REMIND ME LATER', style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500))),
                      ),
                      InkWell(
                        onTap: () {
                          Navigator.pop(context);
                          launchUrlString(SHOP_URL, mode: LaunchMode.externalApplication);
                        },
                        child: Container(
                          padding: EdgeInsets.only(right: 20, left: 20, top: 10, bottom: 10),
                          decoration: BoxDecoration(
                            color: Colors.red,
                            borderRadius: BorderRadius.circular(20.0),
                            boxShadow: [BoxShadow(color: Colors.black.withOpacity(.5), blurRadius: 1, offset: Offset(0, 1))],
                          ),
                          child: Text('SHOP NOW', style: TextStyle(color: Colors.white, fontSize: 14, fontWeight: FontWeight.w500)),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
