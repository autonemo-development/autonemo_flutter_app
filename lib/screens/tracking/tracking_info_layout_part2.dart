import 'package:autonemogps/api_calls/gps_server_requests.dart';
import 'package:autonemogps/models/device_model.dart';
import 'package:autonemogps/models/gen_info_data.dart';
import 'package:autonemogps/models/tracking_device_model.dart';
import 'package:flutter/material.dart';

class TrackingPageLayoutPart2 extends StatefulWidget {
  final TrackingDevice currentTrackingDevice;

  final DeviceModel deviceModel;

  TrackingPageLayoutPart2(this.currentTrackingDevice, this.deviceModel);

  @override
  State<StatefulWidget> createState() {
    return TrackingPageLayoutPart2State();
  }
}

class TrackingPageLayoutPart2State extends State<TrackingPageLayoutPart2> {
  late TrackingDevice currentTrackingDevice;

  List<GenInfoData> genInfoDataList = [];

  @override
  void initState() {
    currentTrackingDevice = widget.currentTrackingDevice;
    fetchWidgetStats();
    super.initState();
  }

  @override
  void didUpdateWidget(covariant TrackingPageLayoutPart2 oldWidget) {
    currentTrackingDevice = widget.currentTrackingDevice;
    fetchWidgetStats();
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(color: Colors.white),
      width: MediaQuery.of(context).size.width,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
              padding: EdgeInsets.only(left: 10, top: 5, bottom: 5),
              child: Text(
                'Today\'s Statistics',
                style: TextStyle(fontSize: 12),
              )),
          Container(
            child: SingleChildScrollView(scrollDirection: Axis.horizontal, child: Row(children: children())),
          ),
        ],
      ),
    );
  }

  List<Widget> children() {
    List<Widget> list = [
      DetailWidgetWidget(image: 'images/odometer_1.png', name: 'Odometer', value: '${this.currentTrackingDevice.odometer!.toStringAsFixed(0)} km'),
    ];

    this.genInfoDataList.forEach((element) {
      list.add(DetailWidgetWidget(
        image: element.image,
        name: element.title,
        value: element.value,
      ));
    });

    return list;
  }

  void fetchWidgetStats() {
    GPSServerRequests.getWidgetStatisticsData(widget.deviceModel.imei, widget.deviceModel.name).then((List<GenInfoData> genInfoDataList) {
      if (mounted) setState(() => this.genInfoDataList = genInfoDataList);
    });
  }
}

class DetailWidgetWidget extends StatelessWidget {
  const DetailWidgetWidget({Key? key, required this.image, required this.name, required this.value}) : super(key: key);

  final String image, name, value;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 35,
      margin: EdgeInsets.only(left: 10, bottom: 10),
      decoration: BoxDecoration(color: Color(0xffF1F1F1), borderRadius: BorderRadius.circular(5)),
      padding: EdgeInsets.all(2),
      child: Row(
        children: [
          Container(child: Image.asset(image), margin: EdgeInsets.only(left: 5), height: 25, width: 25),
          Container(
            margin: EdgeInsets.only(left: 5, right: 5),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(child: Text(name, style: TextStyle(fontSize: 12, color: Color(0xff5397CC), height: 1.2))),
                Container(child: Expanded(child: Text(value, style: TextStyle(fontSize: 14, height: 1.2, color: Colors.black))))
              ],
            ),
          )
        ],
      ),
    );
  }
}
