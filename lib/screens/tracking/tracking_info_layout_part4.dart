import 'package:autonemogps/config/colors.dart';
import 'package:autonemogps/models/tracking_device_model.dart';
import 'package:flutter/material.dart';

class TrackingPageLayoutPart4 extends StatelessWidget {
  final bool showLoader;

  const TrackingPageLayoutPart4({Key? key, required this.currentTrackingDevice, required this.showLoader}) : super(key: key);

  final TrackingDevice currentTrackingDevice;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(color: Colors.white),
      width: MediaQuery.of(context).size.width,
      padding: EdgeInsets.all(5),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Column(
            children: [
              Text(currentTrackingDevice.serverTime ?? '', style: TextStyle(fontSize: 12)),
              Text('Server Time', style: TextStyle(color: AppColors.sensorActiveColor, fontSize: 12)),
            ],
          ),
          if (!showLoader) Container(margin: EdgeInsets.symmetric(horizontal: 20), width: 1, color: Color(0xffE0E0E0), height: 20),
          if (showLoader)
            Container(
              height: 30,
              width: 30,
              child: CircularProgressIndicator(strokeWidth: 2),
              margin: EdgeInsets.symmetric(horizontal: 10),
              padding: EdgeInsets.all(5),
            ),
          Column(
            children: [
              Text(currentTrackingDevice.deviceTime ?? '', style: TextStyle(fontSize: 12)),
              Text('Device Time', style: TextStyle(color: AppColors.sensorActiveColor, fontSize: 12)),
            ],
          ),
        ],
      ),
    );
  }
}
