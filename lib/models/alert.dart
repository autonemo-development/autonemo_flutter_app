import 'dart:convert';

import 'package:autonemogps/config/alert_types.dart';
import 'package:autonemogps/config/string_constants.dart';
import 'package:autonemogps/storage/repository.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class Alert {
  String alertName, date, vehicleName, iconPath;
  String? id, alertId, alertType;
  Color iconColor;
  Map<String, dynamic>? json;
  static DateFormat _outputDateFormat = DateFormat(STANDARD_DATE_TIME_OUTPUT_PATTERN);
  static DateFormat _inputDateFormat = DateFormat(STANDARD_DATE_TIME_INPUT_PATTERN);

  Alert({required this.vehicleName, required this.date, required this.alertName, this.alertType, required this.iconColor, required this.iconPath, this.json, this.id, this.alertId});

  factory Alert.fromGlideJson(Map<String, dynamic> json) {
    Map<String, dynamic> iconData = getIconData(json['alarm'] as String, alarmValue: json['alarm_value']);
    return Alert(
        date: json['device_time'] as String,
        alertName: json['alert_name'] as String,
        vehicleName: json['vehicleName'] as String,
        alertType: json['alarm'] as String,
        iconPath: iconData['iconPath'],
        iconColor: iconData['iconColor'],
        json: json);
  }

  factory Alert.fromGPSServerJson(String id, List<dynamic> json) {
    String iconPath;
    Color iconColor;
    if ((json[2] as String).contains('Ignition On')) {
      iconPath = 'images/ignition.png';
      iconColor = Color(0xFF0C8E44);
    } else if ((json[2] as String).contains('Ignition Off')) {
      iconPath = 'images/ignition.png';
      iconColor = Colors.red;
    } else if ((json[2] as String).contains('Low Battery')) {
      iconPath = 'images/light.png';
      iconColor = Color(0xFFF26611);
    } else if ((json[2] as String).contains('Parking Alert')) {
      iconPath = 'images/p.png';
      iconColor = Color(0xFF0060A4);
    } else if ((json[2] as String).contains('Speed Alert')) {
      iconPath = 'images/car_speed.png';
      iconColor = Color(0xFFFF4E4E);
    } else if ((json[2] as String).contains('underspeed')) {
      iconPath = 'images/car_speed.png';
      iconColor = Color(0xFF474747);
    } else if ((json[2] as String).contains('Zone In')) {
      iconPath = 'images/z.png';
      iconColor = Color(0xFF0060A4);
    } else if ((json[2] as String).contains('Zone Out')) {
      iconPath = 'images/z.png';
      iconColor = Color(0xFF474747);
    } else if ((json[2] as String).toLowerCase().contains('ac off') || (json[2] as String).toLowerCase().contains('air condition off')) {
      iconPath = 'images/ac.png';
      iconColor = Color(0xFF474747);
    } else if ((json[2] as String).toLowerCase().contains('ac on') || (json[2] as String).toLowerCase().contains('air condition on')) {
      iconPath = 'images/z.png';
      iconColor = Color(0xFF0C8E44);
    } else if ((json[2] as String).toLowerCase().contains('power cut off')) {
      iconPath = 'images/energy.png';
      iconColor = Color(0xFF474747);
    } else if ((json[2] as String).toLowerCase().contains('vibration alert')) {
      iconPath = 'images/vibration.png';
      iconColor = Color(0xFF0060A4);
    } else if ((json[2] as String).toLowerCase().contains('offline alert')) {
      iconPath = 'images/offline.png';
      iconColor = Color(0xFF474747);
    } else if ((json[2] as String).toLowerCase().contains('sos alert')) {
      iconPath = 'images/sos.png';
      iconColor = Color(0xFFF26611);
    } else if ((json[2] as String).toLowerCase().contains('route in')) {
      iconPath = 'images/route_in.png';
      iconColor = Color(0xFF0C8E44);
    } else if ((json[2] as String).toLowerCase().contains('route out')) {
      iconPath = 'images/route_out.png';
      iconColor = Color(0xFF474747);
    } else if ((json[2] as String).toLowerCase().contains('door open')) {
      iconPath = 'images/door_open.png';
      iconColor = Color(0xFF0C8E44);
    } else if ((json[2] as String).toLowerCase().contains('harsh breaking')) {
      iconPath = 'images/harsh_breaking.png';
      iconColor = Color(0xFFF26611);
    } else if ((json[2] as String).toLowerCase().contains('harsh acceleration')) {
      iconPath = 'images/harsh_accleration.png';
      iconColor = Color(0xFFF26611);
    } else if ((json[2] as String).toLowerCase().contains('engine idle')) {
      iconPath = 'images/engne_idle.png';
      iconColor = Color(0xFFF26611);
    } else if ((json[2] as String).toLowerCase().contains('low dc')) {
      iconPath = 'images/low_dc.png';
      iconColor = Color(0xFFF26611);
    } else {
      iconPath = 'images/alert.png';
      iconColor = Colors.grey;
    }
    return Alert(
      id: id,
      date: _outputDateFormat.format(_inputDateFormat.parse(json[0] as String)),
      alertName: json[2] as String,
      vehicleName: json[1] as String,
      iconPath: iconPath,
      iconColor: iconColor,
    );
  }

  factory Alert.fromGPSWoxJson(Map<String, dynamic> json) {
    String iconPath;
    Color iconColor;
    switch (json['type'] as String) {
      case 'overspeed':
        iconPath = 'images/car_speed.png';
        iconColor = Color(0xFFFF4E4E);
        break;
      default:
        iconPath = 'images/alert.png';
        iconColor = Colors.grey;
    }

    return Alert(
        id: (json['id'] as int).toString(),
        date: json['time'] as String,
        alertName: json['message'] as String,
        vehicleName: json['device_name'] as String,
        alertId: (json['alert_id']).toString(),
        iconPath: iconPath,
        iconColor: iconColor,
        json: json);
  }
}

Map<String, dynamic> getIconData(String alarm, {dynamic alarmValue}) {
  String iconPath;
  Color iconColor;
  if (alarm == ACC) {
    iconPath = 'images/ignition.png';
    if (alarmValue != null) {
      if ((alarmValue as String).toLowerCase() == 'true') {
        iconColor = Color(0xFF0C8E44);
      } else {
        iconColor = Colors.red;
      }
    } else
      iconColor = Color(0xFF0C8E44);
  } else if (alarm == LOW_BATTERY) {
    iconPath = 'images/light.png';
    iconColor = Color(0xFFF26611);
  } else if (alarm.contains('Parking Alert')) {
    iconPath = 'images/p.png';
    iconColor = Color(0xFF0060A4);
  } else if (alarm == OVERSPEED) {
    iconPath = 'images/car_speed.png';
    iconColor = Color(0xFFFF4E4E);
  } else if (alarm == ENTER_FENCE) {
    iconPath = 'images/z.png';
    iconColor = Color(0xFF0060A4);
  } else if (alarm == EXIT_FENCE) {
    iconPath = 'images/z.png';
    iconColor = Colors.red;
  } else if (alarm == POWER_CUT || alarm == POWER_OFF) {
    iconPath = 'images/energy.png';
    iconColor = Colors.red;
  } else if (alarm == POWER_ON) {
    iconPath = 'images/energy.png';
    iconColor = Color(0xFF0060A4);
  } else if (alarm == VIBRATION) {
    iconPath = 'images/vibration.png';
    iconColor = Color(0xFF0060A4);
  } else if (alarm.toLowerCase().contains('offline alert')) {
    iconPath = 'images/offline.png';
    iconColor = Colors.red;
  } else if (alarm == SOS) {
    iconPath = 'images/sos.png';
    iconColor = Color(0xFFF26611);
  } else if (alarm == DOOR) {
    iconPath = 'images/door_open.png';
    iconColor = Color(0xFF0C8E44);
  } else if (alarm == HARSH_BRAKING) {
    iconPath = 'images/harsh_breaking.png';
    iconColor = Color(0xFFF26611);
  } else if (alarm == HARSH_ACCL) {
    iconPath = 'images/harsh_accleration.png';
    iconColor = Color(0xFFF26611);
  } else if (alarm == EXTERNAL_LOW_BATTERY) {
    iconPath = 'images/battery.png';
    iconColor = Colors.red;
  } else {
    iconPath = 'images/alert.png';
    iconColor = Colors.grey;
  }

  return {'iconColor': iconColor, 'iconPath': iconPath};
}

List<Alert> parseAlerts(String response, Map<String, String> filter, String deviceId) {
  List<Alert> alertList = List.empty(growable: true);
  if (Repository.getServerType() == GPSWoxConstants.type) {
    List eventData = json.decode(response)['items']['data'] as List;
    if (filter.length > 0) {
      eventData.forEach((event) {
        Alert alert = Alert.fromGPSWoxJson(event);
        if (filter.containsKey(alert.alertId)) {
          alertList.add(alert);
        }
      });
    } else {
      eventData.forEach((event) {
        alertList.add(Alert.fromGPSWoxJson(event));
      });
    }
  } else {
    var events = json.decode(response);
    if (events['rows'] == null) {
      return alertList;
    }
    List<dynamic> responseJson = (events['rows'] as List<dynamic>);
    if (filter.length > 0) {
      responseJson.forEach((value) {
        Alert alert = Alert.fromGPSServerJson(value['id'] as String, value['cell']);
        if (filter.values.contains(alert.alertName)) {
          alertList.add(alert);
        }
      });
    } else {
      responseJson.forEach((value) => alertList.add(Alert.fromGPSServerJson(value['id'] as String, value['cell'])));
    }
  }

  return alertList;
}
