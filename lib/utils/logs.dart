import 'dart:developer';

import 'package:flutter/foundation.dart';
import 'package:autonemogps/config/string_constants.dart';

void writeDebugLogs(Object s) {
  if(kDebugMode){
    print(s.toString());
  }
  log(s.toString(), name: '$APP_NAME Logs');
}

void writeErrorLogs(String s, {Object? e}) {
  log(s, error: e, name: '$APP_NAME Logs');
}
