import 'package:autonemogps/api_calls/gps_server_requests.dart';
import 'package:autonemogps/config/helpers.dart';
import 'package:autonemogps/models/device_model.dart';
import 'package:autonemogps/models/tracking_device_model.dart';
import 'package:autonemogps/models/vehicle_status_mode.dart';
import 'package:autonemogps/screens/tracking/speedometer_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:overlay_support/overlay_support.dart';

class TrackingInfoLayoutPart1 extends StatefulWidget {
  final TrackingDevice currentTrackingDevice;

  final DeviceModel deviceModel;

  TrackingInfoLayoutPart1(this.currentTrackingDevice, this.deviceModel);

  @override
  State<StatefulWidget> createState() {
    return TrackingInfoLayoutPart1State();
  }
}

class TrackingInfoLayoutPart1State extends State<TrackingInfoLayoutPart1> {
  late TrackingDevice currentTrackingDevice;

  String fetchedAddress = '';

  @override
  void initState() {
    currentTrackingDevice = widget.currentTrackingDevice;
    super.initState();
  }

  @override
  void didUpdateWidget(covariant TrackingInfoLayoutPart1 oldWidget) {
    setState(() {
      currentTrackingDevice = widget.currentTrackingDevice;
    });
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    VehicleStatusModel vehicleStatusModel = getVehicleStatus(currentTrackingDevice, widget.deviceModel);

    return Container(
        width: MediaQuery.of(context).size.width,
        child: Column(
          children: [
            Stack(
              children: [
                Positioned(
                  bottom: 0,
                  child: Container(
                    decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.only(topLeft: Radius.circular(20), topRight: Radius.circular(20))),
                    width: MediaQuery.of(context).size.width,
                    height: 50,
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Expanded(
                      child: Container(
                        padding: EdgeInsets.symmetric(horizontal: 10),
                        height: 100,
                        child: Container(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: [
                              InkWell(
                                onTap: () => fetchAndShowAddress(),
                                child: Container(
                                  height: 50,
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Container(child: Image.asset('images/see_address.png'), height: 15, width: 15),
                                      SizedBox(width: 5),
                                      Expanded(child: Text(fetchedAddress == '' ? 'See Address' : 'Hide Address', style: TextStyle(fontSize: 12, fontWeight: FontWeight.w600))),
                                    ],
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    SpeedometerWidget(this.currentTrackingDevice.speed!, height: 100, width: 100),
                    Expanded(
                      child: Container(
                        padding: EdgeInsets.symmetric(horizontal: 10),
                        height: 100,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Container(
                              height: 50,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Container(decoration: BoxDecoration(color: vehicleStatusModel.statusColor, shape: BoxShape.circle), height: 10, width: 10),
                                  SizedBox(width: 5),
                                  Expanded(child: Text(widget.deviceModel.name, style: TextStyle(fontSize: 12, fontWeight: FontWeight.w600), maxLines: 1, overflow: TextOverflow.ellipsis)),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
            if (fetchedAddress != '')
              Container(
                decoration: BoxDecoration(color: Colors.white),
                padding: EdgeInsets.only(right: 10, bottom: 10, left: 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Expanded(child: Text(fetchedAddress, style: TextStyle(fontSize: 12, fontFamily: 'Roboto'))),
                    InkWell(
                      onTap: () async {
                        Clipboard.setData(ClipboardData(text: fetchedAddress)).then((value) {
                          showSimpleNotification(Text('Address copied to clipboard', textAlign: TextAlign.center), position: NotificationPosition.bottom, background: Colors.red);
                        });
                      },
                      child: Container(height: 25, width: 25, child: Image.asset('images/copy_address_icon.png')),
                    ),
                  ],
                ),
              )
          ],
        ));
  }

  void fetchAndShowAddress() {
    if (fetchedAddress == '') {
      GPSServerRequests.fetchAddress(this.currentTrackingDevice.lat.toString(), this.currentTrackingDevice.lng.toString()).then((value) {
        setState(() {
          fetchedAddress = value;
        });
      });
    } else {
      setState(() {
        fetchedAddress = '';
      });
    }
  }
}
