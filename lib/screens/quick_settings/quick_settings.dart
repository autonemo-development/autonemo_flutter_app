import 'package:animations/animations.dart';
import 'package:autonemogps/config/colors.dart';
import 'package:autonemogps/config/helpers.dart';
import 'package:autonemogps/config/string_constants.dart';
import 'package:autonemogps/models/device_model.dart';
import 'package:autonemogps/models/tracking_device_model.dart';
import 'package:autonemogps/screens/choose_vehicle_icon_page.dart';
import 'package:autonemogps/screens/quick_settings/update_item_dialog.dart';
import 'package:autonemogps/storage/repository.dart';
import 'package:autonemogps/utils/enums.dart';
import 'package:flutter/material.dart';

class QuickSettings extends StatelessWidget {
  final TrackingDevice trackingDevice;
  final DeviceModel deviceModel;

  const QuickSettings(this.deviceModel, this.trackingDevice);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Quick Settings'), backgroundColor: Colors.white, foregroundColor: Colors.black),
      body: Container(
        padding: EdgeInsets.all(20),
        decoration: BoxDecoration(color: Colors.white),
        child: GridView(
          shrinkWrap: true,
          gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 3),
          children: [
            ActionItem(
              imagePath: 'images/update-vehicle_icon.png',
              title: 'Update Icon',
              onTap: () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => ChooseVehicleIconPage(device: deviceModel))),
            ),
            if (Repository.getServerType() == GPSServerConstants.type)
              ActionItem(
                imagePath: 'images/update_odometer.png',
                title: 'Update Odometer',
                onTap: () {
                  showModal(
                    context: context,
                    builder: (context) => Dialog(
                        child: StatefulBuilder(
                      builder: (BuildContext context, StateSetter setVehicleUpdatingState) => UpdateItemDialog(
                        'images/update_odometer.png',
                        trackingDevice.odometer.toString(),
                        SettingUpdateType.ODOMETER,
                        deviceModel.imei,
                        TextInputType.number,
                        (String newOdoValue) => trackingDevice.odometer = double.parse(newOdoValue),
                        deviceModel: deviceModel,
                      ),
                    )),
                  );
                },
              ),
            ActionItem(
              imagePath: 'images/call_and_listen.png',
              title: 'Listen And Call',
              onTap: () {
                if (deviceModel.simNumber.isNotEmpty) {
                  launchUrl('tel://${deviceModel.simNumber}');
                } else {
                  showSnackBar(context, 'Sim number not set', AppColors.kOrangeColor);
                }
              },
            ),
            if (Repository.getServerType() == GPSServerConstants.type)
              ActionItem(
                imagePath: 'images/update_engine_hours.png',
                title: 'Update Engine Hours',
                onTap: () {
                  int engineHrs = int.parse(trackingDevice.engineHrs!);
                  int value = engineHrs ~/ 3600;
                  showModal(
                    context: context,
                    builder: (context) => Dialog(
                      child: StatefulBuilder(
                        builder: (BuildContext context, StateSetter setVehicleUpdatingState) => UpdateItemDialog(
                          'images/update_engine_hours.png',
                          value.toString(),
                          SettingUpdateType.ENGINE_HOURS,
                          deviceModel.imei,
                          TextInputType.number,
                          (newOdoValue) => trackingDevice.engineHrs = (int.parse(newOdoValue) * 3600).toString(),
                          deviceModel: deviceModel,
                        ),
                      ),
                    ),
                  );
                },
              ),
            ActionItem(
              imagePath: 'images/update_fuel_cost.png',
              title: 'Cost Per Liter',
              onTap: () => showModal(
                context: context,
                builder: (context) => Dialog(
                  child: StatefulBuilder(
                    builder: (BuildContext context, StateSetter setVehicleUpdatingState) => UpdateItemDialog(
                      'images/update_fuel_cost.png',
                      deviceModel.fuelPrice,
                      SettingUpdateType.FUEL_COST,
                      deviceModel.imei,
                      TextInputType.number,
                      (newOdoValue) => deviceModel.fuelPrice = newOdoValue,
                      deviceModel: deviceModel,
                    ),
                  ),
                ),
              ),
            ),
            ActionItem(
              imagePath: 'images/update_mileage.png',
              title: 'Mileage Per Liter',
              onTap: () => showModal(
                context: context,
                builder: (context) => Dialog(
                  child: StatefulBuilder(
                    builder: (BuildContext context, StateSetter setVehicleUpdatingState) => UpdateItemDialog(
                      'images/update_mileage.png',
                      deviceModel.fuelQuantity,
                      SettingUpdateType.MILEAGE,
                      deviceModel.imei,
                      TextInputType.number,
                      (newOdoValue) => deviceModel.fuelQuantity = newOdoValue,
                      deviceModel: deviceModel,
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class ActionItem extends StatelessWidget {
  final String title;
  final void Function() onTap;
  final String imagePath;

  const ActionItem({required this.title, required this.imagePath, required this.onTap});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(5),
      decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(10), boxShadow: [BoxShadow(color: Colors.grey.withOpacity(.3), blurRadius: 10.0, offset: Offset(1.0, 1.0))]),
      child: Container(
        padding: EdgeInsets.all(5),
        child: InkWell(
          onTap: onTap,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(margin: EdgeInsets.only(top: 10), child: Image.asset(imagePath, height: 40)),
              Expanded(
                child: Container(
                  margin: EdgeInsets.only(top: 10),
                  child: Text(title, textAlign: TextAlign.center, style: TextStyle(color: AppColors.sensorInActiveColor, fontSize: 13, fontWeight: FontWeight.w500)),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
