import 'package:autonemogps/api_calls/gps_server_requests.dart';
import 'package:autonemogps/config/colors.dart';
import 'package:autonemogps/config/helpers.dart';
import 'package:autonemogps/config/string_constants.dart';
import 'package:autonemogps/models/maintenance.dart';
import 'package:autonemogps/screens/maintenance_page/add_maintenance_page.dart';
import 'package:autonemogps/screens/maintenance_page/add_maintence_gpswox_page.dart';
import 'package:autonemogps/storage/repository.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class MaintenancePage extends StatefulWidget {
  static const String page = 'maintenance_page';

  @override
  _MaintenancePageState createState() => _MaintenancePageState();
}

class _MaintenancePageState extends State<MaintenancePage> {
  bool _isDataLoading = true;
  List<Maintenance> maintenanceList = [];
  String error = '';

  @override
  void initState() {
    super.initState();
    fetchData();
  }

  fetchData() {
    if (mounted) {
      setState(() {
        _isDataLoading = true;
        error = '';
      });
    }
    GPSServerRequests.fetchMaintenances().then((value) {
      if (mounted)
        setState(() {
          _isDataLoading = false;
          if (value.length > 1) {
            maintenanceList = value;
          } else {
            error = 'No Maintenance Done';
          }
        });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Maintenance'),
        backgroundColor: Colors.white,
        foregroundColor: Colors.black,
        actions: [
          IconButton(
              onPressed: () async {
                bool isMaintenanceAdded =
                    await Navigator.push(context, MaterialPageRoute(builder: (context) => Repository.getServerType() == GPSWoxConstants.type ? AddMaintenanceGPSWoxPage() : AddMaintenancePage()));
                if (isMaintenanceAdded) {
                  fetchData();
                  GPSServerRequests.fetchFnSettings().then((isFnSettingFetched) {});
                }
              },
              icon: Icon(Icons.add)),
        ],
      ),
      body: SafeArea(
        child: Container(
          height: MediaQuery.of(context).size.height,
          decoration: BoxDecoration(gradient: LinearGradient(begin: Alignment.topCenter, end: Alignment.bottomCenter, colors: [Color(0x90DC0E18), Color(0x10DC0E18)])),
          child: Column(
            children: [
              SizedBox(height: 10),
              Expanded(
                child: _isDataLoading
                    ? SpinKitRing(lineWidth: 3.0, color: AppColors.primaryColor, size: 35.0)
                    : error.isEmpty
                        ? ListView.builder(
                            itemCount: maintenanceList.length + 1,
                            //separatorBuilder: (context,index)=>Divider(),
                            itemBuilder: (context, index) {
                              if (index == 0) {
                                return SizedBox(height: 10.0);
                              }
                              return MaintenanceItem(maintenanceList.elementAt(index - 1), () {
                                _asyncConfirmDialog(context).then((value) {
                                  if (value == null) {
                                    return;
                                  }
                                  final ConfirmAction action = value;
                                  if (action == ConfirmAction.Accept) {
                                    showLoaderDialog(context);
                                    GPSServerRequests.deleteMaintenanceId(maintenanceList.elementAt(index - 1).id).then((list) {
                                      Navigator.pop(context);
                                      if (list[0] == 0) {
                                        showSnackBar(context, 'Something went wrong!', Colors.red);
                                      } else {
                                        maintenanceList.removeAt(index - 1);
                                        setState(() {});
                                      }
                                    });
                                  }
                                });
                              }, () async {
                                bool? isMaintenanceEdited = await Navigator.push(context, MaterialPageRoute(builder: (context) => AddMaintenancePage(maintenance: maintenanceList.elementAt(index - 1))));
                                if (isMaintenanceEdited != null && isMaintenanceEdited) {
                                  fetchData();
                                  GPSServerRequests.fetchFnSettings().then((isFnSettingFetched) {});
                                }
                              });
                            },
                          )
                        : Center(child: Text(error)),
              ),
            ],
          ),
        ),
      ),
    );
  }

  showLoaderDialog(BuildContext context) {
    showDialog(barrierDismissible: false, context: context, builder: (context) => Center(child: SpinKitRing(lineWidth: 3.0, color: AppColors.primaryColor, size: 35.0)));
  }
}

class MaintenanceItem extends StatelessWidget {
  final Maintenance maintenance;

  MaintenanceItem(this.maintenance, this.onCancelTapped, this.onEditTapped, {Key? key}) : super(key: key);

  final void Function() onCancelTapped, onEditTapped;

  @override
  Widget build(BuildContext context) {
    String daysLeft = getHtmlText(maintenance.daysLeft);
    String engineHoursLeft = getHtmlText(maintenance.enginHoursLeft);
    String odometerLeft = getHtmlText(maintenance.odometerLeft);

    return Container(
      padding: EdgeInsets.all(15.0),
      margin: EdgeInsets.symmetric(horizontal: 16, vertical: 4.0),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(16.0),
        boxShadow: [BoxShadow(color: AppColors.primaryMaterialColor[100]!, blurRadius: 5.0, spreadRadius: 1.0, offset: Offset(1.0, 2.0))],
      ),
      child: Row(
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                if (maintenance.isExpired)
                  Column(
                    children: <Widget>[
                      Row(
                        children: [
                          Container(
                              padding: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                              decoration: BoxDecoration(color: Color(0xFFFFF4EE), borderRadius: BorderRadius.circular(5.0)),
                              child: Text('Expired', style: TextStyle(color: Color(0xFFF35656), fontSize: 11, fontWeight: FontWeight.w500))),
                        ],
                      ),
                      SizedBox(height: 5)
                    ],
                  ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(maintenance.name, overflow: TextOverflow.ellipsis, style: TextStyle(color: AppColors.headerTextColor, fontSize: 16, fontWeight: FontWeight.w700)),
                          Text(maintenance.objectName, style: TextStyle(color: AppColors.headerTextColor, fontSize: 12, fontWeight: FontWeight.w400)),
                        ],
                      ),
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Text(maintenance.odometer.replaceAll('km', 'KM'), style: TextStyle(color: Color(0xFF0060A4), fontSize: 14, fontWeight: FontWeight.w500)),
                        if (odometerLeft != '-') Text(odometerLeft.replaceAll('km', 'KM Left'), style: TextStyle(color: Color(0xFFFF6B6B), fontSize: 11, fontWeight: FontWeight.w500))
                      ],
                    )
                  ],
                ),
                SizedBox(
                  height: 7,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(maintenance.engineHours.replaceAll('h', 'Hours'), style: TextStyle(color: Color(0xFF0060A4), fontSize: 14, fontWeight: FontWeight.w500)),
                        if (engineHoursLeft != '-') Text(engineHoursLeft.replaceAll('h', 'Hours Left'), style: TextStyle(color: Color(0xFF0060A4), fontSize: 12, fontWeight: FontWeight.w500)),
                      ],
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        if (maintenance.days != '-')
                          Text(
                            '${maintenance.days}${maintenance.days == '-' ? '' : ' Days'}',
                            style: TextStyle(color: Color(0xFF0060A4), fontSize: 14, fontWeight: FontWeight.w500),
                          ),
                        if (daysLeft != '-')
                          Text(
                            daysLeft.contains('d') ? daysLeft.replaceAll('d', 'Days Left') : '${daysLeft} ${maintenance.days == '-' ? '' : ' Days'}',
                            style: TextStyle(color: Color(0xFFF35656), fontSize: 12, fontWeight: FontWeight.w500),
                          ),
                      ],
                    ),
                  ],
                )
              ],
            ),
          ),
          SizedBox(width: 16),
          Column(
            children: [
              InkWell(
                onTap: onEditTapped,
                child: Container(
                  height: 30,
                  width: 30,
                  padding: EdgeInsets.all(7.0),
                  decoration: BoxDecoration(borderRadius: BorderRadius.circular(3.0), color: AppColors.sensorLightActiveColor),
                  child: Image.asset('images/edit.png', color: AppColors.sensorActiveColor),
                ),
              ),
              SizedBox(height: 6.0),
              InkWell(
                onTap: onCancelTapped,
                child: Container(
                  height: 30,
                  width: 30,
                  padding: EdgeInsets.all(7.0),
                  decoration: BoxDecoration(borderRadius: BorderRadius.circular(3.0), color: Color(0xFFFFEEE4)),
                  child: Image.asset('images/cancel.png', color: Color(0xFFD43333)),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}

enum ConfirmAction { Cancel, Accept }

Future<ConfirmAction?> _asyncConfirmDialog(BuildContext context) async {
  return showDialog<ConfirmAction>(
    context: context,
    barrierDismissible: false, // user must tap button for close dialog!
    builder: (BuildContext context) {
      return AlertDialog(
        content: const Text('Are you sure you want to delete?'),
        actions: <Widget>[
          TextButton(child: const Text('Yes'), onPressed: () => Navigator.of(context).pop(ConfirmAction.Accept)),
          TextButton(child: const Text('No'), onPressed: () => Navigator.of(context).pop(ConfirmAction.Cancel))
        ],
      );
    },
  );
}
