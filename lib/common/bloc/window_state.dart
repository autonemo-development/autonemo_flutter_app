
import 'package:equatable/equatable.dart';

abstract class WindowState extends Equatable {
  @override
  List<Object> get props => [];
}

class PositionChangedState extends WindowState{
  final double offsetY, offsetX;
  PositionChangedState({required this.offsetX, required this.offsetY});
}

class NoEventState extends WindowState{}