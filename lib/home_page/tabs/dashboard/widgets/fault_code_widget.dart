import 'package:autonemogps/api_calls/gps_server_requests.dart';
import 'package:autonemogps/config/colors.dart';
import 'package:autonemogps/config/helpers.dart';
import 'package:autonemogps/models/dtc_model.dart';
import 'package:autonemogps/models/expense.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class FaultCodeDashboard extends StatefulWidget {
  FaultCodeDashboard({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return FaultCodeDashboardState();
  }
}

class FaultCodeDashboardState extends State {
  bool isReportFetched = true;
  List<DTCModel> dtcList = [];

  List<dynamic> faultCodeJson = [];

  bool loading = false;
  bool hasNoData = false;

  @override
  void initState() {
    fetchFaultCodeData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (loading) {
      return SpinKitCircle(color: AppColors.primaryColor);
    }

    if (hasNoData) {
      return Container();
    }

    return Container(
      padding: EdgeInsets.all(10),
      margin: EdgeInsets.only(bottom: 16),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(16),
        boxShadow: [BoxShadow(color: AppColors.shadowMaterialColor[100]!, blurRadius: 5.0, spreadRadius: 1.0, offset: Offset(1.0, 2.0))],
      ),
      child: Column(mainAxisSize: MainAxisSize.min, crossAxisAlignment: CrossAxisAlignment.start, children: [
        Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [Text('Fault Code (DTC)', style: TextStyle(color: AppColors.headerTextColor))]),
        SizedBox(width: 20),
        Divider(),
        SizedBox(height: 5),
        Container(
          height: dtcList.length * 55,
          constraints: BoxConstraints(maxHeight: 250),
          child: ListView.builder(
            itemCount: dtcList.length,
            itemBuilder: (context, index) {
              DTCModel dtc = dtcList.elementAt(index);
              return Container(
                decoration: BoxDecoration(color: Color(0xFFF8F8F8), borderRadius: BorderRadius.circular(10)),
                margin: EdgeInsets.only(top: 5),
                padding: EdgeInsets.all(10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(dtc.objectName, style: TextStyle(fontSize: 12, fontWeight: FontWeight.w600)),
                          SizedBox(height: 2),
                          Text(getFaultCodeDetail(dtc.code), style: TextStyle(fontSize: 10)),
                        ],
                      ),
                    ),
                    SizedBox(width: 10),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        Text(dtc.code, style: TextStyle(fontSize: 12, fontWeight: FontWeight.w600, color: Colors.red)),
                        Text(dtc.date, style: TextStyle(fontSize: 10)),
                      ],
                    ),
                  ],
                ),
              );
            },
          ),
        )
      ]),
    );
  }

  void fetchFaultCodeData() {
    setState(() => loading = true);
    GPSServerRequests.fetchDTC().then((value) {
      setState(() => this.hasNoData = value.length == 0);
      setState(() => dtcList = value);
      setState(() => loading = false);
    });

    getFaultCodeJson().then((value) {
      setState(() => faultCodeJson = value);
    });
  }

  String getTotalExpense(List<Expense> expenseList) {
    double totalExpense = 0;

    expenseList.forEach((element) {
      double cost = double.parse(element.cost.replaceAll(' BDT', ''));
      totalExpense = totalExpense + cost;
    });

    return totalExpense.toString();
  }

  String getFaultCodeDetail(String code) {
    Map<String, dynamic> selectedFaultCodeDetail = faultCodeJson.firstWhere((element) => element['Code'] == code);
    return selectedFaultCodeDetail['Text'] ?? '';
  }
}
