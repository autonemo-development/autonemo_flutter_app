import 'dart:async';

import 'package:autonemogps/config/string_constants.dart';
import 'package:autonemogps/home_page/home_page.dart';
import 'package:autonemogps/screens/login_page/login_page.dart';
import 'package:autonemogps/storage/repository.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class SplashPage extends StatefulWidget {
  static const String page = 'splash_page';

  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  MethodChannel nativeMethodsChannel = MethodChannel(CHANNEL_ID);

  @override
  void initState() {
    super.initState();
    Timer(Duration(seconds: 2), () async {
      bool isLoggedIn = Repository.isLoggedIn();
      Widget page;
      if (isLoggedIn) {
        page = await buildHomePageAsync();
      } else {
        page = await buildLoginPageAsync();
      }
      Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => page));
    });
  }

  Future<Widget> buildLoginPageAsync() async {
    return Future.microtask(() {
      return LoginPage();
    });
  }

  Future<Widget> buildHomePageAsync() async {
    return Future.microtask(() {
      return HomePage();
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        color: Colors.white,
        child: Stack(
          children: <Widget>[
            Container(
              child: Center(
                child: Image.asset('images/logo.png', width: MediaQuery.of(context).size.width / 2),
              ),
            ),
            Positioned(
              bottom: 0,
              left: 0,
              right: 0,
              child: Image.asset('images/login_illustration.png', alignment: Alignment.center, fit: BoxFit.cover, width: MediaQuery.of(context).size.width),
            ),
          ],
        ),
      ),
    );
  }
}
