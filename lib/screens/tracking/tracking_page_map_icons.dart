import 'package:autonemogps/common/near_by_dialog.dart';
import 'package:autonemogps/common/share_location_dialog.dart';
import 'package:autonemogps/config/colors.dart';
import 'package:autonemogps/config/string_constants.dart';
import 'package:autonemogps/models/device_model.dart';
import 'package:autonemogps/models/tracking_device_model.dart';
import 'package:autonemogps/screens/tracking/tracking_page_map.dart';
import 'package:autonemogps/storage/repository.dart';
import 'package:autonemogps/utils/dateUtilities.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:intl/intl.dart';
import 'package:location/location.dart';
import 'package:share/share.dart';
import 'package:url_launcher/url_launcher_string.dart';

class TrackingPageMapIcons extends StatefulWidget {
  final GlobalKey<TrackingPageMapState> trackingPageMapState;

  final TrackingDevice currentTrackingDevice;

  final DeviceModel deviceModel;

  TrackingPageMapIcons(this.currentTrackingDevice, this.deviceModel, this.trackingPageMapState);

  @override
  State<StatefulWidget> createState() {
    return TrackingPageMapIconsState();
  }
}

class TrackingPageMapIconsState extends State<TrackingPageMapIcons> {
  MapType _mapType = MapType.normal;

  bool isTrafficClicked = false;

  bool focusOnVehicle = false;

  @override
  Widget build(BuildContext context) {
    return PopupMenuButton(
      child: Container(
        height: 40,
        width: 40,
        decoration:
            BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(50), boxShadow: [BoxShadow(color: Colors.grey, offset: Offset(0.0, 1.0), blurRadius: 6.0)]),
        child: Icon(Icons.keyboard_arrow_down_outlined),
      ),
      itemBuilder: (context) => <PopupMenuItem<String>>[
        buildMyLocationPopupMenuItem(),
        buildMapViewPopupMenuItem(),
        buildTrafficEnabledPopupMenuItem(),
        buildDirectionsPopupMenuItem(),
        buildNearbyPopupMenuItem(),
        buildShareLocationPopupMenuItem(),
        buildPointVehiclePopupMenuItem()
      ],
    );
  }

  PopupMenuItem<String> buildPointVehiclePopupMenuItem() {
    return PopupMenuItem<String>(
      onTap: () => widget.trackingPageMapState.currentState!.focusOnVehicle(),
      child: Container(child: Center(child: Image.asset('images/point-to-vehicle.png', height: 30, width: 30))),
      value: 'point_vehicle',
    );
  }

  PopupMenuItem<String> buildShareLocationPopupMenuItem() {
    return PopupMenuItem<String>(
      onTap: () {
        WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
          if (Repository.getServerType() == GPSServerConstants.type) {
            showDialog(context: context, builder: (context) => ShareLocationDialog());
          } else if (Repository.getServerType() == GPSWoxConstants.type) {
            String geoUri = 'http://maps.google.com/maps?q=loc:' + widget.currentTrackingDevice.lat.toString() + ',' + widget.currentTrackingDevice.lng.toString();
            Share.share('Vehicle Name - ' + (widget.deviceModel.name) + '\n' + 'Date - ' + (dateTimeOutput.format(DateTime.now())) + '\n' + 'Map - ' + geoUri);
          }
        });
      },
      child: Container(child: Center(child: Image.asset('images/share-location.png', height: 30, width: 30))),
      value: 'share_location',
    );
  }

  PopupMenuItem<String> buildNearbyPopupMenuItem() {
    return PopupMenuItem<String>(
      onTap: () {
        WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
          showNearbyLocationDialog(context, widget.currentTrackingDevice.lat.toString(), widget.currentTrackingDevice.lng.toString());
        });
      },
      child: Container(child: Center(child: Image.asset('images/nearby.png', height: 30, width: 30))),
      value: 'nearby',
    );
  }

  PopupMenuItem<String> buildDirectionsPopupMenuItem() {
    return PopupMenuItem<String>(
      onTap: () {
        String uri = 'https://maps.google.com/maps?saddr=My+Location&daddr=' + widget.currentTrackingDevice.lat.toString() + ',' + widget.currentTrackingDevice.lng.toString();
        launchUrlString(uri, mode: LaunchMode.externalApplication);
      },
      child: Container(child: Center(child: Image.asset('images/traffic-right-turn.png', height: 30, width: 30))),
      value: 'directions',
    );
  }

  PopupMenuItem<String> buildTrafficEnabledPopupMenuItem() {
    return PopupMenuItem<String>(
        onTap: () {
          setState(() {
            isTrafficClicked = !isTrafficClicked;
            widget.trackingPageMapState.currentState!.changeMapTraffic(isTrafficClicked);
          });
        },
        child: Container(
          child: Center(
            child: Image.asset(
              isTrafficClicked ? 'images/traffic-lights-active.png' : 'images/traffic-lights.png',
              height: 30,
              width: 30,
              color: isTrafficClicked ? null : AppColors.primaryColor2,
            ),
          ),
        ),
        value: 'traffic');
  }

  PopupMenuItem<String> buildMapViewPopupMenuItem() {
    return PopupMenuItem<String>(
        onTap: () {
          setState(() {
            if (_mapType == MapType.normal) {
              _mapType = MapType.hybrid;
            } else {
              _mapType = MapType.normal;
            }
            widget.trackingPageMapState.currentState!.changeMapType(_mapType);
          });
        },
        child: Container(child: Center(child: Image.asset(_mapType == MapType.normal ? 'images/roadmap.png' : 'images/satellite-map.png', height: 30, width: 30))),
        value: 'map_view');
  }

  PopupMenuItem<String> buildMyLocationPopupMenuItem() {
    return PopupMenuItem<String>(
      onTap: () async {
        Location location = new Location();
        try {
          LocationData currentLocation = await location.getLocation();
          widget.trackingPageMapState.currentState!.showCurrentLocation(currentLocation);
        } on Exception {}
      },
      child: Container(child: Center(child: Image.asset('images/my_location.png', height: 30, width: 30))),
      value: 'my_location',
    );
  }

  Widget ShareLocationDialog() {
    return Dialog(
      child: Container(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            InkWell(
              child: Padding(child: Text('Share Current Location'), padding: EdgeInsets.all(10)),
              onTap: () {
                Navigator.pop(context);
                String geoUri = 'http://maps.google.com/maps?q=loc:' + widget.currentTrackingDevice.lat.toString() + ',' + widget.currentTrackingDevice.lng.toString();
                Share.share(
                    'Vehicle Name - ' + (widget.deviceModel.name) + '\n' + 'Date - ' + (new DateFormat('yyyy-MM-dd HH:mm:ss').format(DateTime.now())) + '\n' + 'Map - ' + geoUri);
              },
            ),
            if (Repository.getServerType() == GPSServerConstants.type)
              InkWell(
                child: Padding(child: Text('Share Live Location'), padding: EdgeInsets.all(10)),
                onTap: () {
                  String todayDate = DateFormat('yyyy-MM-dd').format(DateTime.now().add(Duration(days: 1)));
                  showShareLocationDialog(context, todayDate, () => Navigator.of(context, rootNavigator: true).pop(context), widget.deviceModel.imei);
                },
              ),
          ],
        ),
      ),
    );
  }
}
