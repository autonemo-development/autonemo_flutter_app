import 'package:flutter/material.dart';

// ignore: must_be_immutable
class DropDownFromFieldForFilter extends StatelessWidget {

  DropDownFromFieldForFilter({required this.list,required this.selectedValue,required this.onChanged});

  var list = [''];
  Function(void)? onChanged;
  String selectedValue;

  @override
  Widget build(BuildContext context) {
    return DropdownButtonFormField(
        validator: (value){
          if(value == null){
            return "Field can't be empty";
          }
          return null;
        },
        iconSize: 0.0,
        style: TextStyle(fontWeight: FontWeight.w600,color: Colors.black),
        items: list.map((String category) {
          return new DropdownMenuItem(
              value: category,
              child: Text(category, style: TextStyle(fontSize: 15.0),),
          );
        }).toList(),
        onChanged: onChanged,
        value: selectedValue,
        decoration: InputDecoration(
          suffixIcon: Padding(
            padding: const EdgeInsets.only(top:17.0,bottom: 17.0),
            child: Image.asset('images/drop-down-arrow.png',width: 5,height: 5,),
          ),
          contentPadding: EdgeInsets.only(left: 12,right: 12),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(3.0),
            borderSide: BorderSide.none,
          ),
          filled: true,
          fillColor: Color(0xFFF5F9FF),
        )
    );
  }
}