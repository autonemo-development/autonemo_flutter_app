import 'package:autonemogps/common/loading.dart';
import 'package:autonemogps/api_calls/gps_server_requests.dart';
import 'package:autonemogps/api_calls/gps_wox_requests.dart';
import 'package:autonemogps/config/colors.dart';
import 'package:autonemogps/config/string_constants.dart';
import 'package:autonemogps/models/geofence_model.dart';
import 'package:autonemogps/screens/geofence_map_page.dart';
import 'package:autonemogps/storage/repository.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class GeofenceSettingsPage extends StatefulWidget {
  @override
  _GeofenceSettingsPageState createState() => _GeofenceSettingsPageState();
}

class _GeofenceSettingsPageState extends State<GeofenceSettingsPage> {
  bool _isLoading = true;
  String _error = '';
  List<Geofence> placesList = List.empty(growable: true);

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      refreshPlaces();
    });
  }

  refreshPlaces() {
    placesList.clear();
    if (Repository.getServerType() == GPSWoxConstants.type) {
      GPSWoxRequests.fetchGeoPlaces().then((returnedList) {
        if (returnedList.elementAt(0) as bool) {
          setState(() {
            _isLoading = false;
            (returnedList.elementAt(1) as List<dynamic>).forEach(
              (value) {
                placesList.add(Geofence.fromGPSWoxJson(
                  value,
                ));
              },
            );
          });
        } else {
          setState(() {
            _isLoading = false;
            _error = returnedList.elementAt(1);
          });
        }
      });
    } else {
      GPSServerRequests.fetchGeoPlaces().then((returnedList) {
        if (returnedList.elementAt(0) as bool) {
          setState(() {
            _isLoading = false;
            (returnedList.elementAt(1) as Map<String, dynamic>).forEach(
              (key, value) {
                placesList.add(Geofence.fromGPSServerJson(value, key));
              },
            );
          });
        } else {
          setState(() {
            _isLoading = false;
            _error = returnedList.elementAt(1);
          });
        }
      });
    }
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Geofence'),
        backgroundColor: Colors.white,
        foregroundColor: Colors.black,
        systemOverlayStyle: SystemUiOverlayStyle.light,
        actions: [
          IconButton(
              onPressed: () {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => GeofenceMap(onZoneAdded: () {
                          refreshPlaces();
                          setState(() {
                            _isLoading = true;
                          });
                        })));
              },
              icon: Icon(Icons.add))
        ],
      ),
      backgroundColor: AppColors.defaultBackground,
      body: _isLoading
          ? LoadingDialog()
          : _error.isNotEmpty
              ? Center(child: Text(_error))
              : Container(
                  child: ListView.builder(
                      itemCount: placesList.length,
                      itemBuilder: (context, index) {
                        return GeofenceItem(placesList.elementAt(index), () {
                          refreshPlaces();
                          setState(() {
                            _isLoading = true;
                          });
                        });
                      }),
                ),
    );
  }
}

class GeofenceItem extends StatefulWidget {
  final Geofence geofence;
  final void Function() onZoneAdded;

  GeofenceItem(this.geofence, this.onZoneAdded);

  @override
  _GeofenceItemState createState() => _GeofenceItemState();
}

class _GeofenceItemState extends State<GeofenceItem> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.of(context).push(MaterialPageRoute(
            builder: (context) => GeofenceMap(
                  geofence: widget.geofence,
                  onZoneAdded: widget.onZoneAdded,
                )));
      },
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 6, horizontal: 12),
        padding: EdgeInsets.all(10),
        decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.only(topRight: Radius.circular(5), topLeft: Radius.circular(5))),
        child: Text(
          widget.geofence.name!,
          style: TextStyle(
            color: AppColors.headerTextColor,
            fontSize: 18,
          ),
        ),
      ),
    );
  }
}
