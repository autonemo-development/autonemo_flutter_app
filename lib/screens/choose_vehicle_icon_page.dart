import 'package:autonemogps/api_calls/gps_server_requests.dart';
import 'package:autonemogps/config/colors.dart';
import 'package:autonemogps/config/helpers.dart';
import 'package:autonemogps/config/string_constants.dart';
import 'package:autonemogps/models/device_model.dart';
import 'package:autonemogps/storage/repository.dart';
import 'package:autonemogps/utils/enums.dart';
import 'package:autonemogps/utils/icon_helper.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class ChooseVehicleIconPage extends StatefulWidget {
  static const String page = 'choose_vehicle_icon_page';
  final DeviceModel device;

  ChooseVehicleIconPage({required this.device});

  @override
  _ChooseVehicleIconPageState createState() => _ChooseVehicleIconPageState();
}

class _ChooseVehicleIconPageState extends State<ChooseVehicleIconPage> {
  String? _error;
  String selectedIcon = '';
  bool _isSubmitting = false;

  List<String> iconNames = [];

  @override
  void initState() {
    super.initState();
    selectedIcon = widget.device.icon;
    iconNames.addAll(IconHelper.icons.keys);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Choose Icon',
          style: TextStyle(color: Color(0xFF474747), fontWeight: FontWeight.w500, fontSize: 16),
        ),
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(
          color: Color(0xFF7E7E7E), //change your color here
        ),
        actions: [
          _isSubmitting
              ? SpinKitRing(
                  lineWidth: 3.0,
                  color: AppColors.primaryColor,
                  size: 30.0,
                )
              : IconButton(onPressed: onChangeIconPressed, icon: Icon(Icons.done))
        ],
        systemOverlayStyle: SystemUiOverlayStyle.dark,
      ),
      body: Container(
        padding: EdgeInsets.all(15),
        child: _error == null
            ? GridView.builder(
                itemCount: IconHelper.icons.length,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisSpacing: 10, mainAxisSpacing: 10, crossAxisCount: 3, childAspectRatio: 1),
                itemBuilder: (context, index) {
                  return VehicleIcon(
                    '${IconHelper.icons[iconNames.elementAt(index)]!}_green.png',
                    selectedIcon == iconNames.elementAt(index),
                    () {
                      onIconTapped(iconNames.elementAt(index));
                    },
                  );
                })
            : Center(
                child: Text(_error!),
              ),
      ),
    );
  }

  void onChangeIconPressed() {
    if (Repository.getServerType() == GPSServerConstants.type) {
      setState(() => _isSubmitting = true);
      String iconName = 'img/markers/objects/${IconHelper.iconsToServerImage[selectedIcon] ?? 'automobile.png'}';

      GPSServerRequests.updateValues(widget.device.imei, iconName, SettingUpdateType.ICON).then((value) {
        if (value == 'OK') {
          widget.device.icon = selectedIcon;
          Navigator.pop(context);
        } else {
          setState(() => _isSubmitting = false);
          showSnackBar(context, value, Colors.red);
        }
      });
    } else {
      Repository.setVehicleIcon(widget.device.imei, selectedIcon);
      widget.device.icon = selectedIcon;
      Navigator.pop(context);
    }
  }

  onIconTapped(String icon) {
    setState(() {
      selectedIcon = icon;
    });
  }
}

class VehicleIcon extends StatelessWidget {
  final String imagePath;
  final bool isActive;
  final void Function() onTap;

  VehicleIcon(this.imagePath, this.isActive, this.onTap);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        margin: EdgeInsets.all(2),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(3),
          border: Border.all(color: isActive ? Color(0xFF39A98B) : Colors.transparent, width: 1.5),
          boxShadow: [BoxShadow(color: AppColors.shadowMaterialColor, blurRadius: 1)],
        ),
        child: Container(
          margin: EdgeInsets.all(15),
          decoration: BoxDecoration(
            color: Color(0xFFF5F9FF),
            shape: BoxShape.circle,
          ),
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Image.asset(imagePath),
          ),
        ),
      ),
    );
  }
}
