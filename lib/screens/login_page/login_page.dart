import 'package:autonemogps/api_calls/gps_server_requests.dart';
import 'package:autonemogps/api_calls/gps_wox_requests.dart';
import 'package:autonemogps/config/colors.dart';
import 'package:autonemogps/config/helpers.dart';
import 'package:autonemogps/config/string_constants.dart';
import 'package:autonemogps/home_page/home_page.dart';
import 'package:autonemogps/screens/contact_us.dart';
import 'package:autonemogps/screens/login_page/forgot_password_dialog.dart';
import 'package:autonemogps/screens/login_page/switch_server_dialog.dart';
import 'package:autonemogps/screens/privacy_policy_page.dart';
import 'package:autonemogps/screens/terms_of_use_page.dart';
import 'package:autonemogps/storage/repository.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:package_info_plus/package_info_plus.dart';

class LoginPage extends StatefulWidget {
  static const String page = 'login_page';

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> with TickerProviderStateMixin {
  late AnimationController _loginButtonAnimationController;

  late TextEditingController pswdController;
  late TextEditingController userNameController;

  bool passwordVisible = false;
  bool rememberChecked = true;
  bool isLoggingIn = false;
  String version = '';

  @override
  void initState() {
    super.initState();
    _loginButtonAnimationController = AnimationController(duration: const Duration(milliseconds: 100), vsync: this);

    pswdController = TextEditingController();
    userNameController = TextEditingController();

    String username = Repository.getRememberedUsername();
    userNameController.text = username;
    String password = Repository.getRememberedPassword();
    pswdController.text = password;

    if (username.isNotEmpty && password.isNotEmpty) {
      rememberChecked = true;
    }
    PackageInfo.fromPlatform().then((PackageInfo packageInfo) {
      setState(() {
        version = packageInfo.version;
      });
    });
    // if (kDebugMode) {
    //   pswdController.text = 'RadiantGPS2021\$';
    //   userNameController.text = 'cs@radiantnetwork.com';
    // }
  }

  @override
  void dispose() {
    _loginButtonAnimationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(statusBarColor: Colors.transparent, statusBarBrightness: Brightness.light, statusBarIconBrightness: Brightness.light));

    return Scaffold(
      backgroundColor: Color(0xfffefefe),
      resizeToAvoidBottomInset: true,
      body: SafeArea(
        top: false,
        child: Stack(
          children: <Widget>[
            Container(height: MediaQuery.of(context).size.height),
            Positioned(bottom: 0, left: 0, right: 0, child: Image.asset('images/login_illustration.png', alignment: Alignment.center, fit: BoxFit.contain)),
            Positioned(bottom: 10, left: 0, right: 0, child: Text('App Version : $version', textAlign: TextAlign.center, style: TextStyle(color: AppColors.headerTextColor, fontWeight: FontWeight.w600))),
            Container(
              margin: EdgeInsets.only(left: 20, right: 20, top: 100, bottom: 40),
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    Image.asset('images/logo_colored.png', fit: BoxFit.fitHeight, height: 50, alignment: Alignment.center),
                    Container(
                      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 20),
                      margin: EdgeInsets.only(top: 50),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(10)),
                        boxShadow: [BoxShadow(color: Colors.grey[200]!, blurRadius: 10.0, spreadRadius: 1.0, offset: Offset(0, 1.0))],
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text('Login', style: TextStyle(color: Color(0xff3e4a59), fontWeight: FontWeight.bold, fontSize: 24)),
                          SizedBox(height: 10),
                          TextField(controller: userNameController, decoration: InputDecoration(border: UnderlineInputBorder(borderSide: BorderSide(color: AppColors.primaryColor)), labelText: 'Username')),
                          SizedBox(height: 10),
                          PasswordTextField(pswdController, false),
                          SizedBox(height: 20),
                          Align(
                            alignment: Alignment.centerRight,
                            child: InkWell(
                              onTap: onForgotPasswordTapped,
                              child: Padding(child: Text('Forgot Password?', style: TextStyle(color: AppColors.primaryColor, fontWeight: FontWeight.w600)), padding: EdgeInsets.all(10)),
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: 30),
                    Row(
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        InkWell(
                          onTap: () {
                            setState(() {
                              rememberChecked = !rememberChecked;
                            });
                          },
                          child: Padding(
                            padding: EdgeInsets.all(10),
                            child: Row(
                              children: [
                                Container(
                                  width: 20,
                                  height: 20,
                                  decoration: BoxDecoration(shape: BoxShape.circle, color: rememberChecked ? Color(0xffb0b6d0) : Colors.transparent, border: Border.all(color: Color(0xffb0b6d0))),
                                  child: rememberChecked ? Center(child: Icon(Icons.check, size: 15, color: Colors.white)) : null,
                                ),
                                SizedBox(width: 10),
                                Text('Remember me', style: TextStyle(fontSize: 13, color: Color(0xffb8babe), fontWeight: FontWeight.w500)),
                              ],
                            ),
                          ),
                        ),
                        Expanded(child: Container()),
                        InkWell(
                          child: ScaleTransition(
                            scale: Tween(begin: 1.0, end: .9).animate(CurvedAnimation(parent: _loginButtonAnimationController, curve: Curves.bounceIn))
                              ..addStatusListener((status) {
                                if (status == AnimationStatus.completed) {
                                  _loginButtonAnimationController.reverse();
                                }
                              }),
                            child: Container(
                              margin: EdgeInsets.only(right: 5.0),
                              decoration:
                                  BoxDecoration(color: AppColors.primaryColor, borderRadius: BorderRadius.circular(5), boxShadow: [BoxShadow(color: AppColors.primaryColor.withOpacity(0.6), blurRadius: 5.0)]),
                              padding: EdgeInsets.only(left: 40, right: 40, top: 12, bottom: 12),
                              child: Text('Login', style: TextStyle(fontSize: 14, color: Colors.white, fontWeight: FontWeight.w500)),
                            ),
                          ),
                          onTap: onLoginBtnTapped,
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    RichText(
                      textAlign: TextAlign.center,
                      text: TextSpan(children: [
                        TextSpan(text: 'By Successful login you are Agreeing with our', style: TextStyle(color: Color(0xFF343434), fontSize: 13, fontWeight: FontWeight.w400)),
                        TextSpan(
                            text: ' Terms & Conditions ',
                            style: TextStyle(color: AppColors.primaryColor, fontSize: 13, fontWeight: FontWeight.w400),
                            recognizer: TapGestureRecognizer()
                              ..onTap = () {
                                Navigator.pushNamed(context, TermsOfUsePage.page);
                              }),
                        TextSpan(text: 'And', style: TextStyle(color: Color(0xFF343434), fontSize: 13, fontWeight: FontWeight.w400)),
                        TextSpan(
                            text: ' Privacy Policy',
                            style: TextStyle(color: AppColors.primaryColor, fontSize: 13, fontWeight: FontWeight.w400),
                            recognizer: TapGestureRecognizer()
                              ..onTap = () {
                                Navigator.pushNamed(context, PrivacyPolicyPage.page);
                              })
                      ]),
                    ),
                    SizedBox(height: 32),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        InkWell(
                          onTap: onSwitchServerTapped,
                          child: Column(
                            children: [Image.asset('images/antenna.png', height: 25, width: 25), Text('Switch Server', style: TextStyle(fontWeight: FontWeight.w600))],
                          ),
                        ),
                        InkWell(
                          onTap: onSupportButtonTapped,
                          child: Column(
                            children: [Image.asset('images/support.png', height: 25, width: 25), Text('Support', style: TextStyle(fontWeight: FontWeight.w600))],
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: 20),
                  ],
                ),
              ),
            ),
            isLoggingIn ? Container(color: Color(0xb0000000), child: Center(child: CircularProgressIndicator(strokeWidth: 1, color: Colors.white))) : SizedBox(),
          ],
        ),
      ),
    );
  }

  void onSupportButtonTapped() {
    Navigator.push(context, MaterialPageRoute(builder: (context) => ContactUsScreen()));
  }

  void onForgotPasswordTapped() {
    showDialog(context: context, builder: (context) => Dialog(child: ForgotPasswordDialog()));
  }

  void onLoginBtnTapped() {
    _loginButtonAnimationController.forward();
    if (userNameController.text.isEmpty) {
      showSnackBar(context, 'Enter Username', Colors.red);

      return;
    } else if (pswdController.text.isEmpty) {
      showSnackBar(context, 'Enter Password', Colors.red);
      return;
    }
    setState(() {
      isLoggingIn = true;
    });

    if (Repository.getServerType() == GPSWoxConstants.type) {
      GPSWoxRequests.login(userNameController.text, pswdController.text).then((response) {
        if (response['isLoginSuccess'] as bool) {
          onLoginSuccessful(response['cookie'] as String);
        } else {
          onLoginFailed(response['message']);
        }
      });
    }

    if (Repository.getServerType() == GPSServerConstants.type) {
      GPSServerRequests.login(userNameController.text, pswdController.text).then((response) {
        if (response['isLoginSuccess'] as bool) {
          onLoginSuccessful(response['cookie'] as String);
        } else {
          onLoginFailed('Username Password Combination Incorrect');
        }
      });
    }
  }

  String getServerName() {
    if (Repository.getServerType() == GPSServerConstants.type) {
      if (Repository.getGPSServerUrl() == AutonemoServerConstants.BASE_URL) {
        return 'Autonemo GPS';
      } else {
        return 'Custom Server';
      }
    } else if (Repository.getServerType() == GPSWoxConstants.type) {
      return 'Custom Server';
    }
    return '';
  }

  void onSwitchServerTapped() {
    showDialog(context: context, builder: (context) => Dialog(child: SwitchServerDialog()));
  }

  void onLoginSuccessful(String cookie) {
    if (rememberChecked) {
      Repository.setRememberedCredentials(username: userNameController.text, password: pswdController.text);
    } else {
      Repository.setRememberedCredentials(username: '', password: '');
    }
    Repository.setUsername(userNameController.text);
    Repository.setPswd(pswdController.text);
    Repository.setCookie(cookie);
    Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => HomePage()));
  }

  void onLoginFailed(String message) {
    setState(() {
      isLoggingIn = false;
    });
    showSnackBar(context, message, Colors.red);
  }
}

class PasswordTextField extends StatefulWidget {
  final TextEditingController pswdController;

  final bool hideLabel;

  const PasswordTextField(this.pswdController, this.hideLabel);

  @override
  State<StatefulWidget> createState() {
    return PasswordTextFieldState();
  }
}

class PasswordTextFieldState extends State<PasswordTextField> {
  bool passwordVisible = false;

  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: widget.pswdController,
      obscureText: !passwordVisible,
      decoration: InputDecoration(
          suffixIcon: IconButton(
            onPressed: () {
              setState(() {
                passwordVisible = !passwordVisible;
              });
            },
            icon: Icon(passwordVisible ? Icons.visibility_off : Icons.visibility),
          ),
          border: UnderlineInputBorder(borderSide: BorderSide(color: AppColors.primaryColor)),
          labelText: widget.hideLabel ? '' : 'Password'),
    );
  }
}
