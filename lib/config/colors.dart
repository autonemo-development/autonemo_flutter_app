import 'package:flutter/material.dart';

class AppColors {
  static const Color primaryColor = Color(0xffDC0E18);
  static const Color primaryColor2 = Color(0xff808080);
  static const Color primaryColor_90 = Color(0x90DC0E18);
  static const Color primaryColor_10 = Color(0x10DC0E18);
  static const MaterialColor primaryMaterialColor = MaterialColor(0xffDC0E18, {
    50: Color.fromRGBO(220, 14, 24, .1),
    100: Color.fromRGBO(220, 14, 24, .2),
    200: Color.fromRGBO(220, 14, 24, .3),
    300: Color.fromRGBO(220, 14, 24, .4),
    400: Color.fromRGBO(220, 14, 24, .5),
    500: Color.fromRGBO(220, 14, 24, .6),
    600: Color.fromRGBO(220, 14, 24, .7),
    700: Color.fromRGBO(220, 14, 24, .8),
    800: Color.fromRGBO(220, 14, 24, .9),
    900: Color.fromRGBO(220, 14, 24, 1),
  });
  static const Color defaultBackground = Color(0xfff5f5f5);
  static const Color kOrangeColor = Color(0xFFF26611);
  static const Color kBlackColor = Color(0xFF343434);
  static const Color headerTextColor = Color(0xFF585858);
  static const MaterialColor shadowMaterialColor = MaterialColor(0xff3d87ff, {
    50: Color.fromRGBO(61, 135, 255, .1),
    100: Color.fromRGBO(61, 135, 255, .2),
    200: Color.fromRGBO(61, 135, 255, .3),
    300: Color.fromRGBO(61, 135, 255, .4),
    400: Color.fromRGBO(61, 135, 255, .5),
    500: Color.fromRGBO(61, 135, 255, .6),
    600: Color.fromRGBO(61, 135, 255, .7),
    700: Color.fromRGBO(61, 135, 255, .8),
    800: Color.fromRGBO(61, 135, 255, .9),
    900: Color.fromRGBO(61, 135, 255, 1),
  });

  static const sensorActiveColor = Color(0xff0C8E44);
  static const sensorLightActiveColor = Color(0xffD6EDE0);
  static const sensorInActiveColor = Color(0xff8a8888);
  static const sensorLightInActiveColor = Color(0xfff7f5f5);
}
