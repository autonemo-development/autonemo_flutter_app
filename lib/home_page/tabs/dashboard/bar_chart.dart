/// Bar chart example
import 'package:autonemogps/config/colors.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/material.dart';

class BarGraph extends StatelessWidget {
  final List<charts.Series<dynamic, String>> seriesList;
  final bool animate = true;
  final bool vertical;

  BarGraph(this.seriesList, this.vertical);

  @override
  Widget build(BuildContext context) {
    double height = 150;

    if (seriesList.length > 0) {
      if (seriesList[0].data.length > 1) {
        height = seriesList[0].data.length * 30;
        height = height + 70;
      }

      if (vertical) {
        height = 200;
      }
    }

    return Container(
      padding: EdgeInsets.all(10),
      margin: EdgeInsets.only(bottom: 16),
      height: height,
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(16), boxShadow: [
        BoxShadow(
          color: AppColors.shadowMaterialColor[100]!,
          blurRadius: 5.0,
          spreadRadius: 1.0,
          offset: Offset(1.0, 2.0),
        )
      ]),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                child: Text.rich(TextSpan(children: [
                  TextSpan(
                    text: seriesList.length > 0 ? seriesList[0].id : '',
                    style: TextStyle(fontSize: 16, fontWeight: FontWeight.w600),
                  ),
                ])),
              ),
            ],
          ),
          SizedBox(width: 20),
          Divider(),
          Expanded(
            child: new charts.BarChart(
              seriesList,
              animate: animate,
              vertical: vertical,
              barRendererDecorator: new charts.BarLabelDecorator<String>(),
            ),
          ),
        ],
      ),
    );
  }
}
