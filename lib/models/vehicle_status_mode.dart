import 'dart:ui';

class VehicleStatusModel {
  String status;

  Color statusColor;

  String expiryDate;

  String expiryDateText;

  Color expiryColor;

  VehicleStatusModel(this.status, this.statusColor, this.expiryDate, this.expiryDateText, this.expiryColor);
}
