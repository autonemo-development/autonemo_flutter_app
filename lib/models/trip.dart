import 'package:google_maps_flutter/google_maps_flutter.dart';

class Trip {
  String? startTime, stopLocation, endTime, idleTime, runTime, stopTime, distance, avgSpeed, topSpeed, leftAt;
  Polyline? polyline;

  Trip({this.distance, this.avgSpeed, this.stopLocation, this.endTime, this.idleTime, this.runTime, this.startTime, this.stopTime, this.topSpeed, this.leftAt, this.polyline});
}
