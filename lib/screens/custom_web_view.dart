import 'package:flutter/material.dart';
// import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:webview_flutter/webview_flutter.dart';

class CustomWebView extends StatelessWidget {
  final String title,data;

  const CustomWebView(this.title,this.data, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(title,style: TextStyle(fontSize: 12),)),
      body: WebView(
        onWebViewCreated: (controller){
          controller.loadHtmlString(data);
        },
        // initialData: InAppWebViewInitialData(data: data),
      ),
    );
  }
}
