import 'package:animations/animations.dart';
import 'package:autonemogps/config/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../bloc/home_bloc.dart';
import '../bloc/home_event.dart';

Future showSessionExpiredDialog(BuildContext context) {
  return showModal(
      context: context,
      builder: (context) {
        return Dialog(
          child: Container(
            padding: EdgeInsets.only(top: 20),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Image.asset('images/session_expire.png', height: 100.0),
                SizedBox(height: 40.0),
                Flexible(
                  child: SingleChildScrollView(
                    child: Padding(
                      padding: const EdgeInsets.only(left: 16, right: 16),
                      child: Text(
                        'Your session has expired. Please login again',
                        textAlign: TextAlign.center,
                        style: TextStyle(fontSize: 14, color: AppColors.sensorInActiveColor, fontWeight: FontWeight.w500),
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 10),
                Container(
                  padding: EdgeInsets.all(16),
                  color: AppColors.primaryColor,
                  child: InkWell(
                    onTap: () {
                      Navigator.pop(context);
                      BlocProvider.of<HomeBloc>(context).add(LogoutEvent());
                    },
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text('Login Now', style: TextStyle(color: Colors.white, fontSize: 16, fontWeight: FontWeight.w600)),
                        Icon(Icons.arrow_right_alt_sharp, color: Colors.white, size: 28)
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        );
      });
}
