import 'package:google_maps_flutter/google_maps_flutter.dart';

class Parking {
  String? altitude, came, left, duration, address;
  String position;
  int? angle;
  LatLng latLng;

  Parking({this.angle, this.altitude, this.came, this.duration, this.left, required this.position, required this.latLng, this.address});
}
