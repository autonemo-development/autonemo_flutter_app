import 'package:autonemogps/config/colors.dart';
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class CustomTextFormFieldWithDataTimeForFilter extends StatelessWidget {
  CustomTextFormFieldWithDataTimeForFilter(
      {required this.controller, this.enabled = true, required this.onChanged});

  TextEditingController controller;
  bool enabled;
  Function(String) onChanged;

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: controller,
      enabled: enabled,
      maxLines: 1,
      validator: (value) {
        if (value!.isEmpty) {
          return "Field can't be empty";
        }
        return null;
      },
      style: TextStyle(
          color: AppColors.primaryColor, fontSize: 12, fontWeight: FontWeight.w500),
      decoration: InputDecoration(
        contentPadding:
            EdgeInsets.only(top: 10, bottom: 10, left: 12, right: 12),
        border: OutlineInputBorder(
          /*borderSide: const BorderSide(
            color: Colors.indigo,
            width: 2,
          ),*/
          borderSide: BorderSide.none,
          borderRadius: BorderRadius.circular(5.0),
        ),
        suffixIcon: Padding(
          padding: const EdgeInsets.only(top: 10.0, bottom: 10.0),
          child: Image.asset(
            'images/date-time.png',
            color: AppColors.primaryColor,
            width: 5,
            height: 5,
          ),
        ),
        filled: true,
        fillColor: Color(0xFFF5F9FF),
      ),
      textInputAction: TextInputAction.next,
      onFieldSubmitted: (_) => FocusScope.of(context).nextFocus(),
      onChanged: onChanged,
    );
  }
}
