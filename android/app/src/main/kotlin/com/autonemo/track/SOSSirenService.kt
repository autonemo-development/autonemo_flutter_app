package com.autonemo.track

import android.app.*
import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.Color
import android.media.MediaPlayer
import android.os.Build
import android.os.IBinder
import android.text.TextUtils
import androidx.core.app.NotificationCompat
import java.util.*

class SOSSirenService : Service() {
    lateinit var player: MediaPlayer
    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    override fun onCreate() {
        player = MediaPlayer.create(this, R.raw.siren_sound) //select music file
        player.isLooping = true //set looping
    }

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        player.start()
        val imei = intent.getStringExtra("imei")
        val title = intent.getStringExtra("title")
        val body = intent.getStringExtra("body")
        val type = intent.getStringExtra("type")
        val names = saveImeiInSharedPrefs(imei, body, type)
        showServiceNotification(title, names, imei, type)
        return START_NOT_STICKY
    }

    override fun onDestroy() {
        player.stop()
        player.release()
        stopSelf()
        super.onDestroy()
    }

    private fun saveImeiInSharedPrefs(imei: String?, name: String?, type: String?): String {
        val sharedPreferences = getSharedPreferences("FlutterSharedPreferences", MODE_PRIVATE)
        val imeiSharedPrefStr: String = "flutter.sosIMEI"
        val namesSharedPrefStr: String = "flutter.sosNames"

        val imeis = sharedPreferences.getString(imeiSharedPrefStr, "")!!
        var names = sharedPreferences.getString(namesSharedPrefStr, "")!!
        val imeiList = ArrayList<String?>()
        val namesList = ArrayList<String?>()
        if (names.isNotEmpty()) {
            imeiList.addAll(listOf(*imeis.split(",").toTypedArray()))
            namesList.addAll(listOf(*names.split(", ").toTypedArray()))
        }
        if (!imeiList.contains(imei)) {
            imeiList.add(imei)
            val editor = sharedPreferences.edit()
            editor.putString(imeiSharedPrefStr, TextUtils.join(",", imeiList)).apply()
        }
        if (!namesList.contains(name)) {
            namesList.add(name)
            val editor = sharedPreferences.edit()
            names = TextUtils.join(", ", namesList)
            editor.putString(namesSharedPrefStr, names).apply()
        }
        return names
    }

    private fun showServiceNotification(
        title: String?,
        body: String?,
        imei: String?,
        type: String?
    ) {
        val mNotificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val pattern = longArrayOf(0, 1000, 500, 1000)
            val notificationChannel = NotificationChannel(
                getString(R.string.channel_name), getString(R.string.app_name),
                NotificationManager.IMPORTANCE_HIGH
            )
            notificationChannel.description = ""
            notificationChannel.enableLights(true)
            notificationChannel.lightColor = Color.BLUE
            notificationChannel.vibrationPattern = pattern
            notificationChannel.enableVibration(true)
            mNotificationManager.createNotificationChannel(notificationChannel)
        }
        // to display notification in DND Mode
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel =
                mNotificationManager.getNotificationChannel(getString(R.string.channel_name))
            channel.canBypassDnd()
        }
        val notificationBuilder = NotificationCompat.Builder(this, getString(R.string.channel_name))
        if (body != null) {
            notificationBuilder.setContentText(body)
        }
        notificationBuilder
            .setColor(Color.BLUE)
            .setContentTitle(title)
            .setDefaults(Notification.DEFAULT_ALL)
            .setWhen(System.currentTimeMillis())
            .setPriority(NotificationCompat.PRIORITY_MAX)
            .setSmallIcon(R.drawable.ic_notification)
            .setLargeIcon(BitmapFactory.decodeResource(resources, R.drawable.logo_full))
            .setOngoing(true)
        val ackIntent = Intent(this, MainActivity::class.java)
        ackIntent.putExtra("sirenType", type)
        ackIntent.flags = (Intent.FLAG_ACTIVITY_CLEAR_TOP
                or Intent.FLAG_ACTIVITY_SINGLE_TOP)
        val pendingIntent = PendingIntent.getActivity(applicationContext, 0, ackIntent, 0)
        notificationBuilder.setContentIntent(pendingIntent)
        val notification = notificationBuilder.build()
        notification.flags = notification.flags or Notification.FLAG_AUTO_CANCEL
        val serviceNotiId = imei!!.substring(imei.length - 6, imei.length - 1).toInt()
        startForeground(serviceNotiId, notification)
    }

}