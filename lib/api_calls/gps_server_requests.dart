import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:autonemogps/bloc/home_bloc.dart';
import 'package:autonemogps/bloc/home_event.dart';
import 'package:autonemogps/config/string_constants.dart';
import 'package:autonemogps/main.dart';
import 'package:autonemogps/models/alert_setting.dart';
import 'package:autonemogps/models/device.dart';
import 'package:autonemogps/models/dtc_model.dart';
import 'package:autonemogps/models/expense.dart';
import 'package:autonemogps/models/gen_info_data.dart';
import 'package:autonemogps/models/maintenance.dart';
import 'package:autonemogps/models/parking.dart';
import 'package:autonemogps/models/playback_route.dart' as playback_route;
import 'package:autonemogps/models/response_model.dart';
import 'package:autonemogps/models/reward_point.dart';
import 'package:autonemogps/models/trip.dart';
import 'package:autonemogps/storage/repository.dart';
import 'package:autonemogps/utils/enums.dart';
import 'package:autonemogps/utils/logs.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:home_widget/home_widget.dart';
import 'package:http/http.dart' as http;
import 'package:intl/intl.dart';
import 'package:string_unescape/string_unescape.dart';

class GPSServerRequests {
  static String? baseUrl, baseUri;
  static bool? isServerHttps;

  static Future<String> getBaseUri() async {
    if (baseUri == null) {
      baseUri = (await getBaseUrl()).replaceFirst('http://', '').replaceFirst('https://', '');
    }
    return baseUri!;
  }

  static Future<String> getBaseUrl() async {
    if (baseUrl != null) {
      return baseUrl!;
    }
    baseUrl = await Repository.getGPSServerUrl();
    return baseUrl!.isEmpty ? AutonemoServerConstants.BASE_URL : baseUrl!;
  }

  static Future<bool> isHttps() async {
    if (isServerHttps == null) {
      isServerHttps = (await getBaseUrl()).contains('https://');
    }
    return isServerHttps!;
  }

  static void resetUrl() {
    baseUrl = null;
    baseUri = null;
    isServerHttps = null;
  }

  static onSessionExpired() {
    log('Session Expired ');
    BlocProvider.of<HomeBloc>(navigatorKey.currentState!.context).add(SessionExpiredEvent());
  }

  static Future<Map<String, dynamic>> login(String username, String pswd) async {
    if (username.isEmpty || pswd.isEmpty) {
      return {'isLoginSuccess': false, 'message': 'Invalid user details.'};
    }
    Map data = {'cmd': 'login', 'username': username, 'password': pswd, 'remember_me': 'false', 'mobile': 'false'};

    print(await getBaseUrl());

    var response = await http.post(
      Uri.parse((await getBaseUrl()) + '/func/connect.php'),
      body: data,
      encoding: Encoding.getByName('utf-8'),
      headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},
    );

    print(response.body);

    ResponseModel responseModel = new ResponseModel(response.body);

    if (responseModel.isSuccess()) {
      String cookie = response.headers['set-cookie']!;
      return {'isLoginSuccess': true, 'cookie': cookie};
    }
    resetUrl();

    String error = responseModel.getErrorString();

    return {'isLoginSuccess': false, 'message': error.isNotEmpty ? error : 'Unknown Error'};
  }

  static Map<String, String> getHeader({String? cookies}) {
    String cookie = cookies ?? Repository.getCookie();
    if (cookie.isNotEmpty) {
      return {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8', 'Cookie': cookie};
    } else {
      return {};
    }
  }

  static Future<bool> checkSession() async {
    Map<String, String> header = getHeader();

    if (header.isEmpty) {
      return false;
    }

    try {
      http.Response response = await http.post(Uri.parse((await getBaseUrl()) + '/func/connect.php'), body: {'cmd': 'session_check'}, headers: header);

      ResponseModel responseModel = new ResponseModel(response.body);

      Map<String, dynamic> sessionMap = responseModel.getDataAsMap();

      if (sessionMap.containsKey('session')) {
        return sessionMap['session'];
      }
    } on Exception catch (e) {
      writeErrorLogs('checkSession Error ', e: e);
    }

    return true;
  }

  static Future<bool> fetchFnSettings() async {
    Map<String, String> header = getHeader();

    if (header.isEmpty) {
      return false;
    }

    try {
      bool isSessionActive = await checkSession();

      // writeDebugLogs('isSessionActive ' + isSessionActive.toString());

      if (!isSessionActive) {
        onSessionExpired();
        return false;
      }

      http.Response response = await http.post(
        Uri.parse((await getBaseUrl()) + '/func/settings.objects.php'),
        body: {'cmd': 'object_data_get_all'},
        headers: header,
      );

      if (response.body.isNotEmpty) {
        Repository.setAllDevicesData(response.body);
        return true;
      }
    } on SocketException catch (e) {
      writeErrorLogs('FetchFnSettings Error ', e: e);
    }

    return false;
  }

  static Future<String> fetchFnObjects() async {
    try {
      http.Response response = await http.post(
        Uri.parse((await getBaseUrl()) + '/func/objects.php'),
        body: {'cmd': 'object_data_get_all'},
        headers: getHeader(),
      );

      // writeDebugLogs('fetchFnObjects ' + response.body);

      if (response.body.isNotEmpty) {
        return response.body;
      }
    } on Exception catch (e) {
      writeErrorLogs('FetchFnObjects Error ', e: e);
    }

    return '';
  }

  static Future<Map<String, dynamic>> fetchHistory(String dtf, String dtt, String imei, String stops) async {
    String cookies = Repository.getCookie();
    http.Response response = await http.post(
      Uri.parse((await getBaseUrl()) + '/func/history.php'),
      body: {'cmd': 'route_data_get', 'dtf': dtf, 'dtt': dtt, 'imei': imei, 'min_stop_duration': stops},
      headers: {'Cookie': cookies},
    );

    if (response.body.isNotEmpty) {
      ResponseModel responseModel = new ResponseModel(response.body);

      Map<String, dynamic> data = responseModel.getDataAsMap();

      List<dynamic> drivesList = data['drives'] as List<dynamic>;
      List<dynamic> stopsList = data['stops'] as List<dynamic>;
      List<dynamic> routes = data['route'] as List<dynamic>;

      if (drivesList.length > 0 && stopsList.length > 0) {
        List<Parking> parkingList = [];
        List<Trip> tripList = [];
        Set<Polyline> polyLines = {};
        List<LatLng> routePoints = [];
        List<playback_route.PlaybackRoute> markerRoute = [];
        DateFormat dateFormat = new DateFormat(STANDARD_DATE_TIME_INPUT_PATTERN);
        int conditionVar = 0;
        if (stopsList.length > 0 && drivesList.length > 0) {
          if (dateFormat.parse((stopsList.elementAt(0) as List<dynamic>).elementAt(6) as String).compareTo(dateFormat.parse((drivesList.elementAt(0) as List<dynamic>).elementAt(4) as String)) < 0) {
            conditionVar = 0;
            List stop = stopsList.elementAt(0) as List<dynamic>;
            String startTime = stop[6] as String;
            String endTime = stop[7] as String;
            String duration = stop[8] as String;
            String stopLocation = '${stop[2]}, ${stop[3]}';
            tripList.add(Trip(endTime: startTime, leftAt: endTime, stopTime: duration, stopLocation: stopLocation));
          } else {
            conditionVar = 1;
            List<dynamic> drive = drivesList.elementAt(0) as List<dynamic>;
            String topSpeed = '${drive[8] as int} kph';
            String avgSpeed = '${drive[9] as int} kph';
            String distance = '${drive[7]} KM';
            String startTime = drive[4] as String;
            String runTime = drive[6] as String;
            tripList.add(Trip(avgSpeed: avgSpeed, topSpeed: topSpeed, distance: distance, startTime: startTime, runTime: runTime));
          }
        } else if (stopsList.length > drivesList.length) {
          if (stopsList.length > 0) {
            conditionVar = 0;
            List stop = stopsList.elementAt(0) as List<dynamic>;
            String startTime = stop[6] as String;
            String endTime = stop[7] as String;
            String duration = stop[8] as String;
            String stopLocation = '${stop[2]}, ${stop[3]}';
            tripList.add(Trip(endTime: startTime, leftAt: endTime, stopTime: duration, stopLocation: stopLocation));
          }
        } else {
          if (drivesList.length > 0) {
            conditionVar = 1;
            List<dynamic> drive = drivesList.elementAt(0) as List<dynamic>;
            String topSpeed = '${drive[8] as int} kph';
            String avgSpeed = '${drive[9] as int} kph';
            String distance = '${drive[7]} KM';
            String startTime = drive[4] as String;
            String runTime = drive[6] as String;
            tripList.add(Trip(avgSpeed: avgSpeed, topSpeed: topSpeed, distance: distance, startTime: startTime, runTime: runTime));
          }
        }

        int s = (1 == conditionVar) ? 0 : 1;
        int d = (1 == conditionVar) ? 1 : 0;
        for (int i = 0; i < stopsList.length + drivesList.length; i++) {
          if (i % 2 != conditionVar) {
            if (s < stopsList.length) {
              List<dynamic> stop = stopsList.elementAt(s) as List<dynamic>;
              String startTime = stop[6] as String;
              String endTime = stop[7] as String;
              String duration = stop[8] as String;
              String stopLocation = '${stop[2]}, ${stop[3]}';

              parkingList.add(Parking(
                  altitude: stop.elementAt(4).toString(),
                  angle: stop.elementAt(5) as int,
                  came: startTime,
                  left: endTime,
                  duration: duration,
                  position: stopLocation,
                  latLng: LatLng(double.parse(stop.elementAt(2)), double.parse(stop.elementAt(3)))));
              Trip trip = tripList.last;

              trip.endTime = startTime;
              trip.leftAt = endTime;
              trip.stopTime = duration;
              trip.stopLocation = stopLocation;
              s++;
            }
          } else {
            if (d < drivesList.length) {
              List<dynamic> drives = drivesList.elementAt(d);
              String topSpeed = '${drives[8] as int} kph';
              String avgSpeed = '${drives[9] as int} kph';
              String distance = '${drives[7]} KM';
              String startTime = drives[4] as String;
              String runTime = drives[6] as String;
              tripList.add(Trip(avgSpeed: avgSpeed, topSpeed: topSpeed, distance: distance, startTime: startTime, runTime: runTime));
              d++;
            }
          }
        }

        if (s < stopsList.length - 1) {
          List<dynamic> stop = stopsList.elementAt(stopsList.length - 1);
          String startTime = stop[6] as String;
          String endTime = stop[7] as String;
          String duration = stop[8] as String;
          String stopLocation = '${stop[2]}, ${stop[3]}';

          Trip trip = tripList.last;

          trip.endTime = startTime;
          trip.leftAt = endTime;
          trip.stopTime = duration;
          trip.stopLocation = stopLocation;
        }
        if (d < drivesList.length) {
          List<dynamic> drives = drivesList.elementAt(drivesList.length - 1);
          String topSpeed = '${drives[8] as int} kph';
          String avgSpeed = '${drives[9] as int} kph';
          String distance = '${drives[7]} KM';
          String startTime = drives[4] as String;
          String runTime = drives[6] as String;
          tripList.add(Trip(avgSpeed: avgSpeed, topSpeed: topSpeed, distance: distance, startTime: startTime, runTime: runTime));
        }

        double? x0, x1, y0, y1;
        int tripIndex = 0;
        List<LatLng> tripLine = [];

        for (int i = 0; i < routes.length; i++) {
          List<dynamic> route = (routes.elementAt(i) as List<dynamic>);
          LatLng latLng = LatLng(double.parse(route.elementAt(1) as String), double.parse(route.elementAt(2) as String));
          if (x0 == null) {
            x0 = x1 = latLng.latitude;
            y0 = y1 = latLng.longitude;
          } else {
            if (latLng.latitude > x1!) x1 = latLng.latitude;
            if (latLng.latitude < x0) x0 = latLng.latitude;
            if (latLng.longitude > y1!) y1 = latLng.longitude;
            if (latLng.longitude < y0!) y0 = latLng.longitude;
          }
          if (i > 0) {
            LatLng prevLatLng = routePoints.elementAt(routePoints.length - 1);
            if (prevLatLng.latitude == latLng.latitude && prevLatLng.longitude == latLng.longitude) {
              continue;
            }
          }

          tripLine.add(latLng);

          if (tripList[tripIndex].leftAt == route[0]) {
            tripList[tripIndex].polyline = Polyline(
              polylineId: PolylineId(tripList[tripIndex].leftAt!),
              points: [...tripLine],
              visible: true,
              width: 5,
              color: Colors.blue,
              zIndex: 1,
            );
            tripLine.clear();
            tripIndex++;
          }
          routePoints.add(latLng);
          markerRoute.add(playback_route.PlaybackRoute(angle: route.elementAt(4), latLng: latLng, speed: route.elementAt(5) as int, time: route.elementAt(0) as String));
        }

        polyLines.add(Polyline(polylineId: PolylineId('polyline_one'), visible: true, points: routePoints, width: 5, color: Color(0xFFF26611)));

        if (tripIndex < tripList.length) {
          tripList[tripIndex].polyline = Polyline(
            polylineId: PolylineId(tripList[tripIndex].leftAt ?? tripList[tripIndex].startTime!),
            points: [...tripLine],
            visible: true,
            width: 5,
            color: Colors.blue,
            zIndex: 1,
          );
        }

        return {
          'topSpeed': data['top_speed'].toString(),
          'averageSpeed': data['avg_speed'].toString(),
          'moveDuration': data['drives_duration'],
          'routeLength': data['route_length'].toString(),
          'stopsDuration': data['stops_duration'],
          'fuelConsumption': data['fuel_consumption'].toString(),
          'avgFuelCons100km': data['fuel_consumption_per_100km'].toString(),
          'fuelCost': data['fuel_cost'].toString(),
          'engineWork': data['engine_work'],
          'engineIdle': data['engine_idle'],
          'parkingList': parkingList,
          'polyLines': polyLines,
          'tripList': tripList,
          'routePoints': markerRoute,
          'bounds': LatLngBounds(northeast: LatLng(x1 == null ? 0 : x1, y1 == null ? 0 : y1), southwest: LatLng(x0 == null ? 0 : x0, y0 == null ? 0 : y0)),
        };
      }
    }
    return {};
  }

  static Future<List> fetchFnEvents(String search, Map<String, String> filter) async {
    var header = getHeader();
    if (header.isEmpty) {
      return [0, 'Please relogin to continue'];
    }

    var uri = Uri.https((await getBaseUri()), '/func/events.php', {'cmd': 'event_list_get', '_search': 'false', 'rows': '100', 'page': '1', 's': search, 'sidx': 'dt_tracker', 'sord': 'desc'});

    List returningList = [];

    try {
      var response = await http.get(
        uri,
        headers: header,
      );

      if (response.body.isEmpty) {
        bool isSessionActive = await checkSession();
        if (!isSessionActive) {
          returningList.add(2);
          returningList.add('Invalid session. Please relogin and try again.');
        } else {
          returningList.add(0);
        }
      } else {
        Repository.setAlerts(response.body);
        Repository.setLastRefreshedEventTime();
        returningList.add(1);
        returningList.add(response.body);
      }
    } on SocketException catch (_) {
      returningList.add(0);
      returningList.add('No Internet Connection');
    }
    return returningList;
  }

  static Future<List> fetchGeoPlaces() async {
    List returningList = [];

    try {
      Uri uri = Uri.parse((await getBaseUrl()) + '/func/places.php');
      var response = await http.post(uri, body: {'cmd': 'zone_data_get_all'}, headers: getHeader());
      if (response.body.isEmpty) {
        returningList.add(false);
        returningList.add('Something went wrong. Please try again later.');
      } else {
        ResponseModel responseModel = new ResponseModel(response.body);

        if (responseModel.isSuccess() && responseModel.getDataString() != '[]') {
          Map<String, dynamic> data = responseModel.getDataAsMap();

          returningList.add(true);
          returningList.add(data);
        } else {
          returningList.add(false);
          returningList.add('No Data');
        }
      }
    } on SocketException catch (_) {
      returningList.add(false);
      returningList.add('Internet Error Occurred');
    } on Exception catch (_) {
      returningList.add(false);
      returningList.add('Something went wrong. Please try again later.');
    }
    return returningList;
  }

  static Future<List> addGeoPlace(String name, String vertices, {String? zoneId}) async {
    List returningList = List.empty(growable: true);

    try {
      String cookies = await Repository.getCookie();
      Map<String, String> body = {
        'cmd': 'zone_data_set',
        'id': 'false',
        'group_id': '0',
        'name': name,
        'color': '#FF0000',
        'visible': 'true',
        'name_visible': 'true',
        'area': '0',
        'vertices': vertices,
      };
      if (zoneId != null) {
        body.putIfAbsent('id', () => zoneId);
      }

      var response = await http.post(Uri.parse((await getBaseUrl()) + '/func/places.php'), body: body, headers: {'Cookie': cookies});
      if (response.body.isEmpty) {
        returningList.add(false);
        returningList.add('Something went wrong. Please try again later.');
      } else {
        var zone_id = zoneId;
        if (zone_id == null) {
          var uri = Uri.https((await getBaseUri()), '/func/places.php', {'cmd': 'zone_list_get', '_search': 'false', 'rows': '50', 'page': '1', 's': name, 'sidx': 'group_id asc, name', 'sord': 'asc'});

          var response1 = await http.get(
            uri,
            headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8', 'Cookie': cookies},
          );
          if (response1.body.isNotEmpty) {
            var parsedData = json.decode(response1.body) as Map<String, dynamic>;

            zone_id = (((parsedData['rows'] as List<dynamic>).elementAt(0) as Map<String, dynamic>)['cell'] as List).elementAt(0) as String;
          }
        }
        if (zone_id != null) {
          var response2 = await http.post(Uri.parse((await getBaseUrl()) + '/func/settings.events.php'), body: {
            'cmd': 'event_data_get_all',
          }, headers: {
            'Cookie': cookies
          });

          ResponseModel responseModel = new ResponseModel(response2.body);

          if (responseModel.isSuccess()) {
            Map<String, dynamic> parsedData = responseModel.getDataAsMap();

            bool isZoneOutAvailable = false;
            bool isZoneInAvailable = false;

            parsedData.forEach((key, value) async {
              Map<String, dynamic> map = value as Map<String, dynamic>;
              if (map['type'] == 'zone_in' || map['type'] == 'zone_out') {
                if (!isZoneOutAvailable) {
                  isZoneOutAvailable = map['type'] == 'zone_out';
                }
                if (!isZoneInAvailable) {
                  isZoneInAvailable = map['type'] == 'zone_in';
                }
                List<String> selectedZones = (map['zones'] as String).split(',');
                if (!selectedZones.contains(zone_id)) {
                  map['zones'] = (map['zones'] as String) + ',' + zone_id!;
                  map['id'] = key;
                  await updateEventStatus(map);
                }
              }
            });
            final Map<String, dynamic> saveMap = {
              'cmd': 'event_data_set',
              'id': 'false',
              'type': '',
              'name': '',
              'active': 'true',
              'duration_from_last_event': 'false',
              'duration_from_last_event_minutes': '0',
              'week_days': 'true,true,true,true,true,true,true',
              'day_time':
                  '{"dt":false,"mon":false,"mon_from":"00:00","mon_to":"24:00","tue":false,"tue_from":"00:00","tue_to":"24:00","wed":false,"wed_from":"00:00","wed_to":"24:00","thu":false,"thu_from":"00:00","thu_to":"24:00","fri":false,"fri_from":"00:00","fri_to":"24:00","sat":false,"sat_from":"00:00","sat_to":"24:00","sun":false,"sun_from":"00:00","sun_to":"24:00"}',
              'imei': AllDeviceModelData().deviceModelAndImeiMapGpsServer.keys.toList().join(','),
              'checked_value': '',
              'route_trigger': 'off',
              'zone_trigger': 'off',
              'routes': '',
              'zones': zone_id.toString(),
              'notify_system': 'true,true,true,alarm1.mp3',
              'notify_push': 'true',
              'notify_email': 'false',
              'notify_email_address': '',
              'notify_sms': 'false',
              'notify_sms_number': '',
              'email_template_id': '0',
              'sms_template_id': '0',
              'notify_arrow': 'false',
              'notify_arrow_color': 'arrow_yellow',
              'notify_ohc': 'false',
              'notify_ohc_color': '#FFFF00',
              'webhook_send': 'true',
              'webhook_url': 'https://geocode.autonemogps.com/webhook/receiver.php',
              'cmd_send': 'false',
              'cmd_gateway': 'gprs',
              'cmd_type': 'ascii',
              'cmd_string': ''
            };
            if (!isZoneOutAvailable) {
              saveMap['type'] = 'zone_out';
              saveMap['name'] = 'Zone Out';
              await updateEventStatus(saveMap);
            }
            if (!isZoneInAvailable) {
              saveMap['type'] = 'zone_in';
              saveMap['name'] = 'Zone In';
              await updateEventStatus(saveMap);
            }
          }
        }

        ResponseModel responseModel = new ResponseModel(response.body);

        returningList.add(responseModel.isSuccess());
        returningList.add(responseModel.getDataString());
      }
    } on SocketException catch (_) {
      returningList.add(false);
      returningList.add('Internet Error Occurred');
    } on Exception catch (_) {
      returningList.add(false);
      returningList.add('Something went wrong. Please try again later.');
    }
    return returningList;
  }

  static Future<List> deleteZone(String zoneId) async {
    List returningList = [];

    try {
      String cookies = await Repository.getCookie();
      Map<String, String> body = {'cmd': 'zone_delete', 'id': zoneId};

      var response = await http.post(Uri.parse((await getBaseUrl()) + '/func/places.php'), body: body, headers: {'Cookie': cookies});
      if (response.body.isEmpty) {
        returningList.add(false);
        returningList.add('Something went wrong. Please try again later.');
      } else {
        ResponseModel responseModel = new ResponseModel(response.body);

        returningList.add(responseModel.isSuccess());
        returningList.add(responseModel.getDataString());
      }
    } on SocketException catch (_) {
      returningList.add(false);
      returningList.add('Internet Error Occurred');
    } on Exception catch (_) {
      returningList.add(false);
      returningList.add('Something went wrong. Please try again later.');
    }
    return returningList;
  }

  static Future<List> updateEventStatus(Map<String, dynamic> map) async {
    List returningList = [];

    try {
      Map<String, String> body = {'cmd': 'event_data_set'};
      map.forEach((key, value) => body.putIfAbsent(key, () => value.toString()));

      // writeDebugLogs('Update Event Status Body ' + body.toString());

      http.Response response = await http.post(
        Uri.parse((await getBaseUrl()) + '/func/settings.events.php'),
        body: body,
        headers: getHeader(),
      );

      if (response.body.isEmpty) {
        returningList.add(false);
        returningList.add('Something went wrong. Please try again later.');
      } else {
        ResponseModel responseModel = new ResponseModel(response.body);

        returningList.add(responseModel.isSuccess());
        returningList.add(responseModel.getDataString());
      }
    } on Exception catch (e) {
      writeErrorLogs('updateEventStatus Error ', e: e);
      returningList.add(false);
      returningList.add('Something went wrong. Please try again later.');
    }
    return returningList;
  }

  static Future<List> fetchEventTypes() async {
    List returningList = [];

    try {
      var response = await http.post(Uri.parse((await getBaseUrl()) + '/func/settings.events.php'),
          body: {
            'cmd': 'event_data_get_all',
          },
          headers: getHeader());
      if (response.body.isEmpty) {
        returningList.add(false);
        returningList.add('Something went wrong. Please try again later.');
      } else {
        ResponseModel responseModel = new ResponseModel(response.body);

        Map<String, dynamic> parsedData = responseModel.getDataAsMap();

        Map<String, String> alertTypeMap = {};
        parsedData.forEach((key, value) {
          Map<String, dynamic> map = value as Map<String, dynamic>;
          alertTypeMap[key] = map['name'] as String;
        });
        returningList.add(true);
        returningList.add(alertTypeMap);
        returningList.add(parsedData);
      }
    } on SocketException catch (_) {
      returningList.add(false);
      returningList.add('Internet Error Occurred');
    } on Exception catch (_) {
      returningList.add(false);
      returningList.add('Something went wrong. Please try again later.');
    }
    return returningList;
  }

  static Future<String> updateValues(String imei, String value, SettingUpdateType type) async {
    try {
      String allDeviceData = await Repository.getAllDevicesData();

      ResponseModel responseModel1 = new ResponseModel(allDeviceData);

      Map<String, dynamic> allDeviceDataJson = responseModel1.getDataAsMap();

      Map<String, dynamic> fnSettings = allDeviceDataJson[imei] as Map<String, dynamic>;

      Map<String, dynamic> body = {
        'cmd': 'object_data_set',
        'protocol': fnSettings['protocol'] as String,
        'group_id': fnSettings['group_id'] as String,
        'driver_id': fnSettings['driver_id'] as String,
        'trailer_id': fnSettings['trailer_id'] as String,
        'name': fnSettings['name'] as String,
        'imei': imei,
        'device': fnSettings['device'] as String,
        'sim_number': fnSettings['sim_number'] as String,
        'model': fnSettings['model'] as String,
        'vin': fnSettings['vin'] as String,
        'plate_number': fnSettings['plate_number'] as String,
        'icon': fnSettings['icon'] as String,
        'map_arrows': json.encode(fnSettings['map_arrows'] as Map<String, dynamic>),
        'map_icon': fnSettings['map_icon'] as String,
        'tail_color': fnSettings['tail_color'] as String,
        'tail_points': fnSettings['tail_points'] as String,
        'fcr': json.encode(fnSettings['fcr'] as Map<String, dynamic>),
        'time_adj': fnSettings['time_adj'] as String,
        'accuracy': json.encode(fnSettings['accuracy'] as Map<String, dynamic>),
        'speed_source': fnSettings['speed_source'] as String,
        'unassign_driver': fnSettings['unassign_driver'].toString(),
        'params_to_zero': fnSettings['params_to_zero'].toString(),
        'accvirt': 'false',
        'accvirt_cn': 'false',
        'forward_loc_data': 'false',
        'forward_loc_data_imei': fnSettings['forward_loc_data_imei'] as String,
        'odometer_type': fnSettings['odometer_type'] as String,
        'engine_hours_type': fnSettings['engine_hours_type'] as String,
        'odometer': 'false',
        'engine_hours': 'false'
      };

      if (type == SettingUpdateType.ODOMETER) {
        body['odometer'] = value;
      } else if (type == SettingUpdateType.ENGINE_HOURS) {
        body.update('engine_hours', (s) => value);
      } else if (type == SettingUpdateType.FUEL_COST) {
        Map<String, dynamic> map = fnSettings['fcr'] as Map<String, dynamic>;
        map['cost'] = value;
        body.update('fcr', (s) => json.encode(map));
      } else if (type == SettingUpdateType.MILEAGE) {
        Map<String, dynamic> map = fnSettings['fcr'] as Map<String, dynamic>;
        map['summer'] = value;
        map['winter'] = value;
        body.update('fcr', (s) => json.encode(map));
      } else if (type == SettingUpdateType.NAME) {
        body['name'] = value;
      } else if (type == SettingUpdateType.ICON) {
        body['icon'] = value;
      }

      http.Response response = await http.post(
        Uri.parse((await getBaseUrl()) + '/func/settings.objects.php'),
        body: body,
        headers: getHeader(),
      );

      ResponseModel responseModel = new ResponseModel(response.body);

      if (responseModel.isSuccess()) {
        await fetchFnSettings();
      }
      return responseModel.isSuccess() ? 'OK' : 'false';
    } on SocketException catch (_) {
      return 'Internet Error Occurred';
    } on Exception catch (e) {
      writeErrorLogs('updateValues ', e: e);
      return 'Something Went wrong';
    }
  }

  static Future<List> sendCommand(String cmd, String imei) async {
    http.Response response = await http.post(Uri.parse((await getBaseUrl()) + '/func/cmd.php'),
        body: {
          'cmd': 'cmd_gprs_send',
          'name': 'Custom',
          'imei': imei,
          'type': 'ascii',
          'cmd_': cmd,
        },
        headers: getHeader());
    List returningList = [];

    ResponseModel responseModel = new ResponseModel(response.body);

    if (responseModel.isSuccess()) {
      returningList.add(1);
    } else {
      returningList.add(0);
    }
    return returningList;
  }

  static Future<List> changePswd(String oldPswd, String newPswd, {String? cookie}) async {
    List returningList = [];

    List userDataList = await fetchUserData(cookie: cookie);
    if (userDataList[0] == 0) {
      returningList.addAll(userDataList);
    } else {
      Map<String, dynamic> userDataItem = userDataList[1];

      Map<String, String> jsonObject = userDataItem.map((key, value) => MapEntry(key, value.toString()));

      String units = jsonObject['unit_distance']! + ',' + jsonObject['unit_capacity']! + ',' + jsonObject['unit_temperature']!;

      http.Response response = await http.post(Uri.parse((await getBaseUrl()) + '/func/settings.php'),
          body: {
            'cmd': 'user_data_set',
            ...jsonObject,
            'units': units,
            'old_password': oldPswd,
            'new_password': newPswd,
            'new_password_repeat': newPswd,
          },
          headers: getHeader(cookies: cookie));
      if (response.body.isEmpty) {
        bool isSessionActive = await checkSession();
        if (isSessionActive) {
          returningList.add(0);
          returningList.add('Something went wrong. Please try again later.');
        } else {
          returningList.add(0);
          returningList.add('Session is not valid. Please relogin to continue.');
        }
      } else {
        ResponseModel responseModel = new ResponseModel(response.body);

        if (responseModel.isSuccess()) {
          returningList.add(1);
        } else {
          returningList.add(0);
          returningList.add(responseModel.getErrorString());
        }
      }
    }
    return returningList;
  }

  static Future<List> fetchUserData({String? cookie}) async {
    var response = await http.post(Uri.parse((await getBaseUrl()) + '/func/settings.php'), body: {'cmd': 'user_data_get'}, headers: getHeader(cookies: cookie));
    List returningList = [];
    if (response.body.isEmpty) {
      bool isSessionActive = await checkSession();
      if (isSessionActive) {
        returningList.add(0);
        returningList.add('Something went wrong. Please try again later.');
      } else {
        returningList.add(0);
        returningList.add('Session is not valid. Please relogin to continue.');
      }
    } else {
      ResponseModel responseModel = new ResponseModel(response.body);

      returningList.add(1);
      returningList.add(responseModel.getDataAsMap());
    }
    return returningList;
  }

  static Future<List> fetchDetailedEvent(
    String eventId,
  ) async {
    var response = await http.post(Uri.parse((await getBaseUrl()) + '/func/events.php'),
        body: {
          'cmd': 'event_data_get',
          'id': eventId,
        },
        headers: getHeader());
    List returningList = [];
    if (response.body.isEmpty) {
      returningList.add(0);
      returningList.add('Something went wrong. Please try again later.');
    } else {
      ResponseModel responseModel = new ResponseModel(response.body);

      Map<String, dynamic> parsedData = responseModel.getDataAsMap();
      String lat = parsedData['lat'] as String;
      String lng = parsedData['lng'] as String;
      String address = await fetchAddress(lat, lng);
      returningList.add(1);
      returningList.add({
        'imei': parsedData['imei'] as String,
        'time': parsedData['dt_tracker'] as String,
        'address': address,
        'object': parsedData['name'] as String,
        'speed': parsedData['speed'] as int,
        'position': lat + '° ' + lng + '°',
        'event': parsedData['desc'] as String,
        'angle': parsedData['angle'].toString() + '°',
        'altitude': parsedData['altitude'].toString(),
        'lat': lat,
        'lng': lng
      });
    }
    return returningList;
  }

  static Future<String> fetchAddress(String lat, String lng) async {
    http.Response response = await http.post(Uri.parse((await getBaseUrl()) + '/tools/geocoder.php'), body: {'cmd': 'latlng_to_address', 'lat': lat, 'lng': lng}, headers: getHeader());

    ResponseModel responseModel = new ResponseModel(response.body);

    String addressString = responseModel.getDataString();
    return unescape(addressString).replaceAll('"', '');
  }

  static Future<String> generateReport(
      String dataItems, String dtf, String dtt, String imei, String sensorNames, String showAddresses, String showCoordinates, String stopDuration, String type, String zoneAddresses,
      {Map<String, String>? headers}) async {
    try {
      var response = await http.post(Uri.parse((await getBaseUrl()) + '/func/reports.gen.php'),
          body: {
            'cmd': 'report_generate',
            'data_items': dataItems,
            'dtf': dtf,
            'dtt': dtt,
            'format': 'html',
            'imei': imei,
            'name': '',
            'sensor_names': sensorNames,
            'show_addresses': showAddresses,
            'show_coordinates': showCoordinates,
            'speed_limit': '',
            'stop_duration': stopDuration,
            'type': type,
            'zone_ids': '',
            'zones_addresses': zoneAddresses,
            'ignore_empty_reports': 'false',
            'marker_ids': '',
            'other': '',
          },
          headers: headers ?? getHeader());

      ResponseModel responseModel = new ResponseModel(response.body);

      String reportId = responseModel.getDataString().replaceAll('\"', '');

      var response1 = await http.get(
        Uri.parse((await getBaseUrl()) + '/func/reports.php?cmd=generated_download&id=' + reportId),
        headers: headers ?? getHeader(),
      );
      if (response1.body == 'Error connecting to database.') {
        throw 'Error connecting to database.';
      }

      return response1.body;
    } on Exception catch (e) {
      throw e.toString();
    }
  }

  static Future<List<DTCModel>> fetchDTC() async {
    http.Response response = await http.get(
      Uri.parse((await getBaseUrl()) + '/func/dtc.php?cmd=dtc_list_get&_search=false&nd=1681655982723&rows=50&page=1&sidx=dt_tracker&sord=desc&filters='),
      headers: getHeader(),
    );

    List<DTCModel> dtc = [];

    if (response.body.isNotEmpty) {
      var parsedData = json.decode(response.body) as Map<String, dynamic>;

      if (parsedData.containsKey('rows')) {
        List<dynamic> rowsList = parsedData['rows'] as List<dynamic>;
        if (rowsList.length > 0) {
          for (int i = 0; i < rowsList.length; i++) {
            List<dynamic> cellList = (rowsList.elementAt(i) as Map<String, dynamic>)['cell'] as List<dynamic>;
            dtc.add(DTCModel(date: cellList.elementAt(0) as String, objectName: cellList.elementAt(1) as String, code: cellList.elementAt(2) as String));
          }
        }
      }
    }
    return dtc;
  }

  static Future<List<Expense>> fetchExpense() async {
    http.Response response = await http.get(
      Uri.parse((await getBaseUrl()) + '/func/expenses.php?cmd=expense_list_get&_search=false&nd=1573829958337&rows=50&page=1&sidx=dt_expense&sord=desc'),
      headers: getHeader(),
    );

    List<Expense> expenseList = [];

    if (response.body.isNotEmpty) {
      Map<String, dynamic> parsedData = json.decode(response.body) as Map<String, dynamic>;

      if (!parsedData.containsKey('rows')) {
        return expenseList;
      }

      List<dynamic> rowsList = parsedData['rows'] as List<dynamic>;
      if (rowsList.length > 0) {
        for (int i = 0; i < rowsList.length; i++) {
          List<dynamic> cellList = (rowsList.elementAt(i) as Map<String, dynamic>)['cell'] as List<dynamic>;
          expenseList.add(Expense(
            id: (rowsList.elementAt(i) as Map<String, dynamic>)['id'] as String,
            objectName: cellList.elementAt(2) as String,
            date: cellList.elementAt(0) as String,
            buyer: cellList.elementAt(6) as String,
            cost: cellList.elementAt(4) as String,
            name: cellList.elementAt(1) as String,
            qty: cellList.elementAt(3) as String,
            supplier: cellList.elementAt(5) as String,
          ));
        }
      }
    }
    return expenseList;
  }

  static Future<List> fetchExpenseById(String id) async {
    var response = await http.post(Uri.parse((await getBaseUrl()) + '/func/expenses.php'), body: {'cmd': 'expense_data_get', 'id': id}, headers: getHeader());
    List returningList = List.empty(growable: true);
    if (response.body.isEmpty) {
      returningList.add(0);
      returningList.add('Something went wrong. Please try again later.');
    } else {
      ResponseModel responseModel = new ResponseModel(response.body);

      Map<String, dynamic> parsedData = responseModel.getDataAsMap();

      returningList.add(1);
      returningList.add({
        'desc': parsedData['desc'] as String,
        'engineHrs': int.parse(parsedData['engine_hours'] as String),
        'imei': parsedData['imei'] as String,
        'odometer': int.parse(parsedData['odometer'] as String),
        'expense': Expense(
          buyer: parsedData['buyer'] as String,
          name: parsedData['name'] as String,
          qty: parsedData['quantity'] as String,
          supplier: parsedData['supplier'] as String,
          cost: parsedData['cost'] as String,
          date: parsedData['dt_expense'] as String,
        )
      });
    }
    return returningList;
  }

  static Future<List> deleteExpenseById(String id) async {
    var response = await http.post(Uri.parse((await getBaseUrl()) + '/func/expenses.php'), body: {'cmd': 'expense_delete', 'id': id}, headers: getHeader());
    List returningList = List.empty(growable: true);

    ResponseModel responseModel = new ResponseModel(response.body);

    if (responseModel.isSuccess()) {
      returningList.add(1);
    } else {
      returningList.add(0);
    }
    return returningList;
  }

  static Future<List> saveExpense(String expenseid, String name, String imei, String date, String qty, String cost, String supplier, String buyer, String odometer, String engineHrs, String desc) async {
    var response = await http.post(Uri.parse((await getBaseUrl()) + '/func/expenses.php'),
        body: {
          'cmd': 'expense_data_set',
          'id': expenseid,
          'name': name,
          'imei': imei,
          'date': date,
          'quantity': qty,
          'cost': cost,
          'supplier': supplier,
          'buyer': buyer,
          'odometer': odometer,
          'engine_hours': engineHrs,
          'desc': desc,
        },
        headers: getHeader());
    List returningList = List.empty(growable: true);

    ResponseModel responseModel = new ResponseModel(response.body);

    if (responseModel.isSuccess()) {
      returningList.add(1);
    } else {
      returningList.add(0);
    }
    return returningList;
  }

  static Future<List<Maintenance>> fetchMaintenances() async {
    List<Maintenance> maintenanceList = [];

    try {
      http.Response response = await http.get(
        Uri.parse((await getBaseUrl()) + '/func/maintenance.php?cmd=maintenance_list_get&_search=false&rows=500&page=1&sidx=gs_objects.name&sord=asc'),
        headers: getHeader(),
      );

      if (response.body.isNotEmpty) {
        Map<String, dynamic> parsedData = json.decode(response.body) as Map<String, dynamic>;

        if (!parsedData.containsKey('rows')) {
          return maintenanceList;
        }

        List<dynamic> rows = parsedData['rows'] as List<dynamic>;
        for (int i = 0; i < rows.length; i++) {
          Map<String, dynamic> row = rows.elementAt(i) as Map<String, dynamic>;
          List<dynamic> cell = row['cell'] as List<dynamic>;
          String objectName = cell.elementAt(0);
          String name = cell.elementAt(1) as String;
          String odometer = cell.elementAt(2) as String;
          String odometerLeft = cell.elementAt(3) as String;
          String engineHours = cell.elementAt(4) as String;
          String enginHoursLeft = cell.elementAt(5) as String;
          String imei =
              (cell.elementAt(9) as String).split('maintenanceObjectServiceProperties')[1].split(';" title="Edit"><img src="theme/images/edit.svg" />')[0].replaceAll("'", '').split(',')[0].replaceAll('(', '');
          String days = cell.elementAt(6).runtimeType == String ? (cell.elementAt(6) as String) : (cell.elementAt(6) as int).toString();
          String daysLeft = cell.elementAt(7).runtimeType == String ? (cell.elementAt(7) as String) : (cell.elementAt(7) as int).toString();
          String ticker = cell.elementAt(8) as String;
          String id = row['id'] as String;

          days = days == '-' ? days : days + ' Days';
          daysLeft = daysLeft == '-' ? daysLeft : daysLeft + ' Days Left';

          Maintenance maintenance = Maintenance(days, daysLeft, engineHours, enginHoursLeft, id, name, odometer, odometerLeft, objectName, !ticker.toLowerCase().contains('green'), imei);
          maintenanceList.add(maintenance);
        }
      }
    } on Exception catch (_) {}
    return maintenanceList;
  }

  static Future<dynamic> saveMaintenance(
      String name,
      String data_list,
      String popup,
      String odo,
      String odo_interval,
      String odo_last,
      String engh,
      String engh_interval,
      String engh_last,
      String days,
      String days_interval,
      String days_last,
      String odo_left,
      String odo_left_num,
      String engh_left,
      String engh_left_num,
      String days_left,
      String days_left_num,
      String update_last,
      String imei,
      String service_id) async {
    try {
      var response = null;

      if (service_id != 'false') {
        response = await http.post(Uri.parse((await getBaseUrl()) + '/func/settings.service.php'),
            body: {
              'cmd': 'service_data_set',
              'id': service_id,
              'name': name,
              'imei': imei,
              'data_list': data_list,
              'popup': popup,
              'odo': odo,
              'odo_interval': odo_interval,
              'odo_last': odo_last,
              'engh': engh,
              'engh_interval': engh_interval,
              'engh_last': engh_last,
              'days': days,
              'days_interval': days_interval,
              'days_last': days_last,
              'odo_left': odo_left,
              'odo_left_num': odo_left_num,
              'engh_left': engh_left,
              'engh_left_num': engh_left_num,
              'days_left': days_left,
              'days_left_num': days_left_num,
              'update_last': update_last,
            },
            headers: getHeader());
      } else {
        response = await http.post(Uri.parse((await getBaseUrl()) + '/func//maintenance.php'),
            body: {
              'cmd': 'service_data_set',
              'name': name,
              'imei': imei,
              'data_list': data_list,
              'popup': popup,
              'odo': odo,
              'odo_interval': odo_interval,
              'odo_last': odo_last,
              'engh': engh,
              'engh_interval': engh_interval,
              'engh_last': engh_last,
              'days': days,
              'days_interval': days_interval,
              'days_last': days_last,
              'odo_left': odo_left,
              'odo_left_num': odo_left_num,
              'engh_left': engh_left,
              'engh_left_num': engh_left_num,
              'days_left': days_left,
              'days_left_num': days_left_num,
              'update_last': update_last,
            },
            headers: getHeader());
      }

      ResponseModel responseModel = new ResponseModel(response.body);

      if (responseModel.isSuccess()) {
        return true;
      }
    } on SocketException catch (e) {
      return e.message;
    }
    return 'Something went wrong';
  }

  static Future<List> deleteMaintenanceId(String id) async {
    var response = await http.post(Uri.parse((await getBaseUrl()) + '/func/maintenance.php'), body: {'cmd': 'service_delete', 'id': id}, headers: getHeader());
    List returningList = [];

    ResponseModel responseModel = new ResponseModel(response.body);

    if (responseModel.isSuccess()) {
      returningList.add(1);
    } else {
      returningList.add(0);
    }
    return returningList;
  }

  static Future<List> generateLink(String imei, String expiryDate, String id) async {
    var response = await http.post(Uri.parse((await getBaseUrl()) + '/func/share.php'),
        body: {
          'cmd': 'share_position_data_set',
          'id': 'false',
          'active': 'true',
          'name': 'test',
          'email': 'test@test.com',
          'phone': '00',
          'imei': imei,
          'expire': 'true',
          'expire_dt': expiryDate,
          'send_email': 'false',
          'send_sms': 'false',
          'su': id
        },
        headers: getHeader());
    List returningList = [];

    ResponseModel responseModel = new ResponseModel(response.body);

    if (responseModel.isSuccess()) {
      returningList.add(1);
    } else {
      returningList.add(0);
      returningList.add(responseModel.getErrorString());
    }
    return returningList;
  }

  static Future<Map<String, Map<String, dynamic>>> fetchRoutes() async {
    try {
      var response = await http.post(
          Uri.parse(
            (await getBaseUrl()) + '/func/places.php',
          ),
          body: {'cmd': 'route_data_get_all'},
          headers: getHeader());
      if (response.body.isEmpty) {
        throw 'Something went wrong!!!';
      } else {
        ResponseModel responseModel = new ResponseModel(response.body);

        if (responseModel.getDataString() != '[]') {
          Map<String, dynamic> parsedData = responseModel.getDataAsMap();
          Map<String, Map<String, dynamic>> nameList = Map<String, Map<String, dynamic>>();
          parsedData.forEach((key, value) {
            Map<String, dynamic> map = (value as Map<String, dynamic>)['data'] as Map<String, dynamic>;
            nameList.putIfAbsent(key, () => map);
          });
          return nameList;
        } else {
          throw 'Not found any Route';
        }
      }
    } on SocketException catch (_) {
      throw 'Internet Error Occurred';
    } on Exception catch (_) {
      throw 'Something went wrong!!!';
    }
  }

  static Future<bool> deleteEvent(String eventId) async {
    try {
      Map<String, String> body = {'cmd': 'event_delete', 'id': eventId};

      var response = await http.post(
          Uri.parse(
            (await getBaseUrl()) + '/func/settings.events.php',
          ),
          body: body,
          headers: getHeader());
      if (response.body.isEmpty) {
        throw 'Something went wrong!!!';
      } else {
        ResponseModel responseModel = new ResponseModel(response.body);

        return responseModel.isSuccess();
      }
    } on SocketException catch (_) {
      throw 'Internet Error Occurred';
    } on Exception catch (_) {
      throw 'Something went wrong!!!';
    }
  }

  static Future<List> fetchEventStatus() async {
    List returningList = new List.empty(growable: true);
    try {
      var response = await http.post(Uri.parse((await getBaseUrl()) + '/func/settings.events.php'),
          body: {
            'cmd': 'event_data_get_all',
          },
          headers: getHeader());
      if (response.body.isEmpty) {
        returningList.add(false);
        returningList.add('Something went wrong!!!');
      } else {
        ResponseModel responseModel = new ResponseModel(response.body);

        Map<String, dynamic> parsedData = responseModel.getDataAsMap();
        List<AlertSetting> alertList = [];
        parsedData.forEach((key, value) {
          alertList.add(AlertSetting.fromGPSServerData(value, key));
        });
        returningList.add(true);
        returningList.add(alertList);
      }
    } on SocketException catch (_) {
      returningList.add(false);
      returningList.add('Internet Error Occurred');
    } on Exception catch (_) {
      returningList.add(false);
      returningList.add('Something went wrong!!!');
    }
    return returningList;
  }

  static Future<Map> getClientData() async {
    Map<String, dynamic> mapData = {};

    try {
      String username = Repository.getUsername();
      String url =
          CRMConstants.CRM_BASE_URL + 'api/data?username=${CRMConstants.ERP_USERNAME}&secret_key=${CRMConstants.ERP_SECRET_KEY}&entity_name=client&query_key=platform_username&query_key_value=' + username;

      http.Response response = await http.get(
        Uri.parse(url),
      );

      Map<String, dynamic> parsedData = json.decode(response.body) as Map<String, dynamic>;

      if (parsedData['status_code'] as String == '200') {
        List<dynamic> data = parsedData['data'];

        if (data.length == 0) {
          mapData.putIfAbsent(STATUS, () => 0);
          mapData.putIfAbsent(MESSAGE, () => 'Data Not Available');
        } else {
          mapData.putIfAbsent(STATUS, () => 1);
          int id = data[0]['id'] as int;
          Map<String, dynamic> clientData = await getClientInfoById(id);
          mapData.putIfAbsent(DATA, () => clientData);

          List<dynamic> rewardPointsHistoryList = await getRewardPointsHistoryByClientId(id);
          mapData.putIfAbsent(REWARD_POINTS_HISTORY, () => rewardPointsHistoryList);
        }
      } else {
        mapData.putIfAbsent(STATUS, () => 0);
        mapData.putIfAbsent(MESSAGE, () => parsedData['message'] as String);
      }
    } on Exception catch (_) {
      mapData.putIfAbsent(STATUS, () => 0);
      mapData.putIfAbsent(MESSAGE, () => 'Something went wrong!!!');
    }
    return mapData;
  }

  static Future<Map<String, dynamic>> getClientInfoById(int clientId) async {
    try {
      String url = '${CRMConstants.CRM_BASE_URL}api/data/?username=${CRMConstants.ERP_USERNAME}&secret_key=${CRMConstants.ERP_SECRET_KEY}&entity_name=client&query_key=id&query_key_value=$clientId';
      http.Response response = await http.get(Uri.parse(url));
      Map<String, dynamic> parsedData = json.decode(response.body) as Map<String, dynamic>;
      if (parsedData['status_code'] as String == '200') {
        Map<String, dynamic> dataList = (parsedData['data'] as Map<String, dynamic>);
        return dataList;
      } else {
        throw 'Something went wrong!!!';
      }
    } on Exception catch (_) {
      throw 'Internet not working';
    }
  }

  static Future<List> getRewardPointsHistoryByClientId(int clientId) async {
    List rewardPointsHistory = [];

    try {
      String url = CRMConstants.CRM_BASE_URL +
          'api/data/?username=${CRMConstants.ERP_USERNAME}&secret_key=${CRMConstants.ERP_SECRET_KEY}&entity_name=RewardPoint&query_key=client_id&query_key_value=' +
          clientId.toString();

      http.Response response = await http.get(Uri.parse(url));
      Map<String, dynamic> parsedData = json.decode(response.body) as Map<String, dynamic>;

      if (parsedData['status_code'] as String == '200') {
        List<dynamic> dataList = (parsedData['data'] as List<dynamic>);

        for (dynamic i in dataList) {
          Map<String, dynamic> dataMap = i as Map<String, dynamic>;
          String date = dataMap['created_at'] as String;
          DateTime t = DateFormat('yyyy-MM-ddTHH:mm:ss.Z').parse(date);
          date = DateFormat('dd MMMM, yyyy').format(t);

          RewardPoint rewardPoint = new RewardPoint(date, dataMap['source']['title'], dataMap['points_earned'].toString());

          rewardPointsHistory.add(rewardPoint);
        }
      }
    } on Exception catch (e) {
      writeErrorLogs(e.toString());
    }

    return rewardPointsHistory;
  }

  static Future<List> sendRedeemRequest(int points, int clientId) async {
    List returningList = [];
    try {
      String url = '${CRMConstants.CRM_BASE_URL}api/request-point-redeem?client_id=$clientId&redeem_amount=$points&redeem_to=payment';

      var response = await http.post(Uri.parse(url));
      var parsedData = json.decode(response.body) as Map<String, dynamic>;
      if (parsedData['status_code'] as String == '200') {
        returningList.add(1);
        returningList.add(parsedData['message'] as String);
      }
    } on SocketException catch (e) {
      writeErrorLogs(e.toString());
      returningList.add(0);
      returningList.add('Internet not working');
    }
    return returningList;
  }

  static Future<String> sendPasswordResetRequest(String credentials) async {
    try {
      String url = '${CRMConstants.CRM_BASE_URL}api/create?query_value=${credentials}&title=Password Reset Request&description=Password Reset Request&ticket_complain_source_id=15&priority=1&call_type=2';

      var response = await http.post(
        Uri.parse(url),
        headers: {'auth': 'username=${CRMConstants.CRM_USERNAME}&secret_key=${CRMConstants.CRM_SECRET_KEY}&entity_name=ticket-reset-pass'},
      );

      var parsedData = json.decode(response.body) as Map<String, dynamic>;
      if (parsedData['status_code'] as String == '200') {
        return 'You will shortly get a sms with your new password.';
      }
    } on Exception catch (e) {
      writeErrorLogs(e.toString());
      throw 'Internet not working';
    }
    throw 'We didn’t find anything in our system, please try again with registered info.';
  }

  static Future<List> fetchDashboardMaintenance() async {
    List parsedData = [];
    try {
      http.Response response = await http.post(Uri.parse((await getBaseUrl()) + '/func/dashboard.php'), body: {'cmd': 'maintenance_data_get'}, headers: getHeader());
      if (response.body.isNotEmpty) {
        ResponseModel responseModel = new ResponseModel(response.body);
        parsedData = responseModel.getDataAsList();
      }
    } on Exception catch (e) {
      writeErrorLogs(e.toString());
    }
    return parsedData;
  }

  static Future<List> getOdometerDashboardGraph() async {
    List odometerDataList = [];

    try {
      http.Response response = await http.post(
        Uri.parse((await getBaseUrl()) + '/func/dashboard.php'),
        body: {'cmd': 'odometer_data_get', 'top_low': 'top10'},
        headers: getHeader(),
      );
      if (response.body.isNotEmpty) {
        ResponseModel responseModel = new ResponseModel(response.body);
        odometerDataList = responseModel.getDataAsList();
      }
    } on Exception catch (e) {
      writeErrorLogs(e.toString());
    }
    return odometerDataList;
  }

  static Future<Map> getEventsDashboardGraph() async {
    Map<String, dynamic> eventsDataList = {};

    try {
      final now = new DateTime.now();
      DateFormat dateFormat = DateFormat('yyyy-MM-dd');

      String fromTime = '00:00:00';
      String toTime = '23:59:59';
      String dtf = dateFormat.format(now) + ' $fromTime';
      String dtt = dateFormat.format(now) + ' $toTime';

      http.Response response = await http.post(
        Uri.parse((await getBaseUrl()) + '/func/dashboard.php'),
        body: {'cmd': 'events_data_get', 'period': 'today', 'dtf': dtf, 'dtt': dtt},
        headers: getHeader(),
      );

      if (response.body.isNotEmpty) {
        ResponseModel responseModel = new ResponseModel(response.body);
        if (responseModel.getDataString() != '[]') {
          eventsDataList = responseModel.getDataAsMap();
        }
      }
    } on Exception catch (_) {}
    return eventsDataList;
  }

  static Future<Map> getMileageDashboardGraph() async {
    try {
      http.Response response = await http.post(Uri.parse((await getBaseUrl()) + '/func/dashboard.php'), body: {'cmd': 'mileage_data_get'}, headers: getHeader());
      if (response.body.isEmpty) {
        throw 'Something went wrong!!!';
      }

      ResponseModel responseModel = new ResponseModel(response.body);

      Map mileageDataList = responseModel.getDataAsMap();

      return mileageDataList;
    } on SocketException catch (_) {
      throw 'Internet Error Occurred';
    } on Exception catch (_) {
      throw 'Something went wrong!!!';
    }
  }

  static Future<List<GenInfoData>> getWidgetStatisticsData(String imei, String selectedDevice) async {
    List<GenInfoData> listGenInfoData = [];

    try {
      Uri uri = Uri.parse((await getBaseUrl()) + '/func/objects.php');

      http.Response response = await http.post(uri, body: {'cmd': 'widget_statistics_data_get', 'imei': imei}, headers: getHeader());

      ResponseModel responseModel = new ResponseModel(response.body);

      List<dynamic> dataJsonArray = responseModel.getDataAsList();

      if (dataJsonArray.length == 0) {
        dataJsonArray = [
          {'name': 'Route length', 'value': '0 km'},
          {'name': 'Move duration', 'value': '0 s'},
          {'name': 'Stop duration', 'value': '0 s'},
          {'name': 'Top speed', 'value': '0 kph'},
          {'name': 'Average speed', 'value': '0 kph'},
          {'name': 'Fuel consumption', 'value': '0 liters'},
          {'name': 'Avg. fuel cons. (100 km)', 'value': '0 liters'},
          {'name': 'Fuel cost', 'value': '0 BDT'},
          {'name': 'Engine work', 'value': '0 s'},
          {'name': 'Engine idle', 'value': '0 s'}
        ];
      }

      dataJsonArray.forEach((element) {
        String title = element['name'];
        String value = element['value'];

        listGenInfoData.add(new GenInfoData(title, value, getImageName(title)));
      });

      HomeWidget.saveWidgetData('device_name', selectedDevice);

      HomeWidget.saveWidgetData('imei', jsonEncode(dataJsonArray));

      HomeWidget.updateWidget(name: 'AutonemoWidget', androidName: 'AutonemoWidget', iOSName: 'AutonemoWidget');
    } on Exception catch (e) {
      writeErrorLogs(e.toString());
    }

    return listGenInfoData;
  }

  static Future<List<dynamic>> getFuelWidgetStatisticsData(String imei) async {
    List<dynamic> data = [];

    try {
      Uri uri = Uri.parse((await getBaseUrl()) + '/func/objects.php');
      http.Response response = await http.post(uri, body: {'cmd': 'widget_fuel_data_get', 'imei': imei}, headers: getHeader());
      ResponseModel responseModel = new ResponseModel(response.body);
      Map<String, dynamic> dataJsonObject = responseModel.getDataAsMap();
      data = dataJsonObject['data'] as List<dynamic>;
    } on Exception catch (e) {
      writeErrorLogs(e.toString());
    }

    return data;
  }

  static int getSeconds(String value) {
    List<String> strArray = value.split(' ');

    int totalSeconds = 0;

    if (strArray.length == 2) {
      totalSeconds = int.parse(strArray[0]);
    }

    if (strArray.length == 4) {
      totalSeconds = totalSeconds + int.parse(strArray[0]) * 60;
      totalSeconds = totalSeconds + int.parse(strArray[2]);
    }

    if (strArray.length == 6) {
      totalSeconds = totalSeconds + int.parse(strArray[0]) * 60 * 60;
      totalSeconds = totalSeconds + int.parse(strArray[2]) * 60;
      totalSeconds = totalSeconds + int.parse(strArray[4]);
    }

    return totalSeconds;
  }

  static String formattedTime(int time) {
    int sec = time % 60;
    int min = (time / 60).floor();
    int hr = (time / 3600).floor();
    String hour = hr.toString().length <= 1 ? '0$hr' : '$hr';
    String minute = min.toString().length <= 1 ? '0$min' : '$min';
    String second = sec.toString().length <= 1 ? '0$sec' : '$sec';
    return '$hour h $minute min $second s';
  }
}
