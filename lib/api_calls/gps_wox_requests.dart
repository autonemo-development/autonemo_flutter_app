import 'dart:convert';
import 'dart:io';

import 'package:autonemogps/config/string_constants.dart';
import 'package:autonemogps/models/device.dart';
import 'package:autonemogps/models/device_model.dart';
import 'package:autonemogps/models/geofence_model.dart';
import 'package:autonemogps/models/history_data.dart';
import 'package:autonemogps/models/parking.dart';
import 'package:autonemogps/models/playback_route.dart';
import 'package:autonemogps/models/trip.dart';
import 'package:autonemogps/storage/repository.dart';
import 'package:autonemogps/utils/enums.dart';
import 'package:autonemogps/utils/logs.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:http/http.dart' as http;

class GPSWoxRequests {
  static String? baseUrl, baseUri;
  static bool? isServerHttps;

  static Future<String> getBaseUri() async {
    if (baseUri == null) {
      baseUri = (await Repository.getGPSWoxUrl()).replaceFirst('http://', '').replaceFirst('https://', '');
    }
    return baseUri!;
  }

  static Future<String> getBaseUrl() async {
    baseUrl = await Repository.getGPSWoxUrl();
    return baseUrl!;
  }

  static Future<bool> isHttps() async {
    if (isServerHttps == null) {
      isServerHttps = (await getBaseUrl()).contains('https://');
    }
    return isServerHttps!;
  }

  static void resetUrl() {
    baseUrl = null;
    baseUri = null;
    isServerHttps = null;
  }

  static Map<String, String> queryMap({String? cookie}) {
    return {
      'lang': 'en',
      'user_api_hash': cookie ?? Repository.getCookie(),
      // 'user_api_hash': '\$2y\$10\$yhSQFbu1z6/9eCaV0VQgouVt41b22LyvbkjePbfqLGKYuSHnHZmiu',
    };
  }

  static Future<Map<String, dynamic>> login(String username, String pswd) async {
    try {
      var request = http.MultipartRequest('POST', Uri.parse((await getBaseUrl()) + '/api/login'));

      Map<String, String> data = {
        'email': username,
        'password': pswd,
      };
      request.fields.addAll(data);

      http.StreamedResponse response = await request.send();
      if (response.statusCode == 200) {
        Map<String, dynamic> responseMap = json.decode(await response.stream.bytesToString());
        if (responseMap['status'] == 1) {
          return {'isLoginSuccess': true, 'cookie': responseMap['user_api_hash']};
        } else {
          resetUrl();
          return {'isLoginSuccess': false, 'message': responseMap['message']};
        }
      } else {
        resetUrl();
        return {'isLoginSuccess': false, 'message': response.reasonPhrase};
      }
    } on SocketException catch (e) {
      resetUrl();
      return {'isLoginSuccess': false, 'message': e.message};
    } on Exception catch (e) {
      resetUrl();
      return {'isLoginSuccess': false, 'message': e.toString()};
    }
  }

  static Future<bool> getDevices() async {
    Uri uri = (await isHttps()) ? Uri.https((await getBaseUri()), '/api/get_devices', queryMap()) : Uri.http((await getBaseUri()), '/api/get_devices', queryMap());
    try {
      var response = await http.get(uri);
      if (response.statusCode == 200) {
        Repository.setAllDevicesData(response.body);
        return true;
      }
    } catch (e) {
      writeErrorLogs(e.toString());
    }
    return false;
  }

  static Future<String?> getDevicesLatest() async {
    Uri uri = (await isHttps())
        ? Uri.https((await getBaseUri()), '/api/get_devices_latest', {'lang': 'en', 'user_api_hash': Repository.getCookie(), 'time': GPSWoxConstants.gpsWoxLastFetchTimeStamp})
        : Uri.http((await getBaseUri()), '/api/get_devices_latest', {'lang': 'en', 'user_api_hash': Repository.getCookie(), 'time': GPSWoxConstants.gpsWoxLastFetchTimeStamp});
    try {
      http.Response response = await http.get(uri);
      if (response.statusCode == 200) {
        Repository.setTrackingData(response.body);
        return response.body;
      }
    } catch (e) {
      writeErrorLogs(e.toString());
    }
    return null;
  }

  static Future<List> fetchEvents() async {
    var uri = (await isHttps()) ? Uri.https((await getBaseUri()), '/api/get_events', queryMap()) : Uri.http((await getBaseUri()), '/api/get_events', queryMap());

    List returningList = [];

    try {
      var response = await http.get(
        uri,
      );
      if (response.statusCode == 200) {
        Repository.setAlerts(response.body);
        Repository.setLastRefreshedEventTime();
        returningList.add(1);
        returningList.add(response.body);
      } else {
        returningList.add(0);
      }
    } on SocketException catch (e) {
      writeErrorLogs('Error ', e: e);
      returningList.add(0);
      returningList.add('No Internet Connection');
    }
    return returningList;
  }

  static Future<List> fetchHistory(String fromDate, String fromTime, String toDate, String toTime, int deviceId) async {
    Map<String, String> query = {
      'lang': 'en',
      'user_api_hash': Repository.getCookie(),
      'device_id': deviceId.toString(),
      'from_date': fromDate,
      'from_time': fromTime,
      'to_date': toDate,
      'to_time': toTime,
      'snap_to_road': 'false'
    };
    var uri = (await isHttps()) ? Uri.https((await getBaseUri()), '/api/get_history', query) : Uri.http((await getBaseUri()), '/api/get_history', query);

    var response = await http.get(uri);
    List returningList = [];
    Map<String, dynamic> parsedData = json.decode(response.body) as Map<String, dynamic>;
    if (response.statusCode == 200) {
      List itemsList = parsedData['items'] as List;
      List<Parking> parkingList = [];
      List<LatLng> routePoints = [];
      List<HistoryData> historyList = [];
      List<Trip> tripList = [];
      List<PlaybackRoute> markerRoute = [];
      double? x0, x1, y0, y1;
      itemsList.forEach((item) {
        switch (item['status']) {
          case 1:
            //route
            List<LatLng> tripPoints = [];
            List routeItems = item['items'] as List;
            routeItems.forEach((routeItem) {
              double latitude = routeItem['lat'];
              double longitude = routeItem['lng'];
              LatLng latLng = LatLng(latitude, longitude);
              routePoints.add(latLng);
              if (x0 == null) {
                x0 = x1 = latitude;
                y0 = y1 = longitude;
              } else {
                if (latitude > x1!) x1 = latitude;
                if (latitude < x0!) x0 = latitude;
                if (longitude > y1!) y1 = longitude;
                if (longitude < y0!) y0 = longitude;
              }
              tripPoints.add(latLng);
              markerRoute.add(PlaybackRoute(angle: routeItem['course'], latLng: latLng, speed: routeItem['speed'], time: routeItem['time']));
            });
            Trip trip = Trip(
                avgSpeed: '${item['average_speed'] as int} kph',
                topSpeed: '${item['top_speed'] as int} kph',
                distance: '${item['distance']} KM',
                startTime: item['show'] as String,
                runTime: item['time'] as String,
                stopLocation: '${tripPoints.first.latitude}, ${tripPoints.first.longitude}',
                polyline: Polyline(polylineId: PolylineId(item['show'] as String), points: tripPoints, visible: true, width: 5, color: Colors.blue, zIndex: 1));

            tripList.add(trip);
            break;
          case 2:
            //stops

            List stopItems = item['items'] as List;
            Map<String, dynamic> stopItem = stopItems[0] as Map<String, dynamic>;
            stopItems.forEach((routeItem) {
              double latitude = routeItem['lat'];
              double longitude = routeItem['lng'];
              LatLng latLng = LatLng(latitude, longitude);
              routePoints.add(latLng);
              if (x0 == null) {
                x0 = x1 = latitude;
                y0 = y1 = longitude;
              } else {
                if (latitude > x1!) x1 = latitude;
                if (latitude < x0!) x0 = latitude;
                if (longitude > y1!) y1 = longitude;
                if (longitude < y0!) y0 = longitude;
              }
              markerRoute.add(PlaybackRoute(angle: routeItem['course'], latLng: latLng, speed: routeItem['speed'], time: routeItem['time']));
            });
            if (tripList.length > 0) {
              Trip trip = tripList.last;
              trip.endTime = item['show'] as String;
              trip.leftAt = item['left'] as String;
              trip.stopTime = item['time'] as String;
              trip.stopLocation = '${stopItem['lat']}, ${stopItem['lng']}';
            } else {
              tripList.add(Trip(endTime: item['show'] as String, leftAt: item['left'] as String, stopTime: item['time'] as String, stopLocation: '${stopItem['lat']}, ${stopItem['lng']}'));
            }
            parkingList.add(Parking(
              left: item['left'] as String,
              altitude: stopItem['altitude'].toString(),
              angle: stopItem['course'],
              came: item['show'],
              duration: item['time'],
              position: '${stopItem['lat']}, ${stopItem['lng']}',
              latLng: LatLng(stopItem['lat'], stopItem['lng']),
            ));
            historyList.add(HistoryData(data: '${item['show']} ${item['time']}', img: 'images/map-point.png'));
            break;
          case 3:
            //start point
            historyList.add(HistoryData(data: item['show'] as String, img: 'images/map-start-point.png'));
            break;
          case 4:
            //end point
            historyList.add(HistoryData(data: '${item['show']}', img: 'images/route-end.png'));
            break;
        }
      });
      Set<Polyline> polylines = {
        Polyline(
          polylineId: PolylineId('polyline_one'),
          visible: true,
          points: routePoints,
          width: 5,
          color: Color(0xFFF26611),
        )
      };
      returningList.add(1);
      returningList.add({
        'topSpeed': parsedData['top_speed'] as String,
        'stopDuration': parsedData['stop_duration'] as String,
        'moveDuration': parsedData['move_duration'] as String,
        'routeLength': parsedData['distance_sum'],
        'parkingList': parkingList,
        'historyList': historyList,
        'polylines': polylines,
        'routePoints': markerRoute,
        'bounds': LatLngBounds(northeast: LatLng(x1 ?? 0, y1 ?? 0), southwest: LatLng(x0 ?? 0, y0 ?? 0)),
        'tripList': tripList
      });
    } else {
      returningList.add(0);
      returningList.add(parsedData['message']);
    }
    return returningList;
  }

  static Future<List> fetchReportTypes() async {
    var uri = (await isHttps()) ? Uri.https((await getBaseUri()), '/api/get_reports_types', queryMap()) : Uri.http((await getBaseUri()), '/api/get_reports_types', queryMap());

    var response = await http.get(uri);
    List returningList = [];
    if (response.statusCode == 200) {
      returningList.add(1);
      returningList.add(response.body);
    } else {
      returningList.add(0);
      returningList.add('Something went wrong!!!');
    }
    return returningList;
  }

  static Future<String> generateReport(String title, int type, List<int> deviceIds, String fromDate, String toDate, String fromTime, String toTime, {Map<String, String>? headerMap}) async {
    try {
      var uri = (isServerHttps!) ? Uri.https((baseUri!), '/api/generate_report', headerMap ?? queryMap()) : Uri.http((baseUri!), '/api/generate_report', headerMap ?? queryMap());
      Map<String, String> body = {'title': title, 'type': type.toString(), 'format': 'html', 'date_from': fromDate, 'date_to': toDate, 'time_from': fromTime, 'time_to': toTime};
      for (int i = 0; i < deviceIds.length; i++) {
        body['devices[$i]'] = deviceIds[i].toString();
      }
      var request = http.MultipartRequest(
        'POST',
        uri,
      );
      request.fields.addAll(body);
      var streamedResponse = await request.send();
      var response = await http.Response.fromStream(streamedResponse);
      if (response.statusCode == 200) {
        Map<String, dynamic> parsedResponse = json.decode(response.body);
        if (parsedResponse['status'] == 3) {
          String reportUrl = parsedResponse['url'];
          var response1 = await http.get(
            Uri.parse(reportUrl),
          );
          return response1.body;
        }
      }
      throw 'Something went wrong!!!';
    } on Exception catch (e) {
      writeErrorLogs('Error ', e: e);
      throw 'Got Error';
    }
  }

  static Future<List> fetchGeoPlaces() async {
    List returningList = [];

    try {
      var uri = (await isHttps()) ? Uri.https((await getBaseUri()), '/api/get_geofences', queryMap()) : Uri.http((await getBaseUri()), '/api/get_geofences', queryMap());
      var response = await http.get(uri);
      if (response.statusCode == 200) {
        var parsedData = json.decode(response.body) as Map<String, dynamic>;
        returningList.add(true);
        returningList.add(parsedData['items']['geofences']);
      } else {
        returningList.add(false);
        returningList.add('Something went wrong. Please try again later.');
      }
    } on SocketException catch (_) {
      returningList.add(false);
      returningList.add('Internet Error Occurred');
    } on Exception catch (_) {
      returningList.add(false);
      returningList.add('Something went wrong. Please try again later.');
    }
    return returningList;
  }

  static Future<List> addGeoPlace(String name, String color, List<Map<String, double>> polygon, String type) async {
    List returningList = [];
    var uri = (await isHttps()) ? Uri.https((await getBaseUri()), '/api/add_geofence', queryMap()) : Uri.http((await getBaseUri()), '/api/add_geofence', queryMap());
    try {
      Map<String, String> body = {'name': name, 'type': type, 'polygon_color': color, 'polygon': json.encode(polygon)};
      var request = http.MultipartRequest(
        'POST',
        uri,
      );
      request.fields.addAll(body);
      var streamedResponse = await request.send();
      var response = await http.Response.fromStream(streamedResponse);
      if (response.statusCode == 200) {
        List geofenceList = await fetchGeoPlaces();
        if (geofenceList[0]) {
          Geofence addedGeofence = Geofence.fromGPSWoxJson((geofenceList.elementAt(1) as List<dynamic>).last);
          try {
            String alertResponse = await fetchAlerts();
            Map<String, dynamic> alertParsedResponse = json.decode(alertResponse) as Map<String, dynamic>;
            bool isAlertAvailable = false;
            (alertParsedResponse['items']['alerts'] as List).forEach((element) async {
              String alertType = element['type'] as String;
              if (alertType == 'geofence_inout') {
                isAlertAvailable = true;
                try {
                  List geofencesList = element['geofences'] as List;
                  geofencesList.add(int.parse(addedGeofence.id!));

                  Map<String, String> body = {
                    'id': element['id'].toString(),
                    'name': element['name'],
                    'type': element['type'],
                  };
                  List devicesList = element['devices'] as List;
                  if (devicesList.length > 0) {
                    for (int i = 0; i < devicesList.length; i++) {
                      body['devices[$i]'] = devicesList[i].toString();
                    }
                    for (int i = 0; i < geofencesList.length; i++) {
                      body['geofences[$i]'] = geofencesList[i].toString();
                    }
                    await editAlert(body);
                  }
                } catch (e) {
                  writeErrorLogs('Error ', e: e);
                }
              }
            });
            if (!isAlertAvailable) {
              Map<String, String> body = {
                'name': 'Geofence Zone In and Out',
                'type': 'geofence_inout',
                'notifications': json.encode({
                  'sound': {'active': true},
                  'push': {'active': true},
                  'webhook': {'active': true, 'input': 'https://geocode.autonemogps.com/gpswoxwebhook/receiver.php'}
                }),
                'zone': '0',
                'schedule': 'false',
                'geofences[0]': addedGeofence.id!,
              };
              List<DeviceModel> deviceIds = AllDeviceModelData().deviceListByDeviceId.values.toList();
              for (int i = 0; i < deviceIds.length; i++) {
                body['devices[$i]'] = deviceIds[i].deviceId.toString();
              }
              await addAlert(body);
            }
          } catch (e) {
            writeErrorLogs('Error ', e: e);
          }
        }
        returningList.add(true);
        returningList.add('OK');
      } else {
        returningList.add(false);
        returningList.add('Something went wrong. Please try again later.');
      }
    } on SocketException catch (_) {
      returningList.add(false);
      returningList.add('Internet Error Occurred');
    } on Exception catch (_) {
      returningList.add(false);
      returningList.add('Something went wrong. Please try again later.');
    }
    return returningList;
  }

  static Future<bool> addAlert(Map<String, String> body) async {
    var uri = (await isHttps()) ? Uri.https((await getBaseUri()), '/api/add_alert', queryMap()) : Uri.http((await getBaseUri()), '/api/add_alert', queryMap());
    try {
      var request = http.MultipartRequest(
        'POST',
        uri,
      );
      request.fields.addAll(body);
      var streamedResponse = await request.send();
      var response = await http.Response.fromStream(streamedResponse);
      if (response.statusCode == 200) {
        return true;
      } else {
        throw 'Something went wrong!!!';
      }
    } on SocketException catch (_) {
      throw 'Internet Error Occurred';
    } on Exception catch (_) {
      throw 'Something went wrong. Please try again later.';
    }
  }

  static Future<bool> editAlert(Map<String, String> body) async {
    var uri = (await isHttps()) ? Uri.https((await getBaseUri()), '/api/edit_alert', queryMap()) : Uri.http((await getBaseUri()), '/api/edit_alert', queryMap());
    try {
      var request = http.MultipartRequest(
        'POST',
        uri,
      );
      request.fields.addAll(body);
      var streamedResponse = await request.send();
      var response = await http.Response.fromStream(streamedResponse);
      if (response.statusCode == 200) {
        return true;
      } else {
        throw 'Something went wrong!!!';
      }
    } on SocketException catch (_) {
      throw 'Internet Error Occurred';
    } on Exception catch (_) {
      throw 'Something went wrong. Please try again later.';
    }
  }

  static Future<String> fetchAlerts() async {
    try {
      var uri = (await isHttps()) ? Uri.https((await getBaseUri()), '/api/get_alerts', queryMap()) : Uri.http((await getBaseUri()), '/api/get_alerts', queryMap());
      var response = await http.get(uri);
      if (response.statusCode == 200) {
        return response.body;
      } else {
        throw 'Something went wrong. Please try again later.';
      }
    } on SocketException catch (_) {
      throw 'Internet Error Occurred';
    } on Exception catch (_) {
      throw 'Something went wrong. Please try again later.';
    }
  }

  static Future<bool> deleteGeofence(int geofenceId) async {
    try {
      var uri = (await isHttps())
          ? Uri.https((await getBaseUri()), '/api/get_alerts', {...queryMap(), 'geofence_id': geofenceId})
          : Uri.http((await getBaseUri()), '/api/get_alerts', {...queryMap(), 'geofence_id': geofenceId});
      var response = await http.get(uri);
      if (response.statusCode == 200) {
        return true;
      } else {
        throw 'Something went wrong. Please try again later.';
      }
    } on SocketException catch (_) {
      throw 'Internet Error Occurred';
    } on Exception catch (_) {
      throw 'Something went wrong. Please try again later.';
    }
  }

  static Future<List> deleteZone(String geofenceId) async {
    List returningList = [];

    try {
      var uri = (await isHttps())
          ? Uri.https((await getBaseUri()), '/api/destroy_geofence', {...queryMap(), 'geofence_id': geofenceId})
          : Uri.http((await getBaseUri()), '/api/destroy_geofence', {...queryMap(), 'geofence_id': geofenceId});
      var response = await http.get(uri);
      if (response.statusCode != 200) {
        returningList.add(false);
        returningList.add('Something went wrong. Please try again later.');
      } else {
        returningList.add(true);
        returningList.add(response.body);
      }
    } on SocketException catch (_) {
      returningList.add(false);
      returningList.add('Internet Error Occurred');
    } on Exception catch (_) {
      returningList.add(false);
      returningList.add('Something went wrong. Please try again later.');
    }
    return returningList;
  }

  static Future<dynamic> saveMaintenance(
      int deviceId, String name, String expirationBy, String interval, String lastService, String triggerEventLeft, int renewAfterExpiration, String email, String mobilePhone) async {
    var uri = (await isHttps())
        ? Uri.https((await getBaseUri()), '/api/add_service', {...queryMap(), 'device_id': deviceId.toString()})
        : Uri.http((await getBaseUri()), '/api/add_service', {...queryMap(), 'device_id': deviceId.toString()});
    try {
      var request = http.MultipartRequest(
        'POST',
        uri,
      );
      Map<String, String> body = {
        'device_id': deviceId.toString(),
        'name': name,
        'expiration_by': expirationBy,
        'interval': interval,
        'last_service': lastService,
        'trigger_event_left': triggerEventLeft,
        'renew_after_expiration': renewAfterExpiration.toString(),
        'email': email,
        'mobile_phone': mobilePhone
      };
      request.fields.addAll(body);
      var streamedResponse = await request.send();
      var response = await http.Response.fromStream(streamedResponse);
      if (response.statusCode == 200) {
        return true;
      } else {
        Map<String, dynamic> parsedResponse = json.decode(response.body);
        throw parsedResponse['message'];
      }
    } on SocketException catch (_) {
      throw 'Internet Error Occurred';
    } on Exception catch (_) {
      throw 'Something went wrong. Please try again later.';
    }
  }

  static Future<List> deleteMaintenance(String maintenanceId) async {
    List returningList = [];

    try {
      var uri = (await isHttps())
          ? Uri.https((await getBaseUri()), '/api/destroy_service', {...queryMap(), 'service_id': maintenanceId})
          : Uri.http((await getBaseUri()), '/api/destroy_service', {...queryMap(), 'service_id': maintenanceId});
      var response = await http.get(uri);
      if (response.statusCode != 200) {
        returningList.add(false);
        returningList.add('Something went wrong. Please try again later.');
      } else {
        returningList.add(true);
        returningList.add(response.body);
      }
    } on SocketException catch (_) {
      returningList.add(false);
      returningList.add('Internet Error Occurred');
    } on Exception catch (_) {
      returningList.add(false);
      returningList.add('Something went wrong. Please try again later.');
    }
    return returningList;
  }

  static Future<String> fetchAddress(String lat, String lng) async {
    var uri = (await isHttps())
        ? Uri.https((await getBaseUri()), '/api/geo_address', {...queryMap(), 'lat': lat, 'lon': lng})
        : Uri.http((await getBaseUri()), '/api/geo_address', {...queryMap(), 'lat': lat, 'lon': lng});

    var response = await http.get(uri);
    String addressString = response.body.replaceAll('"', '');
    return addressString;
  }

  static Future<bool> sendCommand(int deviceId, String type) async {
    var uri = (await isHttps()) ? Uri.https((await getBaseUri()), '/api/send_gprs_command', queryMap()) : Uri.http((await getBaseUri()), '/api/send_gprs_command', queryMap());
    try {
      var request = http.MultipartRequest(
        'POST',
        uri,
      );
      Map<String, String> body = {
        'device_id': deviceId.toString(),
        'type': type,
      };
      request.fields.addAll(body);
      var streamedResponse = await request.send();
      var response = await http.Response.fromStream(streamedResponse);
      if (response.statusCode == 200) {
        return true;
      } else {
        Map<String, dynamic> parsedResponse = json.decode(response.body);
        throw parsedResponse['message'];
      }
    } on SocketException catch (_) {
      throw 'Internet Error Occurred';
    } on Exception catch (_) {
      throw 'Something went wrong. Please try again later.';
    }
  }

  static Future<bool> changePswd(String oldPassword, String newPassword, {String? cookie}) async {
    try {
      var uri = (await isHttps()) ? Uri.https((await getBaseUri()), '/api/change_password', queryMap(cookie: cookie)) : Uri.http((await getBaseUri()), '/api/change_password', queryMap(cookie: cookie));
      var response = await http.post(uri, body: {'password': newPassword, 'password_confirmation': newPassword});
      if (response.statusCode == 200) {
        return true;
      } else {
        Map<String, dynamic> bodyMap = json.decode(response.body);
        throw bodyMap['message'];
      }
    } on SocketException catch (_) {
      throw 'Internet Error Occurred';
    } on Exception catch (e) {
      writeErrorLogs('Error ', e: e);
      throw 'Something went wrong. Please try again later.';
    }
  }

  static Future<bool> changeAlertActive(String alertId, bool status) async {
    var uri = (await isHttps()) ? Uri.https((await getBaseUri()), '/api/change_active_alert', queryMap()) : Uri.http((await getBaseUri()), '/api/change_active_alert', queryMap());
    try {
      var request = http.MultipartRequest(
        'POST',
        uri,
      );
      Map<String, String> body = {
        'id': alertId,
        'active': status.toString(),
      };
      request.fields.addAll(body);
      var streamedResponse = await request.send();
      var response = await http.Response.fromStream(streamedResponse);
      if (response.statusCode == 200) {
        return true;
      } else {
        Map<String, dynamic> parsedResponse = json.decode(response.body);
        throw parsedResponse['message'];
      }
    } on SocketException catch (_) {
      throw 'Internet Error Occurred';
    } on Exception catch (_) {
      throw 'Something went wrong. Please try again later.';
    }
  }

  static Future<Map<String, dynamic>> fetchEditAlertData(int alertId) async {
    try {
      var uri = (await isHttps())
          ? Uri.https((await getBaseUri()), '/api/edit_alert_data', {...queryMap(), 'alert_id': alertId.toString()})
          : Uri.http((await getBaseUri()), '/api/edit_alert_data', {...queryMap(), 'alert_id': alertId.toString()});
      var response = await http.get(uri);
      if (response.statusCode == 200) {
        return json.decode(response.body);
      } else {
        Map<String, dynamic> bodyMap = json.decode(response.body);
        throw bodyMap['message'];
      }
    } on SocketException catch (_) {
      throw 'Internet Error Occurred';
    } on Exception catch (_) {
      throw 'Something went wrong. Please try again later.';
    }
  }

  static Future<String> updateValues(DeviceModel deviceModel, SettingUpdateType type, String value) async {
    try {
      Map<String, String> body = {
        'name': deviceModel.deviceData['name'] as String,
        'imei': deviceModel.deviceData['imei'] as String,
        'icon_id': deviceModel.deviceData['icon_id'].toString(),
        'fuel_measurement_id': deviceModel.deviceData['fuel_measurement_id'].toString(),
        'tail_length': deviceModel.deviceData['tail_length'].toString(),
        'min_moving_speed': deviceModel.deviceData['min_moving_speed'].toString(),
        'min_fuel_fillings': deviceModel.deviceData['min_fuel_fillings'].toString(),
        'min_fuel_thefts': deviceModel.deviceData['min_fuel_thefts'].toString(),
        'fuel_quantity': deviceModel.deviceData['fuel_quantity'].toString(),
        'fuel_price': deviceModel.deviceData['fuel_price'].toString(),
        'group_id': deviceModel.deviceData['group_id'].toString(),
        'device_icons_type': deviceModel.deviceData['icon']['type'],
        'icon_moving': deviceModel.deviceData['icon_colors']['moving'].toString(),
        'icon_stopped': deviceModel.deviceData['icon_colors']['icon_stopped'].toString(),
        'icon_offline': deviceModel.deviceData['icon_colors']['offline'].toString(),
        'icon_engine': deviceModel.deviceData['icon_colors']['engine'].toString(),
        'vin': deviceModel.deviceData['vin'].toString(),
        'device_model': deviceModel.deviceData['device_model'].toString(),
        'plate_number': deviceModel.deviceData['plate_number'].toString(),
        'registration_number': deviceModel.deviceData['registration_number'].toString(),
        'object_owner': deviceModel.deviceData['object_owner'].toString(),
        'additional_notes': deviceModel.deviceData['additional_notes'].toString()
      };
      if (type == SettingUpdateType.ENGINE_HOURS) {
        body.update('engine_hours', (s) => value);
      } else if (type == SettingUpdateType.FUEL_COST) {
        deviceModel.fuelPrice = value;
        body['fuel_price'] = value;
      } else if (type == SettingUpdateType.MILEAGE) {
        deviceModel.fuelQuantity = value;
        body['fuel_quantity'] = value;
      } else if (type == SettingUpdateType.NAME) {
        body['name'] = value;
      }
      var uri = (await isHttps())
          ? Uri.https((await getBaseUri()), '/api/edit_device', {...queryMap(), 'device_id': deviceModel.deviceId.toString()})
          : Uri.http((await getBaseUri()), '/api/edit_device', {...queryMap(), 'device_id': deviceModel.deviceId.toString()});
      var response = await http.post(
        uri,
        body: body,
      );
      if (response.statusCode == 200) {
        await getDevices();
        return 'OK';
      } else {
        Map<String, dynamic> responseMap = json.decode(response.body);
        throw responseMap['message'];
      }
    } on SocketException catch (_) {
      throw 'Internet Error Occurred';
    } on Exception catch (_) {
      throw 'Something Went wrong';
    }
  }
}
