import 'package:autonemogps/api_calls/gps_server_requests.dart';
import 'package:autonemogps/config/colors.dart';
import 'package:autonemogps/models/gen_info_data.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class SingleDeviceWidget extends StatefulWidget {
  final String selectedName;
  final String selectedImei;

  SingleDeviceWidget(this.selectedName, this.selectedImei, {required Key key}) : super(key: key);

  @override
  State<SingleDeviceWidget> createState() => SingleDeviceWidgetState();
}

class SingleDeviceWidgetState extends State<SingleDeviceWidget> {
  List<GenInfoData> genInfoDataList = [];

  bool loading = false;

  @override
  void initState() {
    fetchData();
    super.initState();
  }

  @override
  void didUpdateWidget(covariant SingleDeviceWidget oldWidget) {
    fetchData();
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    if (loading) {
      return SpinKitCircle(color: AppColors.primaryColor);
    }

    return Container(
      margin: EdgeInsets.only(bottom: 16),
      padding: EdgeInsets.all(10),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(16),
        boxShadow: [BoxShadow(color: AppColors.shadowMaterialColor[100]!, blurRadius: 5.0, spreadRadius: 1.0, offset: Offset(1.0, 2.0))],
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                child: Text.rich(TextSpan(children: [
                  TextSpan(text: widget.selectedName, style: TextStyle(fontSize: 16, fontWeight: FontWeight.w600)),
                  TextSpan(text: ' ( Today\'s Stats )', style: TextStyle(fontSize: 12, fontWeight: FontWeight.w500))
                ])),
              ),
            ],
          ),
          Divider(),
          GridView.builder(
            physics: NeverScrollableScrollPhysics(),
            itemCount: genInfoDataList.length,
            shrinkWrap: true,
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2, childAspectRatio: 3),
            itemBuilder: (context, index) {
              GenInfoData data = genInfoDataList[index];

              return Row(
                children: [
                  Image.asset(data.image, height: 25, width: 25),
                  SizedBox(width: 10),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Text(capitalize(data.title), style: TextStyle(fontSize: 12, fontWeight: FontWeight.w600), maxLines: 1, overflow: TextOverflow.ellipsis),
                        Text(data.value, overflow: TextOverflow.ellipsis)
                      ],
                    ),
                  )
                ],
              );
            },
          )
        ],
      ),
    );
  }

  String capitalize(String s) {
    List<String> splits = s.toLowerCase().split(' ');
    List<String> title = [];
    splits.forEach((element) => title.add(element[0].toUpperCase() + element.substring(1)));
    return title.join(' ');
  }

  void fetchData() {
    setState(() => loading = true);
    GPSServerRequests.getWidgetStatisticsData(widget.selectedImei, widget.selectedName).then((List<GenInfoData> genInfoDataList) {
      setState(() => this.genInfoDataList = genInfoDataList);
      setState(() => loading = false);
    });
  }
}
