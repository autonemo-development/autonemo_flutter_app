import 'package:autonemogps/bloc/home_bloc.dart';
import 'package:autonemogps/bloc/home_event.dart';
import 'package:autonemogps/bloc/home_state.dart';
import 'package:autonemogps/home_page/tabs/dashboard/bar_chart.dart';
import 'package:autonemogps/models/chart_key_value_model.dart';
import 'package:autonemogps/models/tracking_device_model.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class VehicleMileage extends StatefulWidget {
  final String selectedImei;

  final String selectedDevice;

  VehicleMileage(this.selectedImei, this.selectedDevice, {required Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return VehicleMileageState();
  }
}

class VehicleMileageState extends State<VehicleMileage> {
  HomeBloc? _homeBloc;
  List<charts.Series<ChartKeyValueModel, String>> mileageSeriesList = [];

  @override
  void initState() {
    super.initState();
    _homeBloc = BlocProvider.of<HomeBloc>(context)..add(GetInitialDataEvent());
  }

  @override
  void didUpdateWidget(covariant VehicleMileage oldWidget) {
    fetchData();
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: BlocBuilder<HomeBloc, HomeState>(
        bloc: _homeBloc,
        buildWhen: (previous, current) => current is DataFetchedState,
        builder: (context, state) {
          if (state is DataFetchedState) {
            TrackingDevice? selectedDeviceTrackingList = state.trackingList[widget.selectedImei];

            if (selectedDeviceTrackingList == null) {
              return Container();
            }

            Map<String, dynamic>? json = selectedDeviceTrackingList.raw;

            List<ChartKeyValueModel> mileageData = [];

            if (json != null) {
              mileageData.add(new ChartKeyValueModel(json['mdt1'], json['m1']));
              mileageData.add(new ChartKeyValueModel(json['mdt2'], json['m2']));
              mileageData.add(new ChartKeyValueModel(json['mdt3'], json['m3']));
              mileageData.add(new ChartKeyValueModel(json['mdt4'], json['m4']));
              mileageData.add(new ChartKeyValueModel(json['mdt5'], json['m5']));
            }

            charts.Series<ChartKeyValueModel, String> eventsSeriesValue = new charts.Series<ChartKeyValueModel, String>(
              id: widget.selectedDevice + ' ( Mileage (km) )',
              colorFn: (_, __) => charts.MaterialPalette.red.shadeDefault,
              domainFn: (ChartKeyValueModel chartKeyValueModel, _) => chartKeyValueModel.key,
              measureFn: (ChartKeyValueModel chartKeyValueModel, _) => chartKeyValueModel.value,
              labelAccessorFn: (ChartKeyValueModel chartKeyValueModel, _) => chartKeyValueModel.value.toString(),
              data: mileageData,
            );

            mileageSeriesList = [eventsSeriesValue];

            return BarGraph(mileageSeriesList, true);
          }
          return Container();
        },
      ),
    );
  }

  fetchData() {
    _homeBloc?.add(RefreshDataEvent());
  }
}
