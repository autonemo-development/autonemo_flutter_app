class IconHelper {
  static Map<String, String> icons = {
    'Car': 'images/car',
    'Bike': 'images/bike',
    'Ambulance': 'images/ambulance',
    'JCB': 'images/jcb',
    'Police Car': 'images/police',
    'Road Roller': 'images/roller',
    'Truck': 'images/truck',
    'Van': 'images/van',
    'Bus': 'images/bus',
    'School Bus': 'images/schoolbus',
    'Scooty': 'images/scooty',
    'Tanker': 'images/tanker',
    'Ship': 'images/ship',
    'Auto': 'images/auto',
    'Person': 'images/person',
    'E-Rickshaw': 'images/rickshaw',
    'Bin Truck': 'images/bin_truck',
    'Watch': 'images/watch',
    'Pet': 'images/pet'
  };
  static Map<String, String> iconsToServerImage = {
    'Car': 'car-rerd.png',
    'Bike': 'bike-red.png',
    'Ambulance': 'ambulance-red.png',
    'JCB': 'jcb-red.png',
    'Police Car': 'police-red.png',
    'Road Roller': 'roller-red.png',
    'Truck': 'truck-red.png',
    'Van': 'van-rd.png',
    'Bus': 'bus-red.png',
    'School Bus': 'school-bus-red.png',
    'Scooty': 'scooty-red.png',
    'Tanker': 'tanker-re4d.png',
    'Ship': 'ship-red.png',
    'Auto': 'auto-green.png',
    'Person': 'person-red.png',
    'E-Rickshaw': 'rickshaw-red.png',
    'Bin Truck': 'bin-truck-red.png',
    'Watch': 'watch-red.png',
    'Pet': 'pet-red.png'
  };

  static Map<String, String> serverImageToIcon = {
    'car-rerd.png': 'Car',
    'bike-red.png': 'Bike',
    'ambulance-red.png': 'Ambulance',
    'jcb-red.png': 'JCB',
    'police-red.png': 'Police Car',
    'roller-red.png': 'Road Roller',
    'truck-red.png': 'Truck',
    'van-rd.png': 'Van',
    'bus-red.png': 'Bus',
    'school-bus-red.png': 'School Bus',
    'scooty-red.png': 'Scooty',
    'tanker-re4d.png': 'Tanker',
    'ship-red.png': 'Ship',
    'auto-green.png': 'Auto',
    'person-red.png': 'Person',
    'rickshaw-red.png': 'E-Rickshaw',
    'bin-truck-red.png': 'Bin Truck',
    'watch-red.png': 'Watch',
    'pet-red.png': 'Pet'
  };

  static String getIcon(String imageLink) {
    return serverImageToIcon[imageLink.split('\/').last] ?? 'Car';
  }
}
