import 'package:animations/animations.dart';
import 'package:autonemogps/config/colors.dart';
import 'package:flutter/material.dart';

showTermsAndConditionDialog(BuildContext context){
  showModal(
      context: context,
      builder: (context){
        return Dialog(
          child: Container(
            padding: EdgeInsets.all(20.0),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal:15.0),
                  child: Text('Terms and Conditions',textAlign: TextAlign.center, style: TextStyle(color: AppColors.primaryColor,fontSize: 16,fontWeight: FontWeight.w500),),
                ),
                SizedBox(height: 15,),
                Text('1. Only Individual Customer references will execute Reward points. Reference by Distributors, Installation Points, and Corporates will not trigger Reward.',textAlign: TextAlign.justify, style: TextStyle(color: Color(0xFF474747),fontSize: 12,fontWeight: FontWeight.w300),),
                SizedBox(height: 10,),
                Text('2. If you are a Distributor, Installation Point or Corporate customer you should create/have a regular Customer account to join Reward Program.',textAlign: TextAlign.justify, style: TextStyle(color: Color(0xFF474747),fontSize: 12,fontWeight: FontWeight.w300),),
                SizedBox(height: 10,),
                Text('3. A partial amount can be redeemed by points. For example, a customer has 500 points and wants to buy another device. So the customer can redeem 500 Tk from the device price.',textAlign: TextAlign.justify, style: TextStyle(color: Color(0xFF474747),fontSize: 12,fontWeight: FontWeight.w300),),
                SizedBox(height: 20,),
                Center(
                  child: InkWell(
                    onTap: (){
                      Navigator.pop(context);
                    },
                    child: Container(
                      padding: EdgeInsets.symmetric(horizontal:25,vertical:7.0),
                      decoration: BoxDecoration(
                        color: AppColors.primaryColor,
                        borderRadius: BorderRadius.circular(20.0),
                      ),
                      child: Text('Ok got it',style: TextStyle(color: Colors.white,fontSize: 14 ,fontWeight: FontWeight.w500),),
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      }
  );
}