import 'package:auto_size_text/auto_size_text.dart';
import 'package:autonemogps/common/slider/shimmer.dart';
import 'package:autonemogps/utils/logs.dart';
import 'package:flutter/material.dart';
import 'package:vibration/vibration.dart';

class SliderButton extends StatefulWidget {
  ///To make button more customizable add your child widget
  final Widget child;

  ///Sets the radius of corners of a button.
  final double? radius;

  ///Use it to define a height and width of widget.
  final double height;
  final double? width;
  final double buttonSize;

  ///Use it to define a color of widget.
  final Color backgroundColor;
  final Color baseColor;
  final Color highlightedColor;
  final Color buttonColor;

  ///Change it to gave a label on a widget of your choice.
  final AutoSizeText label;

  ///Gives a alignment to a slider icon.
  final Alignment alignLabel;
  final BoxShadow? boxShadow;
  final Widget? icon;
  final Function action;

  ///Make it false if you want to deactivate the shimmer effect.
  final bool shimmer;

  ///Make it false if you want maintain the widget in the tree.
  final bool dismissible;

  final bool vibrationFlag;

  ///The offset threshold the item has to be dragged in order to be considered
  ///dismissed e.g. if it is 0.4, then the item has to be dragged
  /// at least 40% towards one direction to be considered dismissed
  final double dismissThresholds;

  final bool disable;

  SliderButton({
    required this.action,
    this.radius,
    this.boxShadow,
    required this.child,
    this.vibrationFlag = true,
    this.shimmer = true,
    this.height = 70,
    this.buttonSize = 60,
    this.width,
    this.alignLabel = const Alignment(0.4, 0),
    this.backgroundColor = const Color(0xffe0e0e0),
    this.baseColor = Colors.black87,
    this.buttonColor = Colors.white,
    this.highlightedColor = Colors.white,
    required this.label,
    this.icon,
    this.dismissible = true,
    this.dismissThresholds = 1.0,
    this.disable = false,
  }) : assert((buttonSize) <= (height));

  @override
  _SliderButtonState createState() => _SliderButtonState();
}

class _SliderButtonState extends State<SliderButton> {
  bool flag = true;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return flag == true
        ? _control()
        : widget.dismissible == true
            ? Container()
            : Container(child: _control());
  }

  Widget _control() => Container(
        height: widget.height,
        width: widget.width ?? 250,
        decoration: BoxDecoration(color: widget.disable ? Colors.grey.shade700 : widget.backgroundColor, borderRadius: BorderRadius.circular(widget.radius ?? 100)),
        alignment: Alignment.centerLeft,
        child: Stack(
          alignment: Alignment.centerLeft,
          children: <Widget>[
            Container(
              alignment: widget.alignLabel,
              child: widget.shimmer
                  ? Shimmer.fromColors(
                      baseColor: widget.disable ? Colors.grey : widget.baseColor,
                      highlightColor: widget.highlightedColor,
                      child: widget.label,
                    )
                  : widget.label,
            ),
            widget.disable
                ? Tooltip(
                    verticalOffset: 50,
                    message: 'Button is disabled',
                    child: Container(
                        width: (widget.width ?? 250) - (widget.height),
                        height: (widget.height - 70),
                        alignment: Alignment.centerLeft,
                        padding: EdgeInsets.only(
                          left: (widget.height - widget.buttonSize) / 2,
                        ),
                        child: widget.child))
                : Dismissible(
                    key: Key('cancel'),
                    direction: DismissDirection.startToEnd,
                    dismissThresholds: {DismissDirection.startToEnd: widget.dismissThresholds},
                    onDismissed: (dir) async {
                      setState(() {
                        if (widget.dismissible) {
                          flag = false;
                        } else {
                          flag = !flag;
                        }
                      });

                      widget.action();
                      if (widget.vibrationFlag) {
                        try {
                          Vibration.vibrate(duration: 200);
                        } catch (e) {
                          writeErrorLogs('Error ', e: e);
                        }
                      }
                    },
                    child: Container(
                        width: (widget.width ?? 250) - (widget.height),
                        height: widget.height,
                        alignment: Alignment.centerLeft,
                        padding: EdgeInsets.only(left: (widget.height - (widget.buttonSize)) / 2),
                        child: widget.child),
                  ),
            Container(
              child: SizedBox.expand(),
            ),
          ],
        ),
      );
}
