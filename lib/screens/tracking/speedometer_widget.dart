import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_gauges/gauges.dart';

class SpeedometerWidget extends StatelessWidget {
  final int speed;

  final double height, width;

  const SpeedometerWidget(this.speed, {Key? key, required this.height, required this.width}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        height: height,
        width: width,
        child: SfRadialGauge(axes: <RadialAxis>[
          RadialAxis(
            minimum: 0,
            maximum: 100,
            startAngle: 180,
            endAngle: 360,
            showAxisLine: false,
            showTicks: false,
            showFirstLabel: false,
            showLastLabel: false,
            showLabels: false,
            backgroundImage: AssetImage('images/radial_speedometer_background.png'),
            ranges: <GaugeRange>[],
            pointers: <GaugePointer>[
              NeedlePointer(
                value: this.speed.toDouble(),
                needleStartWidth: 0.5,
                needleEndWidth: 2,
                enableAnimation: true,
                animationType: AnimationType.bounceOut,
                needleColor: Color(0xFF9A73DF),
                knobStyle: KnobStyle(color: Color(0xFF9A73DF)),
              )
            ],
            annotations: <GaugeAnnotation>[
              GaugeAnnotation(
                widget: Container(child: Text('${this.speed} KM/H', style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500))),
                angle: 90,
                positionFactor: 0.6,
              )
            ],
          )
        ]));
  }
}
