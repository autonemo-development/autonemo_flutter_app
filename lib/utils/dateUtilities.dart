import 'package:autonemogps/config/string_constants.dart';
import 'package:intl/intl.dart';

DateFormat dateFormat = DateFormat('yyyy-MM-dd');
DateFormat dateTimeInput = DateFormat(STANDARD_DATE_TIME_INPUT_PATTERN);
DateFormat dateTimeOutput = DateFormat(STANDARD_DATE_TIME_OUTPUT_PATTERN);

DateTime getCurrentDate() {
  return new DateTime.now();
}

String getYesterdayNightDateTime() {
  return dateFormat.format(getCurrentDate());
}

String getTonightDateTime() {
  return dateFormat.format(getCurrentDate().add(Duration(days: 1)));
}

DateTime dateOnLastMonday() {
  var monday = 1;
  var now = getCurrentDate();

  while (now.weekday != monday) {
    now = now.subtract(new Duration(days: 1));
  }
  return now;
}

DateTime parseString(String datetime) {
  try {
    return DateTime.parse(datetime).toLocal();
  } catch (e) {
    return getCurrentDate();
  }
}

String formatDateTime(DateTime datetime) {
  return dateTimeInput.format(datetime);
}

int getSecondsDifference(DateTime datetime) {
  return getCurrentDate().difference(datetime).inSeconds;
}

int timezoneToMinutes(String tz) {
  String t = tz.replaceAll('+', '');
  t = tz.replaceAll('-', '');
  List<String> timeZoneArray = t.split(':');
  int subtractMinute = int.parse(timeZoneArray[0]) * 60 + int.parse(timeZoneArray[1]);

  return tz.contains('-') ? -subtractMinute : subtractMinute;
}

String secondsToDhms(int seconds) {
  if (seconds == 0) {
    return '0 s';
  }

  int hrs = (seconds / 3600).floor();
  int minutes = ((seconds % 3600) / 60).floor();
  int sec = (seconds % 60).floor();

  String str = '';
  if (hrs == 0) {
    if (minutes == 0) {
      str = '$sec sec';
    } else {
      str = "${minutes.toString().padLeft(2, "0")} : ${sec.toString().padLeft(2, "0")} min";
    }
  } else {
    str = "${hrs.toString().padLeft(2, "0")} : ${minutes.toString().padLeft(2, "0")} : ${sec.toString().padLeft(2, "0")} hrs";
  }

  return str;
}

String calculateDiffTime(int seconds) {
  if (seconds < 10) {
    return 'moments ago';
  }
  var d = (seconds / (3600 * 24)).floor();
  var h = ((seconds % (3600 * 24)) / 3600).floor();
  var m = ((seconds % 3600) / 60).floor();
  var s = (seconds % 60).floor();

  if (d > 0) {
    if (d == 1) {
      return '1 day ago';
    } else
      return '$d days ago';
  } else if (h > 0) {
    if (h == 1) {
      return '1 hour ago';
    } else
      return '$h hours ago';
  } else if (m > 0) {
    if (m == 1) {
      return '1 minute ago';
    } else
      return '$m minutes ago';
  } else {
    if (s < 10) {
      return 'moments ago';
    } else
      return '$s seconds ago';
  }
}
