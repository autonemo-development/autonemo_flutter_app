import 'package:flutter/material.dart';

class SelectedTabModel extends ChangeNotifier {
  int selectedTab=1;

  setSelectedTabPosition(int tabPosition) {
    selectedTab = tabPosition;
    notifyListeners();
  }
}
