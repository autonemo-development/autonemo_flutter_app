import 'package:autonemogps/config/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

// ignore: must_be_immutable
class CustomTextFormField extends StatelessWidget {
  String hinText;
  bool obscureText;
  Color boardColor;
  int maxLines;
  Function? onChanged;
  Widget? suffixIcon;
  TextInputType? textInputType;
  TextEditingController controller;
  bool isEnabled;
  int maxLength;

  CustomTextFormField(
      {required this.controller,
      required this.hinText,
      this.isEnabled = true,
      required this.boardColor,
      this.obscureText = false,
      this.maxLines = 1,
        this.onChanged,
      this.suffixIcon,
      this.maxLength=200,
      this.textInputType});

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: controller,
      maxLines: maxLines,
      keyboardType: textInputType,
      obscureText: obscureText,
      enabled: isEnabled,
      autofocus: false,
      inputFormatters: [
        LengthLimitingTextInputFormatter(maxLength),
      ],
      onChanged:(s){ if(onChanged!=null)onChanged!(s);},
      decoration: InputDecoration(
        contentPadding:
            EdgeInsets.only(top: 10, bottom: 10, left: 15, right: 15),
        hintText: hinText,
        hintStyle: TextStyle(color: Color(0xFFBBBBBB)),
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(color: boardColor),
        ),
        disabledBorder: OutlineInputBorder(
          borderSide: BorderSide(color: AppColors.primaryColor),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(color: boardColor, width: 2.0),
        ),
        suffixIcon: suffixIcon,
      ),
      cursorColor: boardColor,
    );
  }
}
