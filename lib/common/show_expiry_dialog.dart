import 'package:animations/animations.dart';
import 'package:autonemogps/config/colors.dart';
import 'package:autonemogps/config/helpers.dart';
import 'package:autonemogps/storage/repository.dart';
import 'package:flutter/material.dart';

showExpiryDialog(BuildContext context, String text) {
  showModal(
      context: context,
      builder: (context) {
        return Dialog(
          child: Container(
            padding: EdgeInsets.only(top: 20),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Image.asset(
                  'images/expiry_image.png',
                  height: 100.0,
                ),
                SizedBox(
                  height: 40.0,
                ),
                Flexible(
                  child: SingleChildScrollView(
                    child: Padding(
                      padding: const EdgeInsets.only(left: 16, right: 16),
                      child: Text(
                        'Your Device ($text) will expire very soon. For Auto Renewal please pay monthly charges to avoid disconnection.',
                        textAlign: TextAlign.center,
                        style: TextStyle(fontSize: 14, color: AppColors.sensorInActiveColor, fontWeight: FontWeight.w500),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  padding: EdgeInsets.all(16),
                  color: AppColors.primaryColor,
                  child: InkWell(
                    onTap: () {
                      Navigator.pop(context);
                      launchUrl('https://erp.autonemo.io/payment?platformUsername=${Repository.getUsername()}');
                    },
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(
                          'Pay Now',
                          style: TextStyle(color: Colors.white, fontSize: 16, fontWeight: FontWeight.w600),
                        ),
                        Icon(
                          Icons.arrow_right_alt_sharp,
                          color: Colors.white,
                          size: 28,
                        )
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        );
      });
}
