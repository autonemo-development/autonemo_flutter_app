import 'package:autonemogps/config/colors.dart';
import 'package:autonemogps/config/helpers.dart';
import 'package:autonemogps/screens/login_page/login_page.dart';
import 'package:autonemogps/storage/repository.dart';
import 'package:flutter/material.dart';

class LockDialogWidget extends StatelessWidget {
  final String commandType;

  LockDialogWidget(this.commandType);

  final TextEditingController _controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Dialog(
      child: Container(
        padding: EdgeInsets.all(20),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text('Warning!!!', style: TextStyle(decoration: TextDecoration.none, color: AppColors.kBlackColor, fontSize: 16.0, fontFamily: 'Poppins', fontWeight: FontWeight.w600), textAlign: TextAlign.center),
            SizedBox(height: 10),
            Text(
              'This feature is very risky & expect you understand the risk associated with it. This will send GPRS command to $commandType Vehicle Engine.',
              style: TextStyle(decoration: TextDecoration.none, color: Color(0xFF888888), fontSize: 12.0, fontFamily: 'Poppins', fontWeight: FontWeight.w400),
              textAlign: TextAlign.justify,
            ),
            SizedBox(height: 10),
            Text(
              'Confirm Your Password to continue',
              style: TextStyle(decoration: TextDecoration.none, color: Color(0xFF212121), fontSize: 14.0, fontFamily: 'Poppins', fontWeight: FontWeight.w500),
              textAlign: TextAlign.justify,
            ),
            SizedBox(height: 10.0),
            Container(
              height: 40,
              margin: EdgeInsets.only(top: 5, bottom: 5, right: 25, left: 25),
              child: PasswordTextField(_controller, true),
            ),
            SizedBox(height: 10.0),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                InkWell(
                  onTap: () {
                    Navigator.pop(context, [false]);
                  },
                  child: Container(
                      height: 40,
                      width: 100,
                      decoration: BoxDecoration(borderRadius: BorderRadius.circular(20.0), color: Color(0xFFFF6B6B)),
                      child: Center(child: Text('Cancel', style: TextStyle(color: Colors.white, fontSize: 14, fontWeight: FontWeight.w500)))),
                ),
                InkWell(
                  onTap: () {
                    if (_controller.text == Repository.getPswd()) {
                      Navigator.pop(context, [true, _controller.text]);
                    } else {
                      showSnackBar(context, 'Incorrect Password', Colors.red);
                    }
                  },
                  child: Container(
                      height: 40,
                      width: 100,
                      decoration: BoxDecoration(borderRadius: BorderRadius.circular(20.0), color: Colors.black),
                      child: Center(child: Text('Confirm', style: TextStyle(color: Colors.white, fontSize: 14, fontWeight: FontWeight.w500)))),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
