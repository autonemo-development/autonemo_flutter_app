import 'package:autonemogps/api_calls/gps_server_requests.dart';
import 'package:autonemogps/bloc/home_bloc.dart';
import 'package:autonemogps/bloc/home_event.dart';
import 'package:autonemogps/bloc/home_state.dart';
import 'package:autonemogps/config/helpers.dart';
import 'package:autonemogps/models/device_model.dart';
import 'package:autonemogps/models/sensor_model.dart';
import 'package:autonemogps/models/tracking_device_model.dart';
import 'package:autonemogps/models/vehicle_status_mode.dart';
import 'package:autonemogps/screens/advance_info/advance_speedometer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:syncfusion_flutter_gauges/gauges.dart';

class AdvanceInfo extends StatefulWidget {
  final DeviceModel deviceModel;

  final TrackingDevice trackingDevice;

  AdvanceInfo(this.deviceModel, this.trackingDevice);

  @override
  State<StatefulWidget> createState() {
    return AdvanceInfoState();
  }
}

class AdvanceInfoState extends State<AdvanceInfo> {
  TrackingDevice? currentTrackingDevice;

  HomeBloc? homeBloc;

  String address = '';

  bool showLoader = false;

  @override
  void initState() {
    currentTrackingDevice = widget.trackingDevice;
    homeBloc = BlocProvider.of<HomeBloc>(context);
    homeBloc?.add(GetInitialDataEvent());
    homeBloc?.add(StartTimerEvent());
    fetchAndSetAddress(currentTrackingDevice!);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<HomeBloc, HomeState>(
      listener: (context, state) {
        if (state is DataFetchingState) {
          setState(() => this.showLoader = true);
        }

        if (state is DataFetchedState) {
          setState(() => this.showLoader = false);
          TrackingDevice? trackingDevice = state.trackingList[widget.deviceModel.imei];
          if (trackingDevice != null) {
            setState(() => currentTrackingDevice = trackingDevice);
            fetchAndSetAddress(trackingDevice);
          }
        }
      },
      bloc: homeBloc,
      child: Scaffold(
        appBar: AppBar(
          title: Text('Advance Info'),
          backgroundColor: Colors.white,
          foregroundColor: Colors.black,
          actions: [
            Container(
              child: InkWell(
                onTap: _onRefreshTap,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Icon(Icons.refresh, color: Colors.green),
                    SizedBox(width: 5),
                    Text('Refresh Data', style: TextStyle(color: Colors.green)),
                    SizedBox(width: 10),
                  ],
                ),
              ),
            )
          ],
        ),
        body: Container(
          child: Column(
            children: [
              Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(10),
                  boxShadow: [BoxShadow(color: Colors.grey.withOpacity(.3), blurRadius: 10.0, offset: Offset(1.0, 1.0))],
                ),
                margin: EdgeInsets.only(right: 10, left: 10, top: 10, bottom: 10),
                padding: EdgeInsets.only(bottom: 20),
                child: Column(
                  children: [
                    Stack(
                      alignment: Alignment.bottomCenter,
                      children: [
                        Positioned(child: AdvanceSpeedometer(currentTrackingDevice!)),
                        Positioned(
                          bottom: 0,
                          child: Container(
                            height: 80,
                            decoration: BoxDecoration(border: Border.all(width: 0.2), borderRadius: BorderRadius.circular(40), color: Colors.white),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                RpmWidget(currentTrackingDevice!),
                                Container(
                                  margin: EdgeInsets.symmetric(horizontal: 10),
                                  child: Column(
                                    children: [
                                      Container(child: Text(widget.deviceModel.name, style: TextStyle(fontSize: 12)), margin: EdgeInsets.only(top: 5)),
                                      ThreeSensorWidget(currentTrackingDevice!),
                                      OdometerWidget(currentTrackingDevice!.odometer!),
                                    ],
                                  ),
                                ),
                                FuelWidget(currentTrackingDevice!),
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                    MovingAndExpiryWidget(currentTrackingDevice: currentTrackingDevice!, widget: widget),
                    if (address != '') AddressWidget(address)
                  ],
                ),
              ),
              SensorsGrid(currentTrackingDevice!),
              if (showLoader) LinearProgressIndicator(color: Colors.red),
            ],
          ),
        ),
      ),
    );
  }

  void _onRefreshTap() {
    homeBloc?.add(RefreshDataEvent());
  }

  void fetchAndSetAddress(TrackingDevice currentTrackingDevice) {
    GPSServerRequests.fetchAddress(currentTrackingDevice.lat.toString(), currentTrackingDevice.lng.toString()).then((value) => setState(() => address = value));
  }
}

class AddressWidget extends StatelessWidget {
  final String address;

  const AddressWidget(this.address, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 5, right: 20, left: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Text('Address', style: TextStyle(fontSize: 12, fontWeight: FontWeight.w100)),
          Text(address, style: TextStyle(fontSize: 12)),
        ],
      ),
    );
  }
}

class MovingAndExpiryWidget extends StatelessWidget {
  const MovingAndExpiryWidget({Key? key, required this.currentTrackingDevice, required this.widget}) : super(key: key);

  final TrackingDevice currentTrackingDevice;
  final AdvanceInfo widget;

  @override
  Widget build(BuildContext context) {
    VehicleStatusModel vehicleStatusModel = getVehicleStatus(currentTrackingDevice, widget.deviceModel);

    return Container(
      padding: EdgeInsets.only(top: 10, right: 20, left: 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Container(
            child: Row(
              children: [
                Container(decoration: BoxDecoration(color: vehicleStatusModel.statusColor, borderRadius: BorderRadius.circular(20)), height: 10, width: 10, margin: EdgeInsets.only(right: 5)),
                Row(
                  children: [
                    Text(vehicleStatusModel.status, style: TextStyle(fontSize: 12, fontWeight: FontWeight.w100)),
                    SizedBox(width: 5),
                    Text(currentTrackingDevice.statusDuration ?? '', style: TextStyle(fontSize: 12, color: vehicleStatusModel.statusColor)),
                  ],
                )
              ],
            ),
          ),
          Container(
            child: Row(
              children: [
                Text(vehicleStatusModel.expiryDateText, style: TextStyle(fontSize: 12, fontWeight: FontWeight.w100)),
                SizedBox(width: 5),
                Text(vehicleStatusModel.expiryDate, style: TextStyle(color: vehicleStatusModel.expiryColor, fontSize: 12)),
              ],
            ),
          )
        ],
      ),
    );
  }
}

class SensorsGrid extends StatelessWidget {
  final TrackingDevice trackingDevice;

  const SensorsGrid(this.trackingDevice, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Container(
        padding: EdgeInsets.only(right: 5, left: 5),
        child: AlignedGridView.count(
            crossAxisCount: 3,
            itemCount: trackingDevice.sensorList!.length,
            shrinkWrap: true,
            itemBuilder: (context, index) {
              Sensor sensorItem = trackingDevice.sensorList![index];
              return Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(5),
                  boxShadow: [BoxShadow(color: Colors.grey.withOpacity(.3), blurRadius: 10.0, offset: Offset(1.0, 1.0))],
                ),
                constraints: BoxConstraints(minHeight: 80),
                margin: EdgeInsets.all(5),
                child: Container(
                  padding: EdgeInsets.all(10),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Expanded(
                            child: Container(
                              child: Text(sensorItem.sensorName!, style: TextStyle(fontSize: 12, fontWeight: FontWeight.w600, height: 1.3)),
                              padding: EdgeInsets.only(right: 5),
                            ),
                          ),
                        ],
                      ),
                      Row(
                        children: [
                          Container(
                            padding: EdgeInsets.all(5),
                            margin: EdgeInsets.only(right: 10),
                            decoration: BoxDecoration(shape: BoxShape.circle, color: sensorItem.sensorLightColor),
                            child: Image.asset(sensorItem.imageName!, color: sensorItem.sensorColor),
                            width: 25,
                          ),
                          Expanded(child: Align(child: Text(sensorItem.sensorValue, style: TextStyle(fontSize: 12, fontWeight: FontWeight.w300)), alignment: Alignment.topLeft)),
                        ],
                      )
                    ],
                  ),
                ),
              );
            }),
      ),
    );
  }
}

class ThreeSensorWidget extends StatelessWidget {
  final TrackingDevice currentTrackingDevice;

  ThreeSensorWidget(this.currentTrackingDevice);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: getFirstThreeSensor(currentTrackingDevice)
          .map((sensorItem) => Container(
              padding: EdgeInsets.all(5),
              height: 20,
              width: 20,
              margin: EdgeInsets.all(2),
              decoration: BoxDecoration(shape: BoxShape.circle, color: sensorItem.sensorLightColor),
              child: Image.asset(sensorItem.imageName!, color: sensorItem.sensorColor)))
          .toList(),
    );
  }
}

class OdometerWidget extends StatelessWidget {
  final double odometer;

  const OdometerWidget(
    this.odometer, {
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 5),
      child: Row(
          children: odometer
              .toStringAsFixed(0)
              .split('')
              .map((e) => Container(
                    child: Text(e, style: TextStyle(fontSize: 12)),
                    decoration: BoxDecoration(color: Color(0xFFE0E0E0)),
                    padding: EdgeInsets.only(right: 3, left: 3),
                    margin: EdgeInsets.only(right: 1, left: 1),
                  ))
              .toList()),
    );
  }
}

class FuelWidget extends StatelessWidget {
  final TrackingDevice trackingDevice;

  const FuelWidget(this.trackingDevice, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double fuelValue = 0;

    try {
      trackingDevice.sensorList!.forEach((sensorItem) {
        if (sensorItem.sensorName == 'Fuel Level') {
          fuelValue = double.parse(sensorItem.sensorValue.toString().replaceAll(' L', ''));
        }
      });
    } catch (e) {}

    return Container(
      width: 80,
      child: SfRadialGauge(
        axes: [
          RadialAxis(
            showLabels: false,
            showTicks: false,
            startAngle: 180,
            endAngle: 90,
            minimum: 0,
            maximum: 200,
            axisLineStyle: AxisLineStyle(thickness: 5),
            annotations: <GaugeAnnotation>[
              GaugeAnnotation(
                  angle: 90,
                  widget: Container(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text('${fuelValue} L', style: TextStyle(fontSize: 12), textAlign: TextAlign.center),
                        Text('FUEL', style: TextStyle(fontSize: 10, fontWeight: FontWeight.w600), textAlign: TextAlign.center),
                      ],
                    ),
                  ))
            ],
            pointers: <GaugePointer>[
              RangePointer(value: fuelValue, gradient: SweepGradient(colors: <Color>[Colors.green, Colors.red], stops: <double>[0.25, 0.75]), width: 5)
            ],
          ),
        ],
      ),
    );
  }
}

class RpmWidget extends StatelessWidget {
  final TrackingDevice trackingDevice;

  const RpmWidget(this.trackingDevice, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double rpmValue = 0;

    try {
      trackingDevice.sensorList!.forEach((sensorItem) {
        if (sensorItem.sensorName == 'RPM') {
          rpmValue = double.parse(sensorItem.sensorValue.toString().replaceAll(' r/min', ''));
        }
      });
    } catch (e) {}

    return Container(
      width: 80,
      child: SfRadialGauge(
        axes: [
          RadialAxis(
            showLabels: false,
            showTicks: false,
            startAngle: 90,
            endAngle: 0,
            minimum: 0,
            maximum: 7000,
            axisLineStyle: AxisLineStyle(thickness: 5),
            annotations: <GaugeAnnotation>[
              GaugeAnnotation(
                  angle: 90,
                  widget: Container(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(rpmValue.toString(), style: TextStyle(fontSize: 12), textAlign: TextAlign.center),
                        Text('RPM', style: TextStyle(fontSize: 10, fontWeight: FontWeight.w600), textAlign: TextAlign.center),
                      ],
                    ),
                  ))
            ],
            pointers: <GaugePointer>[
              RangePointer(value: rpmValue, gradient: SweepGradient(colors: <Color>[Colors.green, Colors.red], stops: <double>[0.25, 0.75]), width: 5)
            ],
          ),
        ],
      ),
    );
  }
}
