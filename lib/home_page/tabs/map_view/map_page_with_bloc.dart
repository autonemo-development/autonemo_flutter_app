import 'dart:async';
import 'dart:math';

import 'package:autonemogps/bloc/home_bloc.dart';
import 'package:autonemogps/bloc/home_event.dart';
import 'package:autonemogps/bloc/home_state.dart';
import 'package:autonemogps/common/MeasureSizeRenderObject.dart';
import 'package:autonemogps/common/bloc/map_view_info_window.dart';
import 'package:autonemogps/common/bloc/window_bloc.dart';
import 'package:autonemogps/common/bloc/window_event.dart';
import 'package:autonemogps/common/custom_marker.dart';
import 'package:autonemogps/config/colors.dart';
import 'package:autonemogps/config/helpers.dart';
import 'package:autonemogps/config/string_constants.dart';
import 'package:autonemogps/models/device_model.dart';
import 'package:autonemogps/models/tracking_device_model.dart';
import 'package:autonemogps/screens/tracking/tracking_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';

class MapPageWithBloc extends StatefulWidget {
  @override
  _MapPageWithBlocState createState() => _MapPageWithBlocState();
}

class _MapPageWithBlocState extends State<MapPageWithBloc> with AutomaticKeepAliveClientMixin, TickerProviderStateMixin, WidgetsBindingObserver {
  GlobalKey<ScaffoldState> globalKey = GlobalKey<ScaffoldState>();

  HomeBloc? homeBloc;
  GoogleMapController? _controller;
  late LatLng myLatLng;

  Map<String, DeviceModel> deviceModelMap = {};
  Map<String, TrackingDevice> trackingDevicesMap = {};

  bool isTrafficEnabled = false;
  bool isPOIEnabled = false;
  bool showAllVehicles = true;
  bool isMoreClicked = false;
  bool showVehicleNames = true;
  bool showInfoWindow = false;

  TrackingDevice? tappedTrackingDevice;
  String distance = 'N/A', tappedStatus = '';
  double infoWindowHeight = 0;

  FocusNode _focus = new FocusNode();
  TextEditingController searchTextController = TextEditingController();
  Color? statusColor, lightStatusColor;
  CameraUpdate? _cameraUpdate;
  MapType _mapType = MapType.normal;
  MapInfoWindow? _mapInfoWindow;
  BitmapDescriptor? poiIcon;

  LatLng previousLatLng = LatLng(0.0, 0.0), currentLatLng = LatLng(0.0, 0.0);

  Map<String, Marker> _markersMap = {};

  bool isLocationPermissionGranted = false;

  @override
  bool get wantKeepAlive => true;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    getMarkerFromImagePath('images/poi_default.png').then((value) => poiIcon = value);
    homeBloc = BlocProvider.of<HomeBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return BlocConsumer<HomeBloc, HomeState>(
      listener: (context, state) async {
        if (state is DataFetchedState) {
          setState(() => deviceModelMap = state.deviceList);
          setState(() => trackingDevicesMap = state.trackingList);
          await _setMarkers();
        }
      },
      builder: (context, state) {
        return Scaffold(
          key: globalKey,
          drawer: Drawer(
            child: DrawerWidget(_focus, deviceModelMap, trackingDevicesMap, homeBloc?.searchMapText ?? '', (double lat, double lng) {
              LatLng latLng = LatLng(lat, lng);
              _cameraUpdate = CameraUpdate.newLatLngZoom(latLng, 16);
              _controller?.animateCamera(_cameraUpdate!);
            }, () => tappedTrackingDevice = null),
          ),
          resizeToAvoidBottomInset: false,
          body: SafeArea(
            child: Stack(
              children: <Widget>[
                GoogleMap(
                  mapType: _mapType,
                  trafficEnabled: isTrafficEnabled,
                  markers: _markersMap.values.toSet(),
                  onMapCreated: (GoogleMapController controller) => _controller = controller,
                  onTap: _onMapTap,
                  onCameraMoveStarted: () => _focus.unfocus(),
                  initialCameraPosition: CameraPosition(target: center, zoom: 8.0),
                  onCameraMove: (CameraPosition position) => _onChange(),
                  zoomControlsEnabled: false,
                  compassEnabled: false,
                  mapToolbarEnabled: false,
                  myLocationEnabled: true,
                  myLocationButtonEnabled: false,
                ),
                showInfoWindow && !showVehicleNames ? _mapInfoWindow! : SizedBox(height: 0, width: 0),
                Positioned(
                  left: 14,
                  top: 30,
                  child: InkWell(
                    onTap: () => globalKey.currentState?.openDrawer(),
                    child: Container(
                      padding: EdgeInsets.all(7),
                      decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.all(Radius.circular(2))),
                      child: Image.asset('images/menu.png', height: 30, width: 30),
                    ),
                  ),
                ),
                _buildMapIconsPositioned(),
                if (tappedTrackingDevice != null && deviceModelMap[tappedTrackingDevice!.imei] != null) _buildTopHeaderPositioned(context)
              ],
            ),
          ),
        );
      },
    );
  }

  Future<void> _setMarkers() async {
    double? x0, x1, y0, y1;
    Iterable<String> imeiList = trackingDevicesMap.keys;
    Iterable<TrackingDevice> item = trackingDevicesMap.values;

    for (int i = 0; i < item.length; i++) {
      String imei = imeiList.elementAt(i);
      TrackingDevice? trackingDevice = item.elementAt(i);
      DeviceModel? deviceModel = deviceModelMap[imei];
      if (deviceModel == null) {
        continue;
      }
      LatLng latLng = LatLng(trackingDevice.lat!, trackingDevice.lng!);

      if (latLng.latitude == 0 && latLng.longitude == 0) {
        continue;
      }

      if (tappedTrackingDevice != null && tappedTrackingDevice?.imei == trackingDevice.imei) {
        tappedTrackingDevice = trackingDevice;
        double d = calculateDistance(trackingDevice.lat, trackingDevice.lng, myLatLng.latitude, myLatLng.longitude);
        distance = d.toStringAsFixed(2);
        statusColor = Colors.grey[800];
        lightStatusColor = Colors.grey[50];
        switch (tappedTrackingDevice?.status) {
          case 'm':
            statusColor = Colors.green[800];
            lightStatusColor = Colors.green[50];
            tappedStatus = 'Moving since ' + tappedTrackingDevice!.statusDuration!;
            break;
          case 's':
            statusColor = Colors.red[800];
            lightStatusColor = Colors.red[50];
            tappedStatus = 'Stopped since ' + tappedTrackingDevice!.statusDuration!;
            break;
          case 'i':
            statusColor = Colors.orange[800];
            lightStatusColor = Colors.orange[50];
            tappedStatus = 'Idle since ' + tappedTrackingDevice!.statusDuration!;
            break;
          case 'off':
          case 'o':
            lightStatusColor = Colors.blue[50];
            statusColor = Colors.blue[800];
            tappedStatus = 'Offline since ' + tappedTrackingDevice!.statusDuration!;
            break;
          default:
            statusColor = Colors.grey;
            lightStatusColor = Colors.grey[700];
            tappedStatus = tappedTrackingDevice!.statusDuration!;
        }
      }

      BitmapDescriptor bitmapDescriptor;

      Map<String, dynamic> markerData = getVehicleMarkerData(trackingDevice.status!, deviceModel.icon);
      if (showVehicleNames) {
        bitmapDescriptor = await getMarkerIconWithInfo(markerData['iconKey'], deviceModel.name, markerData['color'], trackingDevice.angle!.toDouble());
      } else {
        bitmapDescriptor = await getMarkerFromImagePath(markerData['iconKey']);
      }

      if (x0 == null) {
        x0 = x1 = latLng.latitude;
        y0 = y1 = latLng.longitude;
      } else {
        if (latLng.latitude > x1!) x1 = latLng.latitude;
        if (latLng.latitude < x0) x0 = latLng.latitude;
        if (latLng.longitude > y1!) y1 = latLng.longitude;
        if (latLng.longitude < y0!) y0 = latLng.longitude;
      }

      _markersMap[imei] = Marker(
        onTap: () async {
          showVehicleBottomSheet(context, trackingDevice, deviceModel, true);

          double d = calculateDistance(latLng.latitude, latLng.longitude, myLatLng.latitude, myLatLng.longitude);
          distance = d.toStringAsFixed(2);

          tappedTrackingDevice = trackingDevice;
          statusColor = Colors.grey[800];
          lightStatusColor = Colors.grey[50];
          switch (tappedTrackingDevice?.status) {
            case 'm':
              statusColor = Colors.green[800];
              lightStatusColor = Colors.green[50];
              tappedStatus = 'Moving since ' + tappedTrackingDevice!.statusDuration!;
              break;
            case 's':
              statusColor = Colors.red[800];
              lightStatusColor = Colors.red[50];
              tappedStatus = 'Stopped since ' + tappedTrackingDevice!.statusDuration!;
              break;
            case 'i':
              statusColor = Colors.orange[800];
              lightStatusColor = Colors.orange[50];
              tappedStatus = 'Idle since ' + tappedTrackingDevice!.statusDuration!;
              break;
            case 'o':
              lightStatusColor = Colors.blue[50];
              statusColor = Colors.blue[800];
              tappedStatus = 'Offline since ' + tappedTrackingDevice!.statusDuration!;
              break;
            default:
              statusColor = Colors.grey;
              lightStatusColor = Colors.grey[700];
              tappedStatus = tappedTrackingDevice!.statusDuration!;
          }
          _cameraUpdate = CameraUpdate.newLatLngZoom(latLng, 14);

          _controller?.animateCamera(_cameraUpdate!);

          previousLatLng = currentLatLng;
          currentLatLng = latLng;

          _mapInfoWindow = MapInfoWindow(data: deviceModel.name);

          setState(() {
            if (currentLatLng == previousLatLng) {
              showInfoWindow = !showInfoWindow;
            } else {
              showInfoWindow = true;
            }
          });
          await _onChange();
        },
        anchor: Offset(0.5, showVehicleNames ? 0.70 : 0.5),
        markerId: MarkerId(imei),
        position: latLng,
        rotation: showVehicleNames ? 0 : trackingDevice.angle?.toDouble() ?? 0,
        icon: bitmapDescriptor,
      );
    }
    if (mounted) {
      setState(() {});
    }
    if (showAllVehicles && mounted && x1 != null && x0 != null && y1 != null && y0 != null) {
      setState(() => showAllVehicles = false);

      if (imeiList.length > 1) {
        LatLng latLang1 = LatLng(x1, y1);
        LatLng latLang2 = LatLng(x0, y0);

        LatLngBounds latLangBounds = LatLngBounds(northeast: latLang1, southwest: latLang2);
        CameraUpdate cameraUpdate = CameraUpdate.newLatLngBounds(latLangBounds, 100);

        _controller?.animateCamera(cameraUpdate);
      } else {
        _controller?.animateCamera(CameraUpdate.newLatLngZoom(LatLng(item.first.lat ?? 0, item.first.lng ?? 0), 16));
      }
    } else if (showInfoWindow) {
      _controller?.animateCamera(CameraUpdate.newLatLngZoom(LatLng(tappedTrackingDevice?.lat ?? 0, tappedTrackingDevice?.lng ?? 0), 16));
    }
  }

  _onChange() async {
    ScreenCoordinate coordinate = await _controller!.getScreenCoordinate(currentLatLng);
    BlocProvider.of<WindowBloc>(context).add(ChangePositionEvent(context: context, screenCoordinate: coordinate));
  }

  AnimatedPositioned _buildTopHeaderPositioned(BuildContext context) {
    return AnimatedPositioned(
      duration: Duration(milliseconds: 100),
      top: showInfoWindow ? 0 : -infoWindowHeight,
      left: 0,
      right: 0,
      child: GestureDetector(
        onPanEnd: (dragDetails) {
          if (dragDetails.velocity.pixelsPerSecond.dy < 0) {
            setState(() => showInfoWindow = false);
          }
        },
        child: MeasureSize(
          onChange: (size) => setState(() => infoWindowHeight = size.height),
          child: Container(
            margin: EdgeInsets.all(10),
            padding: EdgeInsets.only(left: 15, right: 15, top: 10, bottom: 10),
            decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(10), boxShadow: kElevationToShadow[6]),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text.rich(
                  TextSpan(
                    children: [
                      TextSpan(
                        text: tappedTrackingDevice!.speed.toString(),
                        style: TextStyle(decoration: TextDecoration.none, color: statusColor, fontSize: 40, fontWeight: FontWeight.bold),
                      ),
                      TextSpan(
                        text: '\nKM/H',
                        style: TextStyle(decoration: TextDecoration.none, color: statusColor, fontSize: 10, fontWeight: FontWeight.w500),
                      ),
                    ],
                  ),
                  textAlign: TextAlign.center,
                ),
                SizedBox(width: 20),
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(left: 10),
                        child: Text(deviceModelMap[tappedTrackingDevice?.imei]?.name ?? '', style: TextStyle(fontWeight: FontWeight.w600, color: Color(0xff002536), fontSize: 18)),
                      ),
                      Container(
                        margin: EdgeInsets.symmetric(vertical: 5),
                        decoration: BoxDecoration(color: lightStatusColor, borderRadius: BorderRadius.all(Radius.circular(10))),
                        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 3),
                        child: Text(tappedStatus, style: TextStyle(fontWeight: FontWeight.w600, color: statusColor, fontSize: 13)),
                      ),
                      if (distance != 'N/A')
                        Padding(
                            padding: const EdgeInsets.only(left: 10, top: 5),
                            child: Row(
                              children: [
                                Image.asset('images/distance_from_me.png', height: 30, width: 30),
                                SizedBox(width: 10),
                                Expanded(
                                  child: Text.rich(TextSpan(children: [
                                    TextSpan(text: '$distance KM', style: TextStyle(fontSize: 18, color: AppColors.primaryColor, fontWeight: FontWeight.w700)),
                                    TextSpan(text: ' From Me', style: TextStyle(fontSize: 14, color: Colors.grey, fontWeight: FontWeight.w400))
                                  ])),
                                )
                              ],
                            ))
                    ],
                  ),
                ),
                InkWell(
                  onTap: () => Navigator.of(context, rootNavigator: true).push(MaterialPageRoute(builder: (context) => TrackingPage(deviceModelMap[tappedTrackingDevice!.imei!]!, tappedTrackingDevice!))),
                  child: Container(padding: EdgeInsets.all(10), child: Image.asset('images/live_tracking.png', height: 40, width: 40)),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Positioned _buildMapIconsPositioned() {
    return Positioned(
      top: 30,
      right: 20,
      child: PopupMenuButton(
        child: Container(
          padding: EdgeInsets.all(10),
          decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.all(Radius.circular(2))),
          child: Icon(Icons.keyboard_arrow_down_outlined, color: AppColors.primaryColor),
        ),
        onSelected: _onPopupMenuItemSelected,
        itemBuilder: (context) => <PopupMenuItem<String>>[
          PopupMenuItem<String>(
            child: Container(child: Center(child: Image.asset(showVehicleNames ? 'images/car_with_name.png' : 'images/car_without_name.png', height: 30, width: 30))),
            value: 'toggle_device_name',
          ),
          PopupMenuItem<String>(child: Container(child: Center(child: Image.asset('images/my_location.png', height: 30, width: 30))), value: 'my_location'),
          PopupMenuItem<String>(
            child: Container(child: Center(child: Image.asset(_mapType == MapType.normal ? 'images/roadmap.png' : 'images/satellite-map.png', height: 30, width: 30))),
            value: 'map_view',
          ),
          PopupMenuItem<String>(
            child: Container(
              child: Center(
                child: Image.asset(
                  isTrafficEnabled ? 'images/traffic-lights-active.png' : 'images/traffic-lights.png',
                  height: 30,
                  width: 30,
                  color: isTrafficEnabled ? null : AppColors.primaryColor2,
                ),
              ),
            ),
            value: 'traffic',
          ),
          PopupMenuItem<String>(child: Container(child: Center(child: Image.asset('images/zoom_out.png', height: 30, width: 30))), value: 'zoom_out')
        ],
      ),
    );
  }

  void _onMapTap(coordinates) => setState(() => showInfoWindow = false);

  void _onPopupMenuItemSelected(selected) {
    if (selected == 'toggle_device_name') {
      showVehicleNames = !showVehicleNames;
      if (showVehicleNames) {
        showInfoWindow = false;
      }
      _setMarkers();
    } else if (selected == 'my_location') {
      _focus.unfocus();
      _currentLocation();
    } else if (selected == 'map_view') {
      setState(() {
        if (_mapType == MapType.normal) {
          _mapType = MapType.hybrid;
        } else {
          _mapType = MapType.normal;
        }
      });
    } else if (selected == 'traffic') {
      setState(() {
        isTrafficEnabled = !isTrafficEnabled;
      });
    } else if (selected == 'zoom_out') {
      _focus.unfocus();
      showAllVehicles = true;
      _setMarkers();
      setState(() {
        showInfoWindow = false;
      });
    }
  }

  void _currentLocation() async {
    this.isLocationPermissionGranted = await getLocationPermission();

    if (this.isLocationPermissionGranted) {
      Location location = new Location();

      var isServiceEnabled = await location.serviceEnabled();

      if (!isServiceEnabled) {
        showSnackBar(context, 'Please Enable Location Service and Try Again', Colors.red);
      }

      LocationData currentLocation = await location.getLocation();
      myLatLng = LatLng(currentLocation.latitude ?? 0, currentLocation.longitude ?? 0);
      _controller?.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(bearing: 0, target: myLatLng, zoom: 17.0)));
    } else {
      showSnackBar(context, 'Please Grant Location Permission From Settings', Colors.red);
    }
  }

  double calculateDistance(lat1, lon1, lat2, lon2) {
    var p = 0.017453292519943295;
    var c = cos;
    var a = 0.5 - c((lat2 - lat1) * p) / 2 + c(lat1 * p) * c(lat2 * p) * (1 - c((lon2 - lon1) * p)) / 2;
    return 12742 * asin(sqrt(a));
  }
}

class DrawerWidget extends StatefulWidget {
  final FocusNode focus;
  final Map<String, TrackingDevice> trackingList;
  final Map<String, DeviceModel> deviceList;
  final String searchText;
  final void Function(double lat, double lng) onItemTapped;
  final Function onSearch;

  DrawerWidget(this.focus, this.deviceList, this.trackingList, this.searchText, this.onItemTapped, this.onSearch);

  @override
  _DrawerWidgetState createState() => _DrawerWidgetState();
}

class _DrawerWidgetState extends State<DrawerWidget> {
  late List<String> imeiList;
  late TextEditingController searchTextController;

  @override
  void initState() {
    super.initState();
    searchTextController = TextEditingController(text: widget.searchText);
  }

  @override
  Widget build(BuildContext context) {
    imeiList = widget.deviceList.keys.toList();

    return Container(
        padding: EdgeInsets.all(10),
        color: Colors.white,
        child: Column(
          children: [
            SizedBox(height: 36),
            Container(
              decoration: BoxDecoration(border: Border.all(color: Color(0xFFD4D4D4), width: .3), borderRadius: BorderRadius.circular(10.0)),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Expanded(
                    child: TextField(
                      onChanged: (s) {
                        BlocProvider.of<HomeBloc>(context).add(SearchEvent(search: s));
                        widget.onSearch();
                      },
                      controller: searchTextController,
                      focusNode: widget.focus,
                      decoration: InputDecoration(prefixIcon: Icon(Icons.search), focusedBorder: InputBorder.none, enabledBorder: InputBorder.none),
                    ),
                  ),
                  IconButton(
                    icon: Icon(Icons.close, color: Colors.grey),
                    onPressed: () {
                      if (searchTextController.text.isNotEmpty) {
                        searchTextController.text = '';
                        BlocProvider.of<HomeBloc>(context).add(MapSearchEvent(search: ''));
                      } else if (widget.focus.hasFocus) {
                        widget.focus.unfocus();
                      } else {
                        Navigator.pop(context);
                      }
                    },
                  )
                ],
              ),
            ),
            Expanded(
              child: ListView.separated(
                  separatorBuilder: (context, index) => Divider(),
                  itemCount: imeiList.length + 1,
                  itemBuilder: (context, index) {
                    if (index == imeiList.length) {
                      return SizedBox(height: 80);
                    }
                    TrackingDevice trackingDevice = widget.trackingList[imeiList[index]]!;
                    DeviceModel deviceModel = widget.deviceList[imeiList[index]]!;
                    Map<String, dynamic> markerData = getVehicleMarkerData(trackingDevice.status ?? 'N/A', deviceModel.icon);

                    return InkWell(
                      onTap: () {
                        if ((trackingDevice.lat == 0 && trackingDevice.lng == 0)) {
                          Fluttertoast.showToast(msg: 'No Data Available');
                          return;
                        }
                        Navigator.pop(context);
                        widget.onItemTapped(trackingDevice.lat ?? 0, trackingDevice.lng ?? 0);
                      },
                      child: Container(
                        child: Row(
                          children: [Image.asset(markerData['iconKey'], height: 50, width: 50), SizedBox(width: 10), Text(deviceModel.name, style: TextStyle(fontWeight: FontWeight.w500))],
                        ),
                      ),
                    );
                  }),
            )
          ],
        ));
  }
}
