import 'package:autonemogps/config/colors.dart';
import 'package:autonemogps/config/helpers.dart';
import 'package:autonemogps/config/string_constants.dart';
import 'package:flutter/material.dart';

class QuickLinks extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10),
      margin: EdgeInsets.only(bottom: 16),
      decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(16), boxShadow: [
        BoxShadow(
          color: AppColors.shadowMaterialColor[100]!,
          blurRadius: 5.0,
          spreadRadius: 1.0,
          offset: Offset(
            1.0, // Move to right 10  horizontally
            2.0, // Move to bottom 5 Vertically
          ),
        )
      ]),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            'Quick Links',
            style: TextStyle(color: AppColors.headerTextColor),
          ),
          SizedBox(
            width: 20,
          ),
          Divider(),
          GridView.count(
            physics: NeverScrollableScrollPhysics(),
            shrinkWrap: true,
            crossAxisCount: 3,
            crossAxisSpacing: 4.0,
            childAspectRatio: 2.0,
            children: [
              InkWell(
                onTap: () {
                  launchUrl('https://bangladesh.gov.bd');
                },
                child: Center(
                  child: Image(
                    image: AssetImage('images/link_driving_instructor.png'),
                  ),
                ),
              ),
              InkWell(
                onTap: () {
                  launchUrl(
                      'http://www.brta.gov.bd/site/page/511a7af4-20c8-4450-b4a6-5f92dde1b23f/%E0%A6%A1%E0%A7%8D%E0%A6%B0%E0%A6%BE%E0%A6%87%E0%A6%AD%E0%A6%BF%E0%A6%82-%E0%A6%B2%E0%A6%BE%E0%A6%87%E0%A6%B8%E0%A7%87%E0%A6%A8%E0%A7%8D%E0%A6%B8%E0%A7%87%E0%A6%B0-%E0%A6%86%E0%A6%AC%E0%A7%87%E0%A6%A6%E0%A6%A8-%E0%A6%AA%E0%A7%8D%E0%A6%B0%E0%A6%95%E0%A7%8D%E0%A6%B0%E0%A6%BF%E0%A7%9F%E0%A6%BE');
                },
                child: Center(
                  child: Image(
                    image: AssetImage('images/link_get_license.png'),
                  ),
                ),
              ),
              InkWell(
                onTap: () {
                  launchUrl('https://bsp.brta.gov.bd/trafficDrivingTestGiudeline;lan=en?lan=en');
                },
                child: Center(
                  child: Image(
                    image: AssetImage('images/link_traffic_signs.png'),
                  ),
                ),
              ),
              InkWell(
                onTap: () {
                  launchUrl('https://bangladesh.gov.bd');
                },
                child: Center(
                  child: Image(
                    image: AssetImage('images/link_brta_instruction.png'),
                  ),
                ),
              ),
              InkWell(
                onTap: () {
                  launchUrl('$SHOP_URL/blog');
                },
                child: Center(
                  child: Image(
                    image: AssetImage('images/link_car_knowledge.png'),
                  ),
                ),
              ),
              InkWell(
                onTap: () {
                  launchUrl('$SHOP_URL/blog');
                },
                child: Center(
                  child: Image(
                    image: AssetImage('images/link_blogs.png'),
                  ),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}
