import 'package:autonemogps/bloc/home_bloc.dart';
import 'package:autonemogps/bloc/home_event.dart';
import 'package:autonemogps/bloc/home_state.dart';
import 'package:autonemogps/home_page/tabs/list_view_page/top_header/list_view_top_header_item.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ListViewTopHeader extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return ListViewTopHeaderState();
  }
}

class ListViewTopHeaderState extends State<ListViewTopHeader> {
  FocusNode searchFocusNode = FocusNode();
  TextEditingController searchTextController = TextEditingController();
  late HomeBloc homeBloc;

  @override
  void initState() {
    super.initState();

    searchTextController.addListener(() {
      homeBloc.add(SearchEvent(search: searchTextController.text));
    });

    searchFocusNode.addListener(() {
      if (!searchFocusNode.hasFocus) {
        homeBloc.add(ChangeSearchWidgetEvent(false));
      }
    });

    homeBloc = BlocProvider.of<HomeBloc>(context);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 56,
      color: Colors.white,
      child: Stack(
        children: [
          Positioned(
            left: 0,
            right: 56,
            top: 0,
            bottom: 0,
            child: BlocListener<HomeBloc, HomeState>(
              listener: (context, state) {
                homeBloc.add(InitEvent());
              },
              child: SingleChildScrollView(
                  controller: homeBloc.topItemScrollController,
                  scrollDirection: Axis.horizontal,
                  child: BlocBuilder<HomeBloc, HomeState>(
                      bloc: homeBloc,
                      buildWhen: (previous, current) {
                        return current is UpdateCountState;
                      },
                      builder: (context, state) {
                        if (state is UpdateCountState) {
                          UpdateCountState updateCountState = state;
                          return Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              ListViewTopHeaderItem(
                                text: 'All',
                                count: updateCountState.allCount,
                                isSelected: updateCountState.selectedList == 1,
                                onTap: () {
                                  homeBloc.add(UpdateListTypeEvent(selectedItem: 1));
                                },
                              ),
                              ListViewTopHeaderItem(
                                text: 'Moving',
                                count: updateCountState.movingCount,
                                isSelected: updateCountState.selectedList == 2,
                                onTap: () {
                                  homeBloc.add(UpdateListTypeEvent(selectedItem: 2));
                                },
                              ),
                              ListViewTopHeaderItem(
                                text: 'Idle',
                                count: updateCountState.idleCount,
                                isSelected: updateCountState.selectedList == 3,
                                onTap: () {
                                  homeBloc.add(UpdateListTypeEvent(selectedItem: 3));
                                },
                              ),
                              ListViewTopHeaderItem(
                                text: 'Stopped',
                                count: updateCountState.stoppedCount,
                                isSelected: updateCountState.selectedList == 4,
                                onTap: () {
                                  homeBloc.add(UpdateListTypeEvent(selectedItem: 4));
                                },
                              ),
                              ListViewTopHeaderItem(
                                text: 'Offline',
                                count: updateCountState.offlineCount,
                                isSelected: updateCountState.selectedList == 5,
                                onTap: () {
                                  homeBloc.add(UpdateListTypeEvent(selectedItem: 5));
                                },
                              ),
                              ListViewTopHeaderItem(
                                text: 'Expired',
                                count: updateCountState.expiredCount,
                                isSelected: updateCountState.selectedList == 6,
                                onTap: () {
                                  homeBloc.add(UpdateListTypeEvent(selectedItem: 6));
                                },
                              ),
                              ListViewTopHeaderItem(
                                text: 'No Data',
                                count: updateCountState.noDataCount,
                                isSelected: updateCountState.selectedList == 7,
                                onTap: () {
                                  homeBloc.add(UpdateListTypeEvent(selectedItem: 7));
                                },
                              ),
                            ],
                          );
                        } else
                          return Container();
                      })),
            ),
          ),
          Positioned(
            right: 0,
            child: BlocBuilder(
              bloc: homeBloc,
              buildWhen: (previous, current) => current is SearchUpdatedState,
              builder: (context, state) {
                bool isSearching = false;
                if (state is SearchUpdatedState) {
                  isSearching = state.showSearch;
                }
                return AnimatedContainer(
                  color: isSearching ? Colors.white : Colors.transparent,
                  duration: Duration(milliseconds: 150),
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  height: 56,
                  width: isSearching ? MediaQuery.of(context).size.width : 56,
                  child: isSearching
                      ? TextField(
                          controller: searchTextController,
                          autofocus: true,
                          focusNode: searchFocusNode,
                          decoration: InputDecoration(
                            contentPadding: EdgeInsets.only(bottom: 0, top: 15),
                            isDense: true,
                            hintText: 'Search',
                            border: InputBorder.none,
                            suffixIcon: IconButton(
                              icon: Icon(Icons.close, color: Colors.grey),
                              onPressed: () {
                                searchTextController.text = '';
                                homeBloc.add(ChangeSearchWidgetEvent(false));
                              },
                            ),
                          ),
                        )
                      : IconButton(
                          splashColor: Colors.blue,
                          onPressed: () {
                            searchTextController.text = '';
                            homeBloc.add(ChangeSearchWidgetEvent(true));
                          },
                          icon: Icon(Icons.search, color: Colors.black),
                        ),
                );
              },
            ),
          ),
          Positioned(left: 0, right: 0, bottom: 0, child: Container(color: Colors.black54, height: 0.1))
        ],
      ),
    );
  }
}
