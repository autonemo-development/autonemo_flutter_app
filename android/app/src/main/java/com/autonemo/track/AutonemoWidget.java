package com.autonemo.track;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.view.View;
import android.widget.RemoteViews;

import androidx.annotation.NonNull;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import es.antonborri.home_widget.HomeWidgetLaunchIntent;
import es.antonborri.home_widget.HomeWidgetProvider;

public class AutonemoWidget extends HomeWidgetProvider {

    @Override
    public void onUpdate(@NonNull Context context, @NonNull AppWidgetManager appWidgetManager, @NonNull int[] appWidgetIds, @NonNull SharedPreferences widgetData) {



        for (int appWidgetId : appWidgetIds) {

            RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.autonemo_widget);

            PendingIntent pendingIntent = HomeWidgetLaunchIntent.INSTANCE.getActivity(context, MainActivity.class, Uri.EMPTY);

            views.setOnClickPendingIntent(R.id.widget_root, pendingIntent);

            String imeiData = widgetData.getString("imei", "[]");

            String deviceName = widgetData.getString("device_name", "");

            if(deviceName.isEmpty()){
                views.setTextViewText(R.id.device_name_text_view, "Please login to get widget data");
                views.setViewVisibility(R.id.data_linear_layout, View.GONE);
                return;
            } else {
                views.setTextViewText(R.id.device_name_text_view, "Daily Stats ("+deviceName+")");
                views.setViewVisibility(R.id.data_linear_layout, View.VISIBLE);
            }


            try {
                JSONArray jsonArray = new JSONArray(imeiData);

                for(int i=0; i<jsonArray.length(); i++){
                    JSONObject itemJsonObject = jsonArray.getJSONObject(i);
                    String name = itemJsonObject.getString("name");
                    String value = itemJsonObject.getString("value");

                    switch (name) {
                        case "Route length" : {
                            views.setTextViewText(R.id.route_length_text_view, value);
                        } break;
                        case "Move duration" : {
                            views.setTextViewText(R.id.move_duration_text_view, value);
                        } break;
                        case "Stop duration" : {
                            views.setTextViewText(R.id.stop_duration_text_view, value);
                        } break;
                        case "Top speed" : {
                            views.setTextViewText(R.id.top_speed_text_view, value);
                        } break;
                        case "Average speed" : {
                            views.setTextViewText(R.id.average_speed_text_view, value);
                        } break;
                        case "Fuel consumption" : {
                            views.setTextViewText(R.id.fuel_cons_text_view, value);
                        } break;
                        case "Avg. fuel cons. (100 km)" : {
                            views.setTextViewText(R.id.avg_fuel_cons_text_view, value);
                        } break;
                        case "Fuel cost" : {
                            views.setTextViewText(R.id.fuel_cost_text_view, value);
                        } break;
                        case "Engine work" : {
                            views.setTextViewText(R.id.engine_work_text_view, value);
                        } break;
                        case "Engine idle" : {
                            views.setTextViewText(R.id.engine_idle_text_view, value);
                        } break;
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

            appWidgetManager.updateAppWidget(appWidgetId, views);

        }
    }
}