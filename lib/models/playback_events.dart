import 'package:google_maps_flutter/google_maps_flutter.dart';

class PlaybackEvents {
  String? object, position, altitude, angle, time, event, speed;
  LatLng? latLng;
  PlaybackEvents({this.angle, this.altitude, this.event, this.speed, this.time, this.object, this.position, this.latLng});
}