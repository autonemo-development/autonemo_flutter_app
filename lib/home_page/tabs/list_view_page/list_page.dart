import 'package:autonemogps/home_page/tabs/list_view_page/list_view/list_view_body.dart';
import 'package:autonemogps/home_page/tabs/list_view_page/top_header/list_view_top_header.dart';
import 'package:flutter/material.dart';

class ListPage extends StatefulWidget {
  @override
  _ListPageState createState() => _ListPageState();
}

class _ListPageState extends State<ListPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Stack(
          children: [
            ListViewTopHeader(),
            ListViewBody(),
          ],
        ),
      ),
    );
  }
}
