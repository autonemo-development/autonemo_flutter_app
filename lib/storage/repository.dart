import 'package:autonemogps/config/string_constants.dart';
import 'package:autonemogps/utils/dateUtilities.dart';
import 'package:home_widget/home_widget.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Repository {
  static String lastEventRefreshTime = '';

  static Future<bool> performLogout() async {
    HomeWidget.saveWidgetData('device_name', '');

    HomeWidget.saveWidgetData('imei', '[]');

    HomeWidget.updateWidget(name: 'AutonemoWidget', androidName: 'AutonemoWidget', iOSName: 'AutonemoWidget');

    String rememberedUsername = getRememberedUsername();
    String rememberedPassword = getRememberedPassword();
    String serverType = getServerType();
    String gpsWoxBaseUrl = await getGPSWoxUrl();
    String gpsServerBaseUrl = await getGPSServerUrl();
    bool isCleared = await prefs?.clear() ?? true;
    setRememberedCredentials(
      username: rememberedUsername,
      password: rememberedPassword,
    );
    setServerType(serverType);
    setGPSWoxUrl(gpsWoxBaseUrl);
    setGPSServerUrl(gpsServerBaseUrl);

    return isCleared;
  }

  static SharedPreferences? prefs;

  static createReference() async {
    if (prefs == null) {
      prefs = await SharedPreferences.getInstance();
    }
  }

  static void setRememberedCredentials({String? username, String? password}) {
    if (username != null) {
      prefs?.setString('rememberedUsername', username);
    }
    if (password != null) {
      prefs?.setString('rememberedPassword', password);
    }
  }

  static String getRememberedUsername() {
    return prefs?.getString('rememberedUsername') ?? '';
  }

  static String getRememberedPassword() {
    return prefs?.getString('rememberedPassword') ?? '';
  }

  static bool isLoggedIn() {
    return (getCookie().isNotEmpty);
  }

  static void setUsername(String username) {
    prefs?.setString('Username', username);
  }

  static String getUsername() {
    return prefs?.getString('Username') ?? '';
  }

  static void setPswd(String pswd) {
    prefs?.setString('Password', pswd);
  }

  static String getPswd() {
    return prefs?.getString('Password') ?? '';
  }

  static void setAllDevicesData(String s) {
    prefs?.setString('AllDevicesData', s);
  }

  static String getAllDevicesData() {
    return prefs?.getString('AllDevicesData') ?? '';
  }

  static void setTrackingData(String s) {
    prefs?.setString('TrackingData', s);
  }

  static String getTrackingData() {
    return prefs?.getString('TrackingData') ?? '';
  }

  static void setAlerts(String s) {
    prefs?.setString('Alerts', s);
  }

  static String getAlerts() {
    return prefs?.getString('Alerts') ?? '';
  }

  static String getLastRefreshedEventTime() {
    return prefs?.getString('LastRefreshedEventTime') ?? '';
  }

  static void setLastRefreshedEventTime() {
    DateTime now = new DateTime.now();
    prefs?.setString('LastRefreshedEventTime', dateTimeInput.format(now));
  }

  static void setVehicleIcon(String imei, String img) {
    prefs?.setString('vehicle_' + imei, img);
  }

  static String getVehicleIcon(String imei) {
    return prefs?.getString('vehicle_' + imei) ?? 'Car';
  }

  static void setPopupDate(String pop_up_date) {
    prefs?.setString('pop_up_date', pop_up_date);
  }

  static String getPopupDate() {
    return prefs?.getString('pop_up_date') ?? '';
  }

  static String getParkingNotificationIMEIs() {
    return prefs?.getString('parkingViolationIMEI') ?? '';
  }

  static setParkingNotificationIMEIs(String s) {
    prefs?.setString('parkingViolationIMEI', s);
  }

  static String getParkingNotificationNames() {
    return prefs?.getString('parkingViolationNames') ?? '';
  }

  static setParkingNotificationNames(String s) {
    prefs?.setString('parkingViolationNames', s);
  }

  static bool getNotificationStatus() {
    return prefs?.getBool('isNotificationEnabled') ?? true;
  }

  static setNotificationStatus(bool s) {
    prefs?.setBool('isNotificationEnabled', s);
  }

  static String getServerType() {
    return prefs?.getString('selectedServer') ?? GPSServerConstants.type;
  }

  static void setServerType(String type) {
    prefs?.setString('selectedServer', type);
  }

  static Future<String> getGPSWoxUrl() async {
    if (prefs == null) {
      await createReference();
    }
    return prefs?.getString('gspWoxServerUrl') ?? '';
  }

  static void setGPSWoxUrl(String url) {
    prefs?.setString('gspWoxServerUrl', url);
  }

  static Future<String> getGPSServerUrl() async {
    if (prefs == null) {
      await createReference();
    }
    return prefs?.getString('gpsServerUrl') ?? AutonemoServerConstants.BASE_URL;
  }

  static void setGPSServerUrl(String url) {
    prefs?.setString('gpsServerUrl', url);
  }

  static void setCookie(String cookie) {
    prefs?.setString('cookie', cookie);
  }

  static String getCookie() {
    return prefs?.getString('cookie') ?? '';
  }

  static void saveWidgetOrder(String string) {
    prefs?.setString('widget_order', string);
  }

  static String getWidgetList() {
    return prefs?.getString('widget_order') ?? '';
  }
}
