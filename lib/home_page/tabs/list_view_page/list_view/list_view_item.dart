import 'package:autonemogps/config/colors.dart';
import 'package:autonemogps/config/helpers.dart';
import 'package:autonemogps/config/string_constants.dart';
import 'package:autonemogps/models/device_model.dart';
import 'package:autonemogps/models/tracking_device_model.dart';
import 'package:autonemogps/models/vehicle_status_mode.dart';
import 'package:autonemogps/storage/repository.dart';
import 'package:flutter/material.dart';

class ListViewItem extends StatelessWidget {
  final TrackingDevice trackingDevice;
  final DeviceModel deviceModel;

  ListViewItem(this.trackingDevice, this.deviceModel);

  @override
  Widget build(BuildContext context) {
    String speed = trackingDevice.speed.toString();
    VehicleStatusModel vehicleStatusModel = getVehicleStatus(trackingDevice, deviceModel);
    String imgName = getVehicleMarkerData(vehicleStatusModel.expiryDate == 'Expired' ? 'N/A' : trackingDevice.status!, deviceModel.icon)['iconKey'];

    void Function() onTap = () {
      if (vehicleStatusModel.expiryDate == 'Expired') {
        showDialog(
          context: context,
          builder: (context) => AlertDialog(
            title: Text('Device Expired'),
            content: Text('Renew this device to continue using it.'),
            actions: [TextButton(onPressed: () => Navigator.pop(context), child: Text('OK'))],
          ),
        );
      } else if (trackingDevice.status == 'NA' || (trackingDevice.lat == 0 && trackingDevice.lng == 0)) {
        showDialog(
          context: context,
          builder: (context) => AlertDialog(
            title: Text('Device just Installed ?'),
            content: Text('Device Location not Updated yet, please drive your vehicle for 10 Minutes under open Sky until location is updated.'),
            actions: [TextButton(onPressed: () => Navigator.pop(context), child: Text('OK'))],
          ),
        );
      } else {
        showVehicleBottomSheet(context, trackingDevice, deviceModel);
      }
    };

    return TextButton(
      onPressed: onTap,
      child: Container(
          padding: EdgeInsets.symmetric(horizontal: 5, vertical: 2),
          child: Row(
            children: [
              Column(
                children: [
                  Text.rich(
                      TextSpan(
                        children: [
                          TextSpan(text: speed, style: TextStyle(decoration: TextDecoration.none, color: vehicleStatusModel.statusColor, fontSize: 28, fontWeight: FontWeight.bold)),
                          TextSpan(text: '\nKM/H', style: TextStyle(decoration: TextDecoration.none, color: vehicleStatusModel.statusColor, fontSize: 8, fontWeight: FontWeight.w500)),
                        ],
                      ),
                      textAlign: TextAlign.center),
                  Transform.rotate(angle: 0, child: Image.asset(imgName, width: 35)),
                ],
              ),
              SizedBox(width: 10),
              Expanded(
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: Colors.white,
                    boxShadow: [BoxShadow(color: AppColors.shadowMaterialColor[100]!, blurRadius: 5.0, spreadRadius: 1.0, offset: Offset(1.0, 1.0))],
                  ),
                  padding: EdgeInsets.all(10),
                  child: Column(
                    children: [
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Expanded(child: Text(deviceModel.name.toUpperCase(), style: TextStyle(fontSize: 16, color: Colors.grey[700], fontWeight: FontWeight.w600))),
                          ...buildFirstThreeSensor(),
                        ],
                      ),
                      Divider(),
                      IntrinsicHeight(
                        child: Row(
                          children: [
                            if (Repository.getServerType() == GPSServerConstants.type)
                              Flexible(
                                flex: 2,
                                fit: FlexFit.tight,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(vehicleStatusModel.status, style: TextStyle(color: Colors.grey, fontSize: 10)),
                                    Text(trackingDevice.statusDuration ?? 'Not Connected Yet', style: TextStyle(color: vehicleStatusModel.statusColor, fontSize: 10))
                                  ],
                                ),
                              ),
                            Expanded(
                              flex: 2,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: [
                                  Text(vehicleStatusModel.expiryDateText, style: TextStyle(color: Colors.grey, fontSize: 10)),
                                  Container(
                                    padding: EdgeInsets.symmetric(horizontal: 5, vertical: 1),
                                    decoration: BoxDecoration(color: vehicleStatusModel.expiryColor, borderRadius: BorderRadius.all(Radius.circular(10))),
                                    child: Text(vehicleStatusModel.expiryDate, textAlign: TextAlign.center, style: TextStyle(color: Colors.white, fontSize: 10)),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          )),
    );
  }

  List<Container> buildFirstThreeSensor() => getFirstThreeSensor(trackingDevice)
      .map((sensorItem) => Container(
            width: 50,
            child: Column(
              children: [
                Image.asset(sensorItem.imageName!, color: sensorItem.sensorColor, width: 15, height: 15),
                SizedBox(height: 5),
                Text(sensorItem.sensorName!, style: TextStyle(color: Colors.grey, fontSize: 10)),
              ],
            ),
          ))
      .toList();
}
