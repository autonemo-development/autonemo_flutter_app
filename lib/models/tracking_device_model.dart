import 'dart:collection';
import 'dart:convert';

import 'package:autonemogps/config/helpers.dart';
import 'package:autonemogps/config/string_constants.dart';
import 'package:autonemogps/models/device.dart';
import 'package:autonemogps/models/device_model.dart';
import 'package:autonemogps/models/sensor_model.dart';
import 'package:autonemogps/storage/repository.dart';
import 'package:autonemogps/utils/logs.dart';
import 'package:intl/intl.dart';

class TrackingDevice {
  String? imei, deviceTime, serverTime, altitude, status, engineHrs, statusDuration, address;
  bool? gps, power, cutoff;
  double? lat, lng, voltageLevel;
  int? angle, gsmSignal;
  int? speed;
  double? odometer;
  List<Sensor>? sensorList;

  Map<String, dynamic>? raw;

  TrackingDevice({
    this.altitude,
    this.lat,
    this.lng,
    this.speed,
    this.status,
    this.deviceTime,
    this.serverTime,
    this.angle,
    this.imei,
    this.power,
    this.gps,
    this.cutoff,
    this.engineHrs,
    this.odometer,
    this.statusDuration,
    this.address,
    this.gsmSignal,
    this.sensorList,
    this.voltageLevel,
    this.raw,
  });

  factory TrackingDevice.fromGpsServerJson(Map<String, dynamic>? fnObject, Map<String, dynamic> fnSettingSensor, String imei) {
    // writeDebugLogs('fromGpsServerJson ' + fnObject.toString());

    List dArray = [];

    String status = '', statusStr = '';
    int connection = 0;
    int engineHrs = 0;
    int odometer = 0;
    bool gps = false;
    int gsmSignal = 0;
    String deviceTime = '', serverTime = '';
    Map<String, dynamic> sensorData = {};
    String statusDuration = '';

    if (fnObject != null) {
      if (fnObject.containsKey('location')) {
        dArray = fnObject['location'];
      }
      if (fnObject.containsKey('status')) {
        status = fnObject['status'];
      }
      statusStr = fnObject['status_string'];
      odometer = fnObject['odometer'];
      engineHrs = fnObject['engine_hours'];
      connection = fnObject['connection'];
    }

    Map<String, dynamic> dataMap = {};

    if (dArray.length > 0) {
      dataMap = dArray.elementAt(0);
      deviceTime = dataMap['dt_tracker'];
      serverTime = dataMap['dt_server'];
      deviceTime = getFormattedDateFromString(deviceTime, STANDARD_DATE_TIME_INPUT_PATTERN, STANDARD_DATE_TIME_OUTPUT_PATTERN);
      serverTime = getFormattedDateFromString(serverTime, STANDARD_DATE_TIME_INPUT_PATTERN, STANDARD_DATE_TIME_OUTPUT_PATTERN);
      sensorData = dataMap['params'];
    }

    switch (connection) {
      case 0:
        gps = false;
        gsmSignal = 0;
        break;
      case 1:
        gps = false;
        gsmSignal = 1;
        break;
      case 2:
        gps = true;
        gsmSignal = 1;
        break;
    }

    switch (status) {
      case '':
        statusDuration = '';
        break;
      case 'i':
        statusDuration = (statusStr).split(' ').sublist(2).join(' ');
        break;
      default:
        statusDuration = (statusStr).split(' ').sublist(1).join(' ');
        break;
    }

    return TrackingDevice(
      imei: imei,
      gps: gps,
      gsmSignal: gsmSignal,
      status: status,
      statusDuration: statusDuration,
      odometer: odometer.toDouble(),
      engineHrs: engineHrs.toString(),
      lat: double.parse(dataMap.length > 0 ? dataMap['lat'] : '0'),
      lng: double.parse(dataMap.length > 0 ? dataMap['lng'] : '0'),
      speed: dataMap.length > 0 ? dataMap['speed'] : 0,
      altitude: dataMap.length > 0 ? dataMap['altitude'].toString() : '',
      angle: dataMap.length > 0 ? dataMap['angle'] : 0,
      deviceTime: deviceTime,
      serverTime: serverTime,
      sensorList: buildSensorData(fnSettingSensor, sensorData),
      raw: fnObject,
    );
  }

  factory TrackingDevice.fromGPSWoxServerJson(Map<String, dynamic> json) {
    Map<String, dynamic> deviceData = json['device_data'] as Map<String, dynamic>;
    String imei = deviceData['imei'] as String;
    String online = json['online'] as String;
    String status = getGPSWoxStatus(json['icon_color'] as String, json['icon_colors'] as Map<String, dynamic>);
    bool gps = false;
    int gsmSignal = 0;
    switch (online) {
      case 'offline':
        gps = false;
        gsmSignal = 0;
        break;
      case 'online':
        gps = true;
        gsmSignal = 1;
        break;
      case 'ack':
        gps = false;
        gsmSignal = 1;
    }

    DateFormat inputFormat = DateFormat('yyyy-MM-dd hh:mm:ss a');
    DateFormat outputFormat = DateFormat('dd-MMM-yy hh:mm:ss a');

    String time = json['time'] as String;
    String serverTime = time;
    DateTime? date1;
    try {
      date1 = inputFormat.parse(time);
      serverTime = outputFormat.format(date1);
    } on FormatException catch (e) {
      writeErrorLogs('Error ', e: e);
    }

    return TrackingDevice(
        imei: imei,
        gps: gps,
        gsmSignal: gsmSignal,
        status: status,
        statusDuration: 'N/A',
        odometer: json['total_distance'].runtimeType == int ? (json['total_distance'] as int).toDouble() : json['total_distance'] as double,
        engineHrs: (json['eh'] ?? '0') as String,
        lat: json['lat'].runtimeType == int ? (json['lat'] as int).toDouble() : json['lat'] as double,
        lng: json['lng'].runtimeType == int ? (json['lng'] as int).toDouble() : json['lng'] as double,
        speed: json['speed'] as int,
        altitude: (json['altitude'] as int).toString(),
        angle: json['course'] as int,
        deviceTime: serverTime,
        serverTime: serverTime,
        sensorList: parseGpsWoxSensorData(json['sensors'] ?? []),
        raw: json);
  }

  static String getGPSWoxStatus(String iconColor, Map<String, dynamic> iconColors) {
    String? status;
    iconColors.forEach((key, value) {
      if (iconColor == (value as String)) {
        status = key;
      }
    });
    switch (status) {
      case 'moving':
        return 'm';
      case 'offline':
        return 'off';
      case 'stopped':
        return 's';
      case 'engine':
        return 'i';
      default:
        return 'off';
    }
  }
}

class AllTrackingData {
  late Map<String, TrackingDevice> trackingDeviceImeiMap;
  static final AllTrackingData _singleton = new AllTrackingData._internal();

  factory AllTrackingData() {
    return _singleton;
  }

  AllTrackingData._internal() {
    trackingDeviceImeiMap = HashMap<String, TrackingDevice>();
  }
}

Map<String, TrackingDevice> parseTrackingData(String trackingData) {
  // writeDebugLogs('Tracking Data ' + trackingData);

  try {
    Map<String, dynamic> data = json.decode(trackingData) as Map<String, dynamic>;

    if (Repository.getServerType() == GPSServerConstants.type) {
      Map<String, dynamic> fnObjectData = data['data'];

      // writeDebugLogs('fnObjectData ' + fnObjectData.toString());

      // writeDebugLogs('parseTrackingData AllDeviceData ' + AllDeviceModelData().deviceModelAndImeiMapGpsServer.length.toString());
      // writeDebugLogs('parseTrackingData fnObjectData ' + fnObjectData.length.toString());

      AllDeviceModelData().deviceModelAndImeiMapGpsServer.forEach((String imei, DeviceModel deviceModel) {
        try {
          Map<String, dynamic> sensors = {};
          sensors = deviceModel.sensorJsonObject;
          var dynamicData = fnObjectData[imei];
          TrackingDevice currentTrackingDevice = TrackingDevice.fromGpsServerJson(dynamicData, sensors, imei);
          AllTrackingData().trackingDeviceImeiMap.update(imei, (oldValue) => currentTrackingDevice, ifAbsent: () => currentTrackingDevice);
        } on Exception catch (e) {
          writeErrorLogs('parseTrackingData Inner', e: e);
        }
      });

      fnObjectData.forEach((imei, dynamicData) {});
    } else if (Repository.getServerType() == GPSWoxConstants.type) {
      List itemsList = data['items'] as List;
      GPSWoxConstants.gpsWoxLastFetchTimeStamp = (data['time'] as int).toString();
      itemsList.forEach((element) {
        DeviceModel deviceModel = DeviceModel.fromGPSWoxServerJson(element as Map<String, dynamic>);
        AllDeviceModelData().deviceModelAndImeiMapGpsServer.putIfAbsent(deviceModel.imei, () => deviceModel);
        TrackingDevice currentTrackingDevice = TrackingDevice.fromGPSWoxServerJson(element);
        AllTrackingData().trackingDeviceImeiMap.update(deviceModel.imei, (oldValue) => currentTrackingDevice, ifAbsent: () => currentTrackingDevice);
      });
    }
  } catch (e) {
    writeErrorLogs('parseTrackingData Outer', e: e);
  }

  return AllTrackingData().trackingDeviceImeiMap;
}

Map<String, List<String>> getSortedImeiList(String searchString) {
  Map<String, List<String>> returningList = Map<String, List<String>>();

  Map<String, TrackingDevice> trackingList = AllTrackingData().trackingDeviceImeiMap;
  Map<String, DeviceModel>? deviceList = AllDeviceModelData().deviceModelAndImeiMapGpsServer;

  List<String> imeiList = List<String>.empty(growable: true);

  List<String> movingList = List<String>.empty(growable: true);
  List<String> idleList = List<String>.empty(growable: true);
  List<String> offlineList = List<String>.empty(growable: true);
  List<String> stoppedList = List<String>.empty(growable: true);
  List<String> expiredList = List<String>.empty(growable: true);
  List<String> toBeExpiredList = List<String>.empty(growable: true);
  List<String> noConnectionList = List<String>.empty(growable: true);

  deviceList.forEach((key, deviceModel) {
    TrackingDevice? value = trackingList[key];

    if (searchString.isEmpty || (deviceModel.name).toLowerCase().contains(searchString.toLowerCase()) || (deviceModel.imei).toLowerCase().contains(searchString.toLowerCase())) {
      String? expiryDate = deviceModel.expiry;
      if (expiryDate == 'Invalid date' || expiryDate == '0000-00-00') {
        expiryDate = '2029-01-01';
      }

      DateTime date = new DateFormat('yyyy-MM-dd').parse(expiryDate);
      Duration expiryDuration = date.difference(DateTime.now());
      if (expiryDuration.inDays < 7) {
        toBeExpiredList.add(key);
      }

      if (expiryDuration.inSeconds < 1) {
        expiredList.add(key);
      } else if (value == null) {
        noConnectionList.add(key);
      } else {
        switch (value.status) {
          case 'off':
            offlineList.add(key);
            break;
          case 'm':
            movingList.add(key);
            break;
          case 'NA':
          case '':
            noConnectionList.add(key);
            break;
          case 'i':
            idleList.add(key);
            break;
          case 's':
            stoppedList.add(key);
            break;
        }
      }
    }
  });
  imeiList.addAll(movingList);
  imeiList.addAll(idleList);
  imeiList.addAll(stoppedList);
  imeiList.addAll(offlineList);
  imeiList.addAll(noConnectionList);
  imeiList.addAll(expiredList);

  returningList.putIfAbsent('all', () => imeiList);
  returningList.putIfAbsent('moving', () => movingList);
  returningList.putIfAbsent('offline', () => offlineList);
  returningList.putIfAbsent('idle', () => idleList);
  returningList.putIfAbsent('na', () => noConnectionList);
  returningList.putIfAbsent('stopped', () => stoppedList);
  returningList.putIfAbsent('expired', () => expiredList);
  returningList.putIfAbsent('toBeExpiredList', () => toBeExpiredList);

  return returningList;
}
