import 'package:flutter/material.dart';

class CustomBackButton extends StatelessWidget {
  const CustomBackButton({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.pop(context);
      },
      child: Container(
        height: 40,
        width: 40,
        decoration: BoxDecoration(color: Colors.black54, borderRadius: BorderRadius.circular(5)),
        child: Icon(Icons.arrow_back, color: Colors.white),
      ),
    );
  }
}
