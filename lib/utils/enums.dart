enum SettingUpdateType{
  ODOMETER,
  ENGINE_HOURS,
  FUEL_COST,
  MILEAGE,
  NAME,
  ICON,
}