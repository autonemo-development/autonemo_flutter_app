import 'package:autonemogps/api_calls/gps_wox_requests.dart';
import 'package:autonemogps/common/custom_text_form_field.dart';
import 'package:autonemogps/common/dropdown_field.dart';
import 'package:autonemogps/config/colors.dart';
import 'package:autonemogps/config/helpers.dart';
import 'package:autonemogps/models/device.dart';
import 'package:autonemogps/models/device_model.dart';
import 'package:autonemogps/models/tracking_device_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class AddMaintenanceGPSWoxPage extends StatefulWidget {
  final Map<String, dynamic>? maintenance;

  const AddMaintenanceGPSWoxPage({this.maintenance, Key? key}) : super(key: key);

  @override
  State<AddMaintenanceGPSWoxPage> createState() => _AddMaintenanceGPSWoxPageState();
}

class _AddMaintenanceGPSWoxPageState extends State<AddMaintenanceGPSWoxPage> {
  bool autoRenew = false;
  Map<String, String> deviceNames = Map<String, String>();
  int selectedDeviceId = 0;
  String selectedDeviceImei = '';
  String selectedValue = '--Select Device--';
  Map<String, String> expirationByMap = {'': 'Expiration By', 'odometer': 'Odometer', 'engine_hours': 'Engine Hours', 'days': 'Days'};
  String expirationBy = '';
  bool _isMaintenanceSubmitting = false;

  TextEditingController _currentOdometerController = new TextEditingController();
  TextEditingController _currentEngineHoursController = new TextEditingController();

  TextEditingController _nameController = new TextEditingController();
  TextEditingController _intervalController = new TextEditingController();
  TextEditingController _lastServiceController = new TextEditingController();
  TextEditingController _triggerEventController = new TextEditingController();
  TextEditingController _emailController = new TextEditingController();
  TextEditingController _mobilePhoneController = new TextEditingController();

  Map<String, List> deviceData = {};

  @override
  void initState() {
    super.initState();
    fetchAllVehicleNames();
  }

  fetchAllVehicleNames() async {
    Map<String, DeviceModel> map = AllDeviceModelData().deviceModelAndImeiMapGpsServer;
    deviceNames.putIfAbsent('', () => '--Select Device--');

    map.forEach((imei, deviceModel) {
      deviceNames.putIfAbsent(imei, () => deviceModel.name);
      deviceData[imei] = [deviceModel, AllTrackingData().trackingDeviceImeiMap[imei]];
    });
    if (mounted) {
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          height: MediaQuery.of(context).size.height,
          decoration: BoxDecoration(
            gradient: LinearGradient(begin: Alignment.topCenter, end: Alignment.bottomCenter, colors: [
              Color(0x90DC0E18),
              Color(0x10DC0E18),
            ]),
          ),
          child: Column(
            children: [
              SizedBox(height: 10),
              Row(
                children: [
                  IconButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      icon: Icon(
                        Icons.keyboard_backspace,
                        color: Colors.white,
                      )),
                  Expanded(
                    child: Text(
                      widget.maintenance == null ? 'Add Maintenance' : 'Edit Maintenance',
                      style: TextStyle(color: Colors.white, fontSize: 24, fontWeight: FontWeight.bold),
                    ),
                  ),
                ],
              ),
              Expanded(
                child: _isMaintenanceSubmitting
                    ? SpinKitRing(
                        lineWidth: 3.0,
                        color: AppColors.primaryColor,
                        size: 35.0,
                      )
                    : SingleChildScrollView(
                        child: Container(
                          padding: EdgeInsets.all(15),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: [
                              Container(
                                padding: EdgeInsets.all(15.0),
                                decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(16.0), boxShadow: [
                                  BoxShadow(
                                    color: AppColors.primaryMaterialColor[100]!,
                                    blurRadius: 5.0,
                                    spreadRadius: 1.0,
                                    offset: Offset(
                                      1.0, // Move to right 10  horizontally
                                      2.0, // Move to bottom 5 Vertically
                                    ),
                                  )
                                ]),
                                child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                                  Text(
                                    'SERVICE',
                                    style: TextStyle(color: Color(0xFFBDBDBD), fontSize: 17, fontWeight: FontWeight.w600),
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  CustomTextFormField(
                                    controller: _nameController,
                                    hinText: 'Name',
                                    boardColor: Color(0xFF0060A4),
                                    onChanged: (value) {},
                                  ),
                                  SizedBox(
                                    height: 20,
                                  ),
                                  DropDownFromField(
                                    list: deviceNames.values.toList(),
                                    selectedValue: selectedValue,
                                    onChanged: (value) {
                                      setState(() {
                                        selectedValue = value;
                                        selectedDeviceImei = deviceNames.keys.firstWhere((k) => deviceNames[k] == value, orElse: () => '');
                                        selectedDeviceId = (deviceData[selectedDeviceImei]![0] as DeviceModel).deviceId;

                                        _currentOdometerController.text = (deviceData[selectedDeviceImei]![1] as TrackingDevice).odometer.toString();
                                        _currentEngineHoursController.text = (deviceData[selectedDeviceImei]![1] as TrackingDevice).engineHrs!;
                                      });
                                    },
                                  ),
                                  SizedBox(
                                    height: 20,
                                  ),
                                  DropDownFromField(
                                    list: expirationByMap.values.toList(),
                                    selectedValue: expirationByMap[expirationBy]!,
                                    onChanged: (value) {
                                      setState(() {
                                        expirationBy = expirationByMap.keys.firstWhere((k) => expirationByMap[k] == value, orElse: () => '');
                                      });
                                    },
                                  ),
                                  SizedBox(
                                    height: 20,
                                  ),
                                  CustomTextFormField(
                                    controller: _intervalController,
                                    hinText: 'Interval',
                                    boardColor: Color(0xFFF26611),
                                    onChanged: (value) {},
                                  ),
                                  SizedBox(
                                    height: 20,
                                  ),
                                  CustomTextFormField(
                                    controller: _lastServiceController,
                                    hinText: 'Last Service',
                                    boardColor: Color(0xFF0060A4),
                                    onChanged: (value) {},
                                  ),
                                  SizedBox(
                                    height: 20,
                                  ),
                                  CustomTextFormField(
                                    controller: _triggerEventController,
                                    hinText: 'Trigger Event When Left',
                                    boardColor: Color(0xFFF26611),
                                    onChanged: (value) {},
                                  ),
                                  SwitchItem(
                                    title: 'Renew after expiration',
                                    activeColor: Color(0xFF0060A4),
                                    onTap: (value) {
                                      setState(() {
                                        autoRenew = value;
                                      });
                                    },
                                    switchControl: autoRenew,
                                  ),
                                ]),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Container(
                                padding: EdgeInsets.all(15.0),
                                decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(16.0), boxShadow: [
                                  BoxShadow(
                                    color: AppColors.primaryMaterialColor[100]!,
                                    blurRadius: 5.0,
                                    spreadRadius: 1.0,
                                    offset: Offset(
                                      1.0, // Move to right 10  horizontally
                                      2.0, // Move to bottom 5 Vertically
                                    ),
                                  )
                                ]),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      'CURRENT OBJECTS COUNTERS',
                                      style: TextStyle(color: Color(0xFFBDBDBD), fontSize: 17, fontWeight: FontWeight.w600),
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    CustomTextFormField(
                                      controller: _currentOdometerController,
                                      hinText: 'Current Odometer (km)',
                                      boardColor: Color(0xFFF26611),
                                      isEnabled: false,
                                      onChanged: (value) {},
                                    ),
                                    SizedBox(
                                      height: 20,
                                    ),
                                    CustomTextFormField(
                                      controller: _currentEngineHoursController,
                                      hinText: 'Current Engine Hours (h)',
                                      isEnabled: false,
                                      boardColor: Color(0xFF0060A4),
                                      onChanged: (value) {},
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(
                                height: 10.0,
                              ),
                              Container(
                                padding: EdgeInsets.all(15.0),
                                decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(16.0), boxShadow: [
                                  BoxShadow(
                                    color: AppColors.primaryMaterialColor[100]!,
                                    blurRadius: 5.0,
                                    spreadRadius: 1.0,
                                    offset: Offset(
                                      1.0, // Move to right 10  horizontally
                                      2.0, // Move to bottom 5 Vertically
                                    ),
                                  )
                                ]),
                                child: Column(
                                  children: [
                                    CustomTextFormField(
                                      controller: _emailController,
                                      hinText: 'Email',
                                      boardColor: Color(0xFFF26611),
                                      onChanged: (value) {},
                                    ),
                                    SizedBox(
                                      height: 20,
                                    ),
                                    CustomTextFormField(
                                      controller: _mobilePhoneController,
                                      hinText: 'Mobile Phone',
                                      boardColor: Color(0xFF0060A4),
                                      onChanged: (value) {},
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(
                                height: 10.0,
                              ),
                              InkWell(
                                onTap: () {
                                  if (_nameController.text.isEmpty) {
                                    showSnackBar(context, 'Please Enter Maintenance Name', Colors.red);
                                  } else if (selectedDeviceId == 0) {
                                    showSnackBar(context, 'Please select Object Name', Colors.red);
                                  } else if (expirationBy.isEmpty) {
                                    showSnackBar(context, 'Please select Expiration By', Colors.red);
                                  } else if (_intervalController.text.isEmpty) {
                                    showSnackBar(context, 'Please enter Interval', Colors.red);
                                  } else if (_lastServiceController.text.isEmpty) {
                                    showSnackBar(context, 'Please enter Last Service Date', Colors.red);
                                  } else if (_triggerEventController.text.isEmpty) {
                                    showSnackBar(context, 'Please enter Trigger event when left value', Colors.red);
                                  } else {
                                    setState(() {
                                      _isMaintenanceSubmitting = true;
                                    });

                                    GPSWoxRequests.saveMaintenance(
                                      selectedDeviceId,
                                      _nameController.text,
                                      expirationBy,
                                      _intervalController.text,
                                      _lastServiceController.text,
                                      _triggerEventController.text,
                                      autoRenew ? 1 : 0,
                                      _emailController.text,
                                      _mobilePhoneController.text,
                                    ).then((result) {
                                      setState(() {
                                        _isMaintenanceSubmitting = false;
                                      });
                                      if (result.runtimeType == bool) {
                                        if (result as bool) {
                                          Navigator.pop(context, true);
                                        } else {
                                          showSnackBar(context, 'Failed to submit maintenance', Colors.red);
                                        }
                                      } else if (result.runtimeType == String) {
                                        showSnackBar(context, result, Colors.red);
                                      }
                                    }).catchError((e) {
                                      setState(() {
                                        _isMaintenanceSubmitting = false;
                                      });
                                      showSnackBar(context, e.toString(), Colors.red);
                                    });
                                  }
                                },
                                child: Container(
                                  height: 48,
                                  padding: EdgeInsets.all(12.0),
                                  decoration: BoxDecoration(
                                    color: AppColors.primaryColor,
                                    borderRadius: BorderRadius.circular(24.0),
                                  ),
                                  child: Center(
                                      child: Text(
                                    'Submit',
                                    style: TextStyle(color: Colors.white, fontWeight: FontWeight.w600),
                                  )),
                                ),
                              ),
                              SizedBox(
                                height: 10.0,
                              ),
                              InkWell(
                                onTap: () {
                                  Navigator.pop(context);
                                },
                                child: Container(
                                  height: 48,
                                  padding: EdgeInsets.all(12.0),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(24.0),
                                    border: Border.all(color: AppColors.primaryColor2, width: 1.5),
                                  ),
                                  child: Center(
                                      child: Text(
                                    'Cancel',
                                    style: TextStyle(color: AppColors.primaryColor2, fontWeight: FontWeight.w600),
                                  )),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class SwitchItem extends StatelessWidget {
  final String title;
  final bool switchControl;
  final void Function(bool) onTap;
  final Color activeColor;

  SwitchItem({required this.title, required this.activeColor, required this.onTap, required this.switchControl});

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Switch(
          onChanged: onTap,
          value: switchControl,
          activeColor: activeColor,
          activeTrackColor: activeColor.withOpacity(.4),
          inactiveThumbColor: Color(0xFF7E7E7E),
          inactiveTrackColor: Color(0xFFD6D6D6),
        ),
        SizedBox(
          width: 5,
        ),
        Text(
          title,
          style: TextStyle(color: Color(0xFF474747), fontSize: 15, fontWeight: FontWeight.w400),
        ),
      ],
    );
  }
}
