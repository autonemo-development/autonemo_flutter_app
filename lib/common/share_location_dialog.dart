import 'dart:io';
import 'dart:math';

import 'package:animations/animations.dart';
import 'package:autonemogps/api_calls/gps_server_requests.dart';
import 'package:autonemogps/common/text_form_field_with_datetime.dart';
import 'package:autonemogps/config/colors.dart';
import 'package:flutter/material.dart';
import 'package:share/share.dart';

import '../config/helpers.dart';

showShareLocationDialog(BuildContext context, String date, Function onCanceled, String imei) {
  /// Modify => showDialog to showModal
  showModal(
    context: context,
    builder: (BuildContext context) {
      return Dialog(
        elevation: 0.0,
        backgroundColor: Colors.transparent,
        child: Container(
          padding: EdgeInsets.all(15),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(5),
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text('Share Location', style: TextStyle(fontSize: 15, fontWeight: FontWeight.w500)),
                  InkWell(onTap: () => onCanceled(), child: Image.asset('images/cancel.png', color: Color(0xFFCCCCCC), width: 15.0)),
                ],
              ),
              SizedBox(height: 10.0),
              ShareLocationWidget(date: date, imei: imei)
            ],
          ),
        ),
      );
    },
  );
}

class ShareLocationWidget extends StatefulWidget {
  final String imei, date;

  ShareLocationWidget({Key? key, required this.imei, required this.date}) : super(key: key);

  @override
  _ShareLocationWidgetState createState() => _ShareLocationWidgetState();
}

class _ShareLocationWidgetState extends State<ShareLocationWidget> with TickerProviderStateMixin {
  late AnimationController smsButtonAnimationController;
  late AnimationController shareButtonAnimationController;
  late AnimationController generateButtonAnimationController;

  String? link;
  late TextEditingController dateTextEditingController;
  bool isLinkGenerating = false;

  @override
  void initState() {
    super.initState();
    smsButtonAnimationController = AnimationController(duration: const Duration(milliseconds: 150), vsync: this);
    shareButtonAnimationController = AnimationController(duration: const Duration(milliseconds: 150), vsync: this);
    generateButtonAnimationController = AnimationController(duration: const Duration(milliseconds: 150), vsync: this);
    dateTextEditingController = TextEditingController(text: widget.date);
  }

  final String _chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
  Random _rnd = Random();

  String getRandomString(int length) => String.fromCharCodes(Iterable.generate(length, (_) => _chars.codeUnitAt(_rnd.nextInt(_chars.length))));

  @override
  Widget build(BuildContext context) {
    return link == null
        ? Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Align(
                  alignment: Alignment.centerLeft,
                  child: Text('Share Location till', style: TextStyle(color: Color(0xFF6A6A6A), fontSize: 11)),
                ),
                SizedBox(height: 5.0),
                CustomTextFormFieldWithDataTime(controller: dateTextEditingController, onChanged: (value) {}),
                SizedBox(height: 10.0),
                InkWell(
                  onTap: () {
                    generateButtonAnimationController.forward();
                    setState(() => isLinkGenerating = true);
                    String id = getRandomString(32);
                    GPSServerRequests.generateLink(widget.imei, dateTextEditingController.text, id).then(
                      (value) async {
                        if (mounted) {
                          String baseUrl = await GPSServerRequests.getBaseUrl();
                          setState(() {
                            isLinkGenerating = false;
                            link = '$baseUrl/index.php?su=' + id + '&m=true';
                          });
                        }
                      },
                    );
                  },
                  child: isLinkGenerating
                      ? SizedBox(height: 30, width: 30, child: CircularProgressIndicator(color: AppColors.primaryColor, strokeWidth: 2))
                      : ScaleTransition(
                          scale: Tween(begin: 1.0, end: .9).animate(CurvedAnimation(parent: generateButtonAnimationController, curve: Curves.bounceIn))
                            ..addStatusListener((status) {
                              if (status == AnimationStatus.completed) {
                                generateButtonAnimationController.reverse();
                              }
                            }),
                          child: Container(
                            width: MediaQuery.of(context).size.width,
                            padding: EdgeInsets.only(left: 2.0, right: 2.0, top: 10.0, bottom: 10.0),
                            decoration: BoxDecoration(color: Color(0xFF34D87B), border: Border.all(color: Color(0xFF34D87B), width: 1), borderRadius: BorderRadius.circular(5.0)),
                            child: Center(child: Text('Generate The Link', style: TextStyle(decoration: TextDecoration.none, color: Colors.white, fontSize: 16, fontWeight: FontWeight.w500))),
                          ),
                        ),
                )
              ],
            ),
          )
        : Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Text('Generated Link', style: TextStyle(color: Color(0xFF6A6A6A), fontSize: 11)),
                SizedBox(height: 5.0),
                Container(
                  padding: EdgeInsets.all(12.0),
                  decoration: BoxDecoration(color: Color(0xFFF1F1F1), borderRadius: BorderRadius.circular(5.0)),
                  child: SingleChildScrollView(scrollDirection: Axis.horizontal, child: Text(link!, style: TextStyle(color: Color(0xFF212121), fontSize: 15))),
                ),
                SizedBox(height: 10),
                Row(
                  children: [
                    Expanded(
                      child: InkWell(
                        onTap: () {
                          smsButtonAnimationController.forward();
                          String uri;
                          if (Platform.isAndroid) {
                            uri = 'sms:?body=' + link!;
                          } else {
                            uri = 'sms:&body=' + link!;
                          }
                          launchUrl(uri);

                          Navigator.pop(context);
                        },
                        child: ScaleTransition(
                          scale: Tween(begin: 1.0, end: .9).animate(CurvedAnimation(parent: smsButtonAnimationController, curve: Curves.bounceIn))
                            ..addStatusListener((status) {
                              if (status == AnimationStatus.completed) {
                                smsButtonAnimationController.reverse();
                              }
                            }),
                          child: Container(
                            padding: EdgeInsets.only(left: 2.0, right: 2.0, top: 10.0, bottom: 10.0),
                            decoration: BoxDecoration(
                              color: Color(0xFFF26611),
                              border: Border.all(color: Color(0xFFF26611), width: 1),
                              borderRadius: BorderRadius.circular(2.0),
                            ),
                            child: Center(
                                child: Row(
                              mainAxisSize: MainAxisSize.min,
                              //mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Image.asset('images/sms.png', height: 20),
                                SizedBox(width: 10.0),
                                Text('SMS', style: TextStyle(decoration: TextDecoration.none, color: Colors.white, fontSize: 16, fontWeight: FontWeight.w500)),
                              ],
                            )),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(width: 10.0),
                    Expanded(
                      child: InkWell(
                        onTap: () {
                          shareButtonAnimationController.forward();
                          Share.share(link!, subject: 'Share Position');
                          Navigator.pop(context);
                        },
                        child: ScaleTransition(
                          scale: Tween(begin: 1.0, end: .9).animate(CurvedAnimation(parent: shareButtonAnimationController, curve: Curves.bounceIn))
                            ..addStatusListener((status) {
                              if (status == AnimationStatus.completed) {
                                shareButtonAnimationController.reverse();
                              }
                            }),
                          child: Container(
                            padding: EdgeInsets.only(left: 2.0, right: 2.0, top: 10.0, bottom: 10.0),
                            decoration: BoxDecoration(
                              color: Color(0xFF0060A4),
                              border: Border.all(color: Color(0xFF0060A4), width: 1),
                              borderRadius: BorderRadius.circular(2.0),
                            ),
                            child: Center(
                                child: Row(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                Image.asset('images/share.png', height: 20),
                                SizedBox(width: 10.0),
                                Text('Share', style: TextStyle(decoration: TextDecoration.none, color: Colors.white, fontSize: 16, fontWeight: FontWeight.w500)),
                              ],
                            )),
                          ),
                        ),
                      ),
                    ),
                  ],
                )
              ],
            ),
          );
  }
}
