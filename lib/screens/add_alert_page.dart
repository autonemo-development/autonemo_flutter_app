import 'package:autonemogps/api_calls/gps_server_requests.dart';
import 'package:autonemogps/common/dropdown_field.dart';
import 'package:autonemogps/config/colors.dart';
import 'package:autonemogps/config/helpers.dart';
import 'package:autonemogps/config/string_constants.dart';
import 'package:autonemogps/models/device.dart';
import 'package:autonemogps/models/key_value_model.dart';
import 'package:autonemogps/storage/repository.dart';
import 'package:autonemogps/utils/logs.dart';
import 'package:collection/collection.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class AddAlertScreen extends StatefulWidget {
  final Map<String, dynamic>? alertMap;
  final String? eventId;

  AddAlertScreen({this.alertMap, this.eventId});

  @override
  _AddAlertScreenState createState() => _AddAlertScreenState();
}

class _AddAlertScreenState extends State<AddAlertScreen> {
  Map<String, String> deviceNames = {};

  List<KeyValueModel> zoneMap = [];
  List<String> selectedImeis = [];
  Map<String, String> alertTypes = {};

  List<String> sensorTypes = [
    'AC',
    'Altitude',
    'Battery Level',
    'Charging Status',
    'Defence',
    'Defense',
    'Device Battery',
    'Engine Block Status',
    'GPS Level',
    'GPS Signal',
    'GSM Level',
    'Ignition',
    'Moving',
    'Moving Status',
    'Speed'
  ];

  Map<String, String> sensorRelations = {'=': 'eq', '>': 'gr', '<': 'lw', '>%': 'grp', '<%': 'lwp'};

  String selectedAlertType = '';
  List<KeyValueModel> selectedZoneList = [];

  bool enableTimePeriod = false;
  bool enableSpeed = false;
  bool enableDistance = false;
  bool isAllDevicesSelected = true;
  bool isALlZonesSelected = true;

  TextEditingController alertNameController = TextEditingController();

  TextEditingController speedController = TextEditingController();

  bool isSubmitting = false;
  bool isDeleting = false;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      if (Repository.getServerType() == GPSServerConstants.type) {
        prepareGPSServerData();
      }
    });
  }

  prepareGPSServerData() async {
    AllDeviceModelData().deviceModelAndImeiMapGpsServer.forEach((key, value) {
      deviceNames.putIfAbsent(key, () => value.name);
      selectedImeis.add(key);
    });
    alertTypes = {'Overspeed': 'overspeed', 'Zone in': 'zone_in', 'Zone out': 'zone_out'};
    enableSpeed = true;
    speedController.text = '60';
    selectedAlertType = alertTypes.keys.first;

    GPSServerRequests.fetchGeoPlaces().then((list) {
      if (!(list[0] as bool)) {
        return;
      }
      Map<String, dynamic> map = list[1];
      map.forEach((key, value) {
        KeyValueModel model = KeyValueModel(key, value['name']);
        zoneMap.add(model);
        selectedZoneList.add(model);
      });
      if (widget.alertMap != null && widget.alertMap?['zones'].isNotEmpty) {
        selectedZoneList.clear();
        List<String> selectedZoneIds = widget.alertMap?['zones'].split(',');
        if (selectedZoneIds.length > 0) {
          selectedZoneIds.forEach((zoneId) {
            KeyValueModel? model = zoneMap.firstWhereOrNull(
              (element) => element.key == zoneId,
            );
            if (model != null) {
              selectedZoneList.add(model);
            }
          });
        }
      }
      isALlZonesSelected = zoneMap.length == selectedZoneList.length;

      setState(() {});
    }).onError((error, stackTrace) {
      writeErrorLogs('fetchGeoPlaces Error', e: error);
    });
    if (widget.alertMap != null) {
      selectedImeis.clear();
      alertNameController.text = widget.alertMap?['name'];
      List<String> selectedSplittedImeis = widget.alertMap?['imei'].split(',');
      selectedSplittedImeis.forEach((element) {
        if (!selectedImeis.contains(element) && deviceNames.keys.contains(element)) {
          selectedImeis.add(element);
        }
      });
      isAllDevicesSelected = selectedImeis.length >= deviceNames.length;
      int alertIndex = alertTypes.values.toList().indexOf(widget.alertMap?['type']);
      selectedAlertType = alertTypes.keys.toList()[alertIndex];

      switch (selectedAlertType) {
        case 'Stopped':
        case 'Moving':
        case 'Engine idle':
        case 'Connection: No':
        case 'GPS: No':
          enableTimePeriod = true;
          enableDistance = false;
          enableSpeed = false;
          // timePeriodController.text = widget.alertMap["checked_value"];
          break;
        case 'Overspeed':
        case 'Underspeed':
          enableTimePeriod = false;
          enableDistance = false;
          enableSpeed = true;
          speedController.text = widget.alertMap?['checked_value'];
          break;
        case 'Proximity':
          enableTimePeriod = false;
          enableDistance = true;
          enableSpeed = false;
          break;
      }
    }
    if (mounted) {
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        centerTitle: false,
        backgroundColor: Colors.white,
        elevation: 0.2,
        title: Text(
          widget.alertMap != null ? 'Update Alert' : 'Add Alert',
          style: TextStyle(color: Colors.black, fontWeight: FontWeight.w600, fontSize: 18),
        ),
        iconTheme: IconThemeData(
          color: AppColors.primaryColor2, //change your color here
        ),
        actions: [
          isDeleting
              ? Center(child: CircularProgressIndicator())
              : widget.alertMap != null
                  ? IconButton(
                      onPressed: () {
                        setState(() {
                          isDeleting = true;
                        });
                        GPSServerRequests.deleteEvent(widget.eventId!).then((value) {
                          if (value)
                            Navigator.pop(context, true);
                          else {
                            setState(() {
                              isDeleting = false;
                            });
                          }
                        }).onError((error, stackTrace) {
                          setState(() {
                            isDeleting = false;
                          });
                          showSnackBar(context, error as String, Colors.red);
                        });
                      },
                      icon: Icon(Icons.delete))
                  : Container()
        ],
        systemOverlayStyle: SystemUiOverlayStyle.dark,
      ),
      body: Container(
        padding: EdgeInsets.all(10),
        child: ListView(
          children: [
            TextField(
              controller: alertNameController,
              decoration: InputDecoration(
                contentPadding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(5.0),
                ),
                filled: false,
                hintStyle: TextStyle(fontSize: 14, color: Colors.grey[400]),
                hintText: 'Alert Name',
              ),
            ),
            SizedBox(
              height: 10,
            ),
            CheckboxListTile(
                contentPadding: EdgeInsets.zero,
                title: Text(
                  'All Objects',
                  textAlign: TextAlign.right,
                ),
                value: isAllDevicesSelected,
                onChanged: (b) {
                  setState(() {
                    isAllDevicesSelected = b ?? false;
                  });
                }),
            Stack(
              children: [
                DropdownSearch<String>.multiSelection(
                    popupProps: PopupPropsMultiSelection.dialog(
                        itemBuilder: (context, imei, isEnabled) {
                          return Padding(
                            padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                            child: Text(deviceNames[imei]!),
                          );
                        },
                        showSearchBox: true,
                        searchFieldProps: TextFieldProps(
                            decoration: InputDecoration(
                          hintText: 'Objects',
                          contentPadding: EdgeInsets.only(left: 16, right: 0, top: 2, bottom: 2),
                          border: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
                          disabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
                          enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
                          focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: AppColors.primaryColor)),
                        ))),
                    items: deviceNames.keys.toList(),
                    onChanged: (s) {
                      selectedImeis = s;
                    },
                    dropdownBuilder: (context, imeis) {
                      List<String> objectsList = [];
                      imeis.forEach((element) {
                        if (deviceNames[element] != null) {
                          objectsList.add(deviceNames[element]!);
                        }
                      });
                      return Text(objectsList.isEmpty ? '---Select Devices---' : objectsList.join(', '));
                    },
                    filterFn: (imei, search) {
                      return (deviceNames[imei] ?? '').toLowerCase().contains(search.toLowerCase());
                    },
                    selectedItems: [...selectedImeis]),
                if (isAllDevicesSelected)
                  Positioned(
                    left: 0,
                    right: 0,
                    top: 0,
                    bottom: 0,
                    child: Container(
                      color: Colors.grey.withOpacity(0.4),
                    ),
                  ),
              ],
            ),
            SizedBox(
              height: 10,
            ),
            DropDownFromField(
              list: alertTypes.keys.toList(),
              selectedValue: selectedAlertType,
              onChanged: (value) {
                setState(() {
                  selectedAlertType = value;
                  switch (selectedAlertType) {
                    case 'Stopped':
                    case 'Moving':
                    case 'Engine idle':
                    case 'Connection: No':
                    case 'GPS: No':
                      enableTimePeriod = true;
                      enableDistance = false;
                      enableSpeed = false;
                      // timePeriodController.text = "5";
                      // distanceController.text = "";
                      speedController.text = '';
                      break;
                    case 'Overspeed':
                    case 'Underspeed':
                      enableTimePeriod = false;
                      enableDistance = false;
                      enableSpeed = true;
                      // timePeriodController.text = "";
                      // distanceController.text = "";
                      speedController.text = '60';
                      break;
                    case 'Proximity':
                      enableTimePeriod = false;
                      enableDistance = true;
                      enableSpeed = false;
                      // timePeriodController.text = "";
                      // distanceController.text = "0.1";
                      speedController.text = '';
                      break;
                    default:
                      enableTimePeriod = false;
                      enableDistance = false;
                      enableSpeed = false;
                      // timePeriodController.text = "";
                      // distanceController.text = "";
                      speedController.text = '';
                      break;
                  }
                });
              },
            ),
            SizedBox(
              height: 10,
            ),
            if (selectedAlertType == 'Zone in' || selectedAlertType == 'Zone out') ...[
              CheckboxListTile(
                  contentPadding: EdgeInsets.zero,
                  title: Text(
                    'All Zones',
                    textAlign: TextAlign.right,
                  ),
                  value: isALlZonesSelected,
                  onChanged: (b) {
                    setState(() {
                      isALlZonesSelected = b ?? false;
                    });
                  }),
              Stack(
                children: [
                  DropdownSearch<KeyValueModel>.multiSelection(
                      popupProps: PopupPropsMultiSelection.dialog(
                          itemBuilder: (context, zoneName, isEnabled) {
                            return Padding(
                              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                              child: Text(zoneName.value),
                            );
                          },
                          showSearchBox: true,
                          searchFieldProps: TextFieldProps(
                            decoration: InputDecoration(
                              hintText: 'Select Zones',
                              contentPadding: EdgeInsets.only(left: 16, right: 0, top: 2, bottom: 2),
                              border: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
                              disabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
                              enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
                              focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: AppColors.primaryColor)),
                            ),
                          )),
                      items: zoneMap,
                      onChanged: (s) {
                        this.selectedZoneList = s;
                      },
                      dropdownBuilder: (context, zoneNames) {
                        return Text(zoneNames.map((element) => element.value).toList().join(', '));
                      },
                      filterFn: (zoneName, search) => zoneName.value.toLowerCase().contains(search.toLowerCase()),
                      selectedItems: [...selectedZoneList]),
                  if (isALlZonesSelected)
                    Positioned(
                      left: 0,
                      right: 0,
                      top: 0,
                      bottom: 0,
                      child: Container(
                        color: Colors.grey.withOpacity(0.4),
                      ),
                    ),
                ],
              ),
            ],
            SizedBox(
              height: 10,
            ),
            if (selectedAlertType == 'Overspeed')
              Container(
                margin: EdgeInsets.only(bottom: 10),
                child: TextField(
                  keyboardType: TextInputType.number,
                  controller: speedController,
                  enabled: enableSpeed,
                  decoration: InputDecoration(
                    contentPadding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                    border: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
                    disabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
                    enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
                    focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: AppColors.primaryColor)),
                    filled: false,
                    hintStyle: TextStyle(fontSize: 14, color: Colors.grey[400]),
                    hintText: 'Speed(kph)',
                  ),
                ),
              ),
            isSubmitting
                ? Center(child: CircularProgressIndicator())
                : ElevatedButton(
                    onPressed: () {
                      String checkedValue = '';
                      if (alertNameController.text.isEmpty) {
                        showSnackBar(context, 'Please Enter Alert Name', Colors.red);
                        return;
                      }
                      if (selectedImeis.isEmpty) {
                        showSnackBar(context, 'Please Select at lease 1 device', Colors.red);
                        return;
                      }
                      if ((selectedAlertType == 'Zone in' || selectedAlertType == 'Zone out') && (selectedZoneList.isEmpty && !isALlZonesSelected)) {
                        showSnackBar(context, 'Please Select at lease 1 zone', Colors.red);
                        return;
                      }
                      if (enableSpeed && speedController.text.isEmpty) {
                        showSnackBar(context, 'Please enter speed', Colors.red);
                        return;
                      }
                      Map<String, dynamic> dataMap;
                      List<String> seletedzoneIds = [];
                      if (enableSpeed) {
                        checkedValue = speedController.text;
                      }
                      selectedZoneList.forEach((element) {
                        seletedzoneIds.add(element.key);
                      });

                      if (widget.alertMap == null) {
                        dataMap = {
                          'active': 'true',
                          'id': 'false',
                          'type': alertTypes[selectedAlertType],
                          'name': alertNameController.text,
                          'duration_from_last_event': 'false',
                          'duration_from_last_event_minutes': 0,
                          'week_days': 'true,true,true,true,true,true,true',
                          'day_time':
                              '{"dt":false,"mon":false,"mon_from":"00:00","mon_to":"24:00","tue":false,"tue_from":"00:00","tue_to":"24:00","wed":false,"wed_from":"00:00","wed_to":"24:00","thu":false,"thu_from":"00:00","thu_to":"24:00","fri":false,"fri_from":"00:00","fri_to":"24:00","sat":false,"sat_from":"00:00","sat_to":"24:00","sun":false,"sun_from":"00:00","sun_to":"24:00"}',
                          'imei': isAllDevicesSelected ? deviceNames.keys.join(',') : selectedImeis.join(','),
                          'checked_value': checkedValue,
                          'route_trigger': 'off',
                          'zone_trigger': 'off',
                          'routes': '',
                          'zones': isALlZonesSelected ? zoneMap.map((e) => e.key).join(',') : seletedzoneIds.join(','),
                          'notify_system': 'true,true,true,alarm1.mp3',
                          'notify_push': 'true',
                          'notify_email': 'false',
                          'notify_email_address': '',
                          'notify_sms': 'false',
                          'notify_sms_number': '',
                          'email_template_id': '0',
                          'sms_template_id': '0',
                          'notify_arrow': 'false',
                          'notify_arrow_color': 'arrow_yellow',
                          'notify_ohc': 'false',
                          'notify_ohc_color': '#FFFF00',
                          'webhook_send': 'true',
                          'webhook_url': 'https://geocode.autonemogps.com/webhook/receiver.php',
                          'cmd_send': 'false',
                          'cmd_gateway': 'gprs',
                          'cmd_type': 'ascii',
                          'cmd_string': ''
                        };
                      } else {
                        dataMap = {};
                        dataMap.addAll(widget.alertMap!);
                        dataMap['id'] = widget.eventId ?? 'false';
                        dataMap['type'] = alertTypes[selectedAlertType];
                        dataMap['name'] = alertNameController.text;
                        dataMap['imei'] = isAllDevicesSelected ? deviceNames.keys.join(',') : selectedImeis.join(',');
                        dataMap['route_trigger'] = 'off';
                        dataMap['zone_trigger'] = 'off';
                        dataMap['routes'] = '';
                        dataMap['zones'] = isALlZonesSelected ? zoneMap.map((e) => e.key).join(',') : seletedzoneIds.join(',');
                        dataMap['checked_value'] = checkedValue;
                        dataMap['webhook_send'] = 'true';
                        dataMap['webhook_url'] = 'https://geocode.autonemogps.com/webhook/receiver.php';
                      }
                      setState(() => isSubmitting = true);
                      GPSServerRequests.updateEventStatus(dataMap).then((value) {
                        Navigator.pop(context, true);
                      }).onError((error, stackTrace) {
                        setState(() => isSubmitting = false);
                        showSnackBar(context, error.toString(), Colors.red);
                      });
                    },
                    style: ElevatedButton.styleFrom(backgroundColor: AppColors.primaryColor),
                    child: Text(widget.alertMap != null ? 'Update' : 'Add'))
          ],
        ),
      ),
    );
  }
}

class SensorItem extends StatelessWidget {
  final String sensorName, cn, value;
  final Function onDeletePressed;

  const SensorItem(this.sensorName, this.cn, this.value, this.onDeletePressed, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.max,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Expanded(
            flex: 5,
            child: Padding(
              padding: const EdgeInsets.only(left: 12.0),
              child: Text(sensorName),
            )),
        SizedBox(
          width: 2,
        ),
        Expanded(
          flex: 3,
          child: Padding(
            padding: const EdgeInsets.only(left: 12.0),
            child: Text(cn),
          ),
        ),
        SizedBox(
          width: 2,
        ),
        Expanded(
          flex: 5,
          child: Padding(
            padding: const EdgeInsets.only(left: 12.0),
            child: Text(value),
          ),
        ),
        SizedBox(
          width: 2,
        ),
        IconButton(
          icon: Icon(Icons.delete),
          onPressed: () => onDeletePressed(),
        ),
      ],
    );
  }
}
