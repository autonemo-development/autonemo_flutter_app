class RewardPoint {
  String date;

  String title;

  String pointsEarned;

  RewardPoint(this.date, this.title, this.pointsEarned);
}
