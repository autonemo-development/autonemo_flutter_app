import 'dart:convert';

import 'package:autonemogps/api_calls/gps_server_requests.dart';
import 'package:autonemogps/api_calls/gps_wox_requests.dart';
import 'package:autonemogps/config/colors.dart';
import 'package:autonemogps/config/helpers.dart';
import 'package:autonemogps/config/string_constants.dart';
import 'package:autonemogps/storage/repository.dart';
import 'package:flutter/material.dart';

class AlertsFilterBottomSheet extends StatefulWidget {
  final Map<String, String> selectedAlertsList;

  AlertsFilterBottomSheet(this.selectedAlertsList);

  @override
  _AlertsFilterBottomSheetState createState() => _AlertsFilterBottomSheetState();
}

class _AlertsFilterBottomSheetState extends State<AlertsFilterBottomSheet> {
  Map<String, String> alertList = {};

  bool isAllSelected = false;
  int alertListNumber = 0;
  List<Widget> alertWidgetList = [];
  bool _isLoading = true;

  List<Widget> getAlertItemList() {
    alertListNumber = alertList.length;
    List<Widget> widgets = [];

    alertList.forEach((key, value) {
      Widget rowWidget = new InkWell(
        onTap: () {
          setState(() {
            if (widget.selectedAlertsList[key] != null) {
              widget.selectedAlertsList.remove(key);
            } else {
              widget.selectedAlertsList[key] = value;
            }
          });
        },
        child: Container(
          padding: EdgeInsets.symmetric(vertical: 5),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                value,
                overflow: TextOverflow.fade,
                softWrap: false,
                maxLines: 1,
                style: TextStyle(color: Color(0xFF474747), fontWeight: FontWeight.w500),
              ),
              Image.asset((widget.selectedAlertsList[key] != null) ? 'images/alert-filter-checked.png' : 'images/alert-filter-unchecked.png', width: 20),
            ],
          ),
        ),
      );

      widgets.add(rowWidget);
    });

    return widgets;
  }

  @override
  void initState() {
    super.initState();
    if (Repository.getServerType() == GPSWoxConstants.type) {
      GPSWoxRequests.fetchAlerts().then((returnedList) {
        Map<String, dynamic> alertObject = json.decode(returnedList);
        List allAlertList = alertObject['items']['alerts'] as List;
        allAlertList.forEach((element) {
          alertList[(element['id']).toString()] = element['name'] as String;
        });
        setState(() {
          _isLoading = false;
          if (widget.selectedAlertsList.length == 0) {
            widget.selectedAlertsList.addAll(alertList);
            isAllSelected = true;
          }
        });
      }).catchError((e) {
        Navigator.pop(context);
        showSnackBar(context, e, Colors.red);
      });
    } else {
      GPSServerRequests.fetchEventTypes().then((returnedList) {
        if (returnedList.elementAt(0) == true) {
          setState(() {
            _isLoading = false;
            alertList = returnedList.elementAt(1);
            if (widget.selectedAlertsList.length == 0) {
              widget.selectedAlertsList.addAll(alertList);
              isAllSelected = true;
            }
          });
        } else {
          Navigator.pop(context);
          showSnackBar(context, returnedList.elementAt(1), Colors.red);
        }
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    alertWidgetList = getAlertItemList();

    return Container(
      height: MediaQuery.of(context).size.height * .8,
      padding: EdgeInsets.all(20),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(topLeft: Radius.circular(30), topRight: Radius.circular(30)),
      ),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Image.asset(
                    'images/tools-and-utensils.png',
                    height: 18,
                  ),
                  SizedBox(
                    width: 10.0,
                  ),
                  Text(
                    'Filters',
                    style: TextStyle(color: Color(0xFFF26611), fontWeight: FontWeight.w500),
                  ),
                ],
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  InkWell(
                      onTap: () {
                        setState(() {
                          widget.selectedAlertsList.clear();
                          if (!isAllSelected) {
                            widget.selectedAlertsList.addAll(alertList);
                          }

                          isAllSelected = !isAllSelected;
                        });
                      },
                      child: Text(
                        isAllSelected ? 'Unselect All' : 'Select All',
                        style: TextStyle(color: AppColors.primaryColor, fontWeight: FontWeight.w500),
                      )),
                  SizedBox(
                    width: 25,
                  ),
                  InkWell(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Image.asset(
                        'images/cancel.png',
                        height: 17,
                      )),
                ],
              )
            ],
          ),
          Expanded(
              child: Padding(
            padding: const EdgeInsets.only(top: 10.0),
            child: _isLoading
                ? Center(child: CircularProgressIndicator())
                : ListView.separated(
                    separatorBuilder: (context, index) => Divider(),
                    itemCount: alertWidgetList.length,
                    itemBuilder: (context, index) {
                      return alertWidgetList[index];
                    }),
          )),
          InkWell(
            onTap: () {
              Navigator.of(context).pop(widget.selectedAlertsList);
            },
            child: Container(
              padding: EdgeInsets.all(10),
              decoration: BoxDecoration(borderRadius: BorderRadius.circular(3), border: Border.all(color: AppColors.primaryColor, width: 1)),
              child: Center(
                  child: Text(
                'Ok (' + widget.selectedAlertsList.length.toString() + ')',
                style: TextStyle(color: AppColors.primaryColor, fontWeight: FontWeight.w500),
              )),
            ),
          ),
        ],
      ),
    );
  }
}
