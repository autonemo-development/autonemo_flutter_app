import 'package:autonemogps/api_calls/gps_server_requests.dart';
import 'package:autonemogps/common/refer_redeem_dialog.dart';
import 'package:autonemogps/common/refer_terms_and_condition_dialog.dart';
import 'package:autonemogps/config/colors.dart';
import 'package:autonemogps/config/helpers.dart';
import 'package:autonemogps/config/string_constants.dart';
import 'package:autonemogps/screens/my_account/referral_history_page.dart';
import 'package:clipboard/clipboard.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:share/share.dart';

class ReferPage extends StatefulWidget {
  static const String page = 'ReferPage';

  @override
  _ReferPageState createState() => _ReferPageState();
}

class _ReferPageState extends State<ReferPage> {
  bool _isLoading = true;
  String? error;
  int? clientId;
  late String referralLink;
  int availablePoints = 0;
  int totalEarned = 0;
  int totalReferred = 0;
  Map<String, String> referralHistory = {};

  @override
  void initState() {
    super.initState();
    GPSServerRequests.getClientData().then((value) {
      if (mounted) {
        setState(() {
          _isLoading = false;
          if (value[STATUS] == 1) {
            Map<String, dynamic> dataMap = value[DATA];
            clientId = dataMap['id'] as int;
            referralLink = dataMap['referral_registration_link_short'] as String;
            availablePoints = dataMap['available_reward_points'] as int;
            totalEarned = dataMap['total_earned_reward_points'] as int;
            totalReferred = dataMap['total_referred_client'] as int;
            referralHistory = dataMap['referred_clients'];
          } else {
            error = value[MESSAGE] as String;
          }
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.defaultBackground,
      appBar: AppBar(
        centerTitle: false,
        title: Text('Refer & Earn', style: TextStyle(color: Color(0xFF474747), fontWeight: FontWeight.w500, fontSize: 16)),
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Color(0xFF7E7E7E)),
        actions: <Widget>[InkResponse(onTap: () => showReferRedeemDialog(context), child: Container(child: Image.asset('images/question-circle.png', height: 27, width: 27))), SizedBox(width: 15)],
      ),
      body: _isLoading
          ? SpinKitRing(lineWidth: 3.0, color: AppColors.primaryColor, size: 35.0)
          : error != null
              ? Center(child: Text(error!))
              : SingleChildScrollView(
                  child: Container(
                    padding: EdgeInsets.only(left: 15, right: 15, bottom: 15),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        SizedBox(height: 20),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 15.0),
                          child: RichText(
                            textAlign: TextAlign.center,
                            text: TextSpan(style: TextStyle(color: Color(0xFF474747), fontSize: 14, fontWeight: FontWeight.w500), children: [
                              TextSpan(text: 'For every Autonemo referral, you win '),
                              TextSpan(text: '500 Points', style: TextStyle(color: Color(0xFF212121), fontSize: 15, fontWeight: FontWeight.w700)),
                              TextSpan(text: ' and your friend wins '),
                              TextSpan(text: '200 Points!', style: TextStyle(color: Color(0xFF212121), fontSize: 15, fontWeight: FontWeight.w700)),
                            ]),
                          ),
                        ),
                        SizedBox(height: 20),
                        Padding(padding: const EdgeInsets.symmetric(horizontal: 40), child: Image.asset('images/refer-cover.png')),
                        SizedBox(height: 20),
                        Container(
                          padding: EdgeInsets.all(15),
                          decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(10), border: Border.all(color: Color(0xFFFF8D8D), width: .8)),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                children: [
                                  Row(
                                    children: [
                                      Image.asset('images/status.png', height: 35, width: 35),
                                      SizedBox(width: 15),
                                      Text('Status', style: TextStyle(color: Color(0xFF212121), fontSize: 15, fontWeight: FontWeight.w500)),
                                    ],
                                  ),
                                  Expanded(
                                    child: RichText(
                                      textAlign: TextAlign.right,
                                      text: TextSpan(style: TextStyle(color: Color(0xFF474747), fontSize: 14, fontWeight: FontWeight.w400), children: [
                                        TextSpan(text: 'Your available points '),
                                        TextSpan(
                                          text: availablePoints.toString(),
                                          style: TextStyle(color: Color(0xFF212121), fontSize: 15, fontWeight: FontWeight.w700),
                                        ),
                                      ]),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                        SizedBox(height: 10),
                        Container(
                          padding: EdgeInsets.all(15),
                          decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(10), border: Border.all(color: Color(0xFF0060A4), width: .8)),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text('Stats', style: TextStyle(color: Color(0xFF212121), fontSize: 15, fontWeight: FontWeight.w500)),
                              SizedBox(height: 10),
                              Row(
                                children: [
                                  Expanded(flex: 3, child: Text('Successful referral', style: TextStyle(color: Color(0xFF474747), fontSize: 13, fontWeight: FontWeight.w400))),
                                  Expanded(flex: 1, child: Text(totalReferred.toString(), style: TextStyle(color: Color(0xFF212121), fontSize: 13, fontWeight: FontWeight.w500))),
                                ],
                              ),
                              SizedBox(height: 10),
                              Row(
                                children: [
                                  Expanded(
                                    flex: 3,
                                    child: Text('Points earned from referrals', style: TextStyle(color: Color(0xFF474747), fontSize: 13, fontWeight: FontWeight.w400)),
                                  ),
                                  Expanded(flex: 1, child: Text(totalEarned.toString(), style: TextStyle(color: Color(0xFF212121), fontSize: 13, fontWeight: FontWeight.w500))),
                                ],
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        Center(
                          child: InkWell(
                            onTap: () {
                              if (availablePoints >= 500) {
                                //redeem
                                showDialog(
                                    context: context,
                                    builder: (context) {
                                      return Material(
                                        type: MaterialType.transparency,
                                        child: Center(child: SpinKitRing(lineWidth: 3.0, color: AppColors.primaryColor, size: 35.0)),
                                      );
                                    });
                                GPSServerRequests.sendRedeemRequest(availablePoints, clientId!).then((value) {
                                  Navigator.pop(context);
                                  showSnackBar(context, value[1], value[0] == 0 ? Colors.red : Colors.green);
                                });
                              }
                            },
                            child: Container(
                              padding: EdgeInsets.symmetric(horizontal: 15, vertical: 7),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(7),
                                border: Border.all(color: availablePoints >= 500 ? Color(0xFF0060A4) : Colors.grey, width: .8),
                              ),
                              child: Text('Redeem your Points', style: TextStyle(color: availablePoints >= 500 ? Color(0xFF0060A4) : Colors.grey, fontWeight: FontWeight.w500)),
                            ),
                          ),
                        ),
                        SizedBox(height: 20),
                        InkWell(
                          onTap: () {
                            Navigator.of(context, rootNavigator: true).push(MaterialPageRoute(builder: (context) => ReferralHistory(referralHistory)));
                          },
                          child: Container(
                            padding: EdgeInsets.all(15),
                            decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(10)),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  children: [
                                    Image.asset('images/history.png', height: 25, width: 25),
                                    SizedBox(width: 15),
                                    Expanded(
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Text('Referred Clients', style: TextStyle(color: Color(0xFF212121), fontSize: 15, fontWeight: FontWeight.w500)),
                                          Text('See Autonemo Point Earning Activity Log', style: TextStyle(color: Color(0xFF474747), fontSize: 13, fontWeight: FontWeight.w400)),
                                        ],
                                      ),
                                    ),
                                    SizedBox(width: 15),
                                    Icon(Icons.chevron_right_rounded, color: AppColors.primaryColor, size: 25)
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ),
                        SizedBox(height: 20),
                        Text('Use the link below to refer your friends', style: TextStyle(color: Color(0xFF474747), fontSize: 13, fontWeight: FontWeight.w500)),
                        SizedBox(height: 5),
                        InkWell(
                          onTap: () => FlutterClipboard.copy(referralLink).then((value) {}),
                          child: Container(
                            width: double.infinity,
                            padding: EdgeInsets.all(10.0),
                            decoration: BoxDecoration(
                              color: Colors.white.withOpacity(.5),
                              borderRadius: BorderRadius.circular(5.0),
                              border: Border.all(color: Color(0xFF707070), width: .8),
                            ),
                            child: Center(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Expanded(child: Text(referralLink, style: TextStyle(color: Color(0xFF7E7E7E), fontSize: 12, fontWeight: FontWeight.w400))),
                                  Text('copy', style: TextStyle(color: AppColors.primaryColor, fontSize: 13, fontWeight: FontWeight.w500)),
                                ],
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                        InkWell(
                          onTap: () async {
                            await Share.share(
                              'Signup with the link from Easytrax and keep your Vehicle Safe.\n\n' +
                                  'Use this link to Signup : ' +
                                  referralLink +
                                  '\n\nReferral Code : ' +
                                  clientId.toString() +
                                  '\n\nHotline : 09606667788',
                              subject: 'Easytrax',
                            );
                          },
                          child: Container(
                            width: double.infinity,
                            padding: EdgeInsets.all(7.0),
                            decoration: BoxDecoration(
                                color: AppColors.primaryColor,
                                borderRadius: BorderRadius.circular(5.0),
                                boxShadow: [BoxShadow(color: AppColors.primaryColor.withOpacity(.55), blurRadius: 5, offset: Offset(0, 3))]),
                            child: Center(child: Text('Share Link', style: TextStyle(color: Colors.white, fontSize: 16, fontWeight: FontWeight.w500))),
                          ),
                        ),
                        SizedBox(height: 7),
                        Center(child: Text('Or', style: TextStyle(color: Color(0xFF060606), fontSize: 16, fontWeight: FontWeight.w500))),
                        SizedBox(height: 7),
                        InkWell(
                          onTap: () => FlutterClipboard.copy(referralLink).then((value) {}),
                          child: Container(
                            width: double.infinity,
                            padding: EdgeInsets.symmetric(vertical: 10.0),
                            decoration: BoxDecoration(color: Colors.white.withOpacity(.5), borderRadius: BorderRadius.circular(5.0)),
                            child: Center(
                              child: RichText(
                                textAlign: TextAlign.center,
                                text: TextSpan(style: TextStyle(color: Color(0xFF474747), fontSize: 14, fontWeight: FontWeight.w400), children: [
                                  TextSpan(text: 'Share your code: '),
                                  TextSpan(text: clientId.toString(), style: TextStyle(color: Color(0xFF212121), fontSize: 15, fontWeight: FontWeight.w700)),
                                ]),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(height: 10),
                        InkWell(
                          onTap: () => showTermsAndConditionDialog(context),
                          child: Container(
                            padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                            decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(5)),
                            child: Center(child: Text('Terms and Conditions', style: TextStyle(color: AppColors.primaryColor, fontWeight: FontWeight.w500))),
                          ),
                        ),
                        SizedBox(height: 10)
                      ],
                    ),
                  ),
                ),
    );
  }
}
