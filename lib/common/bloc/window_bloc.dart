import 'dart:io';

import 'package:autonemogps/common/bloc/window_event.dart';
import 'package:autonemogps/common/bloc/window_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class WindowBloc extends Bloc<WindowEvent, WindowState> {
  WindowBloc() : super(NoEventState()) {
    on<ChangePositionEvent>((event, emit) {
      var devicePixelRatio = Platform.isAndroid ? MediaQuery.of(event.context).devicePixelRatio : 1.0;

      if (height == 0 && width == 0) {
        offsetY = (event.screenCoordinate.y.toDouble()) / devicePixelRatio;
        offsetX = (event.screenCoordinate.x.toDouble()) / devicePixelRatio;
      } else {
        offsetY = (event.screenCoordinate.y.toDouble()) / devicePixelRatio - (height + 30);
        offsetX = (event.screenCoordinate.x.toDouble()) / devicePixelRatio - ((width / 2));
      }

      emit(PositionChangedState(offsetX: offsetX, offsetY: offsetY));
      emit(NoEventState());
    });
    on<WindowLoadedEvent>(
      (event, emit) {
        height = event.height;
        width = event.width;
        offsetY = offsetY - (height + 50);
        offsetX = offsetX - ((width / 2));
        emit(PositionChangedState(offsetX: offsetX, offsetY: offsetY));
        emit(NoEventState());
      },
    );
  }

  double offsetY = 0.0;
  double offsetX = 0.0;
  double height = 0;
  double width = 0;
}
