import UIKit
import Flutter
import GoogleMaps
import Firebase
import FirebaseMessaging

@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate {
    
    let gcmMessageIDKey="gcm.message_id"

    override func application(
        _ application: UIApplication,
        didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
    ) -> Bool {
        let controller : FlutterViewController = window?.rootViewController as! FlutterViewController
        let batteryChannel = FlutterMethodChannel(name: "auto_nemo_channel",
                                                  binaryMessenger: controller.binaryMessenger)
        batteryChannel.setMethodCallHandler({
            (call: FlutterMethodCall, result: @escaping FlutterResult) -> Void in
            // Note: this method is invoked on the UI thread.
            // Handle battery messages.
            
            if( call.method == "changeLanguage"){}
            else if(call.method=="parkingViolationAcknowledged"){
            }
            else if(call.method=="collectGarbage"){}
            else {
                result(FlutterMethodNotImplemented)
                return
              }
        })
        
        FirebaseApp.configure()
        GMSServices.provideAPIKey("AIzaSyCQk0IbpQEAzjovI2PES1_x7ok6810EbKM")
        GeneratedPluginRegistrant.register(with: self)
        
        Messaging.messaging().delegate = self
        
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .sound, .badge]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .sound, .badge], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        application.registerForRemoteNotifications()
        return super.application(application, didFinishLaunchingWithOptions: launchOptions)
    }
//    @available(iOS 10, *)
//    // Receive displayed notifications for iOS 10 devices.
//    override func userNotificationCenter(_ center: UNUserNotificationCenter,
//                                         willPresent notification: UNNotification,
//                                         withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
//        let userInfo = notification.request.content.userInfo
////        print("willPresent \(userInfo)")
////        print(userInfo["type"]!)
////
//
//
//        completionHandler([[.alert, .sound]])
//    }
//    @available(iOS 10, *)
//    override func userNotificationCenter(_ center: UNUserNotificationCenter,
//                                         didReceive response: UNNotificationResponse,
//                                         withCompletionHandler completionHandler: @escaping () -> Void) {
//        let userInfo = response.notification.request.content.userInfo
//
//        // ...
//
//        // With swizzling disabled you must let Messaging know about the message, for Analytics
//        // Messaging.messaging().appDidReceiveMessage(userInfo)
//
//        // Print full message.
////        print("didReceive \(userInfo)")
//
//        completionHandler()    }
//
//    override func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
//                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
//        print("didReceiveRemoteNotification \(userInfo)")
//        if("parking_violation"==userInfo["type"]! as! String){
////            MusicPlayer.shared.startBackgroundMusic()
//        }
//        completionHandler(UIBackgroundFetchResult.newData)
//
//    }
}
// [END ios_10_message_handling]


extension AppDelegate : MessagingDelegate {
    // [START refresh_token]
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        
        let dataDict:[String: String] = ["token": fcmToken!]
        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
        // TODO: If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
    }
    
    
    // [END refresh_token]
    // [START ios_10_data_message]
    // Receive data messages on iOS 10+ directly from FCM (bypassing APNs) when the app is in the foreground.
    // To enable direct data messages, you can set Messaging.messaging().shouldEstablishDirectChannel to true.
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingDelegate) {
//        print("Received data message: \(remoteMessage.description)")
    }
    // [END ios_10_data_message]
}
