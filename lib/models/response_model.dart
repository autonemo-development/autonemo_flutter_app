import 'dart:convert';

class ResponseModel {
  Map<String, dynamic> responseMap = {};
  String responseString = '';

  ResponseModel(response) {
    responseString = response;
    responseMap = json.decode(response) as Map<String, dynamic>;
  }

  bool isSuccess() {
    if (responseMap.containsKey('success')) {
      return responseMap['success'];
    }

    return false;
  }

  String getDataString() {
    if (responseMap.containsKey('data')) {
      return responseMap['data'].toString();
    }

    return '';
  }

  Map<String, dynamic> getDataAsMap() {
    try {
      if (responseMap.containsKey('data')) {
        return responseMap['data'] as Map<String, dynamic>;
      }
    } catch (_) {}

    return {};
  }

  List<dynamic> getDataAsList() {
    if (responseMap.containsKey('data')) {
      return responseMap['data'] as List<dynamic>;
    }

    return [];
  }

  String getErrorString() {
    try {
      if (responseMap['error']) {
        return responseMap['error'];
      }
    } catch (_) {}

    return '';
  }
}
