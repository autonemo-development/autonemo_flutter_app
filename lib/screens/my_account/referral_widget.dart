import 'package:autonemogps/api_calls/gps_server_requests.dart';
import 'package:autonemogps/common/refer_terms_and_condition_dialog.dart';
import 'package:autonemogps/config/colors.dart';
import 'package:autonemogps/config/helpers.dart';
import 'package:autonemogps/config/string_constants.dart';
import 'package:autonemogps/screens/my_account/account_ticket_widget.dart';
import 'package:autonemogps/screens/my_account/referral_history_page.dart';
import 'package:autonemogps/screens/my_account/reward_points_history_page.dart';
import 'package:clipboard/clipboard.dart';
import 'package:flutter/material.dart';
import 'package:share/share.dart';

class ReferralWidget extends StatefulWidget {
  final String referralLink;
  final int availablePoints;
  final int totalEarned;
  final int totalUsed;
  final int totalReferred;
  final Map<String, String> referralHistory;
  final int clientId;
  final List rewardPointsHistoryList;

  const ReferralWidget(
      {required this.availablePoints,
      required this.referralHistory,
      required this.referralLink,
      required this.totalEarned,
      required this.totalUsed,
      required this.totalReferred,
      required this.clientId,
      required this.rewardPointsHistoryList,
      Key? key})
      : super(key: key);

  @override
  State<ReferralWidget> createState() => _ReferralWidgetState();
}

class _ReferralWidgetState extends State<ReferralWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(16),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8),
        color: Colors.white,
      ),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                'Your Reward',
                style: TextStyle(fontWeight: FontWeight.w600, fontSize: 16),
              ),
              InkWell(
                onTap: () => Navigator.of(context, rootNavigator: true).push(MaterialPageRoute(builder: (context) => ReferralHistory(widget.referralHistory))),
                child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8),
                    color: AppColors.primaryMaterialColor[50],
                  ),
                  child: Text('${widget.totalReferred} Referred Clients',
                      style: TextStyle(
                        fontSize: 12,
                        color: AppColors.primaryColor,
                        fontWeight: FontWeight.w700,
                      )),
                ),
              )
            ],
          ),
          SizedBox(
            height: 10,
          ),
          Row(
            children: [
              Expanded(
                child: AccountTicketWidget(
                  title: 'Available Reward Points',
                  count: widget.availablePoints.toString(),
                ),
              ),
              SizedBox(
                width: 10,
              ),
              Expanded(
                child: AccountTicketWidget(
                  title: 'Lifetime Earned Reward Points',
                  count: widget.totalEarned.toString(),
                ),
              ),
              SizedBox(width: 10),
              Expanded(
                child: AccountTicketWidget(
                  title: 'Total Reward Points Used',
                  count: widget.totalUsed.toString(),
                ),
              ),
            ],
          ),
          SizedBox(
            height: 16,
          ),
          InkWell(
            onTap: () {
              if (widget.availablePoints >= 500) {
                showSnackBar(context, 'Sending Redeem Request', AppColors.primaryColor);

                GPSServerRequests.sendRedeemRequest(widget.availablePoints, widget.clientId).then((value) {
                  // Navigator.pop(context);
                  showSnackBar(context, value[1], value[0] == 0 ? Colors.red : Colors.green);
                });
              }
            },
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 15, vertical: 7),
              decoration: BoxDecoration(borderRadius: BorderRadius.circular(7), border: Border.all(color: widget.availablePoints >= 500 ? AppColors.primaryColor : AppColors.primaryColor2, width: .8)),
              child: Text(
                'Redeem your Points',
                style: TextStyle(color: widget.availablePoints >= 500 ? AppColors.primaryColor : AppColors.primaryColor2, fontWeight: FontWeight.w500),
              ),
            ),
          ),
          SizedBox(
            height: 32,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                child: Text(
                  'Use the link below to refer your friends',
                  style: TextStyle(fontWeight: FontWeight.w600, fontSize: 16),
                ),
              ),
              InkWell(
                onTap: () async {
                  await Share.share(
                    'Signup with the link from ${companyName} and keep your Vehicle Safe.\n\nUse this link to Signup : ${widget.referralLink}\n\nReferral Code : ${widget.clientId.toString()}\n\nHotline : +8809606882288',
                    subject: companyName,
                  );
                },
                child: Text('Share Link',
                    style: TextStyle(
                      fontSize: 12,
                      color: AppColors.primaryColor,
                      fontWeight: FontWeight.w700,
                    )),
              )
            ],
          ),
          SizedBox(
            height: 16,
          ),
          Container(
            padding: EdgeInsets.symmetric(horizontal: 16, vertical: 4),
            decoration: BoxDecoration(color: AppColors.shadowMaterialColor[50]!, borderRadius: BorderRadius.circular(8)),
            child: Row(
              children: [
                Expanded(
                  child: Text(
                    widget.referralLink,
                  ),
                ),
                SizedBox(
                  width: 10,
                ),
                ElevatedButton(
                  onPressed: () {
                    FlutterClipboard.copy(widget.referralLink).then((value) => showSnackBar(context, 'Copied!', Colors.orangeAccent));
                  },
                  child: Text(
                    'Copy',
                    style: TextStyle(color: AppColors.headerTextColor),
                  ),
                  style: ElevatedButton.styleFrom(padding: EdgeInsets.zero, backgroundColor: Colors.white, shadowColor: Colors.transparent),
                )
              ],
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Row(
            children: [
              Expanded(
                child: Text.rich(
                  TextSpan(
                    children: [
                      TextSpan(
                        text: 'Share your code: ',
                        style: TextStyle(fontWeight: FontWeight.w600, color: AppColors.headerTextColor),
                      ),
                      TextSpan(
                        text: widget.clientId.toString(),
                        style: TextStyle(fontWeight: FontWeight.w600, color: AppColors.primaryColor),
                      ),
                    ],
                  ),
                ),
              ),
              InkWell(
                onTap: () {
                  showTermsAndConditionDialog(context);
                },
                child: Text(
                  'Terms & Conditions',
                  style: TextStyle(fontWeight: FontWeight.w500, color: AppColors.primaryColor2),
                ),
              ),
            ],
          ),
          SizedBox(
            height: 10,
          ),
          Divider(
            color: Colors.grey[100],
          ),
          SizedBox(
            height: 10,
          ),
          InkWell(
            onTap: () {
              Navigator.of(context, rootNavigator: true).push(MaterialPageRoute(builder: (context) => RewardPointsHistory(widget.rewardPointsHistoryList)));
            },
            child: Row(
              children: [
                Image.asset(
                  'images/history.png',
                  height: 25,
                  width: 25,
                ),
                SizedBox(
                  width: 15,
                ),
                Expanded(
                  child: Text(
                    'Reward Points History',
                    style: TextStyle(color: AppColors.kBlackColor, fontSize: 16, fontWeight: FontWeight.w600),
                  ),
                ),
                SizedBox(
                  width: 15,
                ),
                Icon(
                  Icons.chevron_right_rounded,
                  color: AppColors.primaryColor2,
                  size: 25,
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
