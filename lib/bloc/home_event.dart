import 'package:autonemogps/models/device_model.dart';
import 'package:equatable/equatable.dart';

abstract class HomeEvent extends Equatable {
  const HomeEvent();
  @override
  List<Object> get props => [];
}

class DeviceDataFetchedEvent extends HomeEvent {
  final Map<String, DeviceModel> deviceList;
  DeviceDataFetchedEvent({required this.deviceList});
}

class DataFetchingEvent extends HomeEvent {}

class TrackingDataFetchedEvent extends HomeEvent {}

class InitEvent extends HomeEvent {}

class NoInternetEvent extends HomeEvent {}

class RefreshDataEvent extends HomeEvent {}

class GetInitialDataEvent extends HomeEvent {}

class StartTimerEvent extends HomeEvent {}

class StopTimerEvent extends HomeEvent {}

class LogoutEvent extends HomeEvent {}

class SearchEvent extends HomeEvent {
  final String search;
  SearchEvent({required this.search});
}

class MapSearchEvent extends HomeEvent {
  final String search;
  MapSearchEvent({required this.search});
}

class ChangeSearchWidgetEvent extends HomeEvent {
  final bool showSearch;
  ChangeSearchWidgetEvent(this.showSearch);
}

class FetchSortedImeiList extends HomeEvent {
  final String? search;
  FetchSortedImeiList({this.search});
}

class UpdateListTypeEvent extends HomeEvent {
  final int selectedItem;
  UpdateListTypeEvent({required this.selectedItem});
}

class SessionExpiredEvent extends HomeEvent {}

class CheckForPopupEvent extends HomeEvent {}
