import 'package:animations/animations.dart';
import 'package:autonemogps/screens/my_account/my_account_page.dart';
import 'package:flutter/material.dart';

showAutoReferDialog(BuildContext context) {
  showModal(context: context, builder: (context) => Dialog(backgroundColor: Colors.transparent, child: ReferralDialogUi()));
}

class ReferralDialogUi extends StatelessWidget {
  ReferralDialogUi({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: Colors.white),
      child: Stack(
        children: [
          Positioned(
            top: 0,
            right: 0,
            child: InkWell(
              onTap: () => Navigator.pop(context),
              child: Container(
                child: Icon(Icons.close, size: 20),
                decoration: BoxDecoration(color: Colors.grey, borderRadius: BorderRadius.circular(20)),
                padding: EdgeInsets.all(5),
                margin: EdgeInsets.all(10),
              ),
            ),
          ),
          Container(
            padding: EdgeInsets.all(20),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  child: Column(
                    children: [
                      Container(child: Image.asset('images/referral_image.png'), padding: EdgeInsets.symmetric(horizontal: 20)),
                      Container(
                        padding: EdgeInsets.symmetric(horizontal: 20),
                        child: Text(
                          'Invite your Friends & Earn Points or Free Monthly subscription added to your Account!',
                          style: TextStyle(fontSize: 18, color: Colors.black, fontWeight: FontWeight.w600),
                          textAlign: TextAlign.center,
                        ),
                      )
                    ],
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(top: 20),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      InkWell(
                        onTap: () => Navigator.pop(context),
                        child: Container(child: Text('REMIND ME LATER', style: TextStyle(fontSize: 14, fontWeight: FontWeight.w500))),
                      ),
                      InkWell(
                        onTap: () {
                          Navigator.pop(context);
                          Navigator.of(context).push(MaterialPageRoute(builder: (context) => MyAccountPage()));
                        },
                        child: Container(
                          padding: EdgeInsets.only(right: 20, left: 20, top: 10, bottom: 10),
                          decoration: BoxDecoration(
                            color: Colors.red,
                            borderRadius: BorderRadius.circular(20.0),
                            boxShadow: [BoxShadow(color: Colors.black.withOpacity(.5), blurRadius: 1, offset: Offset(0, 1))],
                          ),
                          child: Text('LEARN MORE', style: TextStyle(color: Colors.white, fontSize: 14, fontWeight: FontWeight.w500)),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
