import 'package:google_maps_flutter/google_maps_flutter.dart';

String APP_NAME = 'Autonemo GPS';
String companyName = 'Autonemo GPS';
String autonemo = 'Autonemo';
String SHOP_URL = 'https://autonemo.com.bd';
String CHANNEL_ID = 'auto_nemo_channel';
LatLng center = const LatLng(23.6850, 90.3563);
String MESSAGE = 'message';
String STATUS = 'status';
String PAYMENT_DATA = 'PAYMENT_DATA';
String DATA = 'data';
String REWARD_POINTS_HISTORY = 'reward_points_history';
String DATE = 'date';
String TITLE = 'title';
String POINTS = 'points';

String STANDARD_DATE_TIME_INPUT_PATTERN = 'yyyy-MM-dd HH:mm:ss';
String STANDARD_DATE_TIME_OUTPUT_PATTERN = 'dd-MMM-yyyy hh:mm:ss a';

class CRMConstants {
  static const String CRM_BASE_URL = 'https://erp.autonemo.io/';
  static const String CRM_BASE_IMAGE_URL = 'https://erp.autonemo.io/uploads/client/client_photo/';
  static const String CRM_USERNAME = 'autonemogpsapp';
  static const String CRM_SECRET_KEY = 'Phvc24FpSEr6JCmzkXbqsHBNGRWw9e3g5Ad8KUDQMaYutfZxTnVyj7';
  static const String ERP_USERNAME = 'pooja_flutter';
  static const String ERP_SECRET_KEY = 'NqbUhVwt5fJTQzn4AGCPvR6EpcSyeZajMHu9KB8kmXFgDW7Ydxs2r3';
  static const String LEARN_MORE_URL = 'https://www.autonemogps.com/autonemo-loyality-programe/';
}

class GPSServerConstants {
  static const type = 'gps-server';
}

class AutonemoServerConstants {
  static const String BASE_URI = 'track.autonemogps.com';
  // static const String BASE_URI = 's2.gps-server.net';
  // static const String BASE_URI = 's1.gps-server.net';
  static const String BASE_URL = 'https://${BASE_URI}';
  static const bool isHttps = true;
}

class GPSWoxConstants {
  static const type = 'gps-wox';
  static String gpsWoxLastFetchTimeStamp = '';
}
