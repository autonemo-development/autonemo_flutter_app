import 'dart:convert';

import 'package:autonemogps/api_calls/gps_server_requests.dart';
import 'package:autonemogps/common/custom_text_form_field.dart';
import 'package:autonemogps/common/dropdown_field.dart';
import 'package:autonemogps/config/colors.dart';
import 'package:autonemogps/config/helpers.dart';
import 'package:autonemogps/models/device.dart';
import 'package:autonemogps/models/device_model.dart';
import 'package:autonemogps/models/maintenance.dart';
import 'package:autonemogps/models/tracking_device_model.dart';
import 'package:autonemogps/screens/maintenance_page/add_maintence_gpswox_page.dart';
import 'package:autonemogps/storage/repository.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class AddMaintenancePage extends StatefulWidget {
  static const String page = 'add_maintenance_page';
  final Maintenance? maintenance;

  AddMaintenancePage({this.maintenance});

  @override
  _AddMaintenancePageState createState() => _AddMaintenancePageState();
}

class _AddMaintenancePageState extends State<AddMaintenancePage> {
  bool serviceOdometerSelected = false;
  bool serviceEngineHoursSelected = false;
  bool serviceDaysSelected = false;
  bool eventOdometerSelected = false;
  bool eventLastServiceSelected = false;
  bool eventEngineHoursLeftSelected = false;
  bool eventDaysLeftSelected = false;
  bool dataList = false;
  bool popup = false;
  Map<String, String> deviceNames = Map<String, String>();
  String selectedImei = 'key';
  String selectedValue = '--Select Device--';
  bool _isMaintenanceSubmitting = false;

  TextEditingController _currentOdometerController = new TextEditingController();
  TextEditingController _currentEngineHoursController = new TextEditingController();
  TextEditingController _odoIntervalController = new TextEditingController();
  TextEditingController _odoLastController = new TextEditingController();
  TextEditingController _enghIntervalController = new TextEditingController();
  TextEditingController _enghLastController = new TextEditingController();
  TextEditingController _daysController = new TextEditingController();
  String _daysLast = '';
  TextEditingController _eventOdoIntervalController = new TextEditingController();
  TextEditingController _eventEnghLeftController = new TextEditingController();
  TextEditingController _eventDaysController = new TextEditingController();
  TextEditingController _nameController = new TextEditingController();
  late Map<String, TrackingDevice> fnObjects;

  @override
  void initState() {
    super.initState();
    fetchAllVehicleNames();
  }

  String getEhFromSeconds(String eh) {
    int ehInt = int.parse(eh);
    return (ehInt ~/ 3600).toString();
  }

  fetchAllVehicleNames() async {
    Map<String, DeviceModel> map = AllDeviceModelData().deviceModelAndImeiMapGpsServer;
    fnObjects = AllTrackingData().trackingDeviceImeiMap;
    deviceNames.putIfAbsent('key', () => '--Select Device--');

    map.forEach((key, value) {
      deviceNames.putIfAbsent(key, () => value.name);
    });
    if (widget.maintenance != null) {
      _isMaintenanceSubmitting = true;
      bool isFetched = await GPSServerRequests.fetchFnSettings();
      setState(() {
        _isMaintenanceSubmitting = false;
      });
      if (!isFetched) {
        return;
      }
      _nameController.text = widget.maintenance!.name;
      selectedImei = widget.maintenance!.imei;
      selectedValue = deviceNames[selectedImei] ?? '--Select Device--';
      _currentOdometerController.text = widget.maintenance!.odometer.replaceAll(' km', '');
      _currentEngineHoursController.text = widget.maintenance!.engineHours.replaceAll(' h', '');
      String fnSettingStr = Repository.getAllDevicesData();
      if (fnSettingStr.isNotEmpty) {
        Map<String, dynamic> fnSettings = json.decode(fnSettingStr);
        List objectSettings = fnSettings[widget.maintenance!.imei] as List;
        if (objectSettings.elementAt(29) is List) {
          return;
        }
        Map<String, dynamic> dataMap = ((objectSettings.elementAt(29) as Map<String, dynamic>)[widget.maintenance!.id] as Map<String, dynamic>);
        setState(() {
          dataList = (dataMap['data_list'] as String) == 'true';
          popup = (dataMap['popup'] as String) == 'true';
          eventLastServiceSelected = (dataMap['update_last'] as String) == 'true';
          if ((dataMap['odo'] as String) == 'true') {
            serviceOdometerSelected = true;
            _odoIntervalController.text = (dataMap['odo_interval'] as int).toString();
            _odoLastController.text = (dataMap['odo_last'] as int).toString();
          } else {
            serviceOdometerSelected = false;
          }

          if ((dataMap['engh'] as String) == 'true') {
            serviceEngineHoursSelected = true;
            _enghIntervalController.text = (dataMap['engh_interval'] as String);
            _enghLastController.text = (dataMap['engh_last'] as String);
          } else {
            serviceEngineHoursSelected = false;
          }

          if ((dataMap['days'] as String) == 'true') {
            serviceDaysSelected = true;
            _daysController.text = (dataMap['days_interval'] as String);
            _daysLast = (dataMap['days_last'] as String);
          } else {
            serviceDaysSelected = false;
          }

          if ((dataMap['odo_left'] as String) == 'true') {
            eventOdometerSelected = true;
            _eventOdoIntervalController.text = (dataMap['odo_left_num'] as int).toString();
          } else {
            eventOdometerSelected = false;
          }

          if ((dataMap['engh_left'] as String) == 'true') {
            eventEngineHoursLeftSelected = true;
            _eventEnghLeftController.text = (dataMap['engh_left_num'] as String);
          } else {
            eventEngineHoursLeftSelected = false;
          }

          if ((dataMap['days_left'] as String) == 'true') {
            eventDaysLeftSelected = true;
            _eventDaysController.text = (dataMap['days_left_num'] as String);
          } else {
            eventDaysLeftSelected = false;
          }
        });
      }
    }
    if (mounted) {
      setState(() {});
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.maintenance == null ? 'Add Maintenance' : 'Edit Maintenance'),
        backgroundColor: Colors.white,
        foregroundColor: Colors.black,
      ),
      body: SafeArea(
        child: Container(
          height: MediaQuery.of(context).size.height,
          decoration: BoxDecoration(
            gradient: LinearGradient(begin: Alignment.topCenter, end: Alignment.bottomCenter, colors: [
              Color(0x90DC0E18),
              Color(0x10DC0E18),
            ]),
          ),
          child: Column(
            children: [
              Expanded(
                child: _isMaintenanceSubmitting
                    ? SpinKitRing(
                        lineWidth: 3.0,
                        color: AppColors.primaryColor,
                        size: 35.0,
                      )
                    : SingleChildScrollView(
                        child: Container(
                          padding: EdgeInsets.all(15),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: [
                              Container(
                                padding: EdgeInsets.all(15.0),
                                decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(16.0), boxShadow: [
                                  BoxShadow(
                                    color: AppColors.primaryMaterialColor[100]!,
                                    blurRadius: 5.0,
                                    spreadRadius: 1.0,
                                    offset: Offset(
                                      1.0, // Move to right 10  horizontally
                                      2.0, // Move to bottom 5 Vertically
                                    ),
                                  )
                                ]),
                                child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                                  Text(
                                    'SERVICE',
                                    style: TextStyle(color: Color(0xFFBDBDBD), fontSize: 17, fontWeight: FontWeight.w600),
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  CustomTextFormField(
                                    controller: _nameController,
                                    hinText: 'Name',
                                    boardColor: Color(0xFF0060A4),
                                    onChanged: (value) {},
                                  ),
                                  SizedBox(
                                    height: 20,
                                  ),
                                  DropDownFromField(
                                    list: deviceNames.values.toList(),
                                    selectedValue: selectedValue,
                                    onChanged: (value) {
                                      setState(() {
                                        selectedValue = value;
                                        selectedImei = deviceNames.keys.firstWhere((k) => deviceNames[k] == value, orElse: () => 'key');
                                        _currentOdometerController.text = fnObjects[selectedImei]!.odometer.toString();
                                        _currentEngineHoursController.text = getEhFromSeconds(fnObjects[selectedImei]!.engineHrs!);
                                      });
                                    },
                                  ),
                                  SizedBox(
                                    height: 20,
                                  ),
                                  Row(
                                    children: [
                                      Expanded(
                                        child: InkWell(
                                          onTap: () {
                                            setState(() {
                                              dataList = !dataList;
                                            });
                                          },
                                          child: Container(
                                            padding: EdgeInsets.all(12.0),
                                            decoration: BoxDecoration(
                                              color: dataList ? AppColors.primaryColor : Colors.transparent,
                                              borderRadius: BorderRadius.circular(4.0),
                                              border: Border.all(color: AppColors.primaryColor, width: 1.5),
                                            ),
                                            child: Center(
                                                child: Text(
                                              'Data List',
                                              style: TextStyle(color: dataList ? Colors.white : AppColors.primaryColor, fontWeight: FontWeight.w500, fontSize: 16),
                                            )),
                                          ),
                                        ),
                                      ),
                                      SizedBox(
                                        width: 10.0,
                                      ),
                                      Expanded(
                                        child: InkWell(
                                          onTap: () {
                                            setState(() {
                                              popup = !popup;
                                            });
                                          },
                                          child: Container(
                                            padding: EdgeInsets.all(12.0),
                                            decoration: BoxDecoration(
                                              color: popup ? AppColors.primaryColor : Colors.transparent,
                                              borderRadius: BorderRadius.circular(4.0),
                                              border: Border.all(color: AppColors.primaryColor, width: 1.5),
                                            ),
                                            child: Center(
                                                child: Text(
                                              'Popup',
                                              style: TextStyle(color: popup ? Colors.white : AppColors.primaryColor, fontSize: 16, fontWeight: FontWeight.w500),
                                            )),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  SwitchItem(
                                    title: 'Odometer',
                                    activeColor: Color(0xFFF26611),
                                    onTap: (value) {
                                      setState(() {
                                        serviceOdometerSelected = value;
                                      });
                                    },
                                    switchControl: serviceOdometerSelected,
                                  ),
                                  if (serviceOdometerSelected)
                                    Column(
                                      children: [
                                        Row(
                                          children: [
                                            Expanded(
                                              child: CustomTextFormField(
                                                textInputType: TextInputType.number,
                                                controller: _odoIntervalController,
                                                hinText: 'Interval (km)',
                                                boardColor: Color(0xFFF26611),
                                                onChanged: (value) {},
                                              ),
                                            ),
                                            SizedBox(
                                              width: 10,
                                            ),
                                            Expanded(
                                              child: CustomTextFormField(
                                                textInputType: TextInputType.number,
                                                controller: _odoLastController,
                                                hinText: 'Last service (km)',
                                                boardColor: Color(0xFFF26611),
                                                onChanged: (value) {},
                                              ),
                                            ),
                                          ],
                                        ),
                                        SizedBox(
                                          height: 10,
                                        ),
                                      ],
                                    ),
                                  SwitchItem(
                                    title: 'Engine Hours',
                                    activeColor: Color(0xFF0060A4),
                                    onTap: (value) {
                                      setState(() {
                                        serviceEngineHoursSelected = value;
                                      });
                                    },
                                    switchControl: serviceEngineHoursSelected,
                                  ),
                                  if (serviceEngineHoursSelected)
                                    Column(
                                      children: [
                                        Row(
                                          children: [
                                            Expanded(
                                              child: CustomTextFormField(
                                                textInputType: TextInputType.number,
                                                controller: _enghIntervalController,
                                                hinText: 'Interval (h)',
                                                boardColor: Color(0xFF0060A4),
                                                onChanged: (value) {},
                                              ),
                                            ),
                                            SizedBox(
                                              width: 10,
                                            ),
                                            Expanded(
                                              child: CustomTextFormField(
                                                textInputType: TextInputType.number,
                                                controller: _enghLastController,
                                                hinText: 'Last service (h)',
                                                boardColor: Color(0xFF0060A4),
                                                onChanged: (value) {},
                                              ),
                                            ),
                                          ],
                                        ),
                                        SizedBox(
                                          height: 10,
                                        ),
                                      ],
                                    ),
                                  SwitchItem(
                                    title: 'Days',
                                    activeColor: Color(0xFFF26611),
                                    onTap: (value) {
                                      setState(() {
                                        serviceDaysSelected = value;
                                      });
                                    },
                                    switchControl: serviceDaysSelected,
                                  ),
                                  if (serviceDaysSelected)
                                    Row(
                                      children: [
                                        Expanded(
                                          child: CustomTextFormField(
                                            textInputType: TextInputType.number,
                                            controller: _daysController,
                                            hinText: 'Interval (km)',
                                            boardColor: Color(0xFFF26611),
                                            onChanged: (value) {},
                                          ),
                                        ),
                                        SizedBox(
                                          width: 10,
                                        ),
                                        Expanded(
                                          child: InkWell(
                                            onTap: () async {
                                              DateTime? selectedDate = await showDatePicker(context: context, initialDate: DateTime.now(), firstDate: DateTime(2000), lastDate: DateTime(2100));
                                              if (selectedDate == null) return;
                                              setState(() {
                                                _daysLast =
                                                    selectedDate.year.toString().padLeft(4, '0') + '-' + selectedDate.month.toString().padLeft(2, '0') + '-' + selectedDate.day.toString().padLeft(2, '0');
                                              });
                                            },
                                            child: Container(
                                              padding: EdgeInsets.symmetric(vertical: 13.5, horizontal: 10),
                                              child: Text(
                                                _daysLast.isEmpty ? 'Last service' : _daysLast,
                                                style: TextStyle(color: _daysLast.isEmpty ? Color(0xFFBBBBBB) : AppColors.headerTextColor, fontSize: 16),
                                              ),
                                              decoration: BoxDecoration(borderRadius: BorderRadius.circular(5), border: Border.all(color: Color(0xFFF26611))),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                ]),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Container(
                                padding: EdgeInsets.all(15.0),
                                decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(16.0), boxShadow: [
                                  BoxShadow(
                                    color: AppColors.primaryMaterialColor[100]!,
                                    blurRadius: 5.0,
                                    spreadRadius: 1.0,
                                    offset: Offset(
                                      1.0, // Move to right 10  horizontally
                                      2.0, // Move to bottom 5 Vertically
                                    ),
                                  )
                                ]),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      'TRIGGER EVENT',
                                      style: TextStyle(color: Color(0xFFBDBDBD), fontSize: 17, fontWeight: FontWeight.w600),
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    Row(
                                      children: [
                                        SwitchItem(
                                          title: 'Odometer',
                                          activeColor: Color(0xFF0060A4),
                                          onTap: (value) {
                                            setState(() {
                                              eventOdometerSelected = value;
                                            });
                                          },
                                          switchControl: eventOdometerSelected,
                                        ),
                                        SizedBox(
                                          width: 10,
                                        ),
                                        Expanded(
                                          child: SwitchItem(
                                            title: 'Last service',
                                            activeColor: Color(0xFF0060A4),
                                            onTap: (value) {
                                              setState(() {
                                                eventLastServiceSelected = value;
                                              });
                                            },
                                            switchControl: eventLastServiceSelected,
                                          ),
                                        ),
                                      ],
                                    ),
                                    if (eventOdometerSelected)
                                      Column(
                                        children: [
                                          CustomTextFormField(
                                            textInputType: TextInputType.number,
                                            controller: _eventOdoIntervalController,
                                            hinText: 'Interval (KM)',
                                            boardColor: Color(0xFF0060A4),
                                            onChanged: (value) {},
                                          ),
                                          SizedBox(
                                            height: 10,
                                          ),
                                        ],
                                      ),
                                    SwitchItem(
                                      title: 'Engine hours left (h)',
                                      activeColor: Color(0xFFF26611),
                                      onTap: (value) {
                                        setState(() {
                                          eventEngineHoursLeftSelected = value;
                                        });
                                      },
                                      switchControl: eventEngineHoursLeftSelected,
                                    ),
                                    if (eventEngineHoursLeftSelected)
                                      Column(
                                        children: [
                                          Row(
                                            children: [
                                              Expanded(
                                                child: CustomTextFormField(
                                                  textInputType: TextInputType.number,
                                                  controller: _eventEnghLeftController,
                                                  hinText: 'Left (h)',
                                                  boardColor: Color(0xFFF26611),
                                                  onChanged: (value) {},
                                                ),
                                              ),
                                            ],
                                          ),
                                          SizedBox(
                                            height: 10,
                                          ),
                                        ],
                                      ),
                                    SwitchItem(
                                      title: 'Days left',
                                      activeColor: Color(0xFF0060A4),
                                      onTap: (value) {
                                        setState(() {
                                          eventDaysLeftSelected = value;
                                        });
                                      },
                                      switchControl: eventDaysLeftSelected,
                                    ),
                                    if (eventDaysLeftSelected)
                                      Row(
                                        children: [
                                          Expanded(
                                            child: CustomTextFormField(
                                              textInputType: TextInputType.number,
                                              controller: _eventDaysController,
                                              hinText: 'Left (d)',
                                              boardColor: Color(0xFF0060A4),
                                              onChanged: (value) {},
                                            ),
                                          ),
                                        ],
                                      ),
                                  ],
                                ),
                              ),
                              SizedBox(
                                height: 10,
                              ),
                              Container(
                                padding: EdgeInsets.all(15.0),
                                decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(16.0), boxShadow: [
                                  BoxShadow(
                                    color: AppColors.primaryMaterialColor[100]!,
                                    blurRadius: 5.0,
                                    spreadRadius: 1.0,
                                    offset: Offset(
                                      1.0, // Move to right 10  horizontally
                                      2.0, // Move to bottom 5 Vertically
                                    ),
                                  )
                                ]),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      'CURRENT OBJECTS COUNTERS',
                                      style: TextStyle(color: Color(0xFFBDBDBD), fontSize: 17, fontWeight: FontWeight.w600),
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    Row(
                                      children: [
                                        Expanded(
                                          child: CustomTextFormField(
                                            controller: _currentOdometerController,
                                            hinText: 'Current Odometer (km)',
                                            boardColor: Color(0xFFF26611),
                                            onChanged: (value) {},
                                          ),
                                        ),
                                      ],
                                    ),
                                    SizedBox(
                                      height: 20,
                                    ),
                                    Row(
                                      children: [
                                        Expanded(
                                          child: CustomTextFormField(
                                            controller: _currentEngineHoursController,
                                            hinText: 'Current Engine Hours (h)',
                                            boardColor: Color(0xFF0060A4),
                                            onChanged: (value) {},
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(
                                height: 10.0,
                              ),
                              InkWell(
                                onTap: () {
                                  if (_nameController.text.isEmpty) {
                                    showSnackBar(context, 'Please Enter Maintenance Name', Colors.red);
                                  } else if (selectedImei == 'key') {
                                    showSnackBar(context, 'Please select Object Name', Colors.red);
                                  } else {
                                    setState(() {
                                      _isMaintenanceSubmitting = true;
                                    });
                                    GPSServerRequests.saveMaintenance(
                                            _nameController.text,
                                            dataList.toString(),
                                            popup.toString(),
                                            serviceOdometerSelected.toString(),
                                            _odoIntervalController.text,
                                            _odoLastController.text,
                                            serviceEngineHoursSelected.toString(),
                                            _enghIntervalController.text,
                                            _enghLastController.text,
                                            serviceDaysSelected.toString(),
                                            _daysController.text,
                                            _daysLast,
                                            eventOdometerSelected.toString(),
                                            _eventOdoIntervalController.text,
                                            eventEngineHoursLeftSelected.toString(),
                                            _eventEnghLeftController.text,
                                            eventDaysLeftSelected.toString(),
                                            _eventDaysController.text,
                                            eventLastServiceSelected.toString(),
                                            selectedImei,
                                            widget.maintenance == null ? 'false' : widget.maintenance!.id)
                                        .then((result) {
                                      setState(() {
                                        _isMaintenanceSubmitting = false;
                                      });
                                      if (result.runtimeType == bool) {
                                        if (result as bool) {
                                          Navigator.pop(context, true);
                                        } else {
                                          showSnackBar(context, 'Failed to submit maintenance', Colors.red);
                                        }
                                      } else if (result.runtimeType == String) {
                                        showSnackBar(context, result, Colors.red);
                                      }
                                    });
                                  }
                                },
                                child: Container(
                                  height: 48,
                                  padding: EdgeInsets.all(12.0),
                                  decoration: BoxDecoration(
                                    color: AppColors.primaryColor,
                                    borderRadius: BorderRadius.circular(24.0),
                                  ),
                                  child: Center(
                                      child: Text(
                                    'Submit',
                                    style: TextStyle(color: Colors.white, fontWeight: FontWeight.w600),
                                  )),
                                ),
                              ),
                              SizedBox(
                                height: 10.0,
                              ),
                              InkWell(
                                onTap: () {
                                  Navigator.pop(context);
                                },
                                child: Container(
                                  height: 48,
                                  padding: EdgeInsets.all(12.0),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(24.0),
                                    border: Border.all(color: AppColors.primaryColor2, width: 1.5),
                                  ),
                                  child: Center(
                                      child: Text(
                                    'Cancel',
                                    style: TextStyle(color: AppColors.primaryColor2, fontWeight: FontWeight.w600),
                                  )),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
