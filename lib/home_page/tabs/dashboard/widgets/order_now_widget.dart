import 'package:autonemogps/config/colors.dart';
import 'package:autonemogps/config/string_constants.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher_string.dart';

class OrderNowWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        launchUrlString(SHOP_URL, mode: LaunchMode.externalApplication);
      },
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.all(5),
            margin: EdgeInsets.only(bottom: 16),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(16),
              boxShadow: [BoxShadow(color: AppColors.shadowMaterialColor[100]!, blurRadius: 5.0, spreadRadius: 1.0, offset: Offset(1.0, 2.0))],
            ),
            child: Image.asset('images/dashboard_order_now.png'),
          ),
          InkWell(
            onTap: () => launchUrlString('https://autonemo.li/zantrikbot', mode: LaunchMode.externalApplication),
            child: Container(
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(16),
                boxShadow: [BoxShadow(color: AppColors.shadowMaterialColor[100]!, blurRadius: 5.0, spreadRadius: 1.0, offset: Offset(1.0, 2.0))],
              ),
              padding: EdgeInsets.all(5),
              margin: EdgeInsets.only(bottom: 16),
              child: Image.asset('images/zantrik_bot_dashboard.png'),
            ),
          )
        ],
      ),
    );
  }
}
