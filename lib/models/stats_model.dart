class StatsModel {
  String moveDuration = '';

  String routeLength = '';

  String stopDuration = '';

  String topSpeed = '';

  String averageSpeed = '';

  String fuelConsumption = '';

  String averageFuelCons = '';

  String fuelCost = '';

  String engineWork = '';

  String engineIdle = '';

  StatsModel() {}
}
