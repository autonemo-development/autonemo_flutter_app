import 'package:autonemogps/api_calls/gps_server_requests.dart';
import 'package:autonemogps/config/colors.dart';
import 'package:autonemogps/home_page/tabs/dashboard/bar_chart.dart';
import 'package:autonemogps/models/chart_key_value_model.dart';
import 'package:autonemogps/utils/logs.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/cupertino.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class EventsGraph extends StatefulWidget {
  EventsGraph({required Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return EventsGraphState();
  }
}

class EventsGraphState extends State<EventsGraph> {
  List<charts.Series<ChartKeyValueModel, String>> eventsSeriesList = [];

  bool loading = false;
  bool hasNoData = false;

  @override
  void initState() {
    super.initState();
    loadGraphData();
  }

  @override
  Widget build(BuildContext context) {
    if (loading) {
      return SpinKitCircle(color: AppColors.primaryColor);
    }

    if (hasNoData) {
      return Container();
    }

    return BarGraph(eventsSeriesList, false);
  }

  void loadGraphData() {
    setState(() => loading = true);

    GPSServerRequests.getEventsDashboardGraph().then((Map eventsMap) {
      setState(() => this.hasNoData = eventsMap.length == 0);

      List<ChartKeyValueModel> listChart = [];

      eventsMap.keys.forEach((key) {
        try {
          var element = eventsMap[key];
          String name = element['name'];
          int count = element['count'];
          ChartKeyValueModel chartKeyValueModel = new ChartKeyValueModel(name, count);
          listChart.add(chartKeyValueModel);
        } catch (e) {
          writeErrorLogs('Event Error ', e: e);
        }
      });

      charts.Series<ChartKeyValueModel, String> eventsSeriesValue = new charts.Series<ChartKeyValueModel, String>(
        id: 'Events (today)',
        colorFn: (_, __) => charts.MaterialPalette.red.shadeDefault,
        domainFn: (ChartKeyValueModel chartKeyValueModel, _) => chartKeyValueModel.key,
        measureFn: (ChartKeyValueModel chartKeyValueModel, _) => chartKeyValueModel.value,
        labelAccessorFn: (ChartKeyValueModel chartKeyValueModel, _) => chartKeyValueModel.value.toString(),
        data: listChart,
      );
      setState(() => eventsSeriesList = [eventsSeriesValue]);
      setState(() => loading = false);
    });
  }
}
