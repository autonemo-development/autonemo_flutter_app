import 'package:autonemogps/api_calls/gps_server_requests.dart';
import 'package:autonemogps/config/colors.dart';
import 'package:autonemogps/config/string_constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:url_launcher/url_launcher_string.dart';

class EngineOilRecommendations extends StatefulWidget {
  EngineOilRecommendations({required Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return EngineOilRecommendationsState();
  }
}

class EngineOilRecommendationsState extends State<EngineOilRecommendations> {
  List<dynamic> vehicleData = [];

  bool loading = false;

  bool hasNoData = false;

  @override
  void initState() {
    loadEngineOilRecommendations();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (loading) {
      return SpinKitCircle(color: AppColors.primaryColor);
    }

    if (hasNoData) {
      return Container();
    }

    return Container(
      padding: EdgeInsets.all(10),
      margin: EdgeInsets.only(bottom: 16),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(16),
        boxShadow: [BoxShadow(color: AppColors.shadowMaterialColor[100]!, blurRadius: 5.0, spreadRadius: 1.0, offset: Offset(1.0, 2.0))],
      ),
      child: Column(
        children: [
          Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [Text('Engine Oil Recommendations', style: TextStyle(color: AppColors.headerTextColor))]),
          SizedBox(width: 20),
          Divider(),
          Container(
            height: vehicleData.length * 55,
            constraints: BoxConstraints(maxHeight: 200),
            child: ListView.builder(
              itemCount: vehicleData.length,
              itemBuilder: (context, index) {
                Map<String, dynamic> vehicleDataItem = vehicleData.elementAt(index);
                return InkWell(
                  onTap: () => launchUrlString(SHOP_URL, mode: LaunchMode.externalApplication),
                  child: Container(
                    decoration: BoxDecoration(color: Color(0xFFF8F8F8), borderRadius: BorderRadius.circular(10)),
                    margin: EdgeInsets.only(top: 5),
                    padding: EdgeInsets.all(10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Container(child: Image.asset('images/engine_oil_recommendation_dashboard_icon.png'), height: 25, width: 25),
                        SizedBox(width: 5),
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(vehicleDataItem['vehicle_model'], style: TextStyle(fontSize: 12, fontWeight: FontWeight.w600)),
                              SizedBox(height: 2),
                              Text(vehicleDataItem['car_reg_number'], style: TextStyle(fontSize: 10)),
                            ],
                          ),
                        ),
                        SizedBox(width: 10),
                        if (vehicleDataItem['engine_oil_grade'] != null)
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              Text(vehicleDataItem['engine_oil_grade'], style: TextStyle(fontSize: 12, fontWeight: FontWeight.w600, color: Colors.red)),
                              Text('Oil Grade', style: TextStyle(fontSize: 10)),
                            ],
                          ),
                      ],
                    ),
                  ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }

  void loadEngineOilRecommendations() {
    setState(() => this.loading = true);
    GPSServerRequests.getClientData().then((value) {
      if (value[STATUS] == 1) {
        Map<String, dynamic> dataMap = value[DATA];
        setState(() => this.vehicleData = dataMap['vehicle_data']);
      }

      setState(() => this.hasNoData = this.vehicleData.length == 0);

      setState(() => this.loading = false);
    });
  }
}
