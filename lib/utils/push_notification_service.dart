import 'dart:convert';

import 'package:autonemogps/config/string_constants.dart';
import 'package:autonemogps/screens/detailed_alert_screen.dart';
import 'package:autonemogps/storage/repository.dart';
import 'package:autonemogps/utils/logs.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async => await Firebase.initializeApp();

class PushNotificationService {
  static Future<String> getFCMToken() async {
    return await FirebaseMessaging.instance.getToken() ?? '';
  }

  static void subscribeToNotification() {
    bool isNotificationEnabled = Repository.getNotificationStatus();
    if (!isNotificationEnabled) {
      return;
    }
    String username = Repository.getUsername();
    username = username.replaceAll('@', '_at_').trim();
    FirebaseMessaging.instance.subscribeToTopic(username).then((value) {
      writeDebugLogs('subscribed to topic $username');
    }).catchError((e) {
      writeErrorLogs(e);
    });
  }

  static void unsubscribeFromNotification() {
    String username = Repository.getUsername();
    username = username.replaceAll('@', '_at_').trim();
    FirebaseMessaging.instance.unsubscribeFromTopic(username).then((value) {
      writeDebugLogs('unsubscribed from topic $username');
    }).catchError((e) {
      writeErrorLogs(e);
    });
  }

  static void openAlertScreen(String s, GlobalKey<NavigatorState> navigatorKey) {
    Map<String, dynamic> body = json.decode(s);
    Map<String, dynamic> eventMap = {};
    if (Repository.getServerType() == GPSServerConstants.type) {
      eventMap = {
        'latitude': double.parse(body['lat']),
        'longitude': double.parse(body['lng']),
        'vehicleName': body['name'],
        'alert_name': body['desc'],
        'orientation': int.parse(body['angle']),
        'speed': int.parse(body['speed']),
        'device_time': body['dt_tracker'],
        'deviceId': body['imei']
      };
    } else if (Repository.getServerType() == GPSWoxConstants.type) {
      eventMap = {
        'latitude': body['latitude'],
        'longitude': body['longitude'],
        'address': body['address'],
        'device_name': body['name'],
        'name': body['message'],
        'course': body['course'],
        'speed': double.parse(body['speed']).toInt(),
        'time': body['time'],
        'device_id': body['device_id']
      };
    }
    navigatorKey.currentState!.push(MaterialPageRoute(builder: (context) => DetailedAlert(event: eventMap)));
  }

  static void initFirebase(GlobalKey<NavigatorState> navigatorKey) {
    FirebaseMessaging.instance.getInitialMessage().then((RemoteMessage? message) {
      if (message != null) {
        openAlertScreen(message.data['body'], navigatorKey);
      }
    });

    FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();

    final AndroidNotificationChannel channel = AndroidNotificationChannel(
      CHANNEL_ID, // id
      APP_NAME, // title
      description: 'This channel is used for notifications.',
      // description
      importance: Importance.high,
      playSound: true,
      enableLights: true,
      enableVibration: true,
    );

    const AndroidNotificationChannel ignitionOnChannel = AndroidNotificationChannel(
      'ign_on_channel', // id
      'Ignition On', // title
      description: 'This channel is used for Ignition On notifications.',
      importance: Importance.high,
      playSound: true,
      enableLights: true,
      enableVibration: true,
    );
    const AndroidNotificationChannel ignitionOffChannel = AndroidNotificationChannel(
      'ign_off_channel', // id
      'Ignition Off', // title
      description: 'This channel is used for Ignition Off notifications.',
      // description
      importance: Importance.high,
      playSound: true,
      enableLights: true,
      enableVibration: true,
    );

    FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);

    flutterLocalNotificationsPlugin.getNotificationAppLaunchDetails().then((value) {
      if (value?.didNotificationLaunchApp ?? false) {
        openAlertScreen(value?.notificationResponse?.payload ?? '', navigatorKey);
      }
    });

    FirebaseMessaging.onMessage.listen((RemoteMessage message) {
      RemoteNotification? notification = message.notification;
      Map<String, dynamic> notificationData = message.data;

      if (notification != null) {
        String channelId = channel.id;
        String channelName = channel.name;
        String channelDescription = channel.description!;
        if (notificationData['type'] != null && notificationData['type'] == 'parking_violation') {}
        RawResourceAndroidNotificationSound sound = RawResourceAndroidNotificationSound('accomplished');
        String iosSound = 'accomplished.caf';
        if (notificationData['type'] == 'ignition_on') {
          sound = RawResourceAndroidNotificationSound('ignition_on');
          iosSound = 'ignition_on.caf';
          channelId = ignitionOnChannel.id;
          channelName = ignitionOnChannel.name;
          channelDescription = ignitionOnChannel.description!;
        } else if (notificationData['type'] == 'ignition_off') {
          sound = RawResourceAndroidNotificationSound('ignition_off');
          iosSound = 'ignition_off.caf';
          channelId = ignitionOffChannel.id;
          channelName = ignitionOffChannel.name;
          channelDescription = ignitionOffChannel.description!;
        }
        flutterLocalNotificationsPlugin.show(
            notification.hashCode,
            notification.title,
            notification.body,
            NotificationDetails(
                android: AndroidNotificationDetails(
                  channelId,
                  channelName,
                  channelDescription: channelDescription,
                  icon: 'ic_notification',
                  importance: Importance.max,
                  priority: Priority.high,
                  playSound: true,
                  enableLights: true,
                  sound: sound,
                ),
                iOS: DarwinNotificationDetails(
                  presentSound: true,
                  presentAlert: true,
                  sound: iosSound,
                )),
            payload: notificationData['body']);
      }
    });

    FirebaseMessaging.onMessageOpenedApp.listen((RemoteMessage message) => openAlertScreen(message.data['body'], navigatorKey));
  }
}
