import 'package:autonemogps/api_calls/gps_server_requests.dart';
import 'package:autonemogps/common/custom_marker.dart';
import 'package:autonemogps/config/colors.dart';
import 'package:autonemogps/config/helpers.dart';
import 'package:autonemogps/config/string_constants.dart';
import 'package:autonemogps/models/device.dart';
import 'package:autonemogps/models/device_model.dart';
import 'package:autonemogps/storage/repository.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class DetailedAlert extends StatefulWidget {
  final Map<String, dynamic>? event;
  final double? lat, lng;
  final String? imei, title;
  final String? eventId;

  DetailedAlert({this.event, this.eventId, this.lat, this.lng, this.imei, this.title});

  @override
  _DetailedAlertState createState() => _DetailedAlertState();
}

class _DetailedAlertState extends State<DetailedAlert> {
  Set<Marker> _markers = Set<Marker>();
  GoogleMapController? _controller;
  MapType _mapType = MapType.normal;
  late LatLng location;
  String? imei;
  bool _isLoading = false;
  Map<String, String> data = Map<String, String>();

  @override
  void initState() {
    super.initState();
    location = center;
    if (widget.event != null)
      parseData();
    else if (widget.eventId != null) {
      fetchData();
    } else {
      location = LatLng(widget.lat ?? 0, widget.lng ?? 0);
      imei = widget.imei!;
    }
  }

  parseData() {
    if (Repository.getServerType() == GPSWoxConstants.type) {
      double lat = widget.event?['latitude'].runtimeType == int ? (widget.event?['latitude'] as int).toDouble() : widget.event?['latitude'] as double;
      double lng = widget.event?['longitude'].runtimeType == int ? (widget.event?['longitude'] as int).toDouble() : widget.event?['longitude'] as double;
      data.putIfAbsent('Object', () => widget.event?['device_name'] as String);
      data.putIfAbsent('Event', () => widget.event?['name'] as String);

      data.putIfAbsent('Position', () => lat.toString() + '°, ' + lng.toString() + '°');
      if (widget.event?['address'] != null) {
        data.putIfAbsent('Address', () => widget.event?['address'] as String);
      }
      data.putIfAbsent('Angle', () => ((widget.event?['course'] ?? 0) as int).toString() + '°');
      data.putIfAbsent('Speed', () => (widget.event?['speed'] as int).toString() + 'kph');
      data.putIfAbsent('Time', () => widget.event?['time'] as String);
      imei = AllDeviceModelData().deviceListByDeviceId[widget.event?['device_id']]!.imei;
      location = LatLng(lat, lng);
    } else {
      double lat = widget.event?['latitude'].runtimeType == int ? (widget.event?['latitude'] as int).toDouble() : widget.event?['latitude'] as double;
      double lng = widget.event?['longitude'].runtimeType == int ? (widget.event?['longitude'] as int).toDouble() : widget.event?['longitude'] as double;
      data.putIfAbsent('Object', () => widget.event?['vehicleName'] as String);
      data.putIfAbsent('Event', () => widget.event?['alert_name'] as String);

      data.putIfAbsent('Position', () => lat.toString() + '°, ' + lng.toString() + '°');
      data.putIfAbsent('Angle', () => (widget.event?['orientation'] as int).toString() + '°');
      data.putIfAbsent('Speed', () => (widget.event?['speed'] as int).toString() + 'kph');
      data.putIfAbsent('Time', () => widget.event?['device_time'] as String);
      imei = AllDeviceModelData().deviceModelAndImeiMapGpsServer[widget.event?['deviceId']]!.imei;
      location = LatLng(lat, lng);
    }
    setMarkers();
  }

  fetchData() {
    _isLoading = true;
    GPSServerRequests.fetchDetailedEvent(widget.eventId!).then((list) {
      if (list[0] == 1) {
        _isLoading = false;
        Map<String, dynamic> mapData = list[1] as Map<String, dynamic>;
        data.putIfAbsent('Object', () => mapData['object'] as String);
        data.putIfAbsent('Event', () => mapData['event'] as String);
        data.putIfAbsent('Address', () => mapData['address'] as String);
        data.putIfAbsent('Position', () => mapData['position'] as String);
        data.putIfAbsent('Altitude', () => mapData['altitude'] as String);
        data.putIfAbsent('Angle', () => mapData['angle'] as String);
        data.putIfAbsent('Speed', () => (mapData['speed'] as int).toString() + ' kph');
        data.putIfAbsent('Time', () => mapData['time'] as String);
        imei = mapData['imei'] as String;
        location = LatLng(double.parse(mapData['lat'] as String), double.parse(mapData['lng'] as String));
        setMarkers();
        if (mounted)
          setState(() {
            _isLoading = false;
          });
      } else {
        showDialog(
            barrierDismissible: true,
            context: context,
            builder: (context) => AlertDialog(
                  title: Text('Error'),
                  content: Text(list[1] as String),
                  actions: <Widget>[
                    TextButton(
                      child: Text('OK', style: TextStyle(color: Colors.black)),
                      onPressed: () {
                        Navigator.pop(context);
                        Navigator.pop(context);
                      },
                    ),
                  ],
                ));
      }
    });
  }

  setMarkers() async {
    DeviceModel? deviceModel = AllDeviceModelData().deviceModelAndImeiMapGpsServer[imei];
    if (deviceModel == null) {
      return;
    }

    Map<String, dynamic> markerData = getVehicleMarkerData('m', deviceModel.icon);
    var markerBitmap = await getMarkerFromImagePath(markerData['iconKey']);

    var markerId = MarkerId('event');

    var marker = Marker(
      markerId: markerId,
      position: location,
      anchor: Offset(0.5, 0.5),
      rotation: widget.event != null ? double.parse(data['Angle']!.replaceAll('°', '')) : 0,
      icon: markerBitmap,
    );
    setState(() {
      _markers.add(marker);
    });
    CameraPosition _kLake = CameraPosition(target: location, zoom: 16);
    _controller?.animateCamera(CameraUpdate.newCameraPosition(_kLake));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          widget.title ?? 'Notification',
          style: TextStyle(color: Colors.white, fontWeight: FontWeight.w600),
        ),
        backgroundColor: AppColors.primaryColor,
        iconTheme: IconThemeData(
          color: Colors.white, //change your color here
        ),
        systemOverlayStyle: SystemUiOverlayStyle.light,
      ),
      resizeToAvoidBottomInset: false,
      body: Stack(
        children: [
          GoogleMap(
            mapType: _mapType,
            zoomGesturesEnabled: true,
            markers: _markers,
            onMapCreated: (GoogleMapController animationController) => _controller = animationController,
            initialCameraPosition: CameraPosition(target: location, zoom: 11.0),
            zoomControlsEnabled: false,
            compassEnabled: false,
            mapToolbarEnabled: false,
            myLocationEnabled: true,
            myLocationButtonEnabled: false,
            tiltGesturesEnabled: false,
          ),
          Positioned(
            top: 10.0,
            right: 10.0,
            child: Container(
              padding: EdgeInsets.all(10.0),
              decoration: BoxDecoration(
                color: _mapType == MapType.normal ? Colors.white : Colors.grey[300],
                borderRadius: BorderRadius.all(Radius.circular(2)),
                boxShadow: [
                  BoxShadow(color: AppColors.shadowMaterialColor, blurRadius: 3, spreadRadius: 0),
                ],
              ),
              child: PopupMenuButton(
                child: Image.asset(
                  'images/roadmap.png',
                  height: 30,
                  width: 30,
                ),
                onSelected: (MapType value) {
                  if (mounted)
                    setState(() {
                      _mapType = value;
                    });
                },
                itemBuilder: (context) => [
                  PopupMenuItem(
                    value: MapType.normal,
                    child: Text('Google Streets'),
                  ),
                  PopupMenuItem(
                    value: MapType.terrain,
                    child: Text('Google Terrain'),
                  ),
                  PopupMenuItem(
                    value: MapType.satellite,
                    child: Text('Google Satellite'),
                  ),
                  PopupMenuItem(
                    value: MapType.hybrid,
                    child: Text('Google Hybrid'),
                  ),
                ],
              ),
            ),
          ),
          _isLoading
              ? Container(
                  color: Colors.black.withOpacity(.3),
                  child: Center(
                    child: SpinKitRing(
                      lineWidth: 3.0,
                      color: AppColors.primaryColor,
                      size: 35.0,
                    ),
                  ),
                )
              : Positioned(
                  bottom: 0,
                  left: 0,
                  right: 0,
                  // height: 200,
                  child: AlertBottomSheet(data),
                ),
        ],
      ),
    );
  }
}

class AlertBottomSheet extends StatelessWidget {
  final Map<String, dynamic> data;

  const AlertBottomSheet(this.data, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<String> keysList = data.keys.toList();
    return Container(
      padding: EdgeInsets.all(10),
      margin: EdgeInsets.all(10),
      // width: MediaQuery.of(context).size.width - 20,
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(color: AppColors.shadowMaterialColor[100]!, blurRadius: 3, spreadRadius: 0),
        ],
      ),
      child: Wrap(
        spacing: 30,
        runSpacing: 10,
        children: keysList.map((key) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                key,
                style: TextStyle(color: AppColors.headerTextColor, fontWeight: FontWeight.bold, fontSize: 12),
              ),
              Text(
                data[key].toString(),
                style: TextStyle(
                  color: AppColors.kBlackColor,
                  fontSize: 14,
                ),
              )
            ],
          );
        }).toList(),
      ),
    );
  }
}
