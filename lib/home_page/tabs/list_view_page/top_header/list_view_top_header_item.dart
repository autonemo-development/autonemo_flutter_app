import 'package:autonemogps/config/colors.dart';
import 'package:flutter/material.dart';

class ListViewTopHeaderItem extends StatelessWidget {
  final String text;
  final int count;
  final bool isSelected;
  final void Function() onTap;

  ListViewTopHeaderItem({
    required this.text,
    this.isSelected = false,
    required this.onTap,
    required this.count,
  });

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: AnimatedContainer(
          curve: Curves.fastLinearToSlowEaseIn,
          duration: Duration(milliseconds: 1000),
          padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
          margin: EdgeInsets.all(5),
          decoration: BoxDecoration(
            color: isSelected ? AppColors.primaryColor : Colors.transparent,
            borderRadius: BorderRadius.circular(30.0),
          ),
          child: Text(
            text + ' (' + count.toString() + ')',
            style: TextStyle(
              decoration: TextDecoration.none,
              color: isSelected ? Colors.white : Colors.black,
              fontFamily: 'Poppins',
              fontSize: 14,
              fontWeight: FontWeight.w500,
            ),
          )),
    );
  }
}
