import 'dart:convert';

import 'package:autonemogps/api_calls/gps_server_requests.dart';
import 'package:autonemogps/api_calls/gps_wox_requests.dart';
import 'package:autonemogps/config/colors.dart';
import 'package:autonemogps/config/helpers.dart';
import 'package:autonemogps/config/string_constants.dart';
import 'package:autonemogps/home_page/tabs/dashboard/my_profile_tab.dart';
import 'package:autonemogps/home_page/tabs/dashboard/widget_settings.dart';
import 'package:autonemogps/home_page/tabs/dashboard/widgets/carousal/carousal_widget.dart';
import 'package:autonemogps/home_page/tabs/dashboard/widgets/engine_oil_recommendations_widget.dart';
import 'package:autonemogps/home_page/tabs/dashboard/widgets/events_graph.dart';
import 'package:autonemogps/home_page/tabs/dashboard/widgets/expenses_widget.dart';
import 'package:autonemogps/home_page/tabs/dashboard/widgets/fault_code_widget.dart';
import 'package:autonemogps/home_page/tabs/dashboard/widgets/fleet_status_widget.dart';
import 'package:autonemogps/home_page/tabs/dashboard/widgets/fuel_graph.dart';
import 'package:autonemogps/home_page/tabs/dashboard/widgets/maintenance_widget.dart';
import 'package:autonemogps/home_page/tabs/dashboard/widgets/odometer_graph.dart';
import 'package:autonemogps/home_page/tabs/dashboard/widgets/order_now_widget.dart';
import 'package:autonemogps/home_page/tabs/dashboard/widgets/quick_links_widget.dart';
import 'package:autonemogps/home_page/tabs/dashboard/widgets/single_device_widget.dart';
import 'package:autonemogps/home_page/tabs/dashboard/widgets/vehicle_mileage_graph.dart';
import 'package:autonemogps/home_page/tabs/dashboard/widgets/vehicle_selector_widget.dart';
import 'package:autonemogps/main.dart';
import 'package:autonemogps/storage/repository.dart';
import 'package:autonemogps/utils/logs.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class Dashboard extends StatefulWidget {
  final Function changeToListTab;

  const Dashboard(this.changeToListTab);

  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> with SingleTickerProviderStateMixin, RouteAware {
  Map<String, String> allDeviceObjects = {};
  Map<int, String> allDeviceIdsObjects = {};
  String selectedDevice = '';
  String selectedImei = '';

  int selectedTab = 0;
  late TabController _tabController;

  GlobalKey<MaintenanceDashboardState> _maintenanceDashboard = new GlobalKey<MaintenanceDashboardState>();
  GlobalKey<ExpensesDashboardState> _expensesDashboard = new GlobalKey<ExpensesDashboardState>();
  GlobalKey<FaultCodeDashboardState> _faultCodeDashboard = new GlobalKey<FaultCodeDashboardState>();
  GlobalKey<SingleDeviceWidgetState> _singleDeviceWidget = new GlobalKey<SingleDeviceWidgetState>();
  GlobalKey<VehicleMileageState> _vehicleMileage = new GlobalKey<VehicleMileageState>();
  GlobalKey<OdometerGraphState> _allOdometerGraph = new GlobalKey<OdometerGraphState>();
  GlobalKey<FuelGraphState> _fuelGraph = new GlobalKey<FuelGraphState>();
  GlobalKey<EventsGraphState> _allEventsGraph = new GlobalKey<EventsGraphState>();
  GlobalKey<EngineOilRecommendationsState> _engineOilWidget = new GlobalKey<EngineOilRecommendationsState>();

  List widgetStringsList = [];

  @override
  void initState() {
    _tabController = new TabController(length: 2, vsync: this);

    _tabController.addListener(() {
      writeDebugLogs('Current Tab Index ' + _tabController.index.toString());
      setState(() {
        selectedTab = _tabController.index;
      });
    });

    String str = Repository.getAllDevicesData();
    if (str.isNotEmpty) {
      parseDeviceData(str);
    } else {
      fetchDeviceData();
    }

    widgetStringsList = getWidgetList();

    WidgetsBinding.instance.addPostFrameCallback((_) async {
      routeObserver.subscribe(this, ModalRoute.of(context) as PageRoute);
    });

    super.initState();
  }

  @override
  void dispose() {
    routeObserver.unsubscribe(this);
    super.dispose();
  }

  @override
  void didPopNext() {
    setState(() => widgetStringsList = getWidgetList());
    super.didPopNext();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: DefaultTabController(
        length: 2,
        child: Scaffold(
          backgroundColor: AppColors.defaultBackground,
          appBar: AppBar(
            centerTitle: false,
            elevation: 0.2,
            backgroundColor: Colors.white,
            title: Image.asset('images/logo_colored.png', width: 150),
            actions: [
              InkWell(
                onTap: () => Navigator.of(context, rootNavigator: true).push(MaterialPageRoute(builder: (context) => DashboardWidgetSettings())),
                child: Container(padding: EdgeInsets.all(2), width: 30, child: Image.asset('images/dashboard_edit_button.png')),
              ),
              SizedBox(width: 10),
              InkWell(
                onTap: () {
                  if (_maintenanceDashboard.currentState != null) {
                    _maintenanceDashboard.currentState?.fetchMaintenanceData();
                  }
                  if (_expensesDashboard.currentState != null) {
                    _expensesDashboard.currentState?.fetchExpensesData();
                  }
                  if (_faultCodeDashboard.currentState != null) {
                    _faultCodeDashboard.currentState?.fetchFaultCodeData();
                  }
                  if (_singleDeviceWidget.currentState != null) {
                    _singleDeviceWidget.currentState?.fetchData();
                  }
                  if (_vehicleMileage.currentState != null) {
                    _vehicleMileage.currentState?.fetchData();
                  }
                  if (_allOdometerGraph.currentState != null) {
                    _allOdometerGraph.currentState?.loadGraphData();
                  }
                  if (_fuelGraph.currentState != null) {
                    _fuelGraph.currentState?.loadDevicesData();
                  }
                  if (_allEventsGraph.currentState != null) {
                    _allEventsGraph.currentState?.loadGraphData();
                  }
                  if (_engineOilWidget.currentState != null) {
                    _engineOilWidget.currentState?.loadEngineOilRecommendations();
                  }
                },
                child: Container(width: 30, child: Icon(Icons.refresh, color: AppColors.headerTextColor, size: 30)),
              ),
              SizedBox(width: 20)
            ],
            bottom: TabBar(labelColor: Colors.black, controller: _tabController, indicatorColor: Colors.red, tabs: [Tab(text: 'Dashboard'), Tab(text: 'My Profile')]),
          ),
          body: IndexedStack(
            index: selectedTab,
            children: [
              SafeArea(
                child: Container(
                  padding: EdgeInsets.only(left: 16, right: 16),
                  child: SingleChildScrollView(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: generateDashboardWidget(widgetStringsList),
                    ),
                  ),
                ),
              ),
              SafeArea(child: MyProfileTab())
            ],
          ),
        ),
      ),
    );
  }

  fetchDeviceData() async {
    if (Repository.getServerType() == GPSServerConstants.type) {
      await GPSServerRequests.fetchFnSettings();
    } else if (Repository.getServerType() == GPSWoxConstants.type) {
      await GPSWoxRequests.getDevices();
    }

    String str = Repository.getAllDevicesData();
    if (str.isNotEmpty) {
      parseDeviceData(str);
    }
  }

  parseDeviceData(String str) {
    allDeviceObjects = {};
    allDeviceIdsObjects = {};

    if (Repository.getServerType() == GPSServerConstants.type) {
      var dynamicData = json.decode(str);
      if (dynamicData.runtimeType == List) {
        return;
      }

      Map<String, dynamic> map = dynamicData as Map<String, dynamic>;

      //Edge case when user has no devices

      if (map['data'].runtimeType == List) {
        return;
      }

      Map<String, dynamic> data = map['data'] as Map<String, dynamic>;

      data.forEach((key, value) => allDeviceObjects[key] = value['name'] as String);

      changeDevicesData('');
    }

    if (Repository.getServerType() == GPSWoxConstants.type) {
      List<dynamic> list = json.decode(str) as List<dynamic>;
      list.forEach((element) {
        Map<String, dynamic> groupMap = element as Map<String, dynamic>;
        List itemsList = groupMap['items'] as List;
        DateFormat inputFormat = DateFormat('yyyy-MM-dd');
        itemsList.forEach((element) {
          Map<String, dynamic> deviceData = element['device_data'] as Map<String, dynamic>;

          String? expirationDate = deviceData['expiration_date'];
          if (expirationDate != null) {
            if (DateTime.now().isBefore(inputFormat.parse(expirationDate))) {
              allDeviceObjects[deviceData['imei']] = element['name'];
              allDeviceIdsObjects[element['id']] = element['name'];
            }
          } else {
            allDeviceObjects[deviceData['imei']] = element['name'];
            allDeviceIdsObjects[element['id']] = element['name'];
          }
        });
      });
    }
  }

  void changeDevicesData(String? selectedDeviceString) {
    allDeviceObjects.forEach((key, value) {
      if (value == selectedDeviceString) {
        setState(() {
          this.selectedDevice = value;
          this.selectedImei = key;
        });
      }
    });

    if (selectedDeviceString!.isEmpty) {
      if (allDeviceObjects.entries.length > 0) {
        MapEntry<String, String> firstData = allDeviceObjects.entries.first;
        setState(() {
          this.selectedDevice = firstData.value;
          this.selectedImei = firstData.key;
        });
      }
    }
  }

  List<Widget> generateDashboardWidget(List<dynamic> widgetStringsList) {
    List<Widget> dashboardWidgets = [];

    widgetStringsList.forEach((element) {
      String widgetName = element['name'];
      bool isEnabled = element['isEnabled'];

      if (isEnabled) {
        switch (widgetName) {
          case 'Discount and Offer Cards':
            {
              dashboardWidgets.add(CarousalDart());
            }
            break;
          case 'Order Now':
            {
              dashboardWidgets.add(OrderNowWidget());
            }
            break;
          case 'Fleet Status':
            {
              dashboardWidgets.add(FleetStatus(widget.changeToListTab));
            }
            break;
          case 'Vehicle Selector':
            {
              dashboardWidgets.add(VehicleSelector(allDeviceObjects, selectedDevice, (selectedDevice) => changeDevicesData(selectedDevice)));
            }
            break;
          case 'Value Card':
            {
              dashboardWidgets.add(SingleDeviceWidget(this.selectedDevice, this.selectedImei, key: _singleDeviceWidget));
            }
            break;
          case 'Engine Oil Recommendations':
            {
              dashboardWidgets.add(EngineOilRecommendations(key: _engineOilWidget));
            }
            break;
          case 'Expenses':
            {
              dashboardWidgets.add(ExpensesDashboard(key: _expensesDashboard));
            }
            break;
          case 'Maintenance':
            {
              dashboardWidgets.add(MaintenanceDashboard(key: _maintenanceDashboard));
            }
            break;
          case 'DTC Codes':
            {
              dashboardWidgets.add(FaultCodeDashboard(key: _faultCodeDashboard));
            }
            break;
          case 'Mileage Graph':
            {
              dashboardWidgets.add(VehicleMileage(selectedImei, selectedDevice, key: _vehicleMileage));
            }
            break;
          case 'Odometer Graph':
            {
              dashboardWidgets.add(OdometerGraph(key: _allOdometerGraph));
            }
            break;
          case 'Fuel Graph':
            {
              dashboardWidgets.add(FuelGraph(selectedImei, selectedDevice, key: _fuelGraph));
            }
            break;
          case 'Events Graph':
            {
              dashboardWidgets.add(EventsGraph(key: _allEventsGraph));
            }
            break;
          case 'Quick Links':
            {
              dashboardWidgets.add(QuickLinks());
            }
            break;
        }
      }
    });

    List<Widget> dashboardWidgetsWithSizedBox = [];

    dashboardWidgetsWithSizedBox.add(SizedBox(height: 16));

    dashboardWidgets.forEach((element) => dashboardWidgetsWithSizedBox.add(element));

    dashboardWidgetsWithSizedBox.add(SizedBox(height: 16));

    return dashboardWidgetsWithSizedBox;
  }
}
