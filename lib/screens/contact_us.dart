import 'package:autonemogps/config/colors.dart';
import 'package:autonemogps/config/helpers.dart';
import 'package:flutter/material.dart';

class ContactUsScreen extends StatefulWidget {
  @override
  _ContactUsScreenState createState() => _ContactUsScreenState();
}

class _ContactUsScreenState extends State<ContactUsScreen> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Stack(
          children: [
            Positioned(
              top: 0,
              left: 0,
              right: 0,
              child: Container(
                height: 150,
                padding: EdgeInsets.only(
                  left: 16,
                  right: 16,
                  top: 10,
                ),
                width: MediaQuery.of(context).size.width,
                decoration: new BoxDecoration(
                  borderRadius: BorderRadius.only(bottomLeft: Radius.circular(24), bottomRight: Radius.circular(24)),
                  gradient: new LinearGradient(begin: Alignment.topCenter, end: Alignment.bottomCenter, colors: [
                    Color(0x90DC0E18),
                    Color(0x10DC0E18),
                  ]),
                ),
                child: Align(
                  alignment: Alignment.topLeft,
                  child: Container(
                    height: 56,
                    child: Row(
                      children: [
                        IconButton(
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                            icon: Icon(
                              Icons.keyboard_backspace,
                              color: Colors.white,
                            )),
                        Text(
                          'Live Support',
                          style: TextStyle(color: Colors.white, fontSize: 24, fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
            Positioned(
              top: 80,
              left: 16,
              right: 16,
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    Row(
                      children: [
                        Expanded(
                            child: LiveSupportItems('images/email.png', 'Email', '9am-6pm', () {
                          String email = 'support@autonemo.io';
                          launchUrl('mailto:${email}');
                        })),
                        SizedBox(
                          width: 16,
                        ),
                        Expanded(
                          child: LiveSupportItems('images/hotline.png', 'Hotline', '9am-11pm', () {
                            launchUrl('tel:+8809606882288');
                          }),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 16,
                    ),
                    Row(
                      children: [
                        Expanded(
                          child: LiveSupportItems('images/chatbot.png', 'Live Chat Bot', '9am-11pm', () {
                            String urlString = 'https://tawk.to/chat/62120f771ffac05b1d7ac529/1fsb8orhu';
                            launchUrl(urlString);
                          }),
                        ),
                        SizedBox(
                          width: 16,
                        ),
                        Expanded(
                          child: SizedBox(),
                        )
                      ],
                    ),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

class LiveSupportItems extends StatelessWidget {
  final String image, title, timings;
  final void Function() onTap;

  const LiveSupportItems(this.image, this.title, this.timings, this.onTap, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
        decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(5.0), boxShadow: [
          BoxShadow(
            color: AppColors.primaryMaterialColor[100]!,
            blurRadius: 5.0,
            spreadRadius: 1.0,
            offset: Offset(
              1.0, // Move to right 10  horizontally
              2.0, // Move to bottom 5 Vertically
            ),
          )
        ]),
        padding: EdgeInsets.all(16),
        child: Column(
          children: [
            Container(
              height: 80,
              width: 80,
              padding: EdgeInsets.all(15),
              decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(40.0), boxShadow: [
                BoxShadow(
                  color: AppColors.shadowMaterialColor[100]!,
                  blurRadius: 5.0,
                  spreadRadius: 1.0,
                  offset: Offset(
                    1.0, // Move to right 10  horizontally
                    2.0, // Move to bottom 5 Vertically
                  ),
                )
              ]),
              child: Image.asset(
                image,
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Text(
              title,
              style: TextStyle(fontSize: 16, color: AppColors.kBlackColor, fontWeight: FontWeight.w600),
            ),
            Text(
              timings,
              style: TextStyle(fontSize: 16, color: AppColors.headerTextColor, fontWeight: FontWeight.w200),
            ),
          ],
        ),
      ),
    );
  }
}
