import 'package:flutter/material.dart';

class GoogleMapSearchBar extends StatefulWidget {
  GoogleMapSearchBar({required this.onChanged, required this.onSubmitted, required this.focus, this.hintText = 'Search Vehicle'});
  final Function(void) onChanged;
  final Function(void) onSubmitted;
  final FocusNode focus;
  final String hintText;

  @override
  GoogleMapSearchBarState createState() => GoogleMapSearchBarState();
}

class GoogleMapSearchBarState extends State<GoogleMapSearchBar> {
  Color backgroundColor = Color(0x90ffffff);
  Color hintTextColor = Colors.grey[600]!;
  @override
  void initState() {
    super.initState();
    widget.focus.addListener(_onFocusChange);
  }

  void _onFocusChange() {
    setState(() {
      if (widget.focus.hasFocus) {
        backgroundColor = Color(0xffffffff);
        hintTextColor = Color(0xFFE1E1E1);
      } else {
        backgroundColor = Color(0x90ffffff);
        hintTextColor = Colors.grey[600]!;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Positioned(
      top: 30.0,
      right: 20.0,
      left: 20.0,
      child: Container(
        /// Modify => height
        height: 50.0,
        width: double.infinity,
        padding: EdgeInsets.only(left: 3),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10.0),
          color: backgroundColor,
          boxShadow: List.from([BoxShadow(color: Colors.black26, blurRadius: 2.0)]),
        ),
        child: Center(
          child: TextField(
            focusNode: widget.focus,

            /// Modify => fontsize
            style: TextStyle(fontSize: 17.0),
            decoration: InputDecoration(
              hintText: widget.hintText,
              hintStyle: TextStyle(color: hintTextColor),
              border: InputBorder.none,
              contentPadding: EdgeInsets.only(top: 10),

              /// Modify => padding
              prefixIcon: Padding(
                  padding: EdgeInsets.only(top: 11.0, bottom: 11.0),
                  child: Image.asset(
                    'images/search.png',
                    height: 15,
                    width: 15,
                    color: Color(0xFFAEAEAE),
                  )),
            ),

            textInputAction: TextInputAction.search,
            onSubmitted: widget.onSubmitted,
            onChanged: widget.onChanged,
          ),
        ),
      ),
    );
  }
}
