import 'package:autonemogps/bloc/home_bloc.dart';
import 'package:autonemogps/bloc/home_event.dart';
import 'package:autonemogps/bloc/home_state.dart';
import 'package:autonemogps/common/back_button.dart';
import 'package:autonemogps/models/device_model.dart';
import 'package:autonemogps/models/tracking_device_model.dart';
import 'package:autonemogps/screens/tracking/tracking_info_layout_part1.dart';
import 'package:autonemogps/screens/tracking/tracking_info_layout_part2.dart';
import 'package:autonemogps/screens/tracking/tracking_info_layout_part3.dart';
import 'package:autonemogps/screens/tracking/tracking_info_layout_part4.dart';
import 'package:autonemogps/screens/tracking/tracking_page_map.dart';
import 'package:autonemogps/screens/tracking/tracking_page_map_icons.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:url_launcher/url_launcher_string.dart';

class TrackingPage extends StatefulWidget {
  final DeviceModel deviceModel;

  final TrackingDevice trackingDevice;

  TrackingPage(this.deviceModel, this.trackingDevice);

  @override
  State<StatefulWidget> createState() {
    return TrackingPageState();
  }
}

class TrackingPageState extends State<TrackingPage> {
  late TrackingDevice currentTrackingDevice;

  late HomeBloc homeBloc;

  final GlobalKey<TrackingPageMapState> _trackingPageMapState = GlobalKey<TrackingPageMapState>();

  bool showLoader = false;

  @override
  void initState() {
    super.initState();
    currentTrackingDevice = widget.trackingDevice;
    homeBloc = BlocProvider.of<HomeBloc>(context);
    homeBloc.add(GetInitialDataEvent());
    homeBloc.add(StartTimerEvent());
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<HomeBloc, HomeState>(
      listener: (context, state) async {
        if (state is DataFetchingState) {
          setState(() => this.showLoader = true);
        }

        if (state is DataFetchedState) {
          setState(() => currentTrackingDevice = state.trackingList[widget.deviceModel.imei]!);
          setState(() => this.showLoader = false);
        }
      },
      bloc: homeBloc,
      child: Scaffold(
        body: Stack(
          children: [
            TrackingPageMap(currentTrackingDevice, widget.deviceModel, key: _trackingPageMapState),
            Positioned(right: 20, top: 50, child: TrackingPageMapIcons(currentTrackingDevice, widget.deviceModel, _trackingPageMapState)),
            if (widget.deviceModel.voiceFeatureText == 'Yes') CallButton(widget: widget),
            Positioned(left: 20, top: 50, child: CustomBackButton()),
            Positioned(
              bottom: 0,
              child: Column(
                children: [
                  TrackingInfoLayoutPart1(currentTrackingDevice, widget.deviceModel),
                  Container(width: MediaQuery.of(context).size.width, color: Color(0xffE0E0E0), height: 1),
                  TrackingPageLayoutPart2(currentTrackingDevice, widget.deviceModel),
                  Container(width: MediaQuery.of(context).size.width, color: Color(0xffE0E0E0), height: 1),
                  TrackingPageLayoutPart3(currentTrackingDevice, widget.deviceModel),
                  Container(width: MediaQuery.of(context).size.width, color: Color(0xffE0E0E0), height: 1),
                  TrackingPageLayoutPart4(currentTrackingDevice: currentTrackingDevice, showLoader: showLoader),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class CallButton extends StatelessWidget {
  const CallButton({
    Key? key,
    required this.widget,
  }) : super(key: key);

  final TrackingPage widget;

  @override
  Widget build(BuildContext context) {
    return Positioned(
      right: 12,
      top: 100,
      child: InkWell(
          onTap: () {
            showDialog(
                context: context,
                builder: (BuildContext context) {
                  return AlertDialog(
                    content: Text('Call to Listen ?'),
                    actions: [
                      TextButton(onPressed: () => Navigator.of(context).pop(), child: Text('No')),
                      TextButton(
                          onPressed: () {
                            launchUrlString('tel:' + widget.deviceModel.simNumber);
                            Navigator.of(context).pop();
                          },
                          child: Text('Yes')),
                    ],
                  );
                });
          },
          child: Container(height: 58, width: 58, child: Image.asset('images/tracking_page_call_button.png'))),
    );
  }
}
