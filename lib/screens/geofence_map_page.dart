import 'dart:math';

import 'package:autonemogps/api_calls/gps_server_requests.dart';
import 'package:autonemogps/api_calls/gps_wox_requests.dart';
import 'package:autonemogps/common/custom_text_form_field.dart';
import 'package:autonemogps/config/colors.dart';
import 'package:autonemogps/config/helpers.dart';
import 'package:autonemogps/config/string_constants.dart';
import 'package:autonemogps/models/geofence_model.dart';
import 'package:autonemogps/storage/repository.dart';
import 'package:autonemogps/utils/logs.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';

class GeofenceMap extends StatefulWidget {
  final Geofence? geofence;
  final void Function() onZoneAdded;

  GeofenceMap({this.geofence, required this.onZoneAdded});

  @override
  _GeofenceMapState createState() => _GeofenceMapState();
}

class _GeofenceMapState extends State<GeofenceMap> {
  GoogleMapController? _controller;

  //Set<Marker> _markers = {};
  Set<Circle> _circles = {};
  Set<Polygon> _polygons = {};
  late LatLng _center;
  LatLng? geofenceCenter;
  double radius = 400;
  int _status = 1; //0-clear map, 1-custom, 2-circle
  List<LatLng> _selectedPosition = List<LatLng>.empty(growable: true);
  TextEditingController _zoneNameController = TextEditingController();
  bool _isSubmitting = false;
  CameraUpdate? _cameraUpdate;
  Color zoneColor = AppColors.primaryColor.withOpacity(0.5);

  @override
  initState() {
    super.initState();
    _center = center;
  }

  buildPolygon() {
    _zoneNameController.text = widget.geofence!.name!;
    Future.delayed(
      Duration(milliseconds: 500),
      () {
        _cameraUpdate = CameraUpdate.newLatLngBounds(widget.geofence!.bounds!, 40);

        _controller?.animateCamera(_cameraUpdate!);
      },
    );

    zoneColor = widget.geofence!.color!;
    _polygons.clear();
    _selectedPosition.addAll(widget.geofence!.points!);
    setState(() {
      _polygons.add(Polygon(polygonId: PolygonId(widget.geofence!.id!), points: _selectedPosition, strokeColor: zoneColor, strokeWidth: 1, fillColor: zoneColor));
    });
  }

  _setDrawing() {
    if (_status == 1) {
      setState(() {
        _circles.clear();
        _polygons.clear();
        if (_selectedPosition.length > 0) {
          _polygons.add(Polygon(polygonId: PolygonId('polygon_id' + _polygons.length.toString()), points: _selectedPosition, fillColor: zoneColor, strokeWidth: 1, strokeColor: zoneColor));
        }
      });
    } else if (_status == 2 && geofenceCenter != null) {
      setState(() {
        _polygons.clear();
        _circles.clear();
        _circles.add(Circle(circleId: CircleId('geofence'), center: geofenceCenter!, radius: radius, fillColor: zoneColor, strokeWidth: 1, strokeColor: zoneColor));
      });
    }
  }

  List<LatLng> getCircleCoordinates() {
    var d2r = pi / 180; // degrees to radians
    var r2d = 180 / pi; // radians to degrees
    var earthsradius = 6378137; // 3963 is the radius of the earth in miles

    var _points = 32;

    // find the raidus in lat/lon
    var rlat = (radius / earthsradius) * r2d;
    var rlng = rlat / cos(geofenceCenter!.latitude * d2r);

    var extp = List<LatLng>.empty(growable: true);
    var start = 0;
    var end = _points + 1;
    for (var i = start; i < end; i++) {
      var theta = pi * (i / (_points / 2));
      double ey = geofenceCenter!.longitude + (rlng * cos(theta)); // center a + radius x * cos(theta)
      double ex = geofenceCenter!.latitude + (rlat * sin(theta)); // center b + radius y * sin(theta)
      extp.add(LatLng(ex, ey));
    }
    return extp;
  }

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: false,
        title: Text(
          widget.geofence == null ? 'Create Geofence' : 'Update Geofence',
          style: TextStyle(color: Colors.white, fontWeight: FontWeight.w500, fontSize: 20),
        ),
        backgroundColor: AppColors.primaryColor,
        iconTheme: IconThemeData(
          color: Colors.white, //change your color here
        ),
        systemOverlayStyle: SystemUiOverlayStyle.light,
      ),
      backgroundColor: AppColors.defaultBackground,
      body: Stack(
        children: [
          SizedBox(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            child: GoogleMap(
              onTap: (latLng) {
                geofenceCenter = latLng;
                _selectedPosition.add(latLng);
                _setDrawing();
              },
              polygons: _polygons,
              circles: _circles,
              onMapCreated: (GoogleMapController controller) {
                _controller = controller;
                if (widget.geofence == null) {
                  _currentLocation(true);
                } else {
                  buildPolygon();
                }
                if (_selectedPosition.length > 0) {
                  _setDrawing();
                }
              },
              initialCameraPosition: CameraPosition(target: _center, zoom: 15.5),
              zoomControlsEnabled: false,
              compassEnabled: false,
              mapToolbarEnabled: false,
              myLocationEnabled: true,
              myLocationButtonEnabled: false,
              tiltGesturesEnabled: false,
            ),
          ),
          Positioned(
            left: 10,
            right: 10,
            bottom: 10.0,
            child: Container(
              padding: EdgeInsets.all(15.0),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(4.0),
              ),
              child: Center(
                child: Column(
                  children: [
                    if (_status == 2)
                      SizedBox(
                        height: 10,
                        width: double.infinity,
                        child: SliderTheme(
                          data: SliderThemeData(trackHeight: 1, thumbShape: RoundSliderThumbShape(enabledThumbRadius: 6, disabledThumbRadius: 6)),
                          child: Slider(
                            value: radius,
                            onChanged: (value) {
                              setState(() {
                                radius = value;
                                _status = 2;
                                _setDrawing();
                              });
                            },
                            activeColor: AppColors.primaryColor,
                            inactiveColor: Color(0xFFF1F1F1),
                            min: 0,
                            max: 1000,
                          ),
                        ),
                      ),
                    SizedBox(
                      height: 10,
                    ),
                    CustomTextFormField(
                      controller: _zoneNameController,
                      hinText: 'Enter zone name',
                      boardColor: AppColors.primaryColor,
                      onChanged: (value) {},
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        InkWell(
                          onTap: () {
                            setState(() {
                              _status = 1;
                            });
                          },
                          child: Container(
                            height: 30,
                            padding: EdgeInsets.only(left: 10, right: 10),
                            decoration:
                                BoxDecoration(borderRadius: BorderRadius.circular(15), border: Border.all(color: AppColors.primaryColor, width: 1), color: _status == 1 ? AppColors.primaryColor : Colors.white),
                            child: Center(
                              child: Text(
                                'Custom',
                                style: TextStyle(color: _status == 1 ? Colors.white : AppColors.primaryColor, fontSize: 14, fontWeight: FontWeight.w600),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(width: 15.0),
                        InkWell(
                          onTap: () {
                            setState(() {
                              _status = 2;
                            });
                          },
                          child: Container(
                            height: 30,
                            padding: EdgeInsets.only(left: 10, right: 10),
                            decoration:
                                BoxDecoration(borderRadius: BorderRadius.circular(15), border: Border.all(color: AppColors.primaryColor, width: 1), color: _status == 2 ? AppColors.primaryColor : Colors.white),
                            child: Center(
                              child: Text(
                                'Circle',
                                style: TextStyle(color: _status == 2 ? Colors.white : AppColors.primaryColor, fontSize: 14, fontWeight: FontWeight.w600),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(width: 5.0),
                        InkWell(
                          child: Padding(padding: EdgeInsets.all(10), child: Text('Clear Map')),
                          onTap: () {
                            _selectedPosition.clear();
                            _setDrawing();
                          },
                        )
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    _isSubmitting
                        ? SpinKitRing(
                            lineWidth: 3.0,
                            color: AppColors.primaryColor,
                            size: 30.0,
                          )
                        : Column(
                            children: [
                              InkWell(
                                  child: Container(
                                    height: 48,
                                    padding: EdgeInsets.symmetric(vertical: 10),
                                    decoration: BoxDecoration(
                                      color: AppColors.primaryColor,
                                      borderRadius: BorderRadius.circular(24),
                                    ),
                                    child: Center(
                                        child: Text(
                                      'Save',
                                      style: TextStyle(color: Colors.white, fontSize: 16, fontWeight: FontWeight.w500),
                                    )),
                                  ),
                                  onTap: onSaveButtonTapped),
                              SizedBox(
                                height: 10,
                              ),
                              widget.geofence == null
                                  ? Container()
                                  : InkWell(
                                      onTap: () async {
                                        bool action = await showDialog(
                                            context: context,
                                            builder: (context) {
                                              return AlertDialog(
                                                title: Text('Delete Zone'),
                                                content: Text('Are you sure you want to delete this zone?'),
                                                actions: [
                                                  TextButton(
                                                    child: Text('No'),
                                                    onPressed: () {
                                                      Navigator.pop(context);
                                                    },
                                                  ),
                                                  TextButton(
                                                    child: Text('Yes'),
                                                    onPressed: () {
                                                      Navigator.pop(context, true);
                                                    },
                                                  ),
                                                ],
                                              );
                                            });
                                        if (action) {
                                          setState(() {
                                            _isSubmitting = true;
                                          });
                                          List returnedList;
                                          if (Repository.getServerType() == GPSWoxConstants.type) {
                                            returnedList = await GPSWoxRequests.deleteZone(widget.geofence!.id!);
                                          } else {
                                            returnedList = await GPSServerRequests.deleteZone(widget.geofence!.id!);
                                          }

                                          setState(() {
                                            _isSubmitting = false;
                                          });
                                          if (returnedList.elementAt(0)) {
                                            widget.onZoneAdded();
                                            Navigator.pop(context);
                                          } else {
                                            showSnackBar(context, returnedList.elementAt(1) as String, Colors.red);
                                          }
                                        }
                                      },
                                      child: Container(
                                        height: 48,
                                        padding: EdgeInsets.symmetric(vertical: 10),
                                        decoration: BoxDecoration(borderRadius: BorderRadius.circular(24), border: Border.all(color: AppColors.primaryColor2, width: 1.5)),
                                        child: Center(
                                            child: Text(
                                          'Delete Zone',
                                          style: TextStyle(color: AppColors.primaryColor2, fontSize: 16, fontWeight: FontWeight.w500),
                                        )),
                                      ),
                                    ),
                            ],
                          )
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  void onSaveButtonTapped() async {
    if (_status == 2) {
      if (geofenceCenter == null) {
        showSnackBar(context, 'Zone not found', Colors.red);
        return;
      }
      _selectedPosition = getCircleCoordinates();
    }
    if (_selectedPosition.length < 3) {
      showSnackBar(context, 'Zone not found', Colors.red);
      return;
    }
    if (_zoneNameController.text.isEmpty) {
      showSnackBar(context, 'Enter Zone Name', Colors.red);
      return;
    }
    setState(() {
      _isSubmitting = true;
    });
    List<Map<String, String>> vertices = List<Map<String, String>>.empty(growable: true);
    for (LatLng latLng in _selectedPosition) {
      vertices.add({'lat': latLng.latitude.toString(), 'lng': latLng.longitude.toString()});
    }
    List returnedList;
    if (Repository.getServerType() == GPSWoxConstants.type) {
      List<Map<String, double>> polygon = [];
      for (LatLng latLng in _selectedPosition) {
        polygon.add({'lat': latLng.latitude, 'lng': latLng.longitude});
      }
      returnedList = await GPSWoxRequests.addGeoPlace(_zoneNameController.text, '#ff0000', polygon, 'polygon');
    } else {
      String vertices = '';
      for (LatLng latLng in _selectedPosition) {
        vertices = vertices + latLng.latitude.toString() + ',' + latLng.longitude.toString() + ',';
      }
      returnedList = await GPSServerRequests.addGeoPlace(_zoneNameController.text, vertices.substring(0, vertices.length - 1), zoneId: widget.geofence?.id);
    }

    setState(() {
      _isSubmitting = false;
    });
    if (returnedList.elementAt(0)) {
      widget.onZoneAdded();
      Navigator.pop(context);
    } else {
      showSnackBar(context, returnedList.elementAt(1) as String, Colors.red);
    }
  }

  void _currentLocation(bool animateLocation) async {
    var location = new Location();
    try {
      LocationData currentLocation = await location.getLocation();
      LatLng latLng = LatLng(currentLocation.latitude ?? 0, currentLocation.longitude ?? 0);
      if (animateLocation) {
        _controller?.animateCamera(CameraUpdate.newCameraPosition(
          CameraPosition(
            bearing: 0,
            target: latLng,
            zoom: 17.0,
          ),
        ));
      }
    } on Exception catch (e) {
      writeErrorLogs(e.toString());
    }
  }
}
