import 'package:animations/animations.dart';
import 'package:autonemogps/api_calls/gps_server_requests.dart';
import 'package:autonemogps/api_calls/gps_wox_requests.dart';
import 'package:autonemogps/common/lock_dialog_widget.dart';
import 'package:autonemogps/config/helpers.dart';
import 'package:autonemogps/config/string_constants.dart';
import 'package:autonemogps/models/device_model.dart';
import 'package:autonemogps/models/tracking_device_model.dart';
import 'package:autonemogps/storage/repository.dart';
import 'package:flutter/material.dart';

class EngineLock extends StatefulWidget {
  final DeviceModel deviceModel;

  final TrackingDevice trackingDevice;

  EngineLock(this.deviceModel, this.trackingDevice);

  @override
  State<StatefulWidget> createState() {
    return EngineLockState();
  }
}

class EngineLockState extends State<EngineLock> {
  String imgName = '';
  bool isLockSupported = false;

  @override
  void initState() {
    imgName = getVehicleMarkerData(widget.trackingDevice.status!, widget.deviceModel.icon)['iconKey'];
    isLockSupported = ((widget.deviceModel.model.isNotEmpty && widget.deviceModel.vin.isNotEmpty) || Repository.getServerType() == GPSWoxConstants.type);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Engine Lock'),
        backgroundColor: Colors.white,
        foregroundColor: Colors.black,
      ),
      body: Container(
        decoration: BoxDecoration(color: Colors.white),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              margin: EdgeInsets.only(top: 10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    width: 150,
                    child: Column(
                      children: [
                        Text(widget.deviceModel.name, style: TextStyle(fontSize: 12), textAlign: TextAlign.center),
                        Text(widget.deviceModel.imei, style: TextStyle(fontSize: 12)),
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 10, right: 10),
                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(20), border: Border.all(color: Colors.grey)),
                    padding: EdgeInsets.all(5),
                    height: 40,
                    width: 40,
                    child: Image.asset(imgName),
                  ),
                  Container(
                    width: 150,
                    child: Column(
                      children: [
                        if (isLockSupported) Text('Device Supported', style: TextStyle(fontSize: 12), textAlign: TextAlign.center),
                        if (!isLockSupported) Text('Device Not Supported', style: TextStyle(fontSize: 12), textAlign: TextAlign.center),
                        Text(widget.deviceModel.simNumber, style: TextStyle(fontSize: 12)),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            if (isLockSupported)
              Container(
                height: 70,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    InkWell(
                        onTap: () {
                          showLockDialog(context, false);
                        },
                        child: Container(child: Image.asset('images/engine_lock.png'), width: 150)),
                    InkWell(
                        onTap: () {
                          showLockDialog(context, true);
                        },
                        child: Container(child: Image.asset('images/engine_unlock.png'), width: 150)),
                  ],
                ),
              ),
            if (isLockSupported)
              Container(
                decoration: BoxDecoration(color: Color(0xffEFEFEF), borderRadius: BorderRadius.circular(5)),
                padding: EdgeInsets.all(10),
                margin: EdgeInsets.all(20),
                child: Text('Please do not use this feature when the GSM network connectivity is poor.'),
              )
          ],
        ),
      ),
    );
  }

  showLockDialog(BuildContext context, bool isLocked) async {
    showModal(context: context, builder: (context) => LockDialogWidget(isLocked ? 'Unlock' : 'Lock')).then(
      (executeCmd) {
        if (executeCmd != null && executeCmd[0]) {
          if (Repository.getServerType() == GPSWoxConstants.type) {
            GPSWoxRequests.sendCommand(widget.deviceModel.deviceId, isLocked ? 'engineResume' : 'engineStop').then((isSuccess) {
              showSnackBar(context, !isSuccess ? 'Command Not Sent. Something went Wrong!' : 'Command Sent successfully.', !isSuccess ? Colors.redAccent : Colors.green);
            });
          } else {
            GPSServerRequests.sendCommand(isLocked ? widget.deviceModel.vin : widget.deviceModel.model, widget.deviceModel.imei).then((returnedList) {
              showSnackBar(context, returnedList[0] != 1 ? 'Command Not Sent. Something went Wrong!' : 'Command Sent successfully.', returnedList[0] != 1 ? Colors.redAccent : Colors.green);
            });
          }
        }
      },
    );
  }
}
