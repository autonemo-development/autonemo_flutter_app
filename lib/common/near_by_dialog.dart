import 'dart:io';

import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher_string.dart';

showNearbyLocationDialog(BuildContext context, String lat, String lng) {
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return Dialog(
        elevation: 0.0,
        backgroundColor: Colors.transparent,
        child: Container(
          height: 400,
          padding: EdgeInsets.all(15),
          decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(5)),
          child: ListView(
            shrinkWrap: true,
            children: [
              Text('Near by Places', style: TextStyle(color: Colors.black, fontSize: 20)),
              SizedBox(height: 20),
              Row(
                children: [
                  NearbyPlaceItem('images/hospital.png', 'Hospital', lat, lng),
                  Expanded(child: Divider()),
                  NearbyPlaceItem('images/mosque.png', 'Mosque', lat, lng),
                ],
              ),
              Row(
                children: [
                  Divider(),
                  Expanded(child: NearbyPlaceItem('images/atm_machine.png', 'Atm', lat, lng)),
                  Divider(),
                ],
              ),
              Row(
                children: [
                  NearbyPlaceItem('images/restaurant.png', 'Restaurant', lat, lng),
                  Expanded(child: Divider()),
                  NearbyPlaceItem('images/petrol_pump.png', 'Petrol Pump', lat, lng),
                ],
              ),
              Row(
                children: [
                  Divider(),
                  Expanded(
                    child: NearbyPlaceItem('images/gas_pump.png', 'Gas Station', lat, lng),
                  ),
                  Divider(),
                ],
              ),
              Row(
                children: [
                  NearbyPlaceItem('images/hotel.png', 'Hotel', lat, lng),
                  Expanded(child: Divider()),
                  NearbyPlaceItem('images/police_station.png', 'Police Station', lat, lng),
                ],
              ),
              Row(
                children: [
                  Divider(),
                  Expanded(child: NearbyPlaceItem('images/mall.png', 'Shopping Mall', lat, lng)),
                  Divider(),
                ],
              ),
              Row(
                children: [
                  NearbyPlaceItem('images/service_point.png', 'Service Point', lat, lng),
                  Expanded(child: Divider()),
                  NearbyPlaceItem('images/bus-stop.png', 'Bus Stop', lat, lng),
                ],
              )
            ],
          ),
        ),
      );
    },
  );
}

class NearbyPlaceItem extends StatelessWidget {
  final String image;
  final String text;
  final String lat, lng;

  NearbyPlaceItem(this.image, this.text, this.lat, this.lng);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () async {
        String url = '';
        if (Platform.isAndroid) {
          url = 'geo:$lat,$lng?q=$text&zoom=15z';
        } else if (Platform.isIOS) {
          url = 'comgooglemaps://?center=$lat,$lng&q=${text.replaceAll(' ', '+')}&zoom=15z';
        }
        launchUrlString(url);
      },
      child: Container(
        child: Column(
          children: [
            Image.asset(
              image,
              height: 40,
              width: 40,
            ),
            SizedBox(
              width: 10,
            ),
            Text(
              text,
              style: TextStyle(color: Colors.grey, fontSize: 15),
            )
          ],
        ),
      ),
    );
  }
}
