import 'package:flutter/material.dart';

class InfoGrphicModel {
  final String text;
  final int count;
  final Color backgroundColor;
  final void Function() onTap;

  InfoGrphicModel(this.text, this.backgroundColor, this.count, this.onTap);
}
