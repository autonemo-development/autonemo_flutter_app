import 'package:flutter/material.dart';

class ParkingDialogWidget extends StatelessWidget {
  final String commandType;

  const ParkingDialogWidget(this.commandType);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        width: MediaQuery.of(context).size.width * .85,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(5.0),
        ),
        child: Stack(
          children: [
            Positioned(
              top: 0,
              child: Image.asset('images/warning-dialog-background.png',
                  width: MediaQuery.of(context).size.width * .85),
            ),
            Container(
              padding: EdgeInsets.all(20.0),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Column(
                    children: [
                      Image.asset(
                        'images/warning.png',
                        height: 45.0,
                      ),
                      SizedBox(
                        height: 5.0,
                      ),
                      Text('Warning!!',
                          style: TextStyle(
                            decoration: TextDecoration.none,
                            color: Colors.white,
                            fontSize: 20.0,
                            fontFamily: 'Poppins',
                            fontWeight: FontWeight.w500,
                          )),
                    ],
                  ),
                  SizedBox(
                    height: 40.0,
                  ),
                  Text(
                    'You are about to $commandType you vehicle.',
                    style: TextStyle(
                      decoration: TextDecoration.none,
                      color: Color(0xFF888888),
                      fontSize: 14.0,
                      fontFamily: 'Poppins',
                      fontWeight: FontWeight.w400,
                    ),
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  Text(
                    'Do you want to continue?',
                    style: TextStyle(
                      decoration: TextDecoration.none,
                      color: Color(0xFF212121),
                      fontSize: 17.0,
                      fontFamily: 'Poppins',
                      fontWeight: FontWeight.w400,
                    ),
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  Row(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Material(
                        child: InkWell(
                          onTap: () {
                            //code
                            Navigator.pop(context, false);
                          },
                          child: Container(
                            width: 120.0,
                            padding: EdgeInsets.only(
                                left: 20.0, top: 10.0, bottom: 10.0),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(4.0),
                              border: Border.all(
                                  color: Color(0xFFFF6B6B), width: 1.5),
                            ),
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              //mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Image.asset(
                                  'images/cancel.png',
                                  color: Color(0xFFFF6B6B),
                                  height: 20.0,
                                ),
                                SizedBox(
                                  width: 20.0,
                                ),
                                Text(
                                  'No',
                                  style: TextStyle(
                                      color: Color(0xFFFF6B6B),
                                      fontSize: 16,
                                      fontWeight: FontWeight.w500),
                                )
                              ],
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 10.0,
                      ),
                      Material(
                        child: InkWell(
                          onTap: () {
                            //code
                            Navigator.pop(context, true);
                          },
                          child: Container(
                            width: 120.0,
                            padding: EdgeInsets.only(
                                left: 20.0, top: 10.0, bottom: 10.0),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(4.0),
                              border: Border.all(
                                  color: Color(0xFF25DFB3), width: 1.5),
                            ),
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              //mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Image.asset(
                                  'images/tick.png',
                                  color: Color(0xFF25DFB3),
                                  height: 20.0,
                                ),
                                SizedBox(
                                  width: 20.0,
                                ),
                                Text(
                                  'Yes',
                                  style: TextStyle(
                                      color: Color(0xFF25DFB3),
                                      fontSize: 16,
                                      fontWeight: FontWeight.w500),
                                )
                              ],
                            ),
                          ),
                        ),
                      )
                    ],
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
