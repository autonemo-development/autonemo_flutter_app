import 'package:autonemogps/api_calls/gps_server_requests.dart';
import 'package:autonemogps/config/colors.dart';
import 'package:autonemogps/config/helpers.dart';
import 'package:autonemogps/models/expense.dart';
import 'package:autonemogps/screens/expense_page/add_expense_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class ExpensesDashboard extends StatefulWidget {
  ExpensesDashboard({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return ExpensesDashboardState();
  }
}

class ExpensesDashboardState extends State {
  List<Expense> expenseList = [];

  bool loading = false;
  bool hasNoData = false;

  @override
  void initState() {
    fetchExpensesData();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    if (loading) {
      return SpinKitCircle(color: AppColors.primaryColor);
    }

    if (hasNoData) {
      return Container();
    }

    return Container(
      padding: EdgeInsets.all(10),
      margin: EdgeInsets.only(bottom: 16),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(16),
        boxShadow: [BoxShadow(color: AppColors.shadowMaterialColor[100]!, blurRadius: 5.0, spreadRadius: 1.0, offset: Offset(1.0, 2.0))],
      ),
      child: Column(mainAxisSize: MainAxisSize.min, crossAxisAlignment: CrossAxisAlignment.start, children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text('Expenses', style: TextStyle(color: AppColors.headerTextColor)),
            InkWell(
              onTap: () => Navigator.push(context, MaterialPageRoute(builder: (context) => AddExpensePage('false'))),
              child: Row(children: [Icon(Icons.add_circle), SizedBox(width: 5), Text('Add Expense')]),
            )
          ],
        ),
        SizedBox(width: 20),
        Divider(),
        SizedBox(height: 5),
        Container(
          decoration: BoxDecoration(color: Color(0xFFF8F8F8), borderRadius: BorderRadius.circular(10)),
          padding: EdgeInsets.all(10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(getTotalExpense(expenseList) + ' BDT', style: TextStyle(fontSize: 25, fontWeight: FontWeight.w600)),
              Container(width: 50, height: 50, child: Image.asset('images/expenses_wallet.png'))
            ],
          ),
        ),
        SizedBox(height: 5),
        Container(
          height: expenseList.length * 55,
          constraints: BoxConstraints(maxHeight: 250),
          child: ListView.builder(
            itemCount: expenseList.length,
            itemBuilder: (context, index) {
              Expense expense = expenseList.elementAt(index);
              return Container(
                decoration: BoxDecoration(color: Color(0xFFF8F8F8), borderRadius: BorderRadius.circular(10)),
                margin: EdgeInsets.only(top: 5),
                padding: EdgeInsets.all(10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(expense.name, style: TextStyle(fontSize: 12, fontWeight: FontWeight.w600)),
                          SizedBox(height: 2),
                          Text(expense.objectName, style: TextStyle(fontSize: 10)),
                        ],
                      ),
                    ),
                    SizedBox(width: 10),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(getFormattedDateFromString(expense.date, 'yyyy-MM-dd', 'dd-MMM-yyyy'), style: TextStyle(fontSize: 12, fontWeight: FontWeight.w600)),
                          Text('Date', style: TextStyle(fontSize: 10)),
                        ],
                      ),
                    ),
                    SizedBox(width: 10),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(expense.cost, style: TextStyle(fontSize: 12, fontWeight: FontWeight.w600)),
                          Text(getHtmlText('Q: ' + expense.qty), style: TextStyle(fontSize: 10)),
                        ],
                      ),
                    ),
                  ],
                ),
              );
            },
          ),
        )
      ]),
    );
  }

  void fetchExpensesData() {
    setState(() => loading = true);

    GPSServerRequests.fetchExpense().then((value) {
      setState(() => this.hasNoData = value.length == 0);
      setState(() => expenseList = value);
      setState(() => loading = false);
    });
  }

  String getTotalExpense(List<Expense> expenseList) {
    double totalExpense = 0;

    expenseList.forEach((element) {
      double cost = double.parse(element.cost.replaceAll(' BDT', ''));
      totalExpense = totalExpense + cost;
    });

    return totalExpense.toStringAsFixed(2);
  }
}
