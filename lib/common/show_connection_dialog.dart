import 'package:animations/animations.dart';
import 'package:flutter/material.dart';

showConnectionDialog(BuildContext context){
  return showModal(
      context: context,
      builder: (context){
        return Dialog(
          child: Container(
            padding: EdgeInsets.all(20.0),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    InkWell(
                        onTap: (){
                          Navigator.of(context).pop();
                        },
                        child: Image.asset('images/cancel.png',width: 15,height: 15,))
                  ],
                ),
                Text('No internet Connection!', style: TextStyle(color: Color(0xFFF35656)),),
                Image.asset('images/internet_connection.png', width: MediaQuery.of(context).size.width*.6,),
                Text('Turn on Internet & Refresh', style: TextStyle(color: Color(0xFF0060A4)),),
              ],
            ),
          ),
        );
      }
  );
}