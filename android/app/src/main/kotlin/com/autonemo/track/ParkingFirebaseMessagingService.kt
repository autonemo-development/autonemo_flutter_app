package com.autonemo.track

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.Color
import android.net.Uri
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import kotlin.math.roundToInt

class ParkingFirebaseMessagingService : FirebaseMessagingService() {
    override fun onNewToken(s: String) {
        Log.e("NEW_TOKEN", s)
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        val params = remoteMessage.data
        Log.i(TAG, "onMessageReceived: $params")
        if ("parking_violation" == params["type"]) {

            val sirenIntent = Intent(applicationContext, SirenService::class.java)
            sirenIntent.putExtra("imei", params["imei"])
            sirenIntent.putExtra("title", params["title"])
            sirenIntent.putExtra("body", params["body"])
            sirenIntent.putExtra("type", params["type"])
            Log.i(TAG, "onMessageReceived: $params")
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                startForegroundService(sirenIntent)
            } else {
                startService(sirenIntent)
            }
        }else if ("sos" == params["type"]) {
            val sirenIntent = Intent(applicationContext, SOSSirenService::class.java)
            sirenIntent.putExtra("imei", params["imei"])
            sirenIntent.putExtra("title", params["title"])
            sirenIntent.putExtra("body", params["body"])
            sirenIntent.putExtra("type", params["type"])
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                startForegroundService(sirenIntent)
            } else {
                startService(sirenIntent)
            }
        }
//        else if ("ignition_on" == params["type"]) {
//            showNotification("ign_on_channel", "Ignition On",
//                    "This channel is used for Ignition On notifications.", R.raw.ignition_on,params["title"].toString(), params["body"].toString());
//        }else if ("ignition_off" == params["type"]) {
//            showNotification("ign_off_channel", "Ignition Off",
//                    "This channel is used for Ignition Off notifications.", R.raw.ignition_off,params["title"].toString(), params["body"].toString());
//        }else{
//            showNotification("gps_daddy_channel", "GPS Daddy",
//                    "This channel is used for notifications.", R.raw.ignition_off,params["title"].toString(), params["body"].toString());
//        }
    }

    private fun showNotification(channelId: String, channelName: String, channelDescription: String, sound:Int, title: String, body: String) {
        val pattern = longArrayOf(0, 1000)
        val mNotificationManager: NotificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notificationChannel = NotificationChannel(channelId, channelName,
                    NotificationManager.IMPORTANCE_HIGH)
            notificationChannel.description = channelDescription
            notificationChannel.enableLights(true)
            notificationChannel.lightColor = Color.RED
            notificationChannel.vibrationPattern = pattern
            notificationChannel.enableVibration(true)
            mNotificationManager.createNotificationChannel(notificationChannel)
            notificationChannel.canBypassDnd()
        }

        val notificationBuilder: NotificationCompat.Builder = NotificationCompat.Builder(this, channelId)

        notificationBuilder
                .setContentTitle(title)
                .setContentText(body)
                .setDefaults(Notification.DEFAULT_ALL)
                .setWhen(System.currentTimeMillis())
                .setPriority(NotificationCompat.PRIORITY_MAX)
                .setSmallIcon(R.drawable.ic_notification)
                .setLargeIcon(BitmapFactory.decodeResource(resources, R.drawable.ic_notification))
                .setAutoCancel(true)
        Log.i(TAG, "showNotification: "+sound)
        if(sound!=0){
            val soundUri: Uri = Uri.parse("android.resource://" + applicationContext.packageName + "/" +sound)
            notificationBuilder.setSound(soundUri)

        }

        mNotificationManager.notify((Math.random() * 1000).roundToInt(), notificationBuilder.build())
    }

    companion object {
        private const val TAG = "MyFirebaseMessagingServ"
    }
}