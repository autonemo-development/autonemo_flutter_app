import 'dart:convert';
import 'dart:io';

import 'package:autonemogps/config/colors.dart';
import 'package:autonemogps/home_page/tabs/list_view_page/list_view_bottom_popup.dart';
import 'package:autonemogps/models/device_model.dart';
import 'package:autonemogps/models/sensor_model.dart';
import 'package:autonemogps/models/tracking_device_model.dart';
import 'package:autonemogps/models/vehicle_status_mode.dart';
import 'package:autonemogps/storage/repository.dart';
import 'package:autonemogps/utils/icon_helper.dart';
import 'package:autonemogps/utils/logs.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:html/parser.dart';
import 'package:intl/intl.dart' as dateTime;
import 'package:overlay_support/overlay_support.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:url_launcher/url_launcher.dart' as urlLauncher;

showSnackBar(BuildContext context, String text, Color backgroundColor, {double bottomMargin = 0, Function? onTap, int durationSec = 2}) {
  showSimpleNotification(Text(text), background: backgroundColor);
}

Future<bool> isInternetConnected() async {
  try {
    var result = await InternetAddress.lookup('google.com');
    if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
      return true;
    }
    return false;
  } on SocketException catch (_) {
    return false;
  }
}

Map<String, dynamic> getVehicleMarkerData(String status, String icon) {
  Map<String, dynamic> returningList = Map<String, dynamic>();
  switch (status) {
    case 'm':
      returningList.putIfAbsent('iconKey', () => (IconHelper.icons[icon] ?? 'car') + '_green.png');
      returningList.putIfAbsent('color', () => Color(0xFF39A78A));
      break;
    case 'i':
      returningList.putIfAbsent('iconKey', () => (IconHelper.icons[icon] ?? 'car') + '_orange.png');
      returningList.putIfAbsent('color', () => Colors.orangeAccent);
      break;
    case 's':
      returningList.putIfAbsent('iconKey', () => (IconHelper.icons[icon] ?? 'car') + '_red.png');
      returningList.putIfAbsent('color', () => Colors.redAccent);
      break;
    case 'o':
      returningList.putIfAbsent('iconKey', () => (IconHelper.icons[icon] ?? 'car') + '_blue.png');
      returningList.putIfAbsent('color', () => Colors.redAccent);
      break;
    default:
      returningList.putIfAbsent('iconKey', () => (IconHelper.icons[icon] ?? 'car') + '_grey.png');
      returningList.putIfAbsent('color', () => Colors.grey);
      break;
  }
  return returningList;
}

launchUrl(String url) {
  urlLauncher.launchUrl(Uri.parse(url), mode: urlLauncher.LaunchMode.externalApplication);
}

Future<bool> getLocationPermission() async {
  PermissionStatus permissionStatus = await Permission.location.status;
  if (permissionStatus == PermissionStatus.granted) {
    return true;
  } else {
    try {
      Map<Permission, PermissionStatus> map = await [Permission.location].request();
      if (map[Permission.location] == PermissionStatus.granted) {
        return true;
      } else {
        return false;
      }
    } catch (e) {
      writeErrorLogs('getLocationPermission Error ', e: e);
      return false;
    }
  }
}

int calculateDifference(DateTime date) {
  DateTime now = DateTime.now();
  return DateTime(date.year, date.month, date.day).difference(DateTime(now.year, now.month, now.day)).inDays;
}

VehicleStatusModel getVehicleStatus(TrackingDevice trackingDevice, DeviceModel deviceModel) {
  String status = 'No Data';
  Color statusColor = Colors.grey;

  switch (trackingDevice.status) {
    case 'm':
      status = 'Moving';
      statusColor = Color(0xFF39A78A);
      break;
    case 'i':
      status = 'Idle';
      statusColor = Colors.orangeAccent;
      break;
    case 's':
      status = 'Stopped';
      statusColor = Colors.redAccent;
      break;
    case 'off':
    case 'o':
      status = 'Offline';
      statusColor = Colors.blue;
      break;
    case 'NA':
      status = 'No Data';
      statusColor = Colors.grey;
      break;
  }

  String expiryDate = deviceModel.expiry;
  String expiryDateText = 'Expires On';
  Color expiryColor = Color(0xFF0C8E44);

  if (expiryDate == '0000-00-00') {
    expiryDate = 'Unlimited';
  } else {
    try {
      DateTime date = new dateTime.DateFormat('yyyy-MM-dd').parse(expiryDate);
      Duration expiryDuration = date.difference(DateTime.now());
      if (expiryDuration.inDays < 7 && expiryDuration.inSeconds >= 0) {
        expiryDateText = 'Expires In';
        expiryDate = expiryDuration.inDays.toString() + ' Days';
        expiryColor = Colors.redAccent;
      } else if (expiryDuration.inDays >= 7) {
        expiryDate = getFormattedDate(date);
      } else {
        expiryDateText = 'Device';
        expiryDate = 'Expired';
        expiryColor = Colors.redAccent;
      }
    } catch (e) {}
  }

  if (expiryDate == 'Expired') {
    status = 'Expired';
    statusColor = Colors.black;
  }

  return new VehicleStatusModel(status, statusColor, expiryDate, expiryDateText, expiryColor);
}

String getFormattedDateFromString(String dateString, String inputPattern, String outputPattern) {
  DateTime date = dateTime.DateFormat(inputPattern).parse(dateString);
  return dateTime.DateFormat(outputPattern).format(date);
}

String getFormattedDate(DateTime date) {
  return new dateTime.DateFormat('dd-MMM-yyyy').format(date);
}

List<Sensor> getFirstThreeSensor(TrackingDevice trackingDevice) {
  List<Sensor> firstThreeSensors = [];

  final bool isGps = trackingDevice.gps!;
  final int gsmSignal = trackingDevice.gsmSignal!;

  bool ignitionImg = false;

  for (int j = 0; j < trackingDevice.sensorList!.length; j++) {
    Sensor sensor = trackingDevice.sensorList!.elementAt(j);
    if (sensor.sensorName == 'Ignition' || sensor.sensorName == 'ACC' || sensor.sensorName == 'Engine' || sensor.sensorName == 'Engine Status') {
      ignitionImg = sensor.paramValue!;
      break;
    }
  }

  Sensor gpsSensor = Sensor(
    imageName: 'images/gps.png',
    sensorName: 'GPS',
    sensorValue: isGps ? 'On' : 'Off',
    sensorColor: (isGps ? AppColors.sensorActiveColor : AppColors.sensorInActiveColor),
    sensorLightColor: (isGps ? AppColors.sensorLightActiveColor : AppColors.sensorLightInActiveColor),
  );

  Sensor gsmSensor = Sensor(
    imageName: 'images/wifi_.png',
    sensorName: 'GSM',
    sensorColor: (gsmSignal > 0 ? AppColors.sensorActiveColor : AppColors.sensorInActiveColor),
    sensorLightColor: (gsmSignal > 0 ? AppColors.sensorLightActiveColor : AppColors.sensorLightInActiveColor),
    sensorValue: gsmSignal > 0 ? 'On' : 'Off',
  );

  Sensor ignitionSensor = Sensor(
    imageName: 'images/ignition-filled.png',
    sensorName: 'Ignition',
    sensorColor: ignitionImg ? AppColors.sensorActiveColor : AppColors.sensorInActiveColor,
    sensorLightColor: ignitionImg ? AppColors.sensorLightActiveColor : AppColors.sensorLightInActiveColor,
    sensorValue: ignitionImg ? 'On' : 'Off',
  );

  firstThreeSensors.add(gpsSensor);
  firstThreeSensors.add(gsmSensor);
  firstThreeSensors.add(ignitionSensor);

  return firstThreeSensors;
}

showVehicleBottomSheet(BuildContext context, TrackingDevice trackingDevice, DeviceModel deviceModel, [bool backdrop = false]) {
  showModalBottomSheet(
    context: context,
    barrierColor: backdrop ? Colors.transparent : Colors.black.withOpacity(0.5),
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.vertical(top: Radius.circular(25))),
    builder: (context) => ListViewBottomPopup(trackingDevice, deviceModel),
  );
}

getHtmlText(String html) {
  var document = parse(html).querySelector('font');
  if (document != null) {
    return document.text;
  } else {
    return html;
  }
}

checkHasRedText(String text) {
  if (text.contains('red')) {
    return Colors.red;
  }
  return Colors.black;
}

Future<List<dynamic>> getFaultCodeJson() async {
  final String response = await rootBundle.loadString('assets/fault_code.json');
  List<dynamic> data = await json.decode(response);
  return data;
}

List<Map<String, dynamic>> defaultWidgetListMap = [
  {'name': 'Discount and Offer Cards', 'isEnabled': true},
  {'name': 'Order Now', 'isEnabled': true},
  {'name': 'Fleet Status', 'isEnabled': true},
  {'name': 'Vehicle Selector', 'isEnabled': true},
  {'name': 'Value Card', 'isEnabled': true},
  {'name': 'Engine Oil Recommendations', 'isEnabled': true},
  {'name': 'Mileage Graph', 'isEnabled': true},
  {'name': 'Fuel Graph', 'isEnabled': true},
  {'name': 'Odometer Graph', 'isEnabled': true},
  {'name': 'Events Graph', 'isEnabled': true},
  {'name': 'Maintenance', 'isEnabled': true},
  {'name': 'Expenses', 'isEnabled': true},
  {'name': 'DTC Codes', 'isEnabled': true},
  {'name': 'Quick Links', 'isEnabled': true},
];

List getWidgetList() {
  String widgetListString = Repository.getWidgetList();

  List widgetListMap = [];

  if (widgetListString.isEmpty) {
    widgetListMap = defaultWidgetListMap;
  } else {
    widgetListMap = json.decode(widgetListString);
  }

  return widgetListMap;
}
