import 'package:autonemogps/config/colors.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';

class VehicleSelector extends StatefulWidget {
  final allDeviceObjects;

  final ValueChanged _callBack;

  final String selectedDevice;

  VehicleSelector(this.allDeviceObjects, this.selectedDevice, this._callBack);

  @override
  State<StatefulWidget> createState() {
    return VehicleSelectorState();
  }
}

class VehicleSelectorState extends State<VehicleSelector> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10),
      margin: EdgeInsets.only(bottom: 16),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(10),
        boxShadow: [BoxShadow(color: AppColors.shadowMaterialColor[100]!, blurRadius: 5.0, spreadRadius: 1.0, offset: Offset(1.0, 2.0))],
      ),
      child: Row(
        children: [
          Container(child: Text('Vehicle', style: TextStyle(fontWeight: FontWeight.w300))),
          Expanded(
            child: Container(
                child: Container(
              child: DropdownSearch<String>(
                dropdownDecoratorProps: DropDownDecoratorProps(dropdownSearchDecoration: InputDecoration(suffixIconColor: Colors.black, border: InputBorder.none, contentPadding: EdgeInsets.only(top: 15))),
                popupProps: PopupProps.dialog(
                  showSelectedItems: true,
                  showSearchBox: true,
                  title: Padding(padding: EdgeInsets.only(left: 16.0, top: 8.0), child: Text('Select Vehicle')),
                  searchFieldProps: TextFieldProps(
                    decoration: InputDecoration(
                      prefixIcon: Icon(Icons.search),
                      fillColor: Color(0xFFF8F8F8),
                      filled: true,
                      border: OutlineInputBorder(borderRadius: BorderRadius.circular(8.0)),
                      contentPadding: EdgeInsets.zero,
                      hintText: 'Search Vehicle',
                    ),
                    padding: EdgeInsets.only(top: 8.0, right: 16.0, left: 16.0, bottom: 8.0),
                  ),
                  itemBuilder: (context, item, isEnabled) {
                    return Container(
                      decoration: BoxDecoration(color: Color(0xFFF8F8F8), borderRadius: BorderRadius.circular(8)),
                      margin: EdgeInsets.symmetric(horizontal: 10, vertical: 3),
                      child: Container(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Expanded(child: Align(child: Container(child: Text(item, style: TextStyle(fontSize: 12)), padding: EdgeInsets.only(left: 10)), alignment: Alignment.centerLeft)),
                            Radio(value: item, groupValue: widget.selectedDevice, onChanged: (selected) {}, activeColor: Colors.red),
                          ],
                        ),
                      ),
                    );
                  },
                ),
                items: [...widget.allDeviceObjects.values.toList()],
                onChanged: (selectedDevice) => widget._callBack(selectedDevice),
                dropdownBuilder: (context, item) => Container(child: Text(item ?? '', textAlign: TextAlign.end, style: TextStyle(fontSize: 14))),
                selectedItem: widget.selectedDevice,
              ),
            )),
          ),
        ],
      ),
    );
  }
}
