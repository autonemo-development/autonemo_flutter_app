import 'package:autonemogps/api_calls/gps_server_requests.dart';
import 'package:autonemogps/config/colors.dart';
import 'package:autonemogps/config/helpers.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class ForgotPasswordDialog extends StatefulWidget {
  const ForgotPasswordDialog({Key? key}) : super(key: key);

  @override
  State<ForgotPasswordDialog> createState() => _ForgotPasswordDialogState();
}

class _ForgotPasswordDialogState extends State<ForgotPasswordDialog> {
  bool _isSubmitting = false;
  TextEditingController _controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(
            'If you have forgotten your password, then please give us your Mobile number or Email Id.We will keep in touch with you to help you reset your password.',
            style: TextStyle(color: AppColors.headerTextColor, fontSize: 12, fontWeight: FontWeight.w300),
          ),
          SizedBox(
            height: 10,
          ),
          TextField(
            controller: _controller,
            style: TextStyle(color: AppColors.primaryColor2),
            decoration: InputDecoration(
              hintText: 'Enter your Mobile Number or Email ID',
              hintStyle: TextStyle(color: AppColors.primaryColor, fontSize: 12),
              border: OutlineInputBorder(
                borderSide: BorderSide(color: AppColors.primaryColor, width: 1),
              ),
            ),
          ),
          SizedBox(
            height: 10,
          ),
          _isSubmitting
              ? SpinKitRing(
                  lineWidth: 3.0,
                  color: AppColors.primaryColor,
                  size: 35.0,
                )
              : ElevatedButton(onPressed: onSubmitPressed, child: Text('Submit'))
        ],
      ),
    );
  }

  void onSubmitPressed() {
    if (_controller.text.isEmpty) {
      return;
    }
    setState(() {
      _isSubmitting = true;
    });
    GPSServerRequests.sendPasswordResetRequest(_controller.text).then((message) {
      showSnackBar(context, message, AppColors.sensorActiveColor);
      Navigator.pop(context);
    }).catchError((error) {
      setState(() {
        _isSubmitting = false;
      });
      showSnackBar(context, error.toString(), AppColors.primaryColor);
    });
  }
}
