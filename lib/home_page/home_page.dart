import 'package:autonemogps/api_calls/gps_server_requests.dart';
import 'package:autonemogps/api_calls/gps_wox_requests.dart';
import 'package:autonemogps/bloc/home_bloc.dart';
import 'package:autonemogps/bloc/home_event.dart';
import 'package:autonemogps/common/bloc/window_bloc.dart';
import 'package:autonemogps/config/colors.dart';
import 'package:autonemogps/config/string_constants.dart';
import 'package:autonemogps/home_page/tabs/alert_page/alert_page.dart';
import 'package:autonemogps/home_page/tabs/dashboard/dashboard.dart';
import 'package:autonemogps/home_page/tabs/list_view_page/list_page.dart';
import 'package:autonemogps/home_page/tabs/map_view/map_page_with_bloc.dart';
import 'package:autonemogps/home_page/tabs/more_page/more_page.dart';
import 'package:autonemogps/storage/repository.dart';
import 'package:autonemogps/utils/push_notification_service.dart';
import 'package:bottom_navy_bar/bottom_navy_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';

class HomePage extends StatefulWidget {
  static const String page = 'home_page';

  HomePage();

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> with TickerProviderStateMixin {
  TabController? _tabController;
  int _tabIndex = 2;
  List<Widget> tabScreens = List.empty(growable: true);
  List<int> browsingHistory = [1];
  DateTime? currentBackPressTime;
  HomeBloc? _homeBloc;

  @override
  void initState() {
    _tabController = getTabController(_tabIndex);

    tabScreens.add(
      Dashboard(
        (index) {
          setState(() => _tabIndex = 1);
          _homeBloc?.add(UpdateListTypeEvent(selectedItem: index));
        },
      ),
    );

    tabScreens.add(ListPage());
    tabScreens.add(BlocProvider<WindowBloc>(create: (context) => WindowBloc(), child: MapPageWithBloc()));
    tabScreens.add(AlertPage());
    tabScreens.add(MorePage());

    _homeBloc = BlocProvider.of<HomeBloc>(context);

    isDeviceDataFetched().then((value) {
      _homeBloc?.add(GetInitialDataEvent());
      _homeBloc?.add(StartTimerEvent());
      _homeBloc?.add(CheckForPopupEvent());
    });

    if (Repository.getServerType() == GPSServerConstants.type || Repository.getServerType() == GPSWoxConstants.type) {
      PushNotificationService.subscribeToNotification();
    }

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(statusBarColor: AppColors.primaryColor, statusBarBrightness: Brightness.light, statusBarIconBrightness: Brightness.light));

    return SafeArea(
      top: false,
      bottom: true,
      child: WillPopScope(
        onWillPop: () async {
          if (browsingHistory.length == 1) {
            DateTime now = DateTime.now();
            if (currentBackPressTime == null || now.difference(currentBackPressTime!) > Duration(seconds: 2)) {
              currentBackPressTime = now;
              Fluttertoast.showToast(msg: 'Press again to exit');
              return false;
            }
          } else if (browsingHistory.length > 1) {
            setState(() {
              browsingHistory.removeLast();
              _tabIndex = browsingHistory.last;
            });
            return false;
          }
          return true;
        },
        child: Scaffold(
          resizeToAvoidBottomInset: false,
          body: IndexedStack(index: _tabIndex, children: tabScreens),
          bottomNavigationBar: _buildBottomNavyBar(),
          backgroundColor: Colors.grey[200],
        ),
      ),
    );
  }

  Future<bool> isDeviceDataFetched() async {
    String str = Repository.getAllDevicesData();
    if (str.isEmpty) {
      if (Repository.getServerType() == GPSWoxConstants.type) {
        return await GPSWoxRequests.getDevices();
      }
      return await GPSServerRequests.fetchFnSettings();
    }
    return false;
  }

  Widget _buildBottomNavyBar() {
    return BottomNavyBar(
        selectedIndex: _tabIndex,
        backgroundColor: Colors.white,
        animationDuration: Duration(milliseconds: 100),
        items: [
          BottomNavyBarItem(
            icon: Icon(Icons.home),
            title: Text('Home'),
            activeColor: AppColors.primaryColor,
            inactiveColor: AppColors.primaryColor2,
          ),
          BottomNavyBarItem(
            icon: Image.asset('images/list.png', height: 20.0, color: _tabIndex == 1 ? AppColors.primaryColor : AppColors.primaryColor2),
            title: Text('List View'),
            activeColor: AppColors.primaryColor,
            inactiveColor: AppColors.primaryColor2,
          ),
          BottomNavyBarItem(
              icon: Image.asset('images/map.png', height: 20.0, color: _tabIndex == 2 ? AppColors.primaryColor : AppColors.primaryColor2),
              title: Text('Map View'),
              activeColor: AppColors.primaryColor,
              inactiveColor: AppColors.primaryColor2),
          BottomNavyBarItem(
              icon: Image.asset('images/bell.png', height: 20.0, color: _tabIndex == 3 ? AppColors.primaryColor : AppColors.primaryColor2),
              title: Text('Alerts'),
              activeColor: AppColors.primaryColor,
              inactiveColor: AppColors.primaryColor2),
          BottomNavyBarItem(
              icon: Image.asset('images/more.png', height: 20.0, color: _tabIndex == 4 ? AppColors.primaryColor : AppColors.primaryColor2),
              title: Text('More'),
              activeColor: AppColors.primaryColor,
              inactiveColor: AppColors.primaryColor2),
        ],
        onItemSelected: (index) {
          setState(() {
            _tabIndex = index;
            browsingHistory.add(_tabIndex);
          });
        });
  }

  @override
  void dispose() {
    _tabController?.dispose();
    super.dispose();
  }

  TabController getTabController(int initialIndex) {
    return TabController(initialIndex: initialIndex, length: 5, vsync: this);
  }
}
