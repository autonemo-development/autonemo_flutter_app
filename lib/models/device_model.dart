import 'package:autonemogps/storage/repository.dart';
import 'package:autonemogps/utils/icon_helper.dart';

class DeviceModel {
  String imei, model, name, expiry, simNumber, vin, voiceFeatureText, icon, fuelPrice, fuelQuantity;
  int deviceId;
  Map<String, dynamic> sensorJsonObject = {};
  Map<String, dynamic> deviceData = {};

  DeviceModel({
    required this.deviceId,
    required this.imei,
    required this.model,
    required this.name,
    required this.simNumber,
    required this.expiry,
    required this.icon,
    required this.voiceFeatureText,
    required this.vin,
    required this.sensorJsonObject,
    required this.fuelPrice,
    required this.fuelQuantity,
    required this.deviceData,
  });

  factory DeviceModel.fromGpsServerJson(Map<String, dynamic> json, String imei) {
    String voiceFeature = 'No';
    Map<String, dynamic> fuelMap = json['fcr'] as Map<String, dynamic>;

    Map<String, dynamic> sensorMap = json['sensors'] is Map ? json['sensors'] : {};
    Map<String, dynamic> customFieldsMap = json['custom_fields'] is Map ? json['custom_fields'] : {};

    customFieldsMap.forEach((key, value) {
      Map<String, dynamic> cData = value;

      if (cData['name'] == 'Voice Feature') {
        voiceFeature = cData['value'];
      }
    });

    return DeviceModel(
      deviceId: 0,
      imei: imei,
      name: json['name'] as String,
      expiry: json['expire_dt'] as String,
      sensorJsonObject: sensorMap,
      simNumber: json['sim_number'] as String,
      model: json['model'] as String,
      vin: json['vin'] as String,
      icon: IconHelper.getIcon(json['icon'] as String),
      voiceFeatureText: voiceFeature,
      fuelPrice: fuelMap['cost'].toString(),
      fuelQuantity: fuelMap['summer'].toString(),
      deviceData: {},
    );
  }

  factory DeviceModel.fromGPSWoxServerJson(Map<String, dynamic> json) {
    Map<String, dynamic> deviceData = json['device_data'] as Map<String, dynamic>;
    String imei = deviceData['imei'] as String;
    return DeviceModel(
      deviceId: json['id'] as int,
      imei: imei,
      name: deviceData['name'] as String,
      expiry: (deviceData['expiration_date'] ?? '0000-00-00') as String,
      simNumber: deviceData['sim_number'] as String,
      model: deviceData['device_model'] as String,
      vin: deviceData['vin'] as String,
      fuelPrice: deviceData['fuel_price'] as String,
      fuelQuantity: deviceData['fuel_quantity'] as String,
      deviceData: deviceData,
      icon: Repository.getVehicleIcon(imei),
      sensorJsonObject: {},
      voiceFeatureText: 'No',
    );
  }
}
