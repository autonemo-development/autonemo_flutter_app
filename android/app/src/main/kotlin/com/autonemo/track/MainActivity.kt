package com.autonemo.track

import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.util.Log
import androidx.annotation.NonNull
import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.android.FlutterActivityLaunchConfigs
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugins.GeneratedPluginRegistrant
import java.lang.Exception
import java.lang.reflect.Method
import java.util.*

class MainActivity : FlutterActivity() {

    private val TAG = "MainActivity"
    private lateinit var methodChannel: MethodChannel

    override fun configureFlutterEngine(@NonNull flutterEngine: FlutterEngine) {
        GeneratedPluginRegistrant.registerWith(flutterEngine);
        methodChannel=MethodChannel(flutterEngine.dartExecutor.binaryMessenger, getString(R.string.channel_name));

        methodChannel.setMethodCallHandler { call, _ ->
            when (call.method) {
                "parkingViolated"->{

                }
                "parkingViolationAcknowledged" -> {
                    stopService(Intent(applicationContext, SirenService::class.java))
                }
                "sosAlertAcknowledged"->{
                    stopService(Intent(applicationContext, SOSSirenService::class.java))
                }
                "changeLanguage" -> {

                    val languageToLoad = call.argument<String>("lang") //here you can select your system local language too...
                    val locale = Locale(languageToLoad!!)
                    Locale.setDefault(locale)


                    val config = Configuration()
                    config.setLocale(locale)
                    baseContext.createConfigurationContext(config)
                }
                "collectGarbage" -> {
                    System.gc();
                    Runtime.getRuntime().gc();
                    clearCache();
                }
            }
        }

    }

    private fun clearCache() {
        val pm: PackageManager = packageManager
        // Get all methods on the PackageManager
        val methods: Array<Method> = pm.javaClass.getDeclaredMethods()
        for (m in methods) {
            if (m.getName().equals("freeStorage")) {
                try {
                    val desiredFreeStorage = 8589934592L
                    m.invoke(pm, desiredFreeStorage, null)
                } catch (e: Exception) {
                    // Method invocation failed. Could be a permission problem
                }
                break
            }
        }
    }


    override fun getBackgroundMode(): FlutterActivityLaunchConfigs.BackgroundMode {
        return FlutterActivityLaunchConfigs.BackgroundMode.transparent
    }

    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)
        val notificationType=intent.getStringExtra("sirenType");
        Log.i(TAG, "onNewIntent: "+notificationType)
        val sharedPreferences = getSharedPreferences("FlutterSharedPreferences", MODE_PRIVATE)
        if(notificationType=="parking_violation"){
            val imeis = sharedPreferences.getString("flutter.parkingViolationIMEI", "")
            val names = sharedPreferences.getString("flutter.parkingViolationNames", "")
            methodChannel.invokeMethod("showParkingViolationAlert", "$imeis;$names");
        }else if(notificationType=="sos"){
            val imeis = sharedPreferences.getString("flutter.sosIMEI", "")
            val names = sharedPreferences.getString("flutter.sosNames", "")
            methodChannel.invokeMethod("showSosAlert", "$imeis;$names");
        }
    }



}
