import 'package:autonemogps/config/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class ReferralHistory extends StatefulWidget {
  static const String page = 'ReferralHistory';
  final Map<String, String> value;

  ReferralHistory(this.value);

  @override
  _ReferralHistoryState createState() => _ReferralHistoryState();
}

class _ReferralHistoryState extends State<ReferralHistory> {
  bool _isLoading = true;
  String? error;
  Map<String, String> data = {};

  @override
  void initState() {
    super.initState();
    _isLoading = false;
    data = widget.value;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: AppColors.defaultBackground,
        appBar: AppBar(
          centerTitle: false,
          title: Text('Referred Clients', style: TextStyle(color: Color(0xFF474747), fontWeight: FontWeight.w500, fontSize: 16)),
          backgroundColor: Colors.white,
          iconTheme: IconThemeData(color: Color(0xFF7E7E7E)),
        ),
        body: _isLoading
            ? SpinKitRing(
                lineWidth: 3.0,
                color: AppColors.primaryColor,
                size: 35.0,
              )
            : error != null
                ? Center(
                    child: Text(error!),
                  )
                : ListView.builder(
                    padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                    itemCount: data.length,
                    itemBuilder: (context, index) {
                      MapEntry<String, String> referredClientMapEntry = data.entries.toList().elementAt(index);
                      return ReferralHistoryItem(referredClientMapEntry: referredClientMapEntry);
                    },
                  ));
  }
}

class ReferralHistoryItem extends StatelessWidget {
  final MapEntry referredClientMapEntry;

  ReferralHistoryItem({required this.referredClientMapEntry});

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.all(4),
        padding: EdgeInsets.symmetric(vertical: 12.0, horizontal: 16),
        width: MediaQuery.of(context).size.width / 2.27,
        decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(5.0)),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Row(children: [Expanded(child: Text(referredClientMapEntry.value, style: TextStyle(color: Color(0xFF3A3A3A), fontSize: 16, fontWeight: FontWeight.w500)))]),
          ],
        ));
  }
}
