import 'package:animations/animations.dart';
import 'package:autonemogps/config/colors.dart';
import 'package:autonemogps/config/helpers.dart';
import 'package:autonemogps/config/string_constants.dart';
import 'package:autonemogps/screens/my_account/refer_page.dart';
import 'package:flutter/material.dart';

showReferRedeemDialog(BuildContext context) {
  showModal(
    context: context,
    builder: (context) {
      return Dialog(
        child: Container(
          padding: EdgeInsets.all(20.0),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 15.0),
                child: Text('Points let you avail offers from ${companyName}', textAlign: TextAlign.center, style: TextStyle(color: AppColors.primaryColor, fontSize: 16, fontWeight: FontWeight.w500)),
              ),
              SizedBox(height: 20),
              Text('You can earn Points in ${companyName} by: ', style: TextStyle(color: Color(0xFF474747), fontSize: 13)),
              SizedBox(height: 10),
              Row(
                children: [
                  Padding(padding: EdgeInsets.all(12.0), child: Image.asset('images/purchase.png', width: 30, height: 30)),
                  Text('Paying Yearly Subscriptions', style: TextStyle(color: Color(0xFF474747), fontSize: 13)),
                ],
              ),
              Divider(),
              InkWell(
                onTap: () => Navigator.of(context).push(MaterialPageRoute(builder: (context) => ReferPage())),
                child: Row(
                  children: [
                    Padding(padding: EdgeInsets.all(12.0), child: Image.asset('images/refer.png', width: 30, height: 30)),
                    Expanded(child: Text('Referring friends to ${autonemo}', style: TextStyle(color: Color(0xFF474747), fontSize: 13))),
                  ],
                ),
              ),
              SizedBox(height: 10),
              Text('You can later use these Points to avail selected offers for FREE', style: TextStyle(color: Color(0xFF474747), fontSize: 13)),
              SizedBox(height: 20),
              Center(
                child: InkWell(
                  onTap: () {
                    Navigator.pop(context);
                    launchUrl(CRMConstants.LEARN_MORE_URL);
                  },
                  child: Container(
                    padding: EdgeInsets.symmetric(horizontal: 25, vertical: 7.0),
                    decoration: BoxDecoration(color: AppColors.primaryColor, borderRadius: BorderRadius.circular(20.0)),
                    child: Text('Learn More', style: TextStyle(color: Colors.white, fontSize: 14, fontWeight: FontWeight.w500)),
                  ),
                ),
              ),
            ],
          ),
        ),
      );
    },
  );
}
