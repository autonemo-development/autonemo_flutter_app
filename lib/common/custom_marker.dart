import 'dart:async';
import 'dart:math';
import 'dart:ui' as ui;

import 'package:autonemogps/config/size_config.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

Future<BitmapDescriptor> getMarkerIconWithInfo(String imagePath, String infoText, Color color, double rotateDegree) async {
  final ui.PictureRecorder pictureRecorder = ui.PictureRecorder();
  final Canvas canvas = Canvas(pictureRecorder);

  //marker size
  /// Modify => Size
  // Size markerSize =
  //     Size(13 * SizeConfig.heightMultiplier, 13 * SizeConfig.heightMultiplier);
  Size markerSize = Size(120, 120);

  // Add info text
  TextPainter infoTextPainter = TextPainter(textDirection: TextDirection.ltr, textAlign: TextAlign.center);
  infoTextPainter.text = TextSpan(
    text: infoText,
    style: TextStyle(fontFamily: 'Poppins', fontSize: 3.2 * SizeConfig.heightMultiplier, fontWeight: FontWeight.w600, color: color),
  );
  infoTextPainter.layout();

  final double infoHeight = 5 * SizeConfig.heightMultiplier;
  final double infoTextWidth = (infoText.length * 2 * SizeConfig.heightMultiplier) > 19 * SizeConfig.heightMultiplier
      ? (infoText.length * 2 * SizeConfig.heightMultiplier) + 3
      : 19 * SizeConfig.heightMultiplier;

  final double rightInfoWidth = 0;
  final double infoBorder = 1.3 * SizeConfig.heightMultiplier;
  final gapBetweenInfoAndMarker = 5 * SizeConfig.heightMultiplier;

  //canvas size
  Size canvasSize = Size(infoTextWidth + rightInfoWidth + infoBorder + 5, infoHeight + markerSize.height + gapBetweenInfoAndMarker - 15);

  final Paint infoPaint = Paint()..color = Colors.white;
  final Paint infoShadowPaint = Paint()
    ..color = Colors.black.withOpacity(.5)
    ..maskFilter = MaskFilter.blur(BlurStyle.normal, gapBetweenInfoAndMarker / 4);

  final double shadowWidth = 2.5 * SizeConfig.heightMultiplier;

  canvas.translate(canvasSize.width / 2, canvasSize.height / 2 + infoHeight / 2 + gapBetweenInfoAndMarker / 2);

  // rect for middle image
  Rect rectMiddle =
      Rect.fromLTWH(-markerSize.width / 2 + .5 * shadowWidth, -markerSize.height / 2 + .5 * shadowWidth, markerSize.width - shadowWidth, markerSize.height - shadowWidth);

  //save canvas before rotate
  canvas.save();

  double rotateRadian = (pi / 180.0) * rotateDegree;

  //Rotate Image
  canvas.rotate(rotateRadian);
  // Add image
  ui.Image image = await getImageFromPath(imagePath);
  paintImage(canvas: canvas, image: image, rect: rectMiddle, fit: BoxFit.fitHeight);

  canvas.restore();

  //shadow
  canvas.drawRRect(
    RRect.fromLTRBR(
      -infoTextWidth / 2 - rightInfoWidth / 2 - infoBorder / 2,
      -canvasSize.height / 2 - infoHeight / 2 / 4 + 1,
      infoTextWidth / 2 + rightInfoWidth / 2 + infoBorder / 2,
      -canvasSize.height / 2 + infoHeight / 2 + 1,
      Radius.circular(1.875 * SizeConfig.heightMultiplier),
    ),
    infoShadowPaint,
  );
  // Add info box
  canvas.drawRRect(
    RRect.fromLTRBR(
      -infoTextWidth / 2 - rightInfoWidth / 2 - infoBorder,
      -canvasSize.height / 2 - infoHeight / 2 - gapBetweenInfoAndMarker / 2 + 1,
      infoTextWidth / 2 + rightInfoWidth / 2 + infoBorder,
      -canvasSize.height / 2 + infoHeight / 2 + 1,
      Radius.circular(1.875 * SizeConfig.heightMultiplier),
    ),
    infoPaint,
  );

  //info text paint
  infoTextPainter.paint(
    canvas,
    Offset(
      -infoTextPainter.width / 2 - rightInfoWidth / 2,
      -canvasSize.height / 2 - gapBetweenInfoAndMarker / 2 - infoHeight / 2 + infoBorder,
    ),
  );

  canvas.restore();

  // Convert canvas to image
  final ui.Image markerAsImage = await pictureRecorder.endRecording().toImage(canvasSize.width.toInt(), canvasSize.height.toInt());

  // Convert image to bytes
  final ByteData? byteData = await markerAsImage.toByteData(format: ui.ImageByteFormat.png);
  final Uint8List uint8List = byteData!.buffer.asUint8List();

  return BitmapDescriptor.fromBytes(uint8List);
}

Future<BitmapDescriptor> getMarkerIcon(Uint8List imageBytes, double rotateDegree) async {
  final ui.PictureRecorder pictureRecorder = ui.PictureRecorder();
  final Canvas canvas = Canvas(pictureRecorder);

  Size markerSize = Size(20 * SizeConfig.heightMultiplier, 20 * SizeConfig.heightMultiplier);
  final double shadowWidth = 30.0;

  canvas.translate(markerSize.width / 2, markerSize.height / 2);

  // rect for middle image
  Rect rectMiddle =
      Rect.fromLTWH(-markerSize.width / 2 + .5 * shadowWidth, -markerSize.height / 2 + .5 * shadowWidth, markerSize.width - shadowWidth, markerSize.height - shadowWidth);

  //save canvas before rotate
  canvas.save();

  double rotateRadian = (pi / 180.0) * rotateDegree;

  //Rotate Image
  canvas.rotate(rotateRadian);

  /*// Add path for middle image
  canvas.clipPath(Path()
    ..addRect(rectMiddle));*/

  // Add image
  ui.Image image = await getImageFromBytes(imageBytes);
  paintImage(canvas: canvas, image: image, rect: rectMiddle, fit: BoxFit.fitHeight);

  canvas.restore();

  // Convert canvas to image
  final ui.Image markerAsImage = await pictureRecorder.endRecording().toImage(markerSize.width.toInt(), markerSize.height.toInt());

  // Convert image to bytes
  final ByteData? byteData = await markerAsImage.toByteData(format: ui.ImageByteFormat.png);
  final Uint8List uint8List = byteData!.buffer.asUint8List();

  return BitmapDescriptor.fromBytes(uint8List);
}

Future<ui.Image> getImageFromBytes(Uint8List imageBytes) async {
  final Completer<ui.Image> completer = new Completer();

  ui.decodeImageFromList(imageBytes, (ui.Image img) {
    return completer.complete(img);
  });

  return completer.future;
}

Future<Uint8List> getBytesFromAsset(String path, int width) async {
  ByteData data = await rootBundle.load(path);
  ui.Codec codec = await ui.instantiateImageCodec(data.buffer.asUint8List(), targetWidth: width);
  ui.FrameInfo fi = await codec.getNextFrame();
  return (await fi.image.toByteData(format: ui.ImageByteFormat.png))!.buffer.asUint8List();
}

Future<BitmapDescriptor> getMarkerFromImagePath(String path, {int size = 100}) async {
  return BitmapDescriptor.fromBytes(await getBytesFromAsset(path, size));
}

Future<ui.Image> getImageFromPath(String imagePath) async {
  ByteData bd = await rootBundle.load(imagePath);
  Uint8List imageBytes = Uint8List.view(bd.buffer);

  final Completer<ui.Image> completer = new Completer();

  ui.decodeImageFromList(imageBytes, (ui.Image img) {
    return completer.complete(img);
  });

  return completer.future;
}
