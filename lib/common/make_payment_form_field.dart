import 'package:autonemogps/config/colors.dart';
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class MakePaymentFormField extends StatelessWidget {
  TextEditingController? controller;
  int maxLines;
  TextInputType textInputType;
  void Function(String) onChanged;
  String hinText;

  MakePaymentFormField(
      {this.controller,
      this.maxLines = 1,
      this.textInputType = TextInputType.text,
      required this.onChanged,
      required this.hinText});

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: controller,
      maxLines: maxLines,
      keyboardType: textInputType,
      
      onChanged: onChanged,
      decoration: InputDecoration(
        contentPadding:
            EdgeInsets.only(top: 10, bottom: 10, left: 15, right: 15),
        hintText: hinText,
        hintStyle: TextStyle(color: Color(0xFFBBBBBB)),
        filled: true,
        fillColor: Color(0xFFF1F1F1),
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.transparent),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(color: AppColors.sensorInActiveColor, width: 1.0),
        ),
      ),
      cursorColor: AppColors.primaryColor,
    );
  }
}