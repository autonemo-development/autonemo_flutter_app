import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

abstract class WindowEvent extends Equatable {
  const WindowEvent();
  @override
  List<Object> get props => [];
}

class ChangePositionEvent extends WindowEvent {
  final BuildContext context;
  final ScreenCoordinate screenCoordinate;
  ChangePositionEvent({required this.context, required this.screenCoordinate});
}

class WindowLoadedEvent extends WindowEvent {
  final double height, width;
  WindowLoadedEvent({required this.height, required this.width});
}
