import 'package:autonemogps/bloc/home_bloc.dart';
import 'package:autonemogps/bloc/home_event.dart';
import 'package:autonemogps/config/colors.dart';
import 'package:autonemogps/config/helpers.dart';
import 'package:autonemogps/config/string_constants.dart';
import 'package:autonemogps/home_page/tabs/more_page/more_item_widget.dart';
import 'package:autonemogps/screens/change_password_page.dart';
import 'package:autonemogps/screens/contact_us.dart';
import 'package:autonemogps/screens/expense_page/expense_page.dart';
import 'package:autonemogps/screens/geofence_settings_page.dart';
import 'package:autonemogps/screens/maintenance_page/maintenance_page.dart';
import 'package:autonemogps/screens/notification_settings_page.dart';
import 'package:autonemogps/screens/privacy_policy_page.dart';
import 'package:autonemogps/screens/reports_page.dart';
import 'package:autonemogps/screens/terms_of_use_page.dart';
import 'package:autonemogps/storage/repository.dart';
import 'package:autonemogps/utils/logs.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:package_info_plus/package_info_plus.dart';

class MorePage extends StatefulWidget {
  @override
  _MorePageState createState() => _MorePageState();
}

class _MorePageState extends State<MorePage> {
  bool switchControl = true;
  bool isMoreHide = false;
  bool isSettingHide = false;
  bool isNotificationStatusChanging = false;
  TextEditingController pinController = TextEditingController();
  TextEditingController confirmPinController = TextEditingController();
  TextEditingController pswdController = TextEditingController();
  String version = '';

  @override
  void initState() {
    super.initState();
    switchControl = Repository.getNotificationStatus();
    PackageInfo.fromPlatform().then((PackageInfo packageInfo) {
      setState(() {
        version = packageInfo.version;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        backgroundColor: Colors.white,
        foregroundColor: Colors.black,
        title: Text('More'),
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                padding: EdgeInsets.symmetric(horizontal: 16),
                width: MediaQuery.of(context).size.width,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    SizedBox(height: 16),
                    Row(
                      children: [
                        MoreItemWidget(
                            imagePath: 'images/report.png',
                            title: 'Reports',
                            onPressed: () {
                              Navigator.pushNamed(context, ReportsPage.page);
                            }),
                        SizedBox(width: 16),
                        MoreItemWidget(
                          imagePath: 'images/geofence.png',
                          title: 'Geofence Settings',
                          onPressed: () async {
                            Navigator.push(context, MaterialPageRoute(builder: (context) => GeofenceSettingsPage()));
                          },
                        ),
                      ],
                    ),
                    SizedBox(height: 16),
                    if (Repository.getServerType() == GPSServerConstants.type)
                      Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              MoreItemWidget(
                                  imagePath: 'images/maintenance-tools.png',
                                  title: 'Maintenance',
                                  onPressed: () {
                                    Navigator.of(context).pushNamed(MaintenancePage.page);
                                  }),
                              SizedBox(width: 16),
                              MoreItemWidget(
                                imagePath: 'images/expence.png',
                                title: 'Expense',
                                onPressed: () async {
                                  Navigator.of(context).pushNamed(ExpensePage.page);
                                },
                              ),
                            ],
                          ),
                          SizedBox(height: 16),
                        ],
                      ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        MoreItemWidget(
                            imagePath: 'images/support.png',
                            title: 'Live Support',
                            onPressed: () {
                              Navigator.push(context, MaterialPageRoute(builder: (context) => ContactUsScreen()));
                            }),
                        SizedBox(width: 16),
                        MoreItemWidget(
                          imagePath: 'images/pay.png',
                          title: 'Pay Now',
                          onPressed: () {
                            launchUrl('https://erp.autonemo.io/payment?platformUsername=${Repository.getUsername()}');
                          },
                        ),
                      ],
                    ),
                    SizedBox(height: 20),
                  ],
                ),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  SizedBox(height: 15.0),
                  isNotificationStatusChanging
                      ? SpinKitRing(
                          lineWidth: 1.0,
                          color: AppColors.primaryColor,
                          size: 40.0,
                        )
                      : SettingsItemWithSwitch(
                          imagePath: 'images/notification_bell.png',
                          title: 'Notification',
                          switchControl: switchControl,
                          onTap: (value) {
                            setState(() {
                              switchControl = !switchControl;
                              Repository.setNotificationStatus(switchControl);
                              String username = Repository.getUsername().replaceAll('@', '_at_');

                              FirebaseMessaging firebaseMessaging = FirebaseMessaging.instance;

                              if (!switchControl)
                                firebaseMessaging.unsubscribeFromTopic(username).then((_) {
                                  writeDebugLogs('unsubscribed from topic ' + username);
                                });
                              else
                                firebaseMessaging.subscribeToTopic(username).then((_) {
                                  writeDebugLogs('subscribed to topic ' + username);
                                });
                            });
                          },
                        ),
                  Divider(),
                  SettingsItem(
                    imagePath: 'images/notification-settings.png',
                    title: 'Notification Settings',
                    onTap: () {
                      Navigator.pushNamed(context, NotificationSettingsPage.pageName);
                    },
                  ),
                  Divider(),
                  SettingsItem(
                    imagePath: 'images/password_change.png',
                    title: 'Change Password',
                    onTap: () {
                      Navigator.of(context).pushNamed(ChangePassword.page);
                    },
                  ),
                  Divider(),
                  SettingsItem(
                    imagePath: 'images/terms_of_use.png',
                    title: 'Terms of Use',
                    onTap: () {
                      Navigator.of(context, rootNavigator: true).pushNamed(TermsOfUsePage.page);
                    },
                  ),
                  Divider(),
                  SettingsItem(
                    imagePath: 'images/privacy_policy.png',
                    title: 'Privacy Policy',
                    onTap: () {
                      Navigator.of(context).pushNamed(PrivacyPolicyPage.page);
                    },
                  ),
                  Divider(),
                  SettingsItem(
                    imagePath: 'images/logout.png',
                    title: 'Logout - ${Repository.getUsername()}',
                    onTap: () async {
                      BlocProvider.of<HomeBloc>(context).add(LogoutEvent());
                    },
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                ],
              ),
              Align(
                alignment: Alignment.center,
                child: Text('Version : $version'),
              ),
              SizedBox(
                height: 20.0,
              ),
            ],
          ),
        ),
      ),
    );
  }
}

// ignore: must_be_immutable
class SettingsItemWithSwitch extends StatelessWidget {
  String imagePath;
  String title;
  bool switchControl;
  void Function(bool) onTap;

  SettingsItemWithSwitch({required this.imagePath, required this.title, required this.onTap, required this.switchControl});

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.all(4),
        padding: EdgeInsets.symmetric(horizontal: 16),
        width: MediaQuery.of(context).size.width / 2.27,
        decoration: BoxDecoration(
          color: Colors.white,
        ),
        child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                Image.asset(
                  imagePath,
                  width: 20,
                ),
                SizedBox(
                  width: 10,
                ),
                Text(
                  title,
                  style: TextStyle(color: Color(0xFF3A3A3A), fontSize: 15, fontWeight: FontWeight.w500),
                ),
              ],
            ),
            Switch(
              onChanged: onTap,
              value: switchControl,
              activeColor: AppColors.primaryColor,
              activeTrackColor: Color(0xFFB7E4FF),
              inactiveThumbColor: Color(0xFF7E7E7E),
              inactiveTrackColor: Color(0xFFD6D6D6),
            )
          ],
        ));
  }
}

// ignore: must_be_immutable
class SettingsItem extends StatelessWidget {
  String imagePath;
  String title;
  void Function() onTap;

  SettingsItem({required this.imagePath, required this.title, required this.onTap});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: Container(
          margin: EdgeInsets.all(4),
          padding: EdgeInsets.symmetric(vertical: 12.0, horizontal: 16),
          width: MediaQuery.of(context).size.width / 2.27,
          decoration: BoxDecoration(
            color: Colors.white,
          ),
          child: Row(
            children: [
              Image.asset(
                imagePath,
                width: 25,
              ),
              SizedBox(
                width: 10,
              ),
              Text(
                title,
                style: TextStyle(color: Color(0xFF3A3A3A), fontSize: 15, fontWeight: FontWeight.w500),
              ),
            ],
          )),
    );
  }
}
