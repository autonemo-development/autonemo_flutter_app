import 'package:autonemogps/models/device_model.dart';
import 'package:autonemogps/models/tracking_device_model.dart';
import 'package:equatable/equatable.dart';

abstract class HomeState extends Equatable {
  @override
  List<Object> get props => [];
}

class IdleState extends HomeState {}

class DataFetchedState extends HomeState {
  final Map<String, TrackingDevice> trackingList;
  final Map<String, DeviceModel> deviceList;

  DataFetchedState({required this.deviceList, required this.trackingList});
}

class MapDataFetchedState extends HomeState {
  final Map<String, TrackingDevice> trackingList;
  final Map<String, DeviceModel> deviceList;

  MapDataFetchedState({required this.deviceList, required this.trackingList});
}

class DataFetchingState extends HomeState {}

class SortedIMEIGeneratedState extends HomeState {
  final Map<String, List<String>> imeiList;

  SortedIMEIGeneratedState(this.imeiList);
}

class SearchedVehicleState extends HomeState {
  final Map<String, TrackingDevice> trackingDataList;

  SearchedVehicleState({required this.trackingDataList});
}

class UpdateCountState extends HomeState {
  final int allCount, movingCount, idleCount, stoppedCount, offlineCount, expiredCount, noDataCount, selectedList;

  UpdateCountState(
      {required this.allCount,
      required this.movingCount,
      required this.idleCount,
      required this.stoppedCount,
      required this.offlineCount,
      required this.expiredCount,
      required this.noDataCount,
      required this.selectedList});
}

class SearchUpdatedState extends HomeState {
  final bool showSearch;

  SearchUpdatedState(this.showSearch);
}

class NoInternetState extends HomeState {}

class DeviceExpiredState extends HomeState {
  final String expiredDeviceNames;

  DeviceExpiredState(this.expiredDeviceNames);
}

class ShowTimedDialog extends HomeState {
  ShowTimedDialog();
}
