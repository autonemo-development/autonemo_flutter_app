import 'package:flutter/material.dart';

// ignore: must_be_immutable
class CustomTextFormFieldWithDataTime extends StatelessWidget {
  CustomTextFormFieldWithDataTime({required this.controller, this.enabled = true, required this.onChanged});

  TextEditingController controller;
  bool enabled;
  Function(String) onChanged;

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: controller,
      enabled: enabled,
      maxLines: 1,
      validator: (value) {
        if (value!.isEmpty) {
          return "Field can't be empty";
        }
        return null;
      },
      style: TextStyle(fontSize: 12, fontWeight: FontWeight.w500),
      decoration: InputDecoration(
        contentPadding: EdgeInsets.only(top: 10, bottom: 10, left: 12, right: 12),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(5.0)),
        suffixIcon: Padding(
          padding: const EdgeInsets.only(top: 10.0, bottom: 10.0),
          child: Image.asset('images/date-time.png', width: 5, height: 5),
        ),
        fillColor: Colors.black,
      ),
      textInputAction: TextInputAction.next,
      onFieldSubmitted: (_) => FocusScope.of(context).nextFocus(),
      onChanged: onChanged,
    );
  }
}
