import 'package:autonemogps/home_page/tabs/alert_page/alert_page.dart';
import 'package:autonemogps/models/device_model.dart';
import 'package:autonemogps/models/tracking_device_model.dart';
import 'package:autonemogps/screens/advance_info/advance_info.dart';
import 'package:autonemogps/screens/engine_lock/engine_lock.dart';
import 'package:autonemogps/screens/playback/playback_page.dart';
import 'package:autonemogps/screens/quick_settings/quick_settings.dart';
import 'package:autonemogps/screens/tracking/tracking_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

class ListViewBottomPopup extends StatefulWidget {
  final TrackingDevice trackingDevice;
  final DeviceModel deviceModel;

  ListViewBottomPopup(this.trackingDevice, this.deviceModel);

  @override
  State createState() {
    return ListViewBottomState();
  }
}

class ListViewBottomState extends State<ListViewBottomPopup> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: AlignedGridView.count(
        crossAxisCount: 3,
        shrinkWrap: true,
        itemCount: 6,
        itemBuilder: (context, index) => InkWell(
          onTap: () => _onTap(index),
          child: Container(
            height: 100,
            padding: EdgeInsets.all(25),
            child: Image(fit: BoxFit.contain, image: AssetImage(getAssetImage(index))),
          ),
        ),
      ),
    );
  }

  _onTap(int index) {
    switch (index) {
      case 0:
        Navigator.of(context, rootNavigator: true).push(MaterialPageRoute(builder: (context) => TrackingPage(widget.deviceModel, widget.trackingDevice)));
        break;
      case 1:
        Navigator.of(context, rootNavigator: true).push(MaterialPageRoute(builder: (context) => PlaybackPage(widget.deviceModel)));
        break;
      case 2:
        Navigator.of(context, rootNavigator: true).push(MaterialPageRoute(builder: (context) => AdvanceInfo(widget.deviceModel, widget.trackingDevice)));
        break;
      case 3:
        Navigator.of(context, rootNavigator: true).push(MaterialPageRoute(builder: (context) => EngineLock(widget.deviceModel, widget.trackingDevice)));
        break;
      case 4:
        Navigator.of(context, rootNavigator: true).push(MaterialPageRoute(builder: (context) => AlertPage(deviceModel: widget.deviceModel)));
        break;
      case 5:
        Navigator.of(context, rootNavigator: true).push(MaterialPageRoute(builder: (context) => QuickSettings(widget.deviceModel, widget.trackingDevice)));
        break;
    }
  }

  String getAssetImage(int index) {
    switch (index) {
      case 0:
        return 'images/bottom_sheet_live_tracking.png';
      case 1:
        return 'images/bottom_sheet_playback.png';
      case 2:
        return 'images/bottom_sheet_advance_info.png';
      case 3:
        return 'images/bottom_sheet_engine_lock.png';
      case 4:
        return 'images/bottom_sheet_alerts.png';
      case 5:
        return 'images/bottom_sheet_quick_settings.png';
    }

    return 'images/bottom_sheet_live_tracking.png';
  }
}
