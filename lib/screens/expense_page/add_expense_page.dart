import 'package:autonemogps/api_calls/gps_server_requests.dart';
import 'package:autonemogps/common/custom_text_form_field.dart';
import 'package:autonemogps/config/colors.dart';
import 'package:autonemogps/config/helpers.dart';
import 'package:autonemogps/models/device.dart';
import 'package:autonemogps/models/device_model.dart';
import 'package:autonemogps/models/expense.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:intl/intl.dart';

class AddExpensePage extends StatefulWidget {
  static const String page = 'add_expense_page';
  final String expenseId;

  AddExpensePage(this.expenseId);

  @override
  _AddExpensePageState createState() => _AddExpensePageState();
}

class _AddExpensePageState extends State<AddExpensePage> {
  bool _isLoading = false;
  String? error;
  TextEditingController nameController = new TextEditingController();
  TextEditingController dateController = new TextEditingController();
  TextEditingController qtyController = new TextEditingController();
  TextEditingController costController = new TextEditingController();
  TextEditingController odometerController = new TextEditingController();
  TextEditingController engienHrsController = new TextEditingController();
  TextEditingController supplierController = new TextEditingController();
  TextEditingController buyerController = new TextEditingController();
  TextEditingController descController = new TextEditingController();

  Map<String, String> deviceNames = Map<String, String>();
  String selectedImei = 'key';
  String selectedValue = '--Select Device--';

  @override
  void initState() {
    super.initState();
    deviceNames.putIfAbsent('key', () => '--Select Device--');
    Map<String, DeviceModel> map = AllDeviceModelData().deviceModelAndImeiMapGpsServer;
    map.forEach((key, value) {
      deviceNames.putIfAbsent(key, () => value.name);
    });
    if (widget.expenseId != 'false') {
      _isLoading = true;
      GPSServerRequests.fetchExpenseById(widget.expenseId).then((list) {
        // Navigator.pop(context);
        setState(() {
          _isLoading = false;
          if (list[0] == 0) {
            error = list[1];
          } else {
            Map<String, dynamic> data = list[1] as Map<String, dynamic>;
            Expense expense = data['expense'] as Expense;
            nameController.text = expense.name;
            dateController.text = expense.date;
            qtyController.text = expense.qty;
            costController.text = expense.cost;
            odometerController.text = (data['odometer'] as int).toString();
            engienHrsController.text = (data['engineHrs'] as int).toString();
            supplierController.text = expense.supplier;
            buyerController.text = expense.buyer;
            descController.text = data['desc'] as String;
            selectedImei = data['imei'] as String;
            selectedValue = deviceNames[selectedImei]!;
          }
        });
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
      child: Container(
        height: MediaQuery.of(context).size.height,
        decoration: BoxDecoration(
          gradient: LinearGradient(begin: Alignment.topCenter, end: Alignment.bottomCenter, colors: [
            Color(0x90DC0E18),
            Color(0x10DC0E18),
          ]),
        ),
        child: Column(
          children: [
            SizedBox(height: 10),
            Row(
              children: [
                IconButton(
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    icon: Icon(
                      Icons.keyboard_backspace,
                      color: Colors.white,
                    )),
                Expanded(
                  child: Text(
                    widget.expenseId == 'false' ? 'Add Expense' : 'Edit Expense',
                    style: TextStyle(color: Colors.white, fontSize: 24, fontWeight: FontWeight.bold),
                  ),
                ),
              ],
            ),
            Expanded(
              child: _isLoading
                  ? SpinKitRing(
                      lineWidth: 3.0,
                      color: AppColors.primaryColor,
                      size: 35.0,
                    )
                  : error == null
                      ? SingleChildScrollView(
                          child: Container(
                            decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(16.0), boxShadow: [
                              BoxShadow(
                                color: AppColors.primaryMaterialColor[100]!,
                                blurRadius: 5.0,
                                spreadRadius: 1.0,
                                offset: Offset(
                                  1.0, // Move to right 10  horizontally
                                  2.0, // Move to bottom 5 Vertically
                                ),
                              )
                            ]),
                            margin: EdgeInsets.symmetric(horizontal: 16),
                            padding: EdgeInsets.all(16.0),
                            child: Column(
                              children: <Widget>[
                                CustomTextFormField(
                                  controller: nameController,
                                  hinText: 'Name',
                                  boardColor: AppColors.primaryColor,
                                  onChanged: (value) {},
                                ),
                                SizedBox(
                                  height: 20,
                                ),
                                Container(
                                  padding: EdgeInsets.only(left: 16, right: 10),
                                  decoration: BoxDecoration(borderRadius: BorderRadius.circular(5), color: Colors.white, border: Border.all(color: Colors.grey, width: 1)),
                                  child: DropdownButtonHideUnderline(
                                    child: DropdownButton(
                                      hint: Text(
                                        'Select POI Type',
                                        style: TextStyle(color: Colors.grey, fontSize: 12, fontWeight: FontWeight.w300),
                                      ),
                                      isExpanded: true,
                                      items: deviceNames.values.map((e) {
                                        return new DropdownMenuItem(
                                          value: e,
                                          child: Text(
                                            e,
                                            style: TextStyle(fontSize: 15.0),
                                          ),
                                        );
                                      }).toList(),
                                      value: selectedValue,
                                      onChanged: (value) {
                                        setState(() {
                                          selectedValue = value.toString();
                                          selectedImei = deviceNames.keys.firstWhere((k) => deviceNames[k] == value, orElse: () => 'key');
                                        });
                                      },
                                      style: TextStyle(color: Colors.black54),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  height: 20,
                                ),
                                CustomTextFormField(
                                  controller: dateController,
                                  hinText: 'Date',
                                  boardColor: AppColors.primaryColor,
                                  suffixIcon: Padding(
                                    padding: EdgeInsets.only(top: 8.0, bottom: 8.0),
                                    child: InkWell(
                                      onTap: () {
                                        DateTime initialDate;
                                        try {
                                          initialDate = DateFormat('yyyy-MM-dd').parse(dateController.text);
                                        } catch (e) {
                                          initialDate = DateTime.now();
                                        }
                                        showDatePicker(context: context, initialDate: initialDate, firstDate: DateTime(2000), lastDate: DateTime(2101)).then((dateTime) {
                                          if (dateTime != null) {
                                            dateController.text = DateFormat('yyyy-MM-dd').format(dateTime);
                                          }
                                        });
                                      },
                                      child: Image.asset(
                                        'images/date-time.png',
                                        height: 15,
                                        color: Color(0xFF707070),
                                      ),
                                    ),
                                  ),
                                  onChanged: (value) {},
                                ),
                                SizedBox(
                                  height: 20,
                                ),
                                Row(
                                  children: [
                                    Expanded(
                                        child: CustomTextFormField(
                                      controller: qtyController,
                                      hinText: 'Quantity',
                                      boardColor: AppColors.primaryColor2,
                                      onChanged: (value) {},
                                    )),
                                    SizedBox(
                                      width: 10.0,
                                    ),
                                    Expanded(
                                        child: CustomTextFormField(
                                      controller: costController,
                                      hinText: 'Cost (BDT)',
                                      boardColor: AppColors.primaryColor2,
                                      onChanged: (value) {},
                                    )),
                                  ],
                                ),
                                SizedBox(height: 20),
                                Row(
                                  children: [
                                    Expanded(
                                        child: CustomTextFormField(
                                      controller: odometerController,
                                      hinText: 'Odometer (km)',
                                      boardColor: AppColors.primaryColor,
                                      onChanged: (value) {},
                                    )),
                                    SizedBox(
                                      width: 10.0,
                                    ),
                                    Expanded(
                                        child: CustomTextFormField(
                                      controller: engienHrsController,
                                      hinText: 'Engine Hours (h)',
                                      boardColor: AppColors.primaryColor,
                                      onChanged: (value) {},
                                    )),
                                  ],
                                ),
                                SizedBox(height: 20),
                                CustomTextFormField(
                                  controller: supplierController,
                                  hinText: 'Supplier',
                                  boardColor: AppColors.primaryColor2,
                                  onChanged: (value) {},
                                ),
                                SizedBox(height: 20),
                                CustomTextFormField(
                                  controller: buyerController,
                                  hinText: 'Buyer',
                                  boardColor: AppColors.primaryColor,
                                  onChanged: (value) {},
                                ),
                                SizedBox(height: 20),
                                CustomTextFormField(
                                  controller: descController,
                                  hinText: 'Description',
                                  boardColor: AppColors.primaryColor2,
                                  maxLines: 6,
                                  onChanged: (value) {},
                                ),
                                SizedBox(
                                  height: 25.0,
                                ),
                                InkWell(
                                  onTap: () {
                                    if (selectedImei == 'key') {
                                      showError('Select Device', context);
                                    } else if (nameController.text == '') {
                                      showError('Enter Expense Name', context);
                                    } else if (dateController.text == '') {
                                      showError('Enter Expense Date', context);
                                    } else if (qtyController.text == '') {
                                      showError('Enter Expense Quantity', context);
                                    } else if (costController.text == '') {
                                      showError('Enter Expense Cost', context);
                                    } else {
                                      setState(() => _isLoading = true);
                                      GPSServerRequests.saveExpense(
                                        widget.expenseId,
                                        nameController.text,
                                        selectedImei,
                                        dateController.text,
                                        qtyController.text,
                                        costController.text,
                                        supplierController.text,
                                        buyerController.text,
                                        odometerController.text,
                                        engienHrsController.text,
                                        descController.text,
                                      ).then((list) {
                                        setState(() => _isLoading = false);
                                        if (list[0] == 1) {
                                          Navigator.pop(context, true);
                                        } else {
                                          showError('saveExpense', context);
                                        }
                                      });
                                    }
                                  },
                                  child: Container(
                                    height: 48,
                                    padding: EdgeInsets.all(12.0),
                                    decoration: BoxDecoration(
                                      color: AppColors.primaryColor,
                                      borderRadius: BorderRadius.circular(24.0),
                                    ),
                                    child: Center(
                                        child: Text(
                                      'Save',
                                      style: TextStyle(color: Colors.white, fontWeight: FontWeight.w500),
                                    )),
                                  ),
                                ),
                                SizedBox(
                                  height: 10.0,
                                ),
                                InkWell(
                                  onTap: () {
                                    Navigator.pop(context);
                                  },
                                  child: Container(
                                    height: 48,
                                    padding: EdgeInsets.all(12.0),
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(24.0),
                                      border: Border.all(color: AppColors.primaryColor2, width: 1.5),
                                    ),
                                    child: Center(
                                        child: Text(
                                      'Cancel',
                                      style: TextStyle(color: AppColors.primaryColor2, fontWeight: FontWeight.w500),
                                    )),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        )
                      : Center(
                          child: Text(error!),
                        ),
            ),
          ],
        ),
      ),
    ));
  }

  showError(String error, BuildContext context) {
    showSnackBar(context, error, Colors.red);
  }
}
