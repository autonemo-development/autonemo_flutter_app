import 'package:autonemogps/bloc/home_bloc.dart';
import 'package:autonemogps/bloc/home_event.dart';
import 'package:autonemogps/bloc/home_state.dart';
import 'package:autonemogps/common/auto_recommendation_dialog.dart';
import 'package:autonemogps/common/auto_referral_dialog.dart';
import 'package:autonemogps/common/show_connection_dialog.dart';
import 'package:autonemogps/common/show_expiry_dialog.dart';
import 'package:autonemogps/config/colors.dart';
import 'package:autonemogps/home_page/tabs/list_view_page/list_view/list_view_item.dart';
import 'package:autonemogps/models/device_model.dart';
import 'package:autonemogps/models/tracking_device_model.dart';
import 'package:custom_refresh_indicator/custom_refresh_indicator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class ListViewBody extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return ListViewBodyState();
  }
}

class ListViewBodyState extends State<ListViewBody> {
  HomeBloc? homeBloc;

  bool showLoader = false;

  List imeiList = [];

  Map<String, TrackingDevice> trackingDataList = {};

  Map<String, DeviceModel> deviceDataList = {};

  @override
  void initState() {
    super.initState();
    homeBloc = BlocProvider.of<HomeBloc>(context);
    homeBloc?.add(GetInitialDataEvent());
    homeBloc?.add(StartTimerEvent());
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: BlocListener<HomeBloc, HomeState>(
      listener: (context, state) {
        if (state is NoInternetState) {
          showConnectionDialog(context).then((_) => homeBloc?.add(InitEvent()));
        } else if (state is DeviceExpiredState) {
          showExpiryDialog(context, state.expiredDeviceNames);
        } else if (state is ShowTimedDialog) {
          showAutoReferDialog(context);
          showAutoRecommendationDialog(context);
        }
      },
      child: BlocListener<HomeBloc, HomeState>(
        bloc: homeBloc,
        listener: (context, state) {
          if (state is DataFetchingState) {
            setState(() => this.showLoader = true);
          }

          if (state is DataFetchedState) {
            setState(() => this.deviceDataList = state.deviceList);
            setState(() => this.imeiList = deviceDataList.keys.toList());
            setState(() => this.trackingDataList = state.trackingList);
            setState(() => this.showLoader = false);
          }
        },
        child: Column(
          children: [
            Expanded(
              child: Container(
                margin: EdgeInsets.only(top: 56),
                child: Scrollbar(
                  controller: ScrollController(),
                  child: CustomRefreshIndicator(
                    onRefresh: () async => homeBloc?.add(RefreshDataEvent()),
                    builder: (context, child, controller) {
                      return AnimatedBuilder(
                        animation: controller,
                        builder: (BuildContext context, _) {
                          return Stack(
                            alignment: Alignment.topCenter,
                            children: <Widget>[
                              !controller.isIdle ? Positioned(top: 25.0 * controller.value, child: Column(children: [SpinKitRing(color: AppColors.primaryColor, lineWidth: 2.0, size: 20.0)])) : SizedBox(),
                              Transform.translate(offset: Offset(0, 50.0 * controller.value), child: child),
                            ],
                          );
                        },
                      );
                    },
                    child: ListView.builder(
                      padding: EdgeInsets.only(top: 0),
                      itemCount: imeiList.length + 1,
                      itemBuilder: (context, index) {
                        if (index == imeiList.length) {
                          return SizedBox(height: 80);
                        }

                        String imei = imeiList.elementAt(index);

                        TrackingDevice? trackingDevice = trackingDataList[imei];

                        DeviceModel? deviceModel = deviceDataList[imei];

                        if (trackingDevice != null && deviceModel != null) {
                          return ListViewItem(trackingDevice, deviceModel);
                        }

                        return null;
                      },
                    ),
                  ),
                ),
              ),
            ),
            if (showLoader) LinearProgressIndicator()
          ],
        ),
      ),
    ));
  }
}
