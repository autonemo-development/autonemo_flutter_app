import 'package:autonemogps/config/colors.dart';
import 'package:flutter/material.dart';

class MoreItemWidget extends StatelessWidget {
  final String imagePath;
  final String title;
  final void Function() onPressed;

  MoreItemWidget({required this.imagePath, required this.title, required this.onPressed});

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 15),
        decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(5.0), border: Border.all(color: Colors.grey[100]!)),
        child: InkResponse(
          onTap: onPressed,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Image.asset(
                imagePath,
                height: 35,
              ),
              SizedBox(
                height: 10.0,
              ),
              Text(
                title,
                maxLines: 1,
                softWrap: true,
                overflow: TextOverflow.fade,
                style: TextStyle(color: AppColors.headerTextColor, fontSize: 12, fontWeight: FontWeight.w800),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
