class Expense {
  final String cost, name, date, supplier, buyer, qty, id, objectName;

  Expense({required this.buyer, required this.cost, required this.date, required this.name, required this.qty, required this.supplier, this.id = '', this.objectName = ''});
}
