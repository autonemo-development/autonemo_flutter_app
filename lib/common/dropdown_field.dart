import 'package:autonemogps/config/colors.dart';
import 'package:flutter/material.dart';

typedef VoidReturnStringCallback=void Function(String);
// ignore: must_be_immutable
class DropDownFromField extends StatelessWidget {

  DropDownFromField({required this.list,required this.selectedValue,required this.onChanged});

  var list = [''];
  void Function(String) onChanged;
  String selectedValue;

  @override
  Widget build(BuildContext context) {
    return DropdownButtonFormField(
        validator: (value){
          if(value == null){
            return "Field can't be empty";
          }
          return null;
        },
        isExpanded: true,
        style: TextStyle(fontWeight: FontWeight.w600,color: Colors.black),
        items: list.map((String category) {
          return new DropdownMenuItem(
              value: category,
              child: Text(category, style: TextStyle(fontSize: 15.0),),
          );
        }).toList(),
        onChanged:(s)=>onChanged(s as String),
        value: selectedValue,
        decoration: InputDecoration(
          contentPadding: EdgeInsets.only(left: 14,right: 14),
          border: OutlineInputBorder(
              borderSide: BorderSide(
                  color: Colors.grey)),
          disabledBorder: OutlineInputBorder(
              borderSide: BorderSide(
                  color: Colors.grey)),
          enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(
                  color: Colors.grey)),
          focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(
                  color: AppColors.primaryColor)),
          fillColor: Colors.black,
        )
    );
  }
}