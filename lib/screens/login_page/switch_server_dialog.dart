import 'package:autonemogps/config/colors.dart';
import 'package:autonemogps/config/string_constants.dart';
import 'package:autonemogps/storage/repository.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher_string.dart';

class SwitchServerDialog extends StatefulWidget {
  const SwitchServerDialog({Key? key}) : super(key: key);

  @override
  State<SwitchServerDialog> createState() => _SwitchServerDialogState();
}

class _SwitchServerDialogState extends State<SwitchServerDialog> {
  TextEditingController _textEditingController = TextEditingController();

  late String serverType;
  bool isHttps = false;
  bool enableButton = true;
  bool isGpsServerUrl = false;
  bool isGpsWoxrUrl = false;

  @override
  void initState() {
    super.initState();
    prepareData();
  }

  prepareData() async {
    serverType = Repository.getServerType();
    String gpsWoxUrl = await Repository.getGPSWoxUrl();
    String gpsServerUrl = await Repository.getGPSServerUrl();
    String selectedUrl = '';
    if (serverType == GPSServerConstants.type) {
      isGpsServerUrl = true;
      if (gpsServerUrl != AutonemoServerConstants.BASE_URL) {
        serverType = GPSWoxConstants.type;
        selectedUrl = gpsServerUrl;
        _textEditingController.text = gpsServerUrl.replaceFirst('http://', '').replaceFirst('https://', '');
      }
    }

    if (serverType == GPSWoxConstants.type) {
      isGpsWoxrUrl = true;

      selectedUrl = gpsWoxUrl;
      _textEditingController.text = gpsWoxUrl.replaceFirst('http://', '').replaceFirst('https://', '');
    }
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      isDoneButtonEnabled().then((isEnabled) {
        setState(() {
          isHttps = selectedUrl.contains('https://');
          enableButton = isEnabled;
        });
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(12),
      child: ListView(
        shrinkWrap: true,
        children: [
          Text('Select Server'),
          RadioListTile(
              dense: true,
              title: Text('Autonemo GPS'),
              value: GPSServerConstants.type,
              groupValue: serverType,
              onChanged: (val) {
                serverType = GPSServerConstants.type;
                isDoneButtonEnabled().then((isEnabled) {
                  setState(() {
                    enableButton = isEnabled;
                  });
                });
              }),
          ExpansionPanelList(
            elevation: 0,
            expansionCallback: (index, isExpanded) {
              serverType = GPSWoxConstants.type;
              isDoneButtonEnabled().then((isEnabled) {
                setState(() {
                  enableButton = isEnabled;
                });
              });
            },
            children: [
              ExpansionPanel(
                isExpanded: serverType == GPSWoxConstants.type,
                canTapOnHeader: true,
                headerBuilder: (context, _) {
                  return IgnorePointer(
                    child: RadioListTile(
                        dense: true,
                        toggleable: true,
                        title: Text('Custom Server'),
                        groupValue: serverType,
                        value: GPSWoxConstants.type,
                        onChanged: (val) {
                          isDoneButtonEnabled().then((isEnabled) {
                            setState(() {
                              enableButton = isEnabled;
                            });
                          });
                        }),
                  );
                },
                body: Padding(
                  padding: const EdgeInsets.only(bottom: 8.0),
                  child: Column(
                    children: [
                      Row(
                        children: [
                          Expanded(
                            child: RadioListTile(
                                title: Text('http://'),
                                value: false,
                                groupValue: isHttps,
                                onChanged: (b) {
                                  setState(() {
                                    isHttps = b as bool;
                                  });
                                }),
                          ),
                          Expanded(
                            child: RadioListTile(
                                title: Text('https://'),
                                value: true,
                                groupValue: isHttps,
                                onChanged: (b) {
                                  setState(() {
                                    isHttps = b as bool;
                                  });
                                }),
                          ),
                        ],
                      ),
                      TextField(
                        onChanged: (url) {
                          isDoneButtonEnabled().then((isEnabled) {
                            setState(() {
                              enableButton = isEnabled;
                            });
                          });
                        },
                        controller: _textEditingController,
                        decoration: InputDecoration(
                          isDense: true,
                          hintText: 'example.com',
                          border: OutlineInputBorder(
                            borderSide: BorderSide(color: AppColors.primaryColor2),
                          ),
                          prefixIcon: Padding(
                            padding: const EdgeInsets.fromLTRB(16.0, 16, 2, 16),
                            child: Text(isHttps ? 'https://' : 'http://', style: TextStyle(color: AppColors.sensorInActiveColor)),
                          ),
                        ),
                      ),
                      CheckboxListTile(
                        value: isGpsServerUrl,
                        onChanged: (val) {
                          setState(() {
                            isGpsServerUrl = val ?? false;
                            isGpsWoxrUrl = false;
                          });
                        },
                        controlAffinity: ListTileControlAffinity.leading,
                        title: Text('GPS-Server'),
                      ),
                      CheckboxListTile(
                        value: isGpsWoxrUrl,
                        onChanged: (val) {
                          setState(() {
                            isGpsServerUrl = false;
                            isGpsWoxrUrl = val ?? false;
                          });
                        },
                        controlAffinity: ListTileControlAffinity.leading,
                        title: Text('GPSWox'),
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
          ElevatedButton(
            onPressed: enableButton ? onSwitchServerItemTapped : null,
            child: Text('Done'),
          )
        ],
      ),
    );
  }

  Future<bool> isDoneButtonEnabled() async {
    if (serverType == GPSWoxConstants.type) {
      String url = _textEditingController.text;
      if (url.endsWith('/')) {
        return false;
      }
      if (await canLaunchUrlString('${isHttps ? 'https://' : 'http://'}${url}')) {
        return true;
      }
      return false;
    }
    return true;
  }

  void onSwitchServerItemTapped() {
    Repository.setServerType(isGpsServerUrl ? GPSServerConstants.type : serverType);

    if (serverType == GPSServerConstants.type) {
      Repository.setGPSServerUrl(AutonemoServerConstants.BASE_URL);
    } else {
      if (isGpsServerUrl) {
        Repository.setGPSServerUrl('${isHttps ? 'https://' : 'http://'}${_textEditingController.text}');
      } else {
        Repository.setGPSWoxUrl('${isHttps ? 'https://' : 'http://'}${_textEditingController.text}');
      }
    }
    Navigator.pop(context);
  }
}
