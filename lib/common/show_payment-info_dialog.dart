import 'package:animations/animations.dart';
import 'package:autonemogps/config/colors.dart';
import 'package:flutter/material.dart';

showPaymentInfoDialog(BuildContext context) {
  showModal(
      context: context,
      builder: (context) {
        return Dialog(
          child: Container(
            padding: EdgeInsets.all(20.0),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    InkWell(
                        onTap: () {
                          Navigator.of(context).pop();
                        },
                        child: Image.asset(
                          'images/cancel.png',
                          width: 15,
                          height: 15,
                        ))
                  ],
                ),
                Image.asset(
                  'images/payment-info-cover.png',
                  width: MediaQuery.of(context).size.width * .5,
                ),
                Text.rich(TextSpan(children: [
                  TextSpan(
                      text: 'Your Device ',
                      style: TextStyle(color: Color(0xFF474747), fontSize: 13)),
                  TextSpan(
                      text: '(DXY-741L, JH05AA- 8968)',
                      style: TextStyle(
                          color: Color(0xFF474747),
                          fontSize: 14,
                          fontWeight: FontWeight.w500)),
                  TextSpan(
                      text: ' will ',
                      style: TextStyle(color: Color(0xFF474747), fontSize: 13)),
                  TextSpan(
                      text: 'expire on <DD-MM-Year>',
                      style: TextStyle(
                          color: Color(0xFFD43333),
                          fontSize: 14,
                          fontWeight: FontWeight.w500)),
                  TextSpan(
                      text:
                          '. For Auto renewal please pay monthly charges to avoid disconnection.',
                      style: TextStyle(color: Color(0xFF474747), fontSize: 13)),
                ])),
                SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    InkWell(
                      onTap: () {
                      },
                      child: Container(
                        width: 120,
                        padding: EdgeInsets.symmetric(vertical: 7),
                        decoration: BoxDecoration(
                          color: AppColors.primaryColor,
                          borderRadius: BorderRadius.circular(4.0),
                        ),
                        child: Center(
                          child: Text(
                            'Pay Now',
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 16,
                                fontWeight: FontWeight.w500),
                          ),
                        ),
                      ),
                    )
                  ],
                )
              ],
            ),
          ),
        );
      });
}
