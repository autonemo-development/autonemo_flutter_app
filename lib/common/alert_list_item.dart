// ignore: must_be_immutable
import 'package:auto_size_text/auto_size_text.dart';
import 'package:autonemogps/config/colors.dart';
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class AlertListItem extends StatelessWidget {
  AlertListItem(
      {required this.iconColor,
      required this.iconPath,
      required this.title,
      required this.subTitle,
      required this.dateTime});

  Color iconColor;
  String iconPath;
  String title;
  String subTitle;
  String dateTime;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(color: Colors.white, boxShadow: [
        BoxShadow(
          color: AppColors.shadowMaterialColor[100]!,
          blurRadius: 3.0,
          spreadRadius: 1.0,
          offset: Offset(
            0.5, // Move to right 10  horizontally
            0.5, // Move to bottom 5 Vertically
          ),
        )
      ]),
      padding: EdgeInsets.all(9),
      child: Row(
        children: [
          Container(
            height: 30,
            width: 30,
            padding: EdgeInsets.all(5),
            decoration: BoxDecoration(
              color: iconColor.withOpacity(.1),
              borderRadius: BorderRadius.circular(5),
            ),
            child: Image.asset(iconPath, color: iconColor),
          ),
          SizedBox(
            width: 10,
          ),
          Expanded(
              child: Row(
            crossAxisAlignment: CrossAxisAlignment.end,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    AutoSizeText(
                      title,
                      maxLines: 1,
                      minFontSize: 10,
                      style: TextStyle(
                          color: Color(0xFF474747),
                          fontSize: 14,
                          fontWeight: FontWeight.w500),
                    ),
                    AutoSizeText(
                      subTitle,
                      maxLines: 1,
                      minFontSize: 10,
                      style: TextStyle(
                          color: Color(0xFF474747),
                          fontSize: 13,
                          fontWeight: FontWeight.w300),
                    ),
                  ],
                ),
              ),
              Expanded(
                child: AutoSizeText(
                  dateTime,
                  maxLines: 1,
                  minFontSize: 10,
                  textAlign: TextAlign.end,
                  style: TextStyle(color: Color(0xFF7E7E7E), fontSize: 10),
                ),
              ),
            ],
          ))
        ],
      ),
    );
  }
}
