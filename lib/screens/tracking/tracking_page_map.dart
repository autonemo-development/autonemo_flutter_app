import 'dart:async';

import 'package:autonemogps/common/custom_marker.dart';
import 'package:autonemogps/config/helpers.dart';
import 'package:autonemogps/models/device_model.dart';
import 'package:autonemogps/models/tracking_device_model.dart';
import 'package:autonemogps/utils/logs.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';

class TrackingPageMap extends StatefulWidget {
  final TrackingDevice currentTrackingDevice;
  final DeviceModel deviceModel;

  TrackingPageMap(this.currentTrackingDevice, this.deviceModel, {Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return TrackingPageMapState();
  }
}

class TrackingPageMapState extends State<TrackingPageMap> with TickerProviderStateMixin {
  LatLng _currentLatLng = new LatLng(0, 0);
  LatLng _previousLatLng = new LatLng(0, 0);
  Tween<double> tween = Tween(begin: 0, end: 1);

  Completer<GoogleMapController> _controller = new Completer();

  BitmapDescriptor? _globalObjectMarker;

  Animation<double>? _animation;

  double _currentMapBearing = 0;

  MarkerId? markerId;

  MapType _mapType = MapType.normal;

  bool _focusOnVehicle = true;

  bool _isTrafficClicked = false;

  String _status = '';

  List<Marker> _markersList = [];

  StreamController<List<Marker>> _mapMarkerSC = StreamController<List<Marker>>();

  late AnimationController animationController;

  double _currentZoom = 15;

  StreamSink<List<Marker>> get _mapMarkerSink => _mapMarkerSC.sink;

  Stream<List<Marker>> get mapMarkerStream => _mapMarkerSC.stream;

  @override
  void initState() {
    super.initState();

    _currentLatLng = LatLng(widget.currentTrackingDevice.lat ?? 0, widget.currentTrackingDevice.lng ?? 0);
    _previousLatLng = LatLng(widget.currentTrackingDevice.lat ?? 0, widget.currentTrackingDevice.lng ?? 0);

    markerId = MarkerId(widget.deviceModel.imei);

    animationController = AnimationController(duration: Duration(milliseconds: 10000), vsync: this);
    _animation = tween.animate(animationController);
    _animation?.addListener(onAnimationAddListener);
    _animation?.addStatusListener(onAnimationStatusChangedListener);

    updateMap();
  }

  @override
  void didUpdateWidget(covariant TrackingPageMap oldWidget) {
    updateMap();
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      margin: EdgeInsets.only(bottom: 200),
      child: StreamBuilder<List<Marker>>(
        stream: mapMarkerStream,
        builder: (context, snapshot) {
          return GoogleMap(
            mapType: _mapType,
            trafficEnabled: _isTrafficClicked,
            initialCameraPosition: CameraPosition(target: _currentLatLng, zoom: _currentZoom),
            onCameraMove: (cameraPosition) {
              _currentMapBearing = cameraPosition.bearing;
              _currentZoom = cameraPosition.zoom;
            },
            onMapCreated: (GoogleMapController controller) => _controller.complete(controller),
            markers: Set<Marker>.of(snapshot.data ?? []),
            zoomControlsEnabled: false,
            compassEnabled: false,
            mapToolbarEnabled: false,
            myLocationEnabled: true,
            myLocationButtonEnabled: false,
            tiltGesturesEnabled: false,
          );
        },
      ),
    );
  }

  changeMapType(MapType mapType) => setState(() => _mapType = mapType);

  changeMapTraffic(bool isTrafficClicked) => setState(() => _isTrafficClicked = isTrafficClicked);

  focusOnVehicle() {
    _focusOnVehicle = true;
    _controller.future.then((value) {
      value.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(bearing: _currentMapBearing, target: _currentLatLng, zoom: 16)));
    });
  }

  showCurrentLocation(LocationData currentLocation) {
    _focusOnVehicle = false;
    _controller.future.then((value) {
      value.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(bearing: _currentMapBearing, target: LatLng(currentLocation.latitude ?? 0, currentLocation.longitude ?? 0), zoom: 17.0)));
    });
  }

  Future<void> updateMap() async {
    _currentLatLng = LatLng(widget.currentTrackingDevice.lat ?? 0, widget.currentTrackingDevice.lng ?? 0);
    await getCustomMarker();
    _markersList.clear();
    animationController.forward();
  }

  getCustomMarker() async {
    String currentStatus = widget.currentTrackingDevice.status ?? 'm';
    if (currentStatus != this._status) {
      Map<String, dynamic> markerData = getVehicleMarkerData(widget.currentTrackingDevice.status ?? 'm', widget.deviceModel.icon);
      String vehicleIcon = markerData['iconKey'];
      this._globalObjectMarker = await getMarkerFromImagePath(vehicleIcon);
      this._status = currentStatus;
    }
  }

  void onAnimationAddListener() {
    final double animationValue = _animation?.value ?? 0;

    // writeDebugLogs('onAnimationAddListener ' + animationValue.toString());

    double newLat = (_currentLatLng.latitude - _previousLatLng.latitude) * animationValue + _previousLatLng.latitude;
    double lngDelta = _currentLatLng.longitude - _previousLatLng.longitude;

    if (lngDelta.abs() > 180) {
      lngDelta -= (lngDelta.sign) * 360;
    }

    double newLng = lngDelta * animationValue + _previousLatLng.longitude;

    LatLng newLatLng = new LatLng(newLat, newLng);

    if (_globalObjectMarker == null) {
      return;
    }

    Marker _marker = Marker(
      rotation: widget.currentTrackingDevice.angle?.toDouble() ?? 0,
      anchor: Offset(0.5, 0.5),
      markerId: markerId ?? new MarkerId(widget.currentTrackingDevice.imei ?? ''),
      position: newLatLng,
      icon: _globalObjectMarker ?? BitmapDescriptor.defaultMarker,
    );

    if (_focusOnVehicle) {
      CameraUpdate _cameraUpdate = CameraUpdate.newLatLngZoom(_currentLatLng, _currentZoom);
      _controller.future.then((value) {
        value.animateCamera(_cameraUpdate);
      });
    }

    _markersList.add(_marker);
    _mapMarkerSink.add(_markersList);
  }

  onAnimationStatusChangedListener(AnimationStatus status) {
    writeDebugLogs('onAnimationStatusChangedListener ' + status.toString());

    if (status == AnimationStatus.completed) {
      _previousLatLng = _currentLatLng;
      animationController.value = 0;
    }
  }
}
