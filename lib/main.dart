import 'dart:async';

import 'package:autonemogps/bloc/home_bloc.dart';
import 'package:autonemogps/common/bloc/window_bloc.dart';
import 'package:autonemogps/config/colors.dart';
import 'package:autonemogps/config/size_config.dart';
import 'package:autonemogps/home_page/home_page.dart';
import 'package:autonemogps/home_page/tabs/dashboard/widget_settings.dart';
import 'package:autonemogps/screens/change_password_page.dart';
import 'package:autonemogps/screens/expense_page/expense_page.dart';
import 'package:autonemogps/screens/login_page/login_page.dart';
import 'package:autonemogps/screens/maintenance_page/add_maintenance_page.dart';
import 'package:autonemogps/screens/maintenance_page/maintenance_page.dart';
import 'package:autonemogps/screens/notification_settings_page.dart';
import 'package:autonemogps/screens/privacy_policy_page.dart';
import 'package:autonemogps/screens/reports_page.dart';
import 'package:autonemogps/screens/terms_of_use_page.dart';
import 'package:autonemogps/storage/repository.dart';
import 'package:autonemogps/utils/push_notification_service.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:home_widget/home_widget.dart';
import 'package:overlay_support/overlay_support.dart';

main() async {
  await runZonedGuarded(() async {
    WidgetsFlutterBinding.ensureInitialized();
    await Firebase.initializeApp();
    await FirebaseCrashlytics.instance.setCrashlyticsCollectionEnabled(true);
    await FirebaseMessaging.instance.setForegroundNotificationPresentationOptions(alert: true, badge: true, sound: true);

    await Repository.createReference();

    FlutterError.onError = FirebaseCrashlytics.instance.recordFlutterError;

    HomeWidget.registerBackgroundCallback(_homeWidgetBackgroundCallback);

    runApp(OverlaySupport.global(child: MyApp()));
  }, (error, stackTrace) {
    FirebaseCrashlytics.instance.recordError(error, stackTrace);
  });
}

_homeWidgetBackgroundCallback(Uri? uri) {
  if (uri!.host == 'update') {}
}

final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();
final RouteObserver<PageRoute> routeObserver = RouteObserver<PageRoute>();

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  void initState() {
    super.initState();
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
    PushNotificationService.initFirebase(navigatorKey);
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(statusBarColor: AppColors.primaryColor, statusBarBrightness: Brightness.light, statusBarIconBrightness: Brightness.light));

    return MultiBlocProvider(
      providers: [BlocProvider<HomeBloc>(create: (context) => HomeBloc()), BlocProvider<WindowBloc>(create: (context) => WindowBloc())],
      child: LayoutBuilder(
        builder: (context, constraints) {
          return OrientationBuilder(
            builder: (context, orientation) {
              SizeConfig().init(constraints, orientation);
              return MaterialApp(
                navigatorKey: navigatorKey,
                debugShowCheckedModeBanner: false,
                theme: ThemeData(primarySwatch: AppColors.primaryMaterialColor, fontFamily: 'Poppins', appBarTheme: AppBarTheme(systemOverlayStyle: SystemUiOverlayStyle.dark)),
                home: Repository.isLoggedIn() ? HomePage() : LoginPage(),
                navigatorObservers: [routeObserver],
                routes: {
                  LoginPage.page: (context) => LoginPage(),
                  HomePage.page: (context) => HomePage(),
                  ExpensePage.page: (context) => ExpensePage(),
                  ChangePassword.page: (context) => ChangePassword(),
                  MaintenancePage.page: (context) => MaintenancePage(),
                  AddMaintenancePage.page: (context) => AddMaintenancePage(),
                  ReportsPage.page: (context) => ReportsPage(),
                  TermsOfUsePage.page: (context) => TermsOfUsePage(),
                  PrivacyPolicyPage.page: (context) => PrivacyPolicyPage(),
                  NotificationSettingsPage.pageName: (context) => NotificationSettingsPage(),
                  DashboardWidgetSettings.pageName: (context) => DashboardWidgetSettings(),
                },
              );
            },
          );
        },
      ),
    );
  }
}
