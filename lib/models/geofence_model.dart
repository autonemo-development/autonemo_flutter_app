import 'dart:convert' as JSON;

import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class Geofence {
  String? id;
  String? name;
  bool? active;
  List<LatLng>? points;
  Color? color;
  LatLngBounds? bounds;

  Geofence(this.id, this.name, this.points, this.active, this.color, this.bounds);

  factory Geofence.fromGlideJson(Map<String, dynamic> json) {
    double? x0, x1, y0, y1;
    List<LatLng> points = List<LatLng>.empty(growable: true);
    (json['coordinates'] as List).forEach((element) {
      Map<String, dynamic> point = element as Map<String, dynamic>;
      double lat = double.parse(point['lat'] as String);
      double lng = double.parse(point['lng'] as String);
      points.add(LatLng(lat, lng));

      if (x0 == null) {
        x0 = x1 = lat;
        y0 = y1 = lng;
      } else {
        if (lat > x1!) x1 = lat;
        if (lat < x0!) x0 = lat;
        if (lng > y1!) y1 = lng;
        if (lng < y0!) y0 = lng;
      }
    });
    Color zoneColor = Color(int.parse('50' + (json['color'] as String).replaceAll('#', ''), radix: 16));
    return Geofence(
        json['_id'], json['name'] as String, points, json['active'] == 1, zoneColor, LatLngBounds(northeast: LatLng(x1 ?? 0, y1 ?? 0), southwest: LatLng(x0 ?? 0, y0 ?? 0)));
  }

  factory Geofence.fromGPSServerJson(Map<String, dynamic> json, String id) {
    List<String> vertices = (json['vertices'] as String).split(',');

    double? x0, x1, y0, y1;
    List<LatLng> points = List<LatLng>.empty(growable: true);

    for (int i = 0; i < vertices.length && vertices.length % 2 == 0; i = i + 2) {
      double lat = double.parse(vertices[i]);
      double lng = double.parse(vertices[i + 1]);
      points.add(LatLng(lat, lng));

      if (x0 == null) {
        x0 = x1 = lat;
        y0 = y1 = lng;
      } else {
        if (lat > x1!) x1 = lat;
        if (lat < x0) x0 = lat;
        if (lng > y1!) y1 = lng;
        if (lng < y0!) y0 = lng;
      }
    }
    return Geofence(id, json['name'], points, json['active'] == 1, Color(int.parse('50' + (json['color'] as String).replaceAll('#', ''), radix: 16)),
        LatLngBounds(northeast: LatLng(x1 == null ? 0 : x1, y1 == null ? 0 : y1), southwest: LatLng(x0 == null ? 0 : x0, y0 == null ? 0 : y0)));
  }

  factory Geofence.fromGPSWoxJson(Map<String, dynamic> json) {
    List<LatLng> points = List<LatLng>.empty(growable: true);
    List coordinates = json['coordinates'].runtimeType == String ? JSON.jsonDecode(json['coordinates']) as List : json['coordinates'] as List;
    double? x0, x1, y0, y1;

    coordinates.forEach((element) {
      double lat = element['lat'].runtimeType == int ? (element['lat'] as int).toDouble() : element['lat'] as double;
      double lng = element['lng'].runtimeType == int ? (element['lng'] as int).toDouble() : element['lng'] as double;
      points.add(LatLng(lat, lng));
      if (x0 == null) {
        x0 = x1 = lat;
        y0 = y1 = lng;
      } else {
        if (lat > x1!) x1 = lat;
        if (lat < x0!) x0 = lat;
        if (lng > y1!) y1 = lng;
        if (lng < y0!) y0 = lng;
      }
    });

    return Geofence(
        (json['id'] as int).toString(),
        json['name'] as String,
        points,
        json['active'] == 1,
        Color(int.parse('50' + (json['polygon_color'] as String).replaceAll('#', ''), radix: 16)),
        LatLngBounds(northeast: LatLng(x1 ?? 0, y1 ?? 0), southwest: LatLng(x0 ?? 0, y0 ?? 0)));
  }
}
