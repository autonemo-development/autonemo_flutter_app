import 'dart:convert';

import 'package:autonemogps/api_calls/gps_server_requests.dart';
import 'package:autonemogps/api_calls/gps_wox_requests.dart';
import 'package:autonemogps/config/colors.dart';
import 'package:autonemogps/config/helpers.dart';
import 'package:autonemogps/config/string_constants.dart';
import 'package:autonemogps/models/alert_setting.dart';
import 'package:autonemogps/screens/add_alert_page.dart';
import 'package:autonemogps/storage/repository.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class NotificationSettingsPage extends StatefulWidget {
  static String pageName = 'notification';

  @override
  _NotificationSettingsPageState createState() => _NotificationSettingsPageState();
}

class _NotificationSettingsPageState extends State<NotificationSettingsPage> {
  bool _isLoading = true;
  List<AlertSetting> alertList = [];

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      fetchData();
    });
  }

  void fetchData() {
    if (Repository.getServerType() == GPSServerConstants.type) {
      GPSServerRequests.fetchEventStatus().then((returnedList) {
        if (returnedList.elementAt(0) == true) {
          setState(() {
            _isLoading = false;
            alertList = returnedList.elementAt(1);
          });
        } else {
          showSnackBar(context, returnedList.elementAt(1), Colors.red);
        }
      });
    } else if (Repository.getServerType() == GPSWoxConstants.type) {
      GPSWoxRequests.fetchAlerts().then((value) {
        Map<String, dynamic> alertObject = json.decode(value);
        List allAlertList = alertObject['items']['alerts'] as List;
        allAlertList.forEach((element) {
          alertList.add(AlertSetting.fromGPSWoxData(element));
        });
        setState(() {
          _isLoading = false;
        });
      }).catchError((e) {
        setState(() {
          _isLoading = false;
        });
        showSnackBar(context, e, Colors.red);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: false,
        backgroundColor: Colors.white,
        elevation: 0.2,
        title: Text(
          'Notification Settings',
          style: TextStyle(color: Colors.black, fontWeight: FontWeight.w600, fontSize: 18),
        ),
        iconTheme: IconThemeData(
          color: AppColors.primaryColor2, //change your color here
        ),
        actions: [
          if (Repository.getServerType() == GPSServerConstants.type)
            IconButton(
                onPressed: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context) => AddAlertScreen())).then((value) {
                    if (value != null) {
                      setState(() {
                        _isLoading = true;
                      });
                      GPSServerRequests.fetchEventStatus().then((returnedList) {
                        if (returnedList.elementAt(0) == true) {
                          setState(() {
                            _isLoading = false;
                            alertList = returnedList.elementAt(1);
                          });
                        } else {
                          showSnackBar(context, returnedList.elementAt(1), Colors.red);
                        }
                      });
                    }
                  });
                },
                icon: Icon(Icons.add))
        ],
        systemOverlayStyle: SystemUiOverlayStyle.dark,
      ),
      // backgroundColor: AppColors.defaultBackground,
      body: _isLoading
          ? Center(
              child: SpinKitRing(
                lineWidth: 3.0,
                color: AppColors.primaryColor,
                size: 35.0,
              ),
            )
          : ListView(
              children: [
                SizedBox(height: 10),
                for (AlertSetting alertSetting in alertList)
                  NotificationItem(
                    title: alertSetting.alertName,
                    switchControl: alertSetting.alertStatus,
                    onItemTap: () {
                      if (Repository.getServerType() == GPSServerConstants.type && (alertSetting.type == 'overspeed' || alertSetting.type == 'zone_in' || alertSetting.type == 'zone_out')) {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => AddAlertScreen(
                                      alertMap: alertSetting.map,
                                      eventId: alertSetting.alertId,
                                    ))).then((value) {
                          if (value != null) {
                            setState(() {
                              _isLoading = true;
                            });
                            GPSServerRequests.fetchEventStatus().then((returnedList) {
                              if (returnedList.elementAt(0) == true) {
                                setState(() {
                                  _isLoading = false;
                                  alertList = returnedList.elementAt(1);
                                });
                              } else {
                                showSnackBar(context, returnedList.elementAt(1), Colors.red);
                              }
                            });
                          }
                        });
                      }
                    },
                    onSwitchTap: (value) {
                      onSwitchTapped(alertSetting);
                    },
                  ),
              ],
            ),
    );
  }

  void onSwitchTapped(AlertSetting alertSetting) async {
    if (Repository.getServerType() == GPSServerConstants.type) {
      setState(() {
        _isLoading = true;
      });

      GPSServerRequests.updateEventStatus({...alertSetting.map, 'id': alertSetting.alertId, 'active': (!alertSetting.alertStatus).toString()}).then((value) {
        setState(() {
          alertSetting.alertStatus = !alertSetting.alertStatus;
          _isLoading = false;
        });
      }).onError((error, stackTrace) {
        setState(() {
          _isLoading = false;
        });
        showSnackBar(context, error.toString(), Colors.red);
      });
    } else if (Repository.getServerType() == GPSWoxConstants.type) {
      setState(() {
        _isLoading = true;
      });
      GPSWoxRequests.changeAlertActive(alertSetting.alertId, !alertSetting.alertStatus).then((value) {
        setState(() {
          alertSetting.alertStatus = !alertSetting.alertStatus;
          _isLoading = false;
        });
      }).onError((error, stackTrace) {
        setState(() {
          _isLoading = false;
        });
        showSnackBar(context, error.toString(), Colors.red);
      });
    }
  }
}

class NotificationItem extends StatelessWidget {
  final String title;
  final bool switchControl;
  final void Function() onItemTap;
  final void Function(bool) onSwitchTap;

  NotificationItem({required this.title, required this.onSwitchTap, required this.onItemTap, required this.switchControl});

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.all(4),
        padding: EdgeInsets.symmetric(horizontal: 16),
        width: MediaQuery.of(context).size.width / 2.27,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(5.0),
          /*boxShadow: [BoxShadow(color: Colors.black.withOpacity(.13),blurRadius: 2.0)]*/
        ),
        child: InkWell(
          onTap: onItemTap,
          child: Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                title,
                style: TextStyle(color: Color(0xFF3A3A3A), fontSize: 15, fontWeight: FontWeight.w500),
              ),
              Switch(
                onChanged: onSwitchTap,
                value: switchControl,
                activeColor: AppColors.primaryColor,
                activeTrackColor: Color(0xFFB7E4FF),
                inactiveThumbColor: AppColors.primaryColor2,
                inactiveTrackColor: Color(0xFFD6D6D6),
              )
            ],
          ),
        ));
  }
}
