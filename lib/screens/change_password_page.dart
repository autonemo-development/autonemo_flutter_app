import 'package:autonemogps/api_calls/gps_server_requests.dart';
import 'package:autonemogps/api_calls/gps_wox_requests.dart';
import 'package:autonemogps/common/custom_text_form_field.dart';
import 'package:autonemogps/config/colors.dart';
import 'package:autonemogps/config/helpers.dart';
import 'package:autonemogps/config/string_constants.dart';
import 'package:autonemogps/storage/repository.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class ChangePassword extends StatefulWidget {
  static const String page = 'change_password_page';
  final String? currentPassword, cookie;

  ChangePassword({this.currentPassword, this.cookie});

  @override
  _ChangePasswordState createState() => _ChangePasswordState();
}

class _ChangePasswordState extends State<ChangePassword> {
  bool isPswdChanging = false;
  TextEditingController _currentPswdController = TextEditingController();
  TextEditingController _newPswdController = TextEditingController();
  TextEditingController _newPswd2Controller = TextEditingController();
  late String currentPassword;

  @override
  void initState() {
    super.initState();
    currentPassword = widget.currentPassword ?? Repository.getPswd();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: widget.cookie == null
          ? AppBar(
              centerTitle: false,
              automaticallyImplyLeading: false,
              leading: IconButton(
                icon: Icon(Icons.arrow_back_outlined),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
              title: Text(
                'Change Password',
                style: TextStyle(color: Colors.white, fontWeight: FontWeight.w500, fontSize: 20),
              ),
              backgroundColor: AppColors.primaryColor,
              iconTheme: IconThemeData(
                color: Colors.white, //change your color here
              ),
              systemOverlayStyle: SystemUiOverlayStyle.light,
            )
          : null,
      body: WillPopScope(
        onWillPop: () async {
          if (widget.cookie != null) {
            return true;
          }
          return true;
        },
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.all(15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                Padding(
                  padding: const EdgeInsets.all(30.0),
                  child: Image.asset(
                    'images/change_password-cover.png',
                    height: widget.cookie != null ? 150 : null,
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                isPswdChanging
                    ? Center(
                        child: SpinKitRing(
                          lineWidth: 3.0,
                          color: AppColors.primaryColor,
                          size: 30.0,
                        ),
                      )
                    : Form(
                        child: Column(
                          children: [
                            CustomTextFormField(
                              controller: _currentPswdController,
                              hinText: 'Current Password',
                              boardColor: Color(0xFF0060A4),
                              onChanged: (value) {},
                            ),
                            SizedBox(
                              height: 15,
                            ),
                            CustomTextFormField(
                              controller: _newPswdController,
                              hinText: 'New Password',
                              boardColor: Color(0xFFF26611),
                              onChanged: (value) {},
                            ),
                            SizedBox(
                              height: 15,
                            ),
                            CustomTextFormField(
                              controller: _newPswd2Controller,
                              hinText: 'Retype New Password',
                              boardColor: Color(0xFF0060A4),
                              onChanged: (value) {},
                            ),
                          ],
                        ),
                      ),
                SizedBox(
                  height: 50.0,
                ),
                Row(
                  children: [
                    if (widget.cookie == null)
                      Expanded(
                        child: InkWell(
                          onTap: () async {
                            Navigator.pop(context);
                          },
                          child: Container(
                            padding: EdgeInsets.all(12.0),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(4.0),
                              border: Border.all(color: Color(0xFF7E7E7E), width: 1.5),
                            ),
                            child: Center(
                                child: Text(
                              'Cancel',
                              style: TextStyle(color: Color(0xFF7E7E7E), fontWeight: FontWeight.w600),
                            )),
                          ),
                        ),
                      ),
                    SizedBox(
                      width: 10.0,
                    ),
                    Expanded(
                      child: InkWell(
                        onTap: () async {
                          // Navigator.pop(context);
                          if (_currentPswdController.text == '') {
                            showSnackBar(context, 'Please Enter Current Password', Colors.red);
                          } else if (_newPswdController.text == '') {
                            showSnackBar(context, 'Please Enter New Password', Colors.red);
                          } else if (_newPswd2Controller.text != _newPswdController.text) {
                            showSnackBar(context, 'New Passwords not matched.', Colors.red);
                          } else if (await currentPassword != _currentPswdController.text) {
                            showSnackBar(context, 'Incorrect Password Entered.', Colors.red);
                          } else {
                            setState(() {
                              isPswdChanging = true;
                            });
                            if (Repository.getServerType() == GPSWoxConstants.type) {
                              GPSWoxRequests.changePswd(_currentPswdController.text, _newPswdController.text, cookie: widget.cookie).then((isSuccess) async {
                                Repository.setPswd(_newPswdController.text);
                                Repository.setRememberedCredentials(password: _newPswdController.text);
                                showSnackBar(context, 'Password Updated Successfully', Colors.green);
                                Map<String, dynamic> loginStatus = await GPSWoxRequests.login(Repository.getUsername(), _newPswdController.text);
                                setState(() {
                                  isPswdChanging = false;
                                });
                                if (loginStatus['isLoginSuccess'] as bool) {
                                  Navigator.pop(context, [true, loginStatus['cookie']]);
                                } else {
                                  setState(() {
                                    isPswdChanging = false;
                                  });
                                  showSnackBar(context, loginStatus['message'], Colors.red);
                                }
                              }).catchError((e) {
                                setState(() {
                                  isPswdChanging = false;
                                });
                                showSnackBar(context, e, Colors.red);
                              });
                            } else {
                              GPSServerRequests.changePswd(_currentPswdController.text, _newPswdController.text, cookie: widget.cookie).then((returnedList) {
                                if (returnedList[0] == 1) {
                                  Repository.setPswd(_newPswdController.text);
                                  Repository.setRememberedCredentials(password: _newPswdController.text);
                                  showSnackBar(context, 'Password Updated Successfully', Colors.green);
                                  Navigator.pop(context, [true, widget.cookie]);
                                } else {
                                  showSnackBar(context, returnedList[1], Colors.red);
                                }
                                setState(() {
                                  isPswdChanging = false;
                                });
                              });
                            }
                          }
                        },
                        child: Container(
                          padding: EdgeInsets.all(12.0),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(4.0),
                            border: Border.all(color: AppColors.primaryColor, width: 1.5),
                          ),
                          child: Center(
                              child: Text(
                            'Save',
                            style: TextStyle(color: AppColors.primaryColor, fontWeight: FontWeight.w600),
                          )),
                        ),
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
