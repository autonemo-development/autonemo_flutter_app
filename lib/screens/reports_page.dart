import 'dart:convert';
import 'dart:io';

import 'package:autonemogps/api_calls/gps_server_requests.dart';
import 'package:autonemogps/api_calls/gps_wox_requests.dart';
import 'package:autonemogps/config/colors.dart';
import 'package:autonemogps/config/helpers.dart';
import 'package:autonemogps/config/string_constants.dart';
import 'package:autonemogps/models/device.dart';
import 'package:autonemogps/storage/repository.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:lecle_downloads_path_provider/lecle_downloads_path_provider.dart';
import 'package:open_filex/open_filex.dart';
import 'package:path_provider/path_provider.dart';

class ReportsPage extends StatefulWidget {
  static const String page = 'reports_page';

  @override
  _ReportsPageState createState() => _ReportsPageState();
}

class _ReportsPageState extends State<ReportsPage> {
  int selectedReportIndex = 0;
  Map<String, dynamic>? selectedReportMap;
  String selectedStops = '> 1 min';
  String selectedFilter = 'Today';

  String dateRange = 'Date Range';
  String timeRange = 'Time Range';
  String dtf = '', dtt = '';
  bool _isLoading = true;
  bool isLoadingReport = false;
  bool isAllDevicesSelected = true;
  String? error;
  List<Map<String, dynamic>> reportTypeMaps = List.empty(growable: true);

  List<Map<String, String>> reportNameForGPSServer = [
    {
      'name': 'General Information',
      'key': 'general',
      'items':
          'route_start,route_end,route_length,move_duration,stop_duration,stop_count,top_speed,avg_speed,overspeed_count,fuel_consumption,fuel_cost,engine_work,engine_idle,odometer,engine_hours,driver,trailer'
    },
    {
      'name': 'General Information (merged)',
      'key': 'general_merged',
      'items':
          'route_start,route_end,route_length,move_duration,stop_duration,stop_count,top_speed,avg_speed,overspeed_count,fuel_consumption,fuel_cost,engine_work,engine_idle,odometer,engine_hours,driver,trailer,total'
    },
    {'name': 'Object Information', 'key': 'object_info', 'items': 'imei,transport_model,vin,plate_number,odometer,engine_hours,driver,trailer,gps_device,sim_card_number'},
    {'name': 'Current Position', 'key': 'current_position', 'items': 'time,position,speed,altitude,angle,status,odometer,engine_hours'},
    {'name': 'Current Position (Offline)', 'key': 'current_position_off', 'items': 'time,position,speed,altitude,angle,status,odometer,engine_hours'},
    {'name': 'Route Data With Sensors', 'key': 'route_data_sensors', 'items': 'time,position,speed,altitude,angle'},
    {'name': 'Drives and Stops', 'key': 'drives_stops', 'items': 'status,start,end,duration,move_duration,stop_duration,route_length,top_speed,avg_speed,fuel_consumption,fuel_cost,engine_work,engine_idle'},
    {'name': 'Travel Sheet', 'key': 'travel_sheet', 'items': 'time_a,position_a,time_b,position_b,duration,route_length,fuel_consumption,fuel_cost, total'},
    {
      'name': 'Travel sheet (day/night)',
      'key': 'travel_sheet_dn',
      'items': 'time_a,position_a,odometer_a,time_b,position_b,odometer_b,duration,route_length,fuel_consumption,avg_fuel_consumption,fuel_cost,driver,trailer,total'
    },
    {'name': 'Mileage (daily)', 'key': 'mileage_daily', 'items': 'time,start,end,route_length,fuel_consumption,avg_fuel_consumption,fuel_cost,engine_hours,total,driver,trailer'},
    {'name': 'Overspeeds', 'key': 'overspeed', 'items': 'start,end,duration,top_speed,avg_speed,overspeed_position'},
    {'name': 'Underspeeds', 'key': 'underspeed', 'items': 'start,end,duration,top_speed,avg_speed,underspeed_position'},
    {'name': 'Zone in/out', 'key': 'zone_in_out', 'items': 'zone_in,zone_out,duration,zone_name,zone_position'},
    {'name': 'Events', 'key': 'events', 'items': 'time,event,event_position,total'},
    {'name': 'Service', 'key': 'service', 'items': 'service,last_service,status'},
    {'name': 'Fuel Fillings', 'key': 'fuelfillings', 'items': 'time,position,before,after,filled,sensor,driver,total'},
    {'name': 'Fuel Thefts', 'key': 'fuelthefts', 'items': 'time,position,before,after,stolen,sensor,driver,total'},
    {'name': 'Logic Sensors', 'key': 'logic_sensors', 'items': 'sensor,activation_time,deactivation_time,duration,activation_position,deactivation_position'},
    {'name': 'Driver behavior (RAG by driver)', 'key': 'rag', 'items': 'overspeed_score,harsh_acceleration_score,harsh_braking_score,harsh_cornering_score'},
    {'name': 'Driver behavior (RAG by Object)', 'key': 'rag_driver', 'items': 'overspeed_score,harsh_acceleration_score,harsh_braking_score,harsh_cornering_score'},
    {'name': 'Tasks', 'key': 'tasks', 'items': 'name,from,start_time,to,end_time,priority,status'},
    {'name': 'RFID and iButton logbook', 'key': 'rilogbook', 'items': 'group,name,position'},
    {'name': 'DTC (Diagnostic Trouble Codes)', 'key': 'dtc', 'items': 'code,position'},
    {
      'name': 'Speed Graph',
      'key': 'speed_graph',
      'items':
          'route_start,route_end,route_length,move_duration,stop_duration,stop_count,top_speed,avg_speed,overspeed_count,fuel_consumption,fuel_cost,engine_work,engine_idle,odometer,engine_hours,driver,trailer'
    },
    {'name': 'Altitude Graph', 'key': 'altitude_graph', 'items': ''},
    {'name': 'Ignition (ACC)', 'key': 'acc_graph', 'items': ''},
    {'name': 'Fuel Level Graph', 'key': 'fuellevel_graph', 'items': ''},
    {'name': 'Temperature Graph', 'key': 'temperature_graph', 'items': ''},
    {'name': 'Sensor Graph', 'key': 'sensor_graph', 'items': ''},
    {
      'name': 'Routes',
      'key': 'routes',
      'items':
          'route_start,route_end,route_length,move_duration,stop_duration,stop_count,top_speed,avg_speed,overspeed_count,fuel_consumption,fuel_cost,engine_work,engine_idle,odometer,engine_hours,driver,trailer'
    },
    {
      'name': 'Routes with stops',
      'key': 'routes_stops',
      'items':
          'route_start,route_end,route_length,move_duration,stop_duration,stop_count,top_speed,avg_speed,overspeed_count,fuel_consumption,fuel_cost,engine_work,engine_idle,odometer,engine_hours,driver,trailer'
    },
    {'name': 'Image Gallery', 'key': 'image_gallery', 'items': 'time,position'},
  ];

  List<String> selectedImeis = List.empty(growable: true);

  Map<String, String> vehicleNames = {};
  List<String> stopsArray = ['> 1 min', '> 2 min', '> 5 min', '> 10 min', '> 20 min', '> 30 min', '> 1 hr', '> 2 hr', '> 5 hr'];

  List<String> filterArray = ['Today', 'Yesterday', 'Before 2 days', 'Before 3 days', 'This week', 'Last week', 'This month', 'Last month'];

  late DateTime now;
  DateFormat zeoTimeDateFormat = DateFormat('yyyy-MM-dd 00:00:00');

  bool showCoordinates = false;
  bool showAddresses = false;
  bool showZones = false;

  @override
  initState() {
    super.initState();
    if (Repository.getServerType() == GPSServerConstants.type) {
      _isLoading = false;
      selectedReportMap = reportNameForGPSServer[0];
    } else if (Repository.getServerType() == GPSWoxConstants.type) {
      GPSWoxRequests.fetchReportTypes().then((list) {
        if (list[0] == 1) {
          setState(() {
            _isLoading = false;
          });
          Map<String, dynamic> map = json.decode(list[1]);
          List items = map['items'] as List;
          items.forEach((element) {
            reportTypeMaps.add(element);
          });
          selectedReportMap = reportTypeMaps[0];
        } else {
          setState(() {
            error = list[1];
            _isLoading = false;
          });
        }
      });
    }

    AllDeviceModelData().deviceModelAndImeiMapGpsServer.forEach((key, value) {
      vehicleNames.putIfAbsent(key, () => value.name);
      selectedImeis.add(key);
    });
    now = new DateTime.now();
    dtf = zeoTimeDateFormat.format(now);
    dtt = zeoTimeDateFormat.format(now.add(Duration(days: 1)));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Reports'),
        backgroundColor: Colors.white,
        foregroundColor: Colors.black,
      ),
      body: SafeArea(
        child: Container(
          height: MediaQuery.of(context).size.height,
          decoration: BoxDecoration(
            gradient: new LinearGradient(begin: Alignment.topCenter, end: Alignment.bottomCenter, colors: [
              Color(0x90DC0E18),
              Color(0x10DC0E18),
            ]),
          ),
          child: Column(
            children: [
              _isLoading
                  ? SizedBox(
                      height: 40,
                      width: 40,
                      child: CircularProgressIndicator(
                        strokeWidth: 2,
                      ))
                  : Expanded(
                      child: Center(
                        child: SingleChildScrollView(
                          child: Container(
                            decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(16.0), boxShadow: [
                              BoxShadow(
                                color: AppColors.primaryMaterialColor[100]!,
                                blurRadius: 5.0,
                                spreadRadius: 1.0,
                                offset: Offset(
                                  1.0, // Move to right 10  horizontally
                                  2.0, // Move to bottom 5 Vertically
                                ),
                              )
                            ]),
                            margin: EdgeInsets.symmetric(horizontal: 24),
                            padding: EdgeInsets.all(15),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                SizedBox(
                                  height: 10,
                                ),
                                Text(
                                  'Report Type',
                                  style: TextStyle(color: AppColors.sensorInActiveColor, fontWeight: FontWeight.w500, fontSize: 12),
                                ),
                                SizedBox(
                                  height: 2,
                                ),
                                DropdownSearch<Map<String, dynamic>>(
                                    popupProps: PopupPropsMultiSelection.dialog(
                                        itemBuilder: (context, item, isEnabled) {
                                          return Padding(
                                            padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                                            child: Text(item['name'] as String),
                                          );
                                        },
                                        showSearchBox: true,
                                        searchFieldProps: TextFieldProps(
                                          decoration: InputDecoration(
                                            hintText: 'Report Type',
                                            contentPadding: EdgeInsets.only(left: 16, right: 0, top: 2, bottom: 2),
                                            border: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey[200]!)),
                                            disabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey[200]!)),
                                            enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey[200]!)),
                                            focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey[200]!)),
                                          ),
                                        )),
                                    items: Repository.getServerType() == GPSServerConstants.type ? reportNameForGPSServer : reportTypeMaps,
                                    onChanged: (s) {
                                      setState(() {
                                        selectedReportMap = s;
                                      });
                                    },
                                    dropdownBuilder: (context, item) {
                                      return Text(item!['name'] as String);
                                    },
                                    selectedItem: (Repository.getServerType() == GPSServerConstants.type ? reportNameForGPSServer : reportTypeMaps)[0]),
                                SizedBox(
                                  height: 10,
                                ),
                                Row(
                                  children: [
                                    Text(
                                      'Devices',
                                      style: TextStyle(color: AppColors.sensorInActiveColor, fontWeight: FontWeight.w500, fontSize: 12),
                                    ),
                                    Expanded(
                                      child: CheckboxListTile(
                                          contentPadding: EdgeInsets.zero,
                                          title: Text(
                                            'All',
                                            textAlign: TextAlign.right,
                                          ),
                                          value: isAllDevicesSelected,
                                          onChanged: (b) {
                                            setState(() {
                                              isAllDevicesSelected = b ?? false;
                                            });
                                          }),
                                    )
                                  ],
                                ),
                                SizedBox(
                                  height: 2,
                                ),
                                Stack(
                                  children: [
                                    DropdownSearch<String>.multiSelection(
                                        popupProps: PopupPropsMultiSelection.dialog(
                                            itemBuilder: (context, imei, isEnabled) {
                                              return Padding(
                                                padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                                                child: Text(vehicleNames[imei]!),
                                              );
                                            },
                                            showSearchBox: true,
                                            searchFieldProps: TextFieldProps(
                                              decoration: InputDecoration(
                                                hintText: 'Objects',
                                                contentPadding: EdgeInsets.only(left: 16, right: 0, top: 2, bottom: 2),
                                                border: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey[200]!)),
                                                disabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey[200]!)),
                                                enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey[200]!)),
                                                focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey[200]!)),
                                              ),
                                            )),
                                        items: vehicleNames.keys.toList(),
                                        onChanged: (s) {
                                          selectedImeis = s;
                                        },
                                        dropdownBuilder: (context, imeis) {
                                          List<String> objectsList = [];
                                          imeis.forEach((element) {
                                            objectsList.add(vehicleNames[element]!);
                                          });
                                          return Text(objectsList.isEmpty ? '---Select Devices---' : objectsList.join(', '));
                                        },
                                        filterFn: (a, b) => vehicleNames[a]!.toLowerCase().contains(b.toLowerCase()),
                                        selectedItems: []),
                                    if (isAllDevicesSelected)
                                      Positioned(
                                        left: 0,
                                        right: 0,
                                        top: 0,
                                        bottom: 0,
                                        child: Container(
                                          color: Colors.grey.withOpacity(0.4),
                                        ),
                                      )
                                  ],
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Text(
                                  'Stops',
                                  style: TextStyle(color: AppColors.sensorInActiveColor, fontWeight: FontWeight.w500, fontSize: 12),
                                ),
                                SizedBox(
                                  height: 2,
                                ),
                                Container(
                                  padding: EdgeInsets.only(left: 16, right: 10),
                                  decoration: BoxDecoration(borderRadius: BorderRadius.circular(5), color: Colors.white, border: Border.all(color: Colors.grey[200]!, width: 1)),
                                  child: DropdownButtonHideUnderline(
                                    child: DropdownButton(
                                      hint: Text(
                                        'Select Stop Duration',
                                        style: TextStyle(color: Colors.grey, fontSize: 12, fontWeight: FontWeight.w300),
                                      ),
                                      isExpanded: true,
                                      value: selectedStops,
                                      items: stopsArray.map((e) {
                                        return new DropdownMenuItem(
                                          value: e,
                                          child: Text(
                                            e,
                                            style: TextStyle(fontSize: 15.0),
                                          ),
                                        );
                                      }).toList(),
                                      onChanged: (value) {
                                        setState(() {
                                          selectedStops = value as String;
                                        });
                                      },
                                      style: TextStyle(color: Colors.black54),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Text(
                                  'Filter',
                                  style: TextStyle(color: Color(0xFF7E7E7E), fontWeight: FontWeight.w500, fontSize: 15),
                                ),
                                SizedBox(
                                  height: 2,
                                ),
                                Container(
                                  padding: EdgeInsets.only(left: 16, right: 10),
                                  decoration: BoxDecoration(borderRadius: BorderRadius.circular(5), color: Colors.white, border: Border.all(color: Colors.grey[200]!, width: 1)),
                                  child: DropdownButtonHideUnderline(
                                    child: DropdownButton(
                                      hint: Text(
                                        'Select POI Type',
                                        style: TextStyle(color: Colors.grey, fontSize: 12, fontWeight: FontWeight.w300),
                                      ),
                                      isExpanded: true,
                                      value: selectedFilter,
                                      items: filterArray.map((e) {
                                        return new DropdownMenuItem(
                                          value: e,
                                          child: Text(
                                            e,
                                            style: TextStyle(fontSize: 15.0),
                                          ),
                                        );
                                      }).toList(),
                                      onChanged: (value) {
                                        switch (filterArray.indexOf(value as String)) {
                                          case 0:
                                            //today
                                            dtf = zeoTimeDateFormat.format(now);
                                            dtt = zeoTimeDateFormat.format(now.add(Duration(days: 1)));
                                            break;
                                          case 1:
                                            //yesterday
                                            dtf = zeoTimeDateFormat.format(now.subtract(Duration(days: 1)));
                                            dtt = zeoTimeDateFormat.format(now);
                                            break;
                                          case 2:
                                            //before 2 days
                                            dtf = zeoTimeDateFormat.format(now.subtract(Duration(days: 2)));
                                            dtt = zeoTimeDateFormat.format(now.subtract(Duration(days: 1)));
                                            break;
                                          case 3:
                                            //before 3 days
                                            dtf = zeoTimeDateFormat.format(now.subtract(Duration(days: 3)));
                                            dtt = zeoTimeDateFormat.format(now.subtract(Duration(days: 2)));
                                            break;
                                          case 4:
                                            //this week
                                            dtf = zeoTimeDateFormat.format(dateOnLastMonday());
                                            dtt = zeoTimeDateFormat.format(now);
                                            break;
                                          case 5:
                                            //last week
                                            var mondayDate = dateOnLastMonday();
                                            dtf = zeoTimeDateFormat.format(mondayDate.subtract(Duration(days: 7)));
                                            dtt = zeoTimeDateFormat.format(mondayDate);
                                            break;
                                          case 6:
                                            //this month
                                            dtf = DateFormat('yyyy-MM-01').format(now);
                                            dtt = zeoTimeDateFormat.format(now.add(Duration(days: 1)));
                                            break;
                                          case 7:
                                            //last month
                                            dtf = DateFormat('yyyy-MM-01').format(new DateTime(now.year, now.month - 1, now.day));
                                            dtt = DateFormat('yyyy-MM-01').format(now);
                                            break;
                                        }
                                        setState(() {
                                          selectedFilter = value;
                                        });
                                      },
                                      style: TextStyle(color: Colors.black54),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Text(
                                  'From',
                                  style: TextStyle(color: Color(0xFF7E7E7E), fontWeight: FontWeight.w500, fontSize: 15),
                                ),
                                SizedBox(height: 2),
                                InkWell(
                                  onTap: () async {
                                    DateTime? selectedDate = await showDatePicker(context: context, initialDate: DateTime.now(), firstDate: DateTime(2000), lastDate: DateTime(2100));
                                    if (selectedDate == null) return;
                                    TimeOfDay? selectedTime = await showTimePicker(
                                      context: context,
                                      initialTime: TimeOfDay.fromDateTime(selectedDate),
                                    );
                                    if (selectedTime == null) return;

                                    setState(() {
                                      dtf = selectedDate.year.toString().padLeft(4, '0') +
                                          '-' +
                                          selectedDate.month.toString().padLeft(2, '0') +
                                          '-' +
                                          selectedDate.day.toString().padLeft(2, '0') +
                                          ' ' +
                                          selectedTime.hour.toString().padLeft(2, '0') +
                                          ':' +
                                          selectedTime.minute.toString().padLeft(2, '0') +
                                          ':00';
                                    });
                                  },
                                  child: Container(
                                    padding: EdgeInsets.symmetric(horizontal: 16, vertical: 12),
                                    decoration: BoxDecoration(border: Border.all(color: Colors.grey[200]!), borderRadius: BorderRadius.circular(5)),
                                    child: Row(
                                      children: [
                                        Image.asset(
                                          'images/start_date.png',
                                          height: 25,
                                          width: 25,
                                        ),
                                        SizedBox(
                                          width: 20,
                                        ),
                                        Text(dtf)
                                      ],
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Text(
                                  'To',
                                  style: TextStyle(color: Color(0xFF7E7E7E), fontWeight: FontWeight.w500, fontSize: 15),
                                ),
                                SizedBox(
                                  height: 2,
                                ),
                                InkWell(
                                  onTap: () async {
                                    DateTime? selectedDate = await showDatePicker(context: context, initialDate: DateTime.now(), firstDate: DateTime(2000), lastDate: DateTime(2100));
                                    if (selectedDate == null) return;
                                    TimeOfDay? selectedTime = await showTimePicker(
                                      context: context,
                                      initialTime: TimeOfDay.fromDateTime(selectedDate),
                                    );
                                    if (selectedTime == null) return;

                                    setState(() {
                                      dtt = selectedDate.year.toString().padLeft(4, '0') +
                                          '-' +
                                          selectedDate.month.toString().padLeft(2, '0') +
                                          '-' +
                                          selectedDate.day.toString().padLeft(2, '0') +
                                          ' ' +
                                          selectedTime.hour.toString().padLeft(2, '0') +
                                          ':' +
                                          selectedTime.minute.toString().padLeft(2, '0') +
                                          ':00';
                                    });
                                  },
                                  child: Container(
                                    padding: EdgeInsets.symmetric(horizontal: 16, vertical: 12),
                                    decoration: BoxDecoration(border: Border.all(color: Colors.grey[200]!), borderRadius: BorderRadius.circular(5)),
                                    child: Row(
                                      children: [
                                        Image.asset(
                                          'images/end_date.png',
                                          height: 25,
                                          width: 25,
                                        ),
                                        SizedBox(
                                          width: 20,
                                        ),
                                        Text(dtt)
                                      ],
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                InkWell(
                                  onTap: onGenerateBtnTapped,
                                  child: Container(
                                    height: 48,
                                    padding: EdgeInsets.symmetric(vertical: 10),
                                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(24), color: isLoadingReport ? AppColors.primaryColor2 : AppColors.primaryColor),
                                    child: Row(
                                      mainAxisSize: MainAxisSize.max,
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: [
                                        Text(
                                          'Generate Report',
                                          style: TextStyle(color: Colors.white, fontSize: 16, fontWeight: FontWeight.w600),
                                        ),
                                        if (isLoadingReport)
                                          Container(
                                            child: CircularProgressIndicator(
                                              color: Colors.white,
                                              strokeWidth: 2,
                                            ),
                                            margin: EdgeInsets.only(left: 12),
                                            height: 24,
                                            width: 24,
                                          )
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
            ],
          ),
        ),
      ),
    );
  }

  onGenerateBtnTapped() async {
    if (isLoadingReport) {
      return;
    }
    if (selectedImeis.length == 0) {
      showSnackBar(context, 'Please select vehicle to continue', Colors.red);
      return;
    }
    if (Repository.getServerType() == GPSWoxConstants.type) {
      int type = selectedReportMap!['type'];
      List<int> deviceIds = [];

      if (isAllDevicesSelected) {
        vehicleNames.forEach((imei, value) {
          deviceIds.add(AllDeviceModelData().deviceModelAndImeiMapGpsServer[imei]!.deviceId);
        });
      } else {
        selectedImeis.forEach((imei) {
          deviceIds.add(AllDeviceModelData().deviceModelAndImeiMapGpsServer[imei]!.deviceId);
        });
      }
      setState(() {
        isLoadingReport = true;
      });
      String fromDate = dtf.split(' ')[0];
      String fromTime = dtf.split(' ')[1];
      String toDate = dtt.split(' ')[0];
      String toTime = dtt.split(' ')[1];
      try {
        String data = await GPSWoxRequests.generateReport(selectedReportMap!['name']!, type, deviceIds, fromDate, toDate, fromTime, toTime);
        DateTime now = DateTime.now();
        String formattedDate = DateFormat('dd_MM_yyyy_HH_mm_ss').format(now);
        String fileName = '';
        Directory downloadsDirectory = await getApplicationDocumentsDirectory();
        fileName = downloadsDirectory.path + '/gps_report_' + formattedDate + '.html';
        File file = File(fileName);
        await file.writeAsString(data);
        showSnackBar(context, 'Report Downloaded', Colors.green);
        setState(() {
          isLoadingReport = false;
        });

        OpenFilex.open(fileName);
      } catch (e) {
        setState(() {
          isLoadingReport = false;
        });
        showSnackBar(context, e.toString(), Colors.red);
      }
    } else {
      String type = selectedReportMap!['key'];
      String sensorNames = '';
      if (type == 'sensor_graph') {
        sensorNames = 'ignition';
      }
      List<String> imeis = [];
      if (isAllDevicesSelected) {
        imeis.addAll(vehicleNames.keys);
      } else {
        imeis.addAll(selectedImeis);
      }
      setState(() {
        isLoadingReport = true;
      });
      String content = await GPSServerRequests.generateReport(selectedReportMap!['items'], dtf, dtt, imeis.join(','), sensorNames, 'false', 'false', selectedStops, type, 'false');

      DateTime now = DateTime.now();
      String formattedDate = DateFormat('dd_MM_yyyy_HH_mm_ss').format(now);
      String name = 'autonemo_report_' + type + '_' + formattedDate + '.html';

      Directory? downloadsDirectory = await getApplicationDocumentsDirectory();

      if (Platform.isAndroid) {
        downloadsDirectory = await DownloadsPath.downloadsDirectory();
      }

      String fileName = '${downloadsDirectory!.path}/$name';
      File file = File(fileName);
      await file.writeAsString(content);

      showSnackBar(context, 'Report Downloaded', Colors.green);
      setState(() {
        isLoadingReport = false;
      });

      OpenFilex.open(fileName);
    }
  }

  DateTime dateOnLastMonday() {
    int monday = 1;
    DateTime now = new DateTime.now();

    while (now.weekday != monday) {
      now = now.subtract(new Duration(days: 1));
    }
    return now;
  }
}
