import 'package:autonemogps/config/colors.dart';
import 'package:flutter/material.dart';

class AccountTicketWidget extends StatelessWidget {
  final String count, title;
  final String? image;

  const AccountTicketWidget(
      {required this.title, this.image, required this.count, Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(8),
      decoration: BoxDecoration(
        border: Border.all(color: Colors.grey[100]!),
        borderRadius: BorderRadius.circular(8),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                count,
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 18,
                    fontWeight: FontWeight.bold),
              ),
              if (image != null)
                Image.asset(
                  image!,
                  height: 20,
                  width: 20,
                  color: AppColors.primaryMaterialColor[100],
                ),
            ],
          ),
          SizedBox(
            height: 8,
          ),
          Text(
            title,
            style: TextStyle(
                color: AppColors.sensorInActiveColor,
                fontWeight: FontWeight.w300),
          ),
        ],
      ),
    );
  }
}
