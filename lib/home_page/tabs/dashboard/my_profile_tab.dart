import 'package:autonemogps/api_calls/gps_server_requests.dart';
import 'package:autonemogps/config/string_constants.dart';
import 'package:autonemogps/screens/my_account/account_detail_widget.dart';
import 'package:autonemogps/screens/my_account/referral_widget.dart';
import 'package:flutter/cupertino.dart';

class MyProfileTab extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return MyProfileTabState();
  }
}

class MyProfileTabState extends State<MyProfileTab> {
  String? error;
  int clientId = 0;
  String referralLink = '';
  int availablePoints = 0;
  int totalEarned = 0;
  int totalUsed = 0;
  int totalReferred = 0;
  Map<String, String> referralHistory = {};
  int ticketCreated = 0;
  int ticketOpened = 0;
  int invoiceCount = 0;
  String dueDate = '';
  String balance = '';
  String totalPaid = '';
  String invoiceTotalPaid = '';
  String paymentLink = '';
  List rewardPointsHistoryList = [];

  @override
  void initState() {
    super.initState();
    loadAccountData();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10),
      child: SingleChildScrollView(
        child: Column(
          children: [
            AccountDetailWidget(
              balance: balance,
              dueDate: dueDate,
              invoiceTotalPaid: invoiceTotalPaid,
              paymentLink: paymentLink,
              invoiceCount: invoiceCount,
              ticketCreated: ticketCreated,
              ticketOpened: ticketOpened,
              totalPaid: totalPaid,
            ),
            SizedBox(
              height: 16,
            ),
            ReferralWidget(
              availablePoints: availablePoints,
              clientId: clientId,
              referralHistory: referralHistory,
              referralLink: referralLink,
              totalEarned: totalEarned,
              totalReferred: totalReferred,
              totalUsed: totalUsed,
              rewardPointsHistoryList: rewardPointsHistoryList,
            ),
          ],
        ),
      ),
    );
  }

  loadAccountData() {
    GPSServerRequests.getClientData().then((value) {
      setState(() {
        if (value[STATUS] == 1) {
          Map<String, dynamic> dataMap = value[DATA];
          rewardPointsHistoryList = value[REWARD_POINTS_HISTORY];

          clientId = dataMap['id'] as int;
          referralLink = dataMap['referral_registration_link_short'] as String;
          availablePoints = dataMap['available_reward_points'] as int;
          totalEarned = dataMap['total_earned_reward_points'] as int;
          totalUsed = dataMap['total_reward_points_used'] as int;
          paymentLink = dataMap['client_payment_link'] as String;
          ticketOpened = dataMap['total_ticket']['total_open'] as int;
          ticketCreated = dataMap['total_ticket']['total_created'] as int;
          invoiceCount = dataMap['payment_and_invoice_info']['invoices_count'] as int;
          balance = dataMap['payment_and_invoice_info']['client_balance'];
          totalPaid = dataMap['payment_and_invoice_info']['payment_amount'];
          invoiceTotalPaid = dataMap['payment_and_invoice_info']['invoice_total_due'];
          totalReferred = dataMap['total_referred_client'] as int;

          try {
            referralHistory = Map<String, String>.from(dataMap['referred_clients']);
          } catch (e) {}

          error = '';
        } else {
          error = value[MESSAGE] as String;
        }
      });
    });
  }
}
