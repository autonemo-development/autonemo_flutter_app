import 'package:autonemogps/config/colors.dart';
import 'package:autonemogps/config/string_constants.dart';
import 'package:flutter/material.dart';

class TermsOfUsePage extends StatefulWidget {
  static const String page = 'terms_of-use_page';
  @override
  _TermsOfUsePageState createState() => _TermsOfUsePageState();
}

class _TermsOfUsePageState extends State<TermsOfUsePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Terms of use', style: TextStyle(color: Color(0xFF474747), fontWeight: FontWeight.w500, fontSize: 16)),
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Color(0xFF7E7E7E)),
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.all(15),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Image.asset('images/termsofuse-cover.png'),
              SizedBox(height: 15),
              Text(
                'The subscriber as mentioned in the registration form agrees to subscribe the ‘Vehicle Tracking Solution’ from $companyName Providing such services in Bangladesh under the following terms & conditions',
                style: TextStyle(color: Color(0xFF7E7E7E), fontWeight: FontWeight.w500),
              ),
              SizedBox(height: 15),
              Text('“$companyName” Service Agreement', style: TextStyle(color: Color(0xFFF26611), fontWeight: FontWeight.w500)),
              SizedBox(height: 15),
              TermsOfUseItem(
                headerText: 'Article 1: Introduction',
                details:
                    'Please read these terms and conditions (this “agreement”) in full. This agreement is between you (“you”) or “customer” are the original, end user purchaser of the GPS tracking device (“device”) that works in conjunction with our web site and the services associated with it (the “services”) and us (we are $companyName, the company that sold the device to you). This agreement contains important terms, conditions, allocations of risk between you and us, and limitations of our liability to you that we want you to know about and applies to all of past, present, and future use of your device and the services, until modified or replaced by new terms and conditions. It is effective when you (1) have a device installed and activated for use; (2) log onto our web site (where a link to this agreement appears) and acknowledge this agreement, i.e., by signing below; (3) use the services; (4) purchase more services; or (5) accept, at any time, any of the benefits of the services. This agreement is not intended to give anyone else a right, remedy, or claim of any kind against you or us. Note: your device is covered by a limited product warranty that is separate from this agreement.',
              ),
              SizedBox(height: 5),
              TermsOfUseItem(
                headerText: 'Article 2: Changes to this Agreement',
                details:
                    'Subject only to the limitations imposed upon us by our service providers or applicable law, we have the right, at our sole discretion, to modify this agreement and the services at any time, which includes (without limitation) the right to suspend the services. Changes to the agreement will be posted on our web site, which you agree to visit regularly, or may be sent to you via e-mail or postal mail. Your continued access of our web site and use of the services following posted notices of changes means that you have accepted and are bound by those changes.',
              ),
              SizedBox(height: 5),
              TermsOfUseItem(
                headerText: 'Article 3: Payment',
                details:
                    'You pay for the services by a valid credit or debit card or cheques or such other method as we expressly approve. If you give us a credit or debit card number, you are authorizing us to charge it for the services and all amounts due under this agreement without further notice to you. If your credit or debit card provider refuses a charge, the services can be terminated or suspended without notice. You assume exclusive liability for any and all taxes, tariffs, fees, duties, withholding or like charges, whether domestic or foreign now imposed or hereafter becoming effective related to the services or your VTS device or its components, (other than those based on our net income) including, without limitation, all federal, provincial, state and local taxes, as well as all value-added, goods and services, stamp documentary, excise and property taxes and duties. Payments not received within 10 days of any applicable due date are past due and we, in our sole discretion. Services will be disrupted without notice after the day of non-payment. Re-activation fees may apply!',
              ),
              SizedBox(height: 5),
              TermsOfUseItem(
                headerText: 'Article 4: Cancellation',
                details:
                    'Most service plans do not provide for pro rata refunds of unused services. You won’t be entitled to a refund of the purchase price of your device, subject to your product warranty. If you have your device deactivated, there may be a charge to reactivate it if you later decide to use it again.',
              ),
              SizedBox(height: 5),
              TermsOfUseItem(
                headerText: 'Article 5: Usage Limitations',
                details:
                    'You may not resell the services. They are for your own reasonable end use consistent with your service plan. The services may only be used for lawful purposes. Limits may be set on your level of use or tiered pricing may be applied based on your level or patterns of use. If you exceed those limits, you may be charged at higher rates (as detailed in your service plan) for your excess usage or we or our service providers may suspend your use of the services if we reasonably deem it to be abusive. You agree that we may use any credit or debit card or other payment account of yours that we have on file for payment of such charges.',
              ),
              SizedBox(height: 5),
              TermsOfUseItem(
                headerText: 'Article 6: No Transfer of the Services',
                details:
                    'The services are not transferable by you, even if you are a commercial user. If you intend to transfer ownership of a vehicle in which a device is installed, you agree that you will have the device uninstalled from it or have the device and associated services deactivated/terminated by us and inform the intended transferee—prior to the transfer—of the fact your vehicle has your device and advise the transferee to contact us with any questions.',
              ),
              SizedBox(height: 5),
              TermsOfUseItem(
                headerText: 'Article 7: Suspension and Termination of the Services',
                details:
                    'The services may be suspended or terminated without prior notice to you for good cause without liability. This means, to give some examples, that the services can be terminated or suspended if you breach any part of this agreement, do not pay amounts that are due under this agreement, interfere with provision of the services, or use the services for any illegal or otherwise improper purpose. The services may also be terminated or suspended if the availability of communication services used by us ends (for example, as a result of the discontinuance of service by a cellular telecommunications carrier) or is interrupted (for example, as a result of telephone/telecommunications network or internet congestion) or should any other circumstance arise or omission occur related to maintaining, repairing, or improving our network.',
              ),
              SizedBox(height: 5),
              TermsOfUseItem(
                headerText: 'Article 8: Privacy Policy',
                details:
                    'The information we get about you includes things such as your address, phone number, email address, and other contact information. The information we may get from your use of the services and your device includes things such as data about your vehicle’s location, speed, distance traveled, and operation, data about your use of the services, and other data depending on the services you use.\n\nYou agree that we can, subject to applicable law, use and share any of this information to: (a) provide the services to you; (b) assist emergency service providers, or others, as reasonably needed; (c) communicate with you about your account and collect payment; (d) check or maintain your device ; (e) help support your end use, i.e., fleet management, of the services; (f) develop new products and services and improve existing ones; (g) enforce this agreement; (h) prevent fraud, abuse, or misuse of the services; (i) comply with legal requirements, including valid court orders and subpoenas; or (j) protect the rights, property, or safety of you or others. You also agree that we and our service providers can, subject to applicable law, use this information: (a) to conduct market research; (b) to determine your eligibility for and offer you new or additional products and services that may be of interest to you; (c) to send you important product and service-related communications; and (d) to troubleshoot and improve the services (e) to Develop & Launch New Business. You also agree that we have all legal rights to use the data cumulatively which are being transmitted from your devices to our servers. This information will otherwise not be shared individually with third parties revealing your identity for their independent use without your consent.\n\nWe may also collect information from you or your device, or from your use of the services, aggregate that information, and use it with other aggregated information obtained from other persons (“aggregated information”). For example, we may use aggregated information to determine overall use of the services, identify usage patterns, and make product and service development decisions. We or our service providers own all rights in, and may share, aggregated information with any third party for any purpose without revealing your identity.\n\nBecause we provide service through wireless and other common carrier networks, we can’t promise that your communications won’t be intercepted by others. You agree we won’t be liable for any damages for any loss of privacy occurring in communication over such networks.',
              ),
              SizedBox(height: 5),
              TermsOfUseItem(
                headerText: 'Article 9: Lost or Stolen Vehicles',
                details:
                    'If your vehicle is lost or stolen, we can try to help you locate it, although we have no responsibility to do so, do not guarantee that it can or will be found, and do not guarantee the condition of your vehicle or the items that were in it should the vehicle be recovered. You may be asked to provide satisfactory identification and/or a police report. In any event, our obligation to assist you in providing commercially reasonable assistance to your efforts to locate your vehicle will end after 48 hours have elapsed from the time it was first reported to the authorities as missing or stolen. Should we provide assistance after such 48-hour period, you agree that we may do so if and when we see fit and that we will not be held liable for any acts or omissions that may arise with regard to such assistance. Regardless of the circumstance, we will be under no obligation to help you locate your vehicle for the purpose of tracking or locating a person or recovering any valuables contained in your vehicle.',
              ),
              SizedBox(height: 5),
              TermsOfUseItem(
                headerText: 'Article 10: Breach of this Agreement',
                details:
                    'You agree to indemnify and hold us and our parent corporation, affiliates, subsidiaries, employees, agents, and service providers harmless from and against any and all claims, demands, actions, causes of action, suits, proceedings, losses, damages, costs and expenses, including reasonable attorneys’ fees, arising from or relating to your use of the services, breach of this agreement, or any act, error, or omission on your part or that of anyone who uses the services. This provision will continue to apply after the termination or cancellation of this agreement.',
              ),
              SizedBox(height: 5),
              TermsOfUseItem(
                headerText: 'Article 11: Two Year Warranty',
                details:
                    'Your device is covered by 7 Days to 2 year full replacement warranty from the date you install based on product models. This includes protection from $companyName’s end to check your device properly and replace if needed.',
              ),
              SizedBox(height: 5),
              TermsOfUseItem(
                headerText: 'Article 12: limitations of the Services',
                details:
                    'The services use cellular or other wireless telecommunications networks in the country as well as the global positioning system (“GPS”) satellite network. The services are not available everywhere, particularly in remote areas, or at all times. Your vehicle has to have a properly functioning electrical system and adequately charged battery for the services to have any chance of working. The services may not work if your device is not properly installed by our authorized representative, not properly maintained, modified by any person other than our authorized representative, or combined with equipment, services, or software not expressly approved by us. Certain elements of the services, such as the ability to remotely unlock or disable your vehicle, may simply be incompatible with your vehicle’s design. There are other problems beyond our control that may prevent us from providing the services to you at any given time, such as damage to your vehicle in an accident, abuse or neglect of your device, terrain (hills, mountains, dips, valleys), buildings, bridges, tunnels, weather, the design of your vehicle, defects (including hidden defects) in your vehicle, localized “gaps” in cellular telephone network coverage, cellular telephone network congestion, and interference with the satellite transmissions that help supply the GPS data used by us in providing the services.',
              ),
              SizedBox(height: 5),
              TermsOfUseItem(
                headerText: 'Article 13: Important Limitations of our Liability',
                details:
                    'These limitations of liability constitute waivers of important legal rights. In that regard, you acknowledge that these limitations are integral to the pricing of the services, and that, were we or our service providers to assume any further liability other than as set forth herein, our prices for the services would necessarily be substantially higher. Unless prohibited by applicable law, you agree as follows:\n\nOne, you expressly agree that operation and use of your device or the services is at your sole risk. Neither we nor our service providers shall have any liability resulting from, or in any way related to, the use of your device or the services. You agree that you will not in any way hold us or our service providers responsible for any selection or retention of, or the acts or omissions of, other parties in connection the services. You understand that you have no contractual relationship whatsoever with any of our service providers and are not a third party beneficiary of any agreement between us and our service providers, although they may be third party beneficiaries of this agreement. You waive any and all claims or demands you would have if you were a third party beneficiary of any agreement between us and our service providers.\n\nTwo, our and our service providers’ maximum, cumulative liability to you under any theory (including but not limited to fraud, misrepresentation, breach of contract, personal injury, or products liability) for any one or more related or unrelated claims is limited to an amount equal to three times what you paid for the services under this agreement as of the date of the earliest event giving rise to your claim(s).\n\nThree, you agree, and we agree, not to make, and to waive to the fullest allowed by law, any claims for (1) punitive damages, (2) treble, consequential, indirect, lost profits, incidental or special damages regardless of whether or not either party was advised of the possibility of such damages, or (3) attorney’s fees. We both agree not to make, and to waive to the fullest extent allowed by law, any claim for damages other than direct, compensatory damages as limited in this agreement.\n\nFour, neither we nor our service providers are liable to you if the services are interrupted, or for problems caused by or contributed to by you, your vehicle (including its electrical system), by any third party, by buildings, hills, tunnels, telecommunications network congestion, weather, interference with satellite transmissions or any other things neither we nor our service providers control. Notwithstanding anything else in this agreement, you agree to excuse any non-performance by us or our service providers caused in whole or in part by an act or omission of a third party, or by any equipment failure, act of god, natural disaster, strike, equipment or facility shortage, or other causes beyond our control or the control of our service providers.\n\nFive, neither we nor our service providers can promise that any data or information supplied will be error-free. All data and information is provided to you on an “as is” basis. You agree that neither we nor any service provider who monitors, processes, or sends or receives you data or information through your device or the services is liable for any errors, defects, problems, or mistakes in that data or information. This means you cannot recover any damages of any kind, including consequential (such as lost revenues or lost contracts), indirect, incidental, special, or punitive damages for those errors, defects, problems, or mistakes. The foregoing limitation of liability covers, without limitation, “angel services” whereby you communicate verbally with us or our service providers for special assistance in utilizing the services.\n\nSix, to provide you with the services, we must enter into agreements with telecommunications carriers operating cellular and satellite networks and other service providers using technology compatible with the particular device you purchased. Neither we nor our service providers are liable if the services become unavailable or are interrupted because a service provider terminates its agreement with us or stops providing service to us, or does anything that renders your device obsolete or incompatible with the technology used by us or any of our other service providers.\n\nSeven, you agree that the limitations of liability and indemnities in this agreement will survive termination or expiration of this agreement and apply to you as well as anyone using your vehicle or the services, anyone making a claim on your behalf, and any claims made by anyone associated with you and arising out of or relating to the services.\n\nEight, the services are not a substitute for insurance. You promise that you will obtain and maintain appropriate insurance covering personal injury, loss of property, and other risks for yourself and for anyone else claiming under you. You hereby release and discharge us and our service providers from and against all hazards covered by your insurance. No insurance company or insurer will have any right of subrogation against us or our service providers.',
              ),
              SizedBox(height: 5),
              TermsOfUseItem(
                headerText: 'Article 14: Governing Law',
                details:
                    'To the fullest extent permitted by law, and except as explicitly provided otherwise, this agreement and any disputes arising out of or relating to it will be governed by the laws of the people’s republic of Bangladesh without regard to its conflict of law principles, and by any applicable tariffs, wherever filed. The UN Convention on contracts for the international sale of goods will not apply.',
              ),
              SizedBox(height: 5),
              TermsOfUseItem(
                headerText: 'Article 15: Resolution of Disputes',
                details:
                    'If you have a disagreement with us related to the services, we will try to resolve it informally. If we cannot resolve it informally in a manner and time frame we reasonably deem appropriate, you agree, and we agree, to the fullest extent permitted by law, to use arbitration and not go through the courts (except smalls claims courts as provided below) with to resolve our disagreement. A government agency can always be contacted for help.',
              ),
              SizedBox(height: 5),
              TermsOfUseItem(
                headerText: 'Article 16: Intellectual Property',
                details:
                    'All intellectual property rights and any other proprietary rights inherent in, and appurtenant to, the device and the services are and will remain the sole and exclusive property of their respective owners or licensors and you are not acquiring any such rights in the device or the services. You may not modify or reverse engineer your device or decompile any software associated with your device or the services.',
              ),
              SizedBox(height: 5),
              TermsOfUseItem(
                headerText: 'Article 17: Miscellaneous',
                details:
                    'If any part of this agreement is considered invalid by a court or arbitrator, the rest of it will remain enforceable. Even after this agreement has ended, its provisions will govern any disputes arising out of or relating to it (unless it has been replaced by changes to it that are issued by us or a new agreement between us). It will also be binding on your heirs and successors and on our successors. No waiver of any part of this agreement, or of any breach of it, in any one instance will require us to waive any other instance or breach. You agree that the limitations on remedies, limitations and exclusions of liability and disclaimers specified in this agreement will survive its termination. We reserve the right to assign this agreement and our rights and obligations under it, whether in whole or in part. If we make an assignment, we will have no further obligations to you.\n\nYou agree not to hold the dealer, $companyName, its owners or partners in any way liable for the use or misuse of this device. This device is not to be used as a “stalking” device. $companyName also does not provide the satellite equipment or map service directly and cannot be held liable in any way for this device as a dealer. This device and service is not designed to be a life saving device in any way. This device is used for informational purpose.\n\nBy their signatures below, customer and $companyName have read, understood, and executed this agreement in one or more counterparts, each of which constitutes an original but all of which together constitute one agreement. Transmission of signature pages by facsimile or other electronic means is acceptable.',
              ),
              SizedBox(
                height: 15,
              ),
              Text(
                'SERVICE LIMITATIONS',
                style: TextStyle(color: Color(0xFFF26611), fontWeight: FontWeight.w500),
              ),
              ServiceLimitationsItem('Services are available only if you have activated your account.'),
              SizedBox(
                height: 5,
              ),
              ServiceLimitationsItem('Services are available only if your vehicle ignition is on and your vehicle battery is charged and connected.'),
              SizedBox(
                height: 5,
              ),
              ServiceLimitationsItem('Services are available only if you are within operating range of the designated wireless network.'),
              SizedBox(
                height: 5,
              ),
              ServiceLimitationsItem(
                  'Services may be impaired by any wireless communication problems caused by atmospheric or topographical conditions, busy cells, capacity limitations, equipment problems, equipment, maintenance and other factors and conditions.'),
              SizedBox(
                height: 5,
              ),
              ServiceLimitationsItem(
                  'Services may be affected by inherent limitations of your vehicle’s electrical problems and architecture or if any of the components of your vehicle’s service limitations.'),
              SizedBox(
                height: 5,
              ),
              ServiceLimitationsItem('Services are not available if the GPS system is not working properly or the signals are obstructed.'),
              SizedBox(
                height: 5,
              ),
              ServiceLimitationsItem(
                  'Services may be affected by the natural calamity e.g. earthquake, hurricane, floods, labor strikes, civil commotion, riots, war: or any other act or event that is beyond reasonable control of $companyName.'),
              SizedBox(
                height: 5,
              ),
              ServiceLimitationsItem('Services may be limited in certain situations to some geographic areas, which are otherwise generally available under GPS.'),
              SizedBox(
                height: 5,
              ),
              ServiceLimitationsItem(
                  'Services are not available if analog and/or digital cellular telephone signals are used or if the wireless carrier demonstrates or restricts analog and/or digital service.'),
              SizedBox(
                height: 5,
              ),
              ServiceLimitationsItem('Services will not be available if Grameenphone, Robi, Teletalk, Banglalink or Airtel network is suspended or terminated due to any unavoidable circumstances.'),
              SizedBox(
                height: 5,
              ),
              ServiceLimitationsItem(
                  'If the Subscriber is unable to pay the yearly subscription bill within due date, the service will be suspended immediately and $companyName will not be liable for any kind of accident/theft of the vehicle during the suspended period of service.'),
              SizedBox(
                height: 5,
              ),
              ServiceLimitationsItem('Mileage report may defer from +- 10 to 15 %'),
              SizedBox(
                height: 5,
              ),
              ServiceLimitationsItem('GPS Accuracy cannot be ensured 100%, this may vary based on location and environment'),
              SizedBox(
                height: 5,
              ),
              ServiceLimitationsItem('$companyName cannot ensure 100% Server uptime, but we will try to ensure 99% server up time'),
              SizedBox(
                height: 5,
              ),
              ServiceLimitationsItem('There might be server issues time to time while we upgrade our server or platform features'),
              SizedBox(
                height: 5,
              ),
              ServiceLimitationsItem('$companyName recommend remote support for any software related after sales service'),
              SizedBox(
                height: 5,
              ),
              ServiceLimitationsItem('$companyName cannot ensure 100% device online due to Mobile operator network availability'),
              SizedBox(
                height: 5,
              ),
              ServiceLimitationsItem('$companyName do not recommend voice feature for your organization as there might be privacy concerns, we will not be liable for any legal obligations'),
              SizedBox(
                height: 5,
              ),
              ServiceLimitationsItem('Heavy commercial vehicles do not support remote engine shutdown feature'),
              SizedBox(
                height: 5,
              ),
              ServiceLimitationsItem('Remote engine shutdown feature is available for most common vehicles like Toyota Allion, Premio, Axio etc'),
              SizedBox(
                height: 5,
              ),
              ServiceLimitationsItem('Mobile Application Push alert might not be 100% real time in some cases based on handset models and in most cases will be delayed upto 2-3 minutes.'),
              SizedBox(
                height: 5,
              ),
              ServiceLimitationsItem(
                  '$companyName do not offer sms alerts, we only offer email / push notifications/ Telegram Bot  for alerts. But these alerts are not guaranteed 100% due to network congestion and other factors. In some cases it might be delayed up to some minutes.'),
              SizedBox(
                height: 80,
              )
            ],
          ),
        ),
      ),
    );
  }
}

// ignore: must_be_immutable
class TermsOfUseItem extends StatelessWidget {
  String headerText;
  String details;

  TermsOfUseItem({required this.headerText, required this.details});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            headerText,
            style: TextStyle(color: AppColors.primaryColor, fontWeight: FontWeight.w500, fontSize: 15),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(vertical: 12.0),
            child: Text(
              details,
              textAlign: TextAlign.justify,
              style: TextStyle(color: Color(0xFF7E7E7E), fontWeight: FontWeight.w400),
            ),
          )
        ],
      ),
    );
  }
}

// ignore: must_be_immutable
class ServiceLimitationsItem extends StatelessWidget {
  String details;

  ServiceLimitationsItem(this.details);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 12.0),
      child: Row(
        children: [
          new Container(
            height: 5,
            width: 5,
            decoration: new BoxDecoration(
              color: Color(0xFF7E7E7E),
              shape: BoxShape.circle,
            ),
          ),
          SizedBox(
            width: 10,
          ),
          Expanded(
            child: Text(
              details,
              textAlign: TextAlign.justify,
              style: TextStyle(color: Color(0xFF7E7E7E), fontWeight: FontWeight.w400),
            ),
          )
        ],
      ),
    );
  }
}
