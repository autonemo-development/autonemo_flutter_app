import 'package:autonemogps/config/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class LoadingDialog extends StatelessWidget {
  const LoadingDialog({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: ()async{return false;},
      child: Material(
        type: MaterialType.transparency,
        child: Center(
          child: SpinKitRing(
            lineWidth: 3.0,
            color: AppColors.primaryColor,
            size: 35.0,
          ),
        ),
      ),
    );
  }
}

/* class Loading extends StatelessWidget {
  const Loading({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: SpinKitFadingCube(
        color: AppColors.primaryColor,
        size: 40.0,
      )
    );
  }
} */

/// for seting animation duration(slow/fast animation)
/* 
import 'package:autonemogps/config/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class Loading extends StatefulWidget{
  const Loading({Key key}) : super(key: key);

  @override
  _LoadingState createState() => _LoadingState();
}

class _LoadingState extends State<Loading> with TickerProviderStateMixin {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: SpinKitFadingCube(
        color: AppColors.primaryColor,
        size: 40.0,
        //controller: AnimationController(vsync: this, duration: const Duration(milliseconds: 1500)),
      )
    );
  }
} */
