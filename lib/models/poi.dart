import 'package:google_maps_flutter/google_maps_flutter.dart';

class POI {
  String? id;
  LatLng? coordinates;
  String? name, iconUrl;
  int? radius;
  POI({this.coordinates, this.iconUrl, this.name, this.radius, this.id});

  factory POI.fromJson(Map<String, dynamic> json) {
    Map<String, dynamic> coordinates = json['coordinates'] as Map<String, dynamic>;
    return POI(id: json['_id'] as String, coordinates: LatLng(coordinates['lat'] as double, coordinates['lng'] as double), name: json['name'] as String, radius: json['radius'] as int);
  }
}
