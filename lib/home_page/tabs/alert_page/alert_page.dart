import 'package:autonemogps/api_calls/gps_server_requests.dart';
import 'package:autonemogps/api_calls/gps_wox_requests.dart';
import 'package:autonemogps/common/alert_list_item.dart';
import 'package:autonemogps/common/alerts_filter.dart';
import 'package:autonemogps/config/colors.dart';
import 'package:autonemogps/config/helpers.dart';
import 'package:autonemogps/config/string_constants.dart';
import 'package:autonemogps/models/alert.dart';
import 'package:autonemogps/models/device_model.dart';
import 'package:autonemogps/screens/detailed_alert_screen.dart';
import 'package:autonemogps/storage/repository.dart';
import 'package:custom_refresh_indicator/custom_refresh_indicator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:intl/intl.dart';

class AlertPage extends StatefulWidget {
  final DeviceModel? deviceModel;

  AlertPage({this.deviceModel});

  @override
  _AlertPageState createState() => _AlertPageState();
}

class _AlertPageState extends State<AlertPage> {
  List<Alert> alertList = List.empty(growable: true);
  bool isLoading = true;
  Map<String, String> _filter = {};
  String lastRefreshTime = '';
  String? error;

  @override
  void initState() {
    super.initState();
    fetchOfflineData();
    fetchOnlineData();
  }

  fetchLastRefreshTime() {
    String s = Repository.getLastRefreshedEventTime();
    if (s.isEmpty) {
      lastRefreshTime = s;
    } else {
      DateFormat dateFormat = DateFormat(STANDARD_DATE_TIME_INPUT_PATTERN);
      DateTime dateTime = dateFormat.parse(s);
      int diff = calculateDifference(dateTime);
      String d = '';
      if (diff == 0) {
        d = 'Today ' + dateTime.hour.toString().padLeft(2, '0') + ':' + dateTime.minute.toString().padLeft(2, '0');
      } else if (diff == -1) {
        d = 'Yesterday ' + dateTime.hour.toString().padLeft(2, '0') + ':' + dateTime.minute.toString().padLeft(2, '0');
      } else {
        d = s;
      }
      lastRefreshTime = 'Last Refresh : ' + d;
    }
  }

  fetchOfflineData() {
    String value = Repository.getAlerts();
    if (value.isNotEmpty && value != 'Error connecting to database.') {
      parse(value);
    }
  }

  parse(String s) {
    alertList = parseAlerts(s, _filter, '');
    fetchLastRefreshTime();
    if (mounted)
      setState(() {
        isLoading = false;
      });
  }

  fetchOnlineData() {
    if (Repository.getServerType() == GPSServerConstants.type) {
      return GPSServerRequests.fetchFnEvents('', _filter).then((value) {
        if (value.length > 0 && value[0] == 1) {
          parse(value[1]);
        }
        return;
      });
    } else if (Repository.getServerType() == GPSWoxConstants.type) {
      return GPSWoxRequests.fetchEvents().then((value) {
        if (value.length > 0 && value[0] == 1) {
          parse(value[1]);
        }
        return;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.white,
        foregroundColor: Colors.black,
        title: Text('Alerts'),
        actions: [
          IconButton(
            onPressed: () async {
              Map<String, String> filterList = await showModalBottomSheet(
                barrierColor: Color(0xFFF5F9FF).withOpacity(.5),
                isScrollControlled: true,
                backgroundColor: Colors.transparent,
                context: context,
                builder: (context) => AlertsFilterBottomSheet(_filter),
              );
              _filter = filterList;
              fetchOfflineData();
            },
            icon: Image.asset('images/filter.png', width: 23, color: Colors.black),
          )
        ],
      ),
      body: SafeArea(
        child: Column(
          children: [
            buildAlertsTopHeader(),
            Expanded(
              child: isLoading
                  ? SpinKitRing(lineWidth: 3.0, color: AppColors.primaryColor, size: 35.0)
                  : error == null
                      ? Scrollbar(
                          child: CustomRefreshIndicator(
                            onRefresh: () async {
                              await fetchOnlineData();
                            },
                            builder: (context, child, controller) {
                              return AnimatedBuilder(
                                animation: controller,
                                builder: (BuildContext context, _) {
                                  fetchLastRefreshTime();
                                  return Stack(
                                    alignment: Alignment.topCenter,
                                    children: <Widget>[
                                      !controller.isIdle
                                          ? Positioned(
                                              top: 20.0 * controller.value,
                                              child: Column(
                                                children: [
                                                  SpinKitRing(color: AppColors.primaryColor, lineWidth: 2.0, size: 20.0),
                                                  SizedBox(height: 15),
                                                  Text(lastRefreshTime, style: TextStyle(fontSize: 12), textAlign: TextAlign.center),
                                                ],
                                              ),
                                            )
                                          : SizedBox(),
                                      Transform.translate(offset: Offset(0, 75.0 * controller.value), child: child),
                                    ],
                                  );
                                },
                              );
                            },
                            child: buildListView(),
                          ),
                        )
                      : Center(child: Text(error!)),
            ),
          ],
        ),
      ),
    );
  }

  Container buildAlertsTopHeader() {
    return Container(
      color: Colors.blue[50],
      padding: EdgeInsets.all(16),
      child: Row(
        children: [
          Icon(Icons.warning_amber_outlined, color: AppColors.primaryColor2),
          SizedBox(width: 10),
          Expanded(
            child: Text(
              'Sometimes you may not get some alarms due to GPS coverage network errors, battery voltage issues and unsupported vehicles',
              style: TextStyle(fontSize: 10, color: AppColors.primaryColor2),
            ),
          ),
        ],
      ),
    );
  }

  ListView buildListView() {
    List<Alert> filteredAlerts = alertList;

    if (widget.deviceModel != null) {
      filteredAlerts = alertList.where((element) => element.vehicleName == widget.deviceModel!.name).toList();
    }

    return ListView.separated(
      separatorBuilder: (context, index) => Container(height: 10.0),
      itemCount: filteredAlerts.length + 1,
      itemBuilder: (context, index) {
        if (index == filteredAlerts.length) {
          return SizedBox(height: 80);
        }

        Alert alert = filteredAlerts[index];

        return InkWell(
            onTap: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) => DetailedAlert(event: alert.json, eventId: alert.id!)));
            },
            child: AlertListItem(iconColor: alert.iconColor, iconPath: alert.iconPath, title: alert.alertName, subTitle: alert.vehicleName, dateTime: alert.date));
      },
    );
  }

  double getTopBarHeight() {
    double width = MediaQuery.of(context).size.width;
    return (336 / 1081) * width;
  }
}
