import 'dart:convert';

import 'package:autonemogps/config/helpers.dart';
import 'package:autonemogps/storage/repository.dart';
import 'package:flutter/material.dart';

class DashboardWidgetSettings extends StatefulWidget {
  static String pageName = 'DashboardWidgetSettings';

  @override
  State<StatefulWidget> createState() {
    return DashboardWidgetSettingsState();
  }
}

class DashboardWidgetSettingsState extends State {
  List widgetListMap = [];

  @override
  void initState() {
    widgetListMap = getWidgetList();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Edit Widget'),
        backgroundColor: Colors.white,
        foregroundColor: Colors.black,
      ),
      body: Column(
        children: [
          Container(
            padding: EdgeInsets.all(5),
            child: Text(
              'Some Widgets will be shown only when data is available',
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 12),
            ),
          ),
          Expanded(
            child: Container(
              decoration: BoxDecoration(color: Colors.white),
              child: ReorderableListView(
                children: widgetListMap.map((item) {
                  String name = item['name'];
                  bool isEnabled = item['isEnabled'];

                  return Container(
                    padding: EdgeInsets.all(5),
                    margin: EdgeInsets.only(right: 10, left: 10, top: 5),
                    decoration: BoxDecoration(color: Color(0xFFF8F8F8), borderRadius: BorderRadius.circular(10)),
                    key: Key(name),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Expanded(child: Container(child: Text(name), padding: EdgeInsets.all(10))),
                        Checkbox(
                          activeColor: Colors.black,
                          value: isEnabled,
                          onChanged: (changedCheck) {
                            List<Map<String, dynamic>> updatedList = widgetListMap.map((e) {
                              Map<String, dynamic> currentItem = e;
                              if (currentItem['name'] == name) {
                                currentItem['isEnabled'] = changedCheck;
                              }
                              return currentItem;
                            }).toList();

                            setState(() {
                              widgetListMap = updatedList;
                            });

                            Repository.saveWidgetOrder(json.encode(widgetListMap));
                          },
                        ),
                        SizedBox(width: 20),
                        Container(child: Image.asset('images/dashboard_drag_button.png'), width: 35, padding: EdgeInsets.all(5))
                      ],
                    ),
                  );
                }).toList(),
                onReorder: (oldIndex, newIndex) {
                  setState(() {
                    if (oldIndex < newIndex) {
                      newIndex -= 1;
                    }
                    final Map<String, dynamic> item = widgetListMap.removeAt(oldIndex);
                    widgetListMap.insert(newIndex, item);

                    setState(() => widgetListMap);

                    Repository.saveWidgetOrder(json.encode(widgetListMap));
                  });
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
}
