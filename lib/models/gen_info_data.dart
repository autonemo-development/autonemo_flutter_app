class GenInfoData {
  final String title, value, image;

  GenInfoData(this.title, this.value, this.image);
}

String getImageName(String title) {
  switch (title) {
    case 'Top speed':
      return 'images/top-speed.png';
    case 'Route length':
      return 'images/route-length.png';
    case 'Fuel consumption':
      return 'images/fuel-cost.png';
    case 'Odometer':
      return 'images/odometer.png';
    case 'Average speed':
      return 'images/average-speed.png';
    case 'Move duration':
      return 'images/time_1.png';
    case 'Stop duration':
      return 'images/time.png';
    case 'Engine idle':
      return 'images/engne_idle.png';
    case 'Fuel cost':
      return 'images/enter-fuel-cost.png';
    case 'Overspeed count':
      return 'images/car_speed.png';
    case 'Stop count':
      return 'images/stop-sign.png';
    case 'Engine work':
      return 'images/engine-time.png';
    default:
      return 'images/route-length.png';
  }
}
