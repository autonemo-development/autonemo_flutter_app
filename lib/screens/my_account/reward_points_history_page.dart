import 'package:autonemogps/config/colors.dart';
import 'package:autonemogps/models/reward_point.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class RewardPointsHistory extends StatefulWidget {
  static const String page = 'ReferralHistory';
  final List value;

  RewardPointsHistory(this.value);

  @override
  _RewardPointsHistoryState createState() => _RewardPointsHistoryState();
}

class _RewardPointsHistoryState extends State<RewardPointsHistory> {
  bool _isLoading = true;
  String? error;
  List data = [];

  @override
  void initState() {
    super.initState();
    _isLoading = false;
    data = widget.value;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: AppColors.defaultBackground,
        appBar: AppBar(
          centerTitle: false,
          title: Text('Reward Points Earned', style: TextStyle(color: Color(0xFF474747), fontWeight: FontWeight.w500, fontSize: 16)),
          backgroundColor: Colors.white,
          iconTheme: IconThemeData(color: Color(0xFF7E7E7E)),
        ),
        body: _isLoading
            ? SpinKitRing(lineWidth: 3.0, color: AppColors.primaryColor, size: 35.0)
            : error != null
                ? Center(child: Text(error!))
                : ListView.builder(
                    padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                    itemCount: data.length,
                    itemBuilder: (context, index) {
                      RewardPoint rewardPoint = data.elementAt(index) as RewardPoint;
                      return ReferralHistoryItem(rewardPoint: rewardPoint);
                    },
                  ));
  }
}

class ReferralHistoryItem extends StatelessWidget {
  final RewardPoint rewardPoint;

  ReferralHistoryItem({required this.rewardPoint});

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.all(4),
        padding: EdgeInsets.symmetric(vertical: 12.0, horizontal: 16),
        width: MediaQuery.of(context).size.width / 2.27,
        decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(5.0)),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Row(
              children: [
                Container(
                  padding: EdgeInsets.all(10),
                  decoration: BoxDecoration(border: Border.all(color: AppColors.primaryColor, width: .8), shape: BoxShape.circle),
                  child: Container(
                    decoration: BoxDecoration(shape: BoxShape.circle),
                    child: Text(rewardPoint.pointsEarned),
                  ),
                ),
                SizedBox(width: 10),
                Expanded(child: Text(rewardPoint.title, style: TextStyle(color: Color(0xFF3A3A3A), fontSize: 16, fontWeight: FontWeight.w500))),
              ],
            ),
          ],
        ));
  }
}
