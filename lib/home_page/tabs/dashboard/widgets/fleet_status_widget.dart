import 'package:autonemogps/bloc/home_bloc.dart';
import 'package:autonemogps/bloc/home_state.dart';
import 'package:autonemogps/config/colors.dart';
import 'package:autonemogps/models/info_graphic_model.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class FleetStatus extends StatefulWidget {
  final Function changeToListTab;

  const FleetStatus(this.changeToListTab, {Key? key}) : super(key: key);

  @override
  State<FleetStatus> createState() => _FleetStatusState();
}

class _FleetStatusState extends State<FleetStatus> {
  late double chartLength;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    Size size = MediaQuery.of(context).size;
    chartLength = size.width / 1.75;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10),
      margin: EdgeInsets.only(bottom: 16),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(16),
        boxShadow: [BoxShadow(color: AppColors.shadowMaterialColor[100]!, blurRadius: 5.0, spreadRadius: 1.0, offset: Offset(1.0, 2.0))],
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text('Fleet Status', style: TextStyle(color: AppColors.headerTextColor)),
          SizedBox(width: 20),
          Divider(),
          BlocBuilder<HomeBloc, HomeState>(
              buildWhen: (previous, current) => current is UpdateCountState,
              builder: (context, state) {
                if (state is UpdateCountState) {
                  List<InfoGrphicModel> modelList = [
                    InfoGrphicModel('Moving', Colors.green, state.movingCount, () => widget.changeToListTab(2)),
                    InfoGrphicModel('Idle', Colors.orange, state.idleCount, () => widget.changeToListTab(3)),
                    InfoGrphicModel('Stopped', Colors.red, state.stoppedCount, () => widget.changeToListTab(4)),
                    InfoGrphicModel('Offline', Colors.blue, state.offlineCount, () => widget.changeToListTab(5)),
                    InfoGrphicModel('No Data', Colors.grey, state.noDataCount, () => widget.changeToListTab(7)),
                    InfoGrphicModel('Expired', Colors.black, state.expiredCount, () => widget.changeToListTab(6)),
                  ];
                  List<charts.Series<InfoGrphicModel, int>> seriesList = [
                    new charts.Series<InfoGrphicModel, int>(
                      id: 'fleetStatus',
                      domainFn: (InfoGrphicModel sales, _) => modelList.indexOf(sales),
                      measureFn: (InfoGrphicModel sales, _) => sales.count,
                      displayName: 'Fleet Status',
                      colorFn: (model, _) => charts.Color(r: model.backgroundColor.red, g: model.backgroundColor.green, b: model.backgroundColor.blue),
                      data: modelList,
                    )
                  ];
                  return Row(
                    children: [
                      Container(
                        height: chartLength,
                        width: chartLength,
                        child: Stack(
                          children: [
                            charts.PieChart(seriesList, animate: false, defaultRenderer: charts.ArcRendererConfig<int>(customRendererId: 'fleetRenderer', strokeWidthPx: 0, arcRatio: 0.5)),
                            Align(
                              alignment: Alignment.center,
                              child: InkWell(
                                onTap: () => widget.changeToListTab(1),
                                child: Column(mainAxisSize: MainAxisSize.min, children: [Text(state.allCount.toString(), style: TextStyle(fontWeight: FontWeight.w600, fontSize: 24)), Text('Total')]),
                              ),
                            )
                          ],
                        ),
                      ),
                      Expanded(child: Material(child: Column(children: modelList.map((e) => FleetStatusItem(e)).toList()))),
                    ],
                  );
                }
                return SpinKitRing(lineWidth: 3.0, color: AppColors.primaryColor, size: 35.0);
              }),
        ],
      ),
    );
  }
}

class FleetStatusItem extends StatelessWidget {
  final InfoGrphicModel model;

  const FleetStatusItem(this.model, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: model.onTap,
      child: Container(
        padding: EdgeInsets.symmetric(vertical: 6),
        child: Row(
          children: [
            Container(height: 10, width: 10, decoration: BoxDecoration(borderRadius: BorderRadius.circular(2), color: model.backgroundColor)),
            SizedBox(width: 10),
            Expanded(child: Text(model.text)),
            Text(model.count.toString())
          ],
        ),
      ),
    );
  }
}
