import 'dart:convert';

import 'package:autonemogps/config/colors.dart';
import 'package:autonemogps/utils/logs.dart';
import 'package:flutter/material.dart';
import 'package:math_expressions/math_expressions.dart';

class Sensor {
  String? sensorName;
  dynamic sensorValue;
  bool? paramValue;
  String? imageName;
  Color? sensorColor;

  Color? sensorLightColor;

  Sensor({
    this.sensorValue,
    this.paramValue,
    this.sensorName,
    this.imageName,
    this.sensorColor = AppColors.sensorActiveColor,
    this.sensorLightColor = AppColors.sensorLightActiveColor,
  });

// factory Sensor.fromJson(Map<String, dynamic> json) {
//   String sensorName = json['sensorName'] as String;
//   String sensorImage = 'images/sensor.png';
//   switch (sensorName) {
//     case 'Air Condition':
//       sensorImage = 'images/ac.png';
//       break;
//     case 'Door':
//       sensorImage = 'images/car-door.png';
//       break;
//     case 'Ignition':
//       sensorImage = 'images/ignition.png';
//       break;
//     case 'Power':
//       sensorImage = 'images/power-plug.png';
//       break;
//     case 'Fuel':
//       sensorImage = 'images/fuel.png';
//       break;
//     case 'Air Conditon':
//       sensorImage = 'images/ac.png';
//       break;
//     case 'Battery':
//       sensorImage = 'images/battery_volt.png';
//       break;
//   }
//   return Sensor(
//       param: json['param'] as String,
//       readingType: json['readingType'] as String,
//       sensorId: json['sensorId'] as String,
//       sensorName: json['sensorName'] as String,
//       sensorType: json['sensorType'] as String,
//       text_1: json['text_1'] as String,
//       text_0: json['text_0'] as String,
//       imageName: sensorImage,
//       fuelCalibData: json['fuelCalibData']);
// }
}

List<Sensor> parseGpsWoxSensorData(dynamic sensorData) {
  List<dynamic> sensorList = [];
  if (sensorData == null) {
    sensorList = [];
  } else if (sensorData is String) {
    if (sensorData == 'null') {
      sensorList = [];
    } else {
      sensorList = json.decode(sensorData) as List<dynamic>;
    }
  } else {
    sensorList = sensorData as List<dynamic>;
  }
  List<Sensor> sensors = [];
  sensorList.forEach((element) {
    Map<String, dynamic> sensorMap = element as Map<String, dynamic>;
    String sensorType = sensorMap['type'];
    String sensorName = sensorMap['name'];
    var val = sensorMap['val'];

    bool paramValue = (sensorName == 'Ignition' || sensorName == 'ACC' || sensorName == 'Engine' || sensorName == 'Engine Status') ? sensorMap['val'] : false;
    Color? sensorLightColor, sensorColor;

    if (sensorType == 'logical' || sensorType == 'ignition') {
      if ((val is bool) ? val : (val as String) == '1') {
        sensorLightColor = AppColors.sensorLightActiveColor;
        sensorColor = AppColors.sensorActiveColor;
      } else {
        sensorLightColor = AppColors.sensorLightInActiveColor;
        sensorColor = AppColors.sensorInActiveColor;
      }
    }
    sensors.add(Sensor(
      sensorValue: sensorMap['value'],
      paramValue: paramValue,
      sensorColor: sensorColor,
      sensorLightColor: sensorLightColor,
      sensorName: sensorName,
      imageName: getImageName(sensorMap['name']),
    ));
  });
  return sensors;
}

List<Sensor> buildSensorData(Map<String, dynamic> sensorJsonObject, Map<String, dynamic> sensorValuesObject) {
  List<Sensor> sensorList = [];
  Color sensorColor = AppColors.sensorActiveColor;
  Color sensorLightColor = AppColors.sensorLightActiveColor;

  for (int j = 0; j < sensorJsonObject.length; j++) {
    try {
      Map<String, dynamic> sensorData = sensorJsonObject.values.elementAt(j);
      String sensorName = sensorData['name'];
      String sensorType = sensorData['result_type'];
      String unit = sensorData['units'];
      String sensorKey = sensorData['param'];
      String sensorValue = sensorValuesObject[sensorKey] == null ? '0' : sensorValuesObject[sensorKey];
      String sensorFinalValue = 'N/A';
      bool paramValue = false;

      switch (sensorType) {
        case 'percentage':
          {
            if (sensorValue.contains('%') || sensorValue.isEmpty) {
              sensorFinalValue = sensorValue;
            } else {
              double hv = double.parse(sensorData['hv'] as String);
              double lv = double.parse(sensorData['lv'] as String);
              double sensorValueDouble = double.parse(sensorValue);

              if (sensorValueDouble > lv && sensorValueDouble < hv) {
                double a = sensorValueDouble - lv;
                double b = hv - lv;

                double percent = (a / b) * 100;
                if (percent > 100) {
                  percent = 100;
                }
                if (percent > 50) {
                  sensorColor = AppColors.sensorActiveColor;
                  sensorLightColor = AppColors.sensorLightActiveColor;
                } else {
                  sensorColor = AppColors.sensorInActiveColor;
                  sensorLightColor = AppColors.sensorLightInActiveColor;
                }
                sensorFinalValue = (percent.round()).toString();
              } else if (sensorValueDouble <= lv) {
                sensorFinalValue = '0';
                sensorColor = AppColors.sensorInActiveColor;
                sensorLightColor = AppColors.sensorLightInActiveColor;
              } else if (sensorValueDouble >= hv) {
                sensorFinalValue = '100';
                sensorColor = AppColors.sensorActiveColor;
                sensorLightColor = AppColors.sensorLightActiveColor;
              }
            }
          }
          break;
        case 'value':
          {
            double fuel = double.parse(sensorValue);

            String fuelFormula = sensorData['formula'];
            if (fuelFormula.isNotEmpty) {
              fuel = evalExp(fuelFormula, fuel);
              sensorFinalValue = fuel.toString();
            }

            List jsonArrayFuelCalib = sensorData['calibration'];
            Map<String, dynamic> jsonObjectFuelCalibData, jsonObjectFuelCalibData2;

            double x1, x2;
            if (jsonArrayFuelCalib.length != 0) {
              jsonArrayFuelCalib.sort((a, b) => (double.parse((a['x'] as String)) - double.parse(b['x'] as String)).round());

              for (int i = 0; i < jsonArrayFuelCalib.length - 1; i++) {
                jsonObjectFuelCalibData = jsonArrayFuelCalib.elementAt(i);
                jsonObjectFuelCalibData2 = jsonArrayFuelCalib.elementAt(i + 1);
                x1 = double.parse(jsonObjectFuelCalibData['x'] as String);
                x2 = double.parse(jsonObjectFuelCalibData2['x'] as String);
                if (x1 < fuel && x2 > fuel) {
                  sensorFinalValue = getMidValue(x1, x2, double.parse(jsonObjectFuelCalibData['y'] as String), double.parse(jsonObjectFuelCalibData2['y'] as String), fuel);
                  break;
                } else if (x1 == fuel) {
                  sensorFinalValue = jsonObjectFuelCalibData['y'] as String;
                  break;
                } else if (x2 == fuel) {
                  sensorFinalValue = jsonObjectFuelCalibData2['y'] as String;
                  break;
                }
              }

              if (sensorFinalValue == 'N/A') {
                if (fuel > double.parse(jsonArrayFuelCalib.elementAt(jsonArrayFuelCalib.length - 1)['x'])) {
                  sensorFinalValue = jsonArrayFuelCalib.elementAt(jsonArrayFuelCalib.length - 1)['y'] as String;
                } else if (fuel < double.parse(jsonArrayFuelCalib.elementAt(0)['x'])) {
                  sensorFinalValue = jsonArrayFuelCalib.elementAt(0)['y'] as String;
                }
              }
            }

            List dictionaryJsonArray = sensorData['dictionary'];
            for (int i = 0; i < dictionaryJsonArray.length; i++) {
              Map<String, dynamic> dictJsonObject = dictionaryJsonArray.elementAt(i);
              if ((dictJsonObject['value'] as String) == (sensorValue)) {
                sensorFinalValue = dictJsonObject['text'] as String;
              }
            }

            if (jsonArrayFuelCalib.length == 0 && dictionaryJsonArray.length == 0) {
              sensorFinalValue = (isInteger(fuel) ? fuel.toInt() : fuel.toStringAsFixed(2)).toString();
            }
          }
          break;
        case 'logic':
          {
            String text_0 = sensorData['text_0'] as String;
            String text_1 = sensorData['text_1'] as String;
            if (sensorValue == ('1')) {
              paramValue = true;
              sensorFinalValue = text_1;
              sensorColor = AppColors.sensorActiveColor;
              sensorLightColor = AppColors.sensorLightActiveColor;
            } else {
              paramValue = false;
              sensorFinalValue = text_0;
              sensorColor = AppColors.sensorInActiveColor;
              sensorLightColor = AppColors.sensorLightInActiveColor;
            }
          }
          break;
        case 'string':
          {
            sensorFinalValue = sensorValue;
            sensorColor = AppColors.sensorActiveColor;
            sensorLightColor = AppColors.sensorLightActiveColor;
          }
          break;
      }

      sensorFinalValue = '$sensorFinalValue $unit';
      if (sensorFinalValue != 'N/A') {
        sensorList.add(Sensor(
          imageName: getImageName(sensorName),
          sensorName: sensorName,
          sensorValue: sensorFinalValue,
          paramValue: paramValue,
          sensorColor: sensorColor,
          sensorLightColor: sensorLightColor,
        ));
      }
    } catch (e) {
      writeErrorLogs(e.toString(), e: e);
    }
  }
  return sensorList;
}

String getImageName(String name) {
  String sensorImage = 'images/sensor.png';
  switch (name) {
    case 'Ignition':
      sensorImage = 'images/ignition-filled.png';
      break;
    case 'Air Condition':
    case 'Air Con':
    case 'AC':
      sensorImage = 'images/ac.png';
      break;
    case 'Battery Level':
    case 'Battery Voltage':
    case 'Battery Voltage Level':
    case 'Internal Battery Level':
    case 'Internal Battery':
    case 'Device Battery':
    case 'Device Battery Level':
    case 'AD1 Voltage':
    case 'AD2 Voltage':
    case 'Vehicle Battery Level':
    case 'External Battery Level':
    case 'External Battery':
    case 'Vehicle Battery':
    case 'Battery Charging':
    case 'Battery Charging Status':
    case 'Battery Charge':
    case 'Charging Status':
      sensorImage = 'images/battery_volt.png';
      break;
    case 'Fuel Level':
    case 'CNG':
    case 'LPG':
    case 'CNG Level':
    case 'LPG Level':
    case 'Oil Level':
    case 'Fuel':
      sensorImage = 'images/fuel.png';
      break;
    case 'Weight':
    case 'Weight Level':
      sensorImage = 'images/weight.png';
      break;
    case 'Humadity':
    case 'Humidity':
    case 'Humadity Level':
      sensorImage = 'images/humidity.png';
      break;
    case 'Defense Mode':
    case 'Defense':
      sensorImage = 'images/defence_mode.png';
      break;
    case 'Door Status':
    case 'Door':
      sensorImage = 'images/car-door.png';
      break;
    case 'GPS Level':
    case 'GPS Signal':
      sensorImage = 'images/gps.png';
      break;
    case 'Lights':
    case 'Vehicle Lights':
      sensorImage = 'images/lights.png';
      break;
    case 'Camera Attached':
    case 'Camera':
      sensorImage = 'images/camera_attach.png';
      break;
    case 'External Voltage':
      sensorImage = 'images/external_voltage.png';
      break;
    case 'GSM Signal':
    case 'GSM Level':
      sensorImage = 'images/gsm_signal.png';
      break;
    case 'Moving Status':
      sensorImage = 'images/moving.png';
      break;
    case 'Temperature':
      sensorImage = 'images/temperature.png';
      break;
  }
  return sensorImage;
}

bool isInteger(num value) => (value % 1) == 0;

String getMidValue(double x1, double x2, double y1, double y2, double fuel) {
  double midX = (x1 + x2) / 2;
  double midY = (y1 + y2) / 2;

  while (x1 <= x2) {
    if (midX > fuel) {
      x2 = midX;
      y2 = midY;
    } else if (midX == fuel) {
      return midY.toStringAsFixed(2);
    } else {
      x1 = midX;
      y1 = midY;
    }
    midX = (x1 + x2) / 2;
    midY = (y1 + y2) / 2;
  }
  return midY.toStringAsFixed(2);
}

double evalExp(String input, double xVal) {
  Parser p = Parser();
  Expression exp = p.parse(input);
  ContextModel cm = ContextModel();
  cm.bindVariable(Variable('x'), Number(xVal));
  double eval = exp.evaluate(EvaluationType.REAL, cm);
  return eval;
}
