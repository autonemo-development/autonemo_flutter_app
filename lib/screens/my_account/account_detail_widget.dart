import 'package:autonemogps/config/colors.dart';
import 'package:autonemogps/config/helpers.dart';
import 'package:autonemogps/screens/my_account/account_ticket_widget.dart';
import 'package:flutter/material.dart';

class AccountDetailWidget extends StatelessWidget {
  final int ticketCreated;
  final int invoiceCount;
  final int ticketOpened;
  final String dueDate;
  final String balance;
  final String totalPaid;
  final String invoiceTotalPaid;
  final String paymentLink;

  const AccountDetailWidget(
      {required this.balance,
      required this.dueDate,
      required this.invoiceTotalPaid,
      required this.paymentLink,
      required this.invoiceCount,
      required this.ticketCreated,
      required this.ticketOpened,
      required this.totalPaid,
      Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(16),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8),
        color: Colors.white,
      ),
      child: Column(
        children: [
          Text(
            'Advance Payment',
            style: TextStyle(fontWeight: FontWeight.w300, color: AppColors.headerTextColor),
          ),
          Text(
            balance,
            style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 24,
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Divider(
            color: Colors.grey[100],
          ),
          Row(
            children: [
              CircleAvatar(
                backgroundColor: AppColors.primaryColor,
                child: Image.asset(
                  'images/pay.png',
                  height: 28,
                  width: 28,
                ),
              ),
              SizedBox(
                width: 10,
              ),
              Expanded(
                child: Text(
                  'Total due',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
                ),
              ),
              SizedBox(
                width: 10,
              ),
              Text(
                invoiceTotalPaid,
                style: TextStyle(color: AppColors.primaryColor, fontWeight: FontWeight.bold, fontSize: 18),
              ),
              SizedBox(
                width: 10,
              ),
              ElevatedButton(
                style: ElevatedButton.styleFrom(backgroundColor: AppColors.primaryColor),
                onPressed: () {
                  launchUrl(paymentLink);
                },
                child: Text('Pay Now'),
              ),
            ],
          ),
          Divider(
            color: Colors.grey[100],
          ),
          Row(
            children: [
              Expanded(
                child: AccountTicketWidget(title: 'Total Tickets', image: 'images/ticket.png', count: ticketCreated.toString()),
              ),
              SizedBox(
                width: 10,
              ),
              Expanded(
                child: AccountTicketWidget(title: 'Open Tickets', image: 'images/ticket.png', count: ticketOpened.toString()),
              ),
              SizedBox(
                width: 10,
              ),
              Expanded(
                child: AccountTicketWidget(title: 'Total Invoices', image: 'images/invoice.png', count: invoiceCount.toString()),
              ),
            ],
          )
        ],
      ),
    );
  }
}
