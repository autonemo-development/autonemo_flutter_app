import 'package:autonemogps/api_calls/gps_server_requests.dart';
import 'package:autonemogps/common/refer_redeem_dialog.dart';
import 'package:autonemogps/config/colors.dart';
import 'package:autonemogps/config/string_constants.dart';
import 'package:autonemogps/screens/my_account/account_detail_widget.dart';
import 'package:autonemogps/screens/my_account/referral_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class MyAccountPage extends StatefulWidget {
  final bool? fromDashboard;
  final int? selectedTab;

  MyAccountPage({Key? key, this.fromDashboard, this.selectedTab}) : super(key: key);

  @override
  _MyAccountPageState createState() => _MyAccountPageState();
}

class _MyAccountPageState extends State<MyAccountPage> {
  bool _isLoading = true;
  String? error;
  int clientId = 0;
  late String referralLink;
  int availablePoints = 0;
  int totalEarned = 0;
  int totalUsed = 0;
  int totalReferred = 0;
  Map<String, String> referralHistory = {};
  int ticketCreated = 0;
  int ticketOpened = 0;
  int invoiceCount = 0;
  String dueDate = '';
  String balance = '';
  String totalPaid = '';
  String invoiceTotalPaid = '';
  String paymentLink = '';
  List rewardPointsHistoryList = [];

  @override
  void initState() {
    loadData();
    super.initState();
  }

  @override
  void didUpdateWidget(covariant MyAccountPage oldWidget) {
    if (widget.selectedTab == 1) {
      loadData();
    }
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Container(
          padding: EdgeInsets.all(20),
          height: MediaQuery.of(context).size.height,
          decoration: BoxDecoration(
            gradient: LinearGradient(begin: Alignment.topCenter, end: Alignment.bottomCenter, colors: [Color(0x90DC0E18), AppColors.primaryMaterialColor[50]!]),
          ),
          child: Column(
            children: [
              if (this.widget.fromDashboard == null) SizedBox(height: 10),
              if (this.widget.fromDashboard == null)
                Row(
                  children: [
                    IconButton(onPressed: () => Navigator.of(context).pop(), icon: Icon(Icons.keyboard_backspace, color: Colors.white)),
                    Expanded(child: Text('My Account', style: TextStyle(color: Colors.white, fontSize: 24, fontWeight: FontWeight.bold))),
                    InkResponse(
                      onTap: () => showReferRedeemDialog(context),
                      child: Container(child: Image.asset('images/question-circle.png', height: 27, width: 27, color: Colors.white)),
                    ),
                    SizedBox(width: 16),
                  ],
                ),
              Expanded(
                child: _isLoading
                    ? SpinKitRing(lineWidth: 3.0, color: AppColors.primaryColor, size: 35.0)
                    : error == null
                        ? ListView(
                            children: [
                              AccountDetailWidget(
                                balance: balance,
                                dueDate: dueDate,
                                invoiceTotalPaid: invoiceTotalPaid,
                                paymentLink: paymentLink,
                                invoiceCount: invoiceCount,
                                ticketCreated: ticketCreated,
                                ticketOpened: ticketOpened,
                                totalPaid: totalPaid,
                              ),
                              SizedBox(height: 16),
                              ReferralWidget(
                                availablePoints: availablePoints,
                                clientId: clientId,
                                referralHistory: referralHistory,
                                referralLink: referralLink,
                                totalEarned: totalEarned,
                                totalReferred: totalReferred,
                                totalUsed: totalUsed,
                                rewardPointsHistoryList: rewardPointsHistoryList,
                              )
                            ],
                          )
                        : Center(child: Text(error!)),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void loadData() {
    GPSServerRequests.getClientData().then((value) {
      setState(() {
        if (value[STATUS] == 1) {
          Map<String, dynamic> dataMap = value[DATA];
          rewardPointsHistoryList = value[REWARD_POINTS_HISTORY];

          clientId = dataMap['id'] as int;
          referralLink = dataMap['referral_registration_link_short'] as String;
          availablePoints = dataMap['available_reward_points'] as int;
          totalEarned = dataMap['total_earned_reward_points'] as int;
          totalUsed = dataMap['total_reward_points_used'] as int;
          paymentLink = dataMap['client_payment_link'] as String;
          ticketOpened = dataMap['total_ticket']['total_open'] as int;
          ticketCreated = dataMap['total_ticket']['total_created'] as int;
          invoiceCount = dataMap['payment_and_invoice_info']['invoices_count'] as int;
          balance = dataMap['payment_and_invoice_info']['client_balance'];
          totalPaid = dataMap['payment_and_invoice_info']['payment_amount'];
          invoiceTotalPaid = dataMap['payment_and_invoice_info']['invoice_total_due'];
          totalReferred = dataMap['total_referred_client'] as int;
          referralHistory = Map<String, String>.from(dataMap['referred_clients']);
        } else {
          error = value[MESSAGE] as String;
        }
        _isLoading = false;
      });
    });
  }
}
