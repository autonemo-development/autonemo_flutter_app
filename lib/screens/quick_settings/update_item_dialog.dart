import 'package:autonemogps/api_calls/gps_server_requests.dart';
import 'package:autonemogps/api_calls/gps_wox_requests.dart';
import 'package:autonemogps/common/make_payment_form_field.dart';
import 'package:autonemogps/config/colors.dart';
import 'package:autonemogps/config/helpers.dart';
import 'package:autonemogps/config/string_constants.dart';
import 'package:autonemogps/storage/repository.dart';
import 'package:autonemogps/utils/enums.dart';
import 'package:flutter/material.dart';

import '../../models/device_model.dart';

class UpdateItemDialog extends StatefulWidget {
  final SettingUpdateType type;
  final Function(String) onValueUpdated;
  final String image, placeholder, imei;
  final TextInputType inputType;
  final DeviceModel? deviceModel;

  const UpdateItemDialog(this.image, this.placeholder, this.type, this.imei, this.inputType, this.onValueUpdated, {Key? key, this.deviceModel}) : super(key: key);

  @override
  State<UpdateItemDialog> createState() => _UpdateItemDialogState();
}

class _UpdateItemDialogState extends State<UpdateItemDialog> {
  bool _isValueUpdating = false;
  String enteredValue = '';

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(15),
      decoration: BoxDecoration(borderRadius: BorderRadius.circular(5), color: Colors.white),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Row(
            children: [
              Image.asset(widget.image, height: 30),
              SizedBox(width: 10),
              Expanded(
                child: MakePaymentFormField(
                  textInputType: widget.inputType,
                  hinText: widget.placeholder,
                  onChanged: (value) {
                    setState(() {
                      enteredValue = value;
                    });
                  },
                ),
              ),
            ],
          ),
          SizedBox(height: 10),
          _isValueUpdating
              ? Container(padding: EdgeInsets.symmetric(horizontal: 25, vertical: 10), child: CircularProgressIndicator())
              : InkWell(
                  onTap: enteredValue.isEmpty
                      ? null
                      : () {
                          if (widget.type == 5) {
                            widget.onValueUpdated(enteredValue);
                            Navigator.of(context).pop();
                          } else {
                            setState(() {
                              _isValueUpdating = true;
                            });
                            if (Repository.getServerType() == GPSServerConstants.type) {
                              GPSServerRequests.updateValues(widget.imei, enteredValue, widget.type).then((response) {
                                if (response == 'OK') {
                                  Navigator.of(context).pop();
                                  widget.onValueUpdated(enteredValue);
                                } else {
                                  showSnackBar(context, response, Colors.red);
                                  setState(() {
                                    _isValueUpdating = false;
                                  });
                                }
                              });
                            } else if (Repository.getServerType() == GPSWoxConstants.type) {
                              GPSWoxRequests.updateValues(widget.deviceModel!, widget.type, enteredValue).then((value) {
                                Navigator.of(context).pop();
                                widget.onValueUpdated(enteredValue);
                              });
                            }
                          }
                        },
                  child: Container(
                    padding: EdgeInsets.symmetric(horizontal: 25, vertical: 10),
                    decoration: BoxDecoration(color: enteredValue.isEmpty ? AppColors.primaryColor2 : AppColors.primaryColor, borderRadius: BorderRadius.circular(5)),
                    child: Text('Update', style: TextStyle(color: Colors.white, fontWeight: FontWeight.w500)),
                  ),
                ),
        ],
      ),
    );
  }
}
