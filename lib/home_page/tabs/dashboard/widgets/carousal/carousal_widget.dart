import 'dart:async';

import 'package:autonemogps/config/string_constants.dart';
import 'package:autonemogps/home_page/tabs/dashboard/widgets/carousal/carousal_item.dart';
import 'package:flutter/material.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

class CarousalDart extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return CarousalDartState();
  }
}

class CarousalDartState extends State<CarousalDart> {
  final PageController _controller = PageController(viewportFraction: 1, initialPage: 0);

  final String shopUrl = SHOP_URL;

  late Timer _timer;

  int _currentPage = 0;

  List<CarousalItem> pages = [];

  @override
  void initState() {
    super.initState();

    pages = [
      CarousalItem('images/top_banner_1.png', shopUrl),
      CarousalItem('images/genuine_air_filters.png', shopUrl),
      CarousalItem('images/genuine_coolant_water.png', shopUrl),
      CarousalItem('images/genuine_oil_filters.png', shopUrl),
      CarousalItem('images/genuine_spark_plugs.png', shopUrl),
      CarousalItem('images/lubricants_banner.png', shopUrl),
      CarousalItem('images/vehicle_dashcam.png', shopUrl),
      CarousalItem('images/lubricants_banner.png', shopUrl),
      CarousalItem('images/helmets_banner.png', shopUrl),
    ];

    _timer = Timer.periodic(Duration(seconds: 3), (Timer timer) {
      if (_currentPage < pages.length - 1) {
        _currentPage++;
      } else {
        _currentPage = 0;
      }

      _controller.animateToPage(_currentPage, duration: Duration(milliseconds: 350), curve: Curves.easeIn);
    });
  }

  @override
  void dispose() {
    super.dispose();
    _timer.cancel();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: 16),
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            SizedBox(
              height: 16,
            ),
            SizedBox(
              height: 120,
              child: Container(
                child: PageView.builder(
                  allowImplicitScrolling: true,
                  controller: _controller,
                  itemBuilder: (context, index) {
                    return pages[index % pages.length];
                  },
                  onPageChanged: (currentPage) {
                    this._currentPage = currentPage;
                  },
                ),
              ),
            ),
            SizedBox(
              height: 16,
            ),
            SmoothPageIndicator(
              controller: _controller,
              count: pages.length,
              effect: WormEffect(
                dotHeight: 10,
                dotWidth: 10,
                type: WormType.thin,
                activeDotColor: Colors.red,
              ),
              onDotClicked: (currentPage) {
                this._currentPage = currentPage;
              },
            ),
          ],
        ),
      ),
    );
  }
}
