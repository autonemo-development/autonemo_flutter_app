class HistoryData {
  String data;
  String img;
  HistoryData({
    required this.img,
    required this.data,
  });
}
