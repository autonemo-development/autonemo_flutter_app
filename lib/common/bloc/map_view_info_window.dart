import 'package:autonemogps/common/bloc/window_bloc.dart';
import 'package:autonemogps/common/bloc/window_event.dart';
import 'package:autonemogps/common/bloc/window_state.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class MapInfoWindow extends StatefulWidget {
  final String data;

  MapInfoWindow({Key? key, required this.data}) : super(key: key);

  @override
  _MapInfoWindowState createState() => _MapInfoWindowState();
}

class _MapInfoWindowState extends State<MapInfoWindow> {
  double offsetY = 0, offsetX = 0;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) => BlocProvider.of<WindowBloc>(context).add(WindowLoadedEvent(height: context.size?.height ?? 0, width: context.size?.width ?? 0)));
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<WindowBloc, WindowState>(
      listener: (context, state) {
        if (state is PositionChangedState) {
          setState(() {
            offsetY = state.offsetY;
            offsetX = state.offsetX;
          });
        }
      },
      child: Transform(
        transform: Matrix4.translationValues(offsetX, offsetY, 0.0),
        child: Container(
          decoration: BoxDecoration(color: Colors.white),
          padding: EdgeInsets.all(10),
          child: Text(widget.data),
        ),
      ),
    );
  }
}
