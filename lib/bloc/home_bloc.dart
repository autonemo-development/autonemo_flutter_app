import 'dart:async';

import 'package:autonemogps/api_calls/gps_server_requests.dart';
import 'package:autonemogps/api_calls/gps_wox_requests.dart';
import 'package:autonemogps/bloc/home_event.dart';
import 'package:autonemogps/bloc/home_state.dart';
import 'package:autonemogps/common/session_expire_dialog.dart';
import 'package:autonemogps/config/helpers.dart';
import 'package:autonemogps/config/string_constants.dart';
import 'package:autonemogps/main.dart';
import 'package:autonemogps/models/device.dart';
import 'package:autonemogps/models/device_model.dart';
import 'package:autonemogps/models/tracking_device_model.dart';
import 'package:autonemogps/screens/login_page/login_page.dart';
import 'package:autonemogps/storage/repository.dart';
import 'package:autonemogps/utils/logs.dart';
import 'package:autonemogps/utils/push_notification_service.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:intl/intl.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  ScrollController topItemScrollController = ScrollController();

  static int selectItem = 1;
  String _searchText = '';
  String searchMapText = '';

  bool isExpiredVehicleShown = false;
  bool isSessionExpiryDialogShown = false;
  Map<String, DeviceModel> deviceModelMap = Map();
  Map<String, TrackingDevice> trackingDataMap = Map();
  Map<String, List<String>> deviceStatusImeiMap = {'all': [], 'moving': [], 'offline': [], 'idle': [], 'na': [], 'stopped': [], 'expired': []};

  Timer? timer;

  HomeBloc() : super(IdleState()) {
    on<SessionExpiredEvent>((event, emit) {
      if (Repository.isLoggedIn() && !isSessionExpiryDialogShown) {
        isSessionExpiryDialogShown = true;
        showSessionExpiredDialog(navigatorKey.currentState!.context).then((_) => isSessionExpiryDialogShown = false);
      }
    });
    on<SearchEvent>((event, emit) {
      _searchText = event.search;
      add(TrackingDataFetchedEvent());
    });
    on<MapSearchEvent>((event, emit) {
      searchMapText = event.search;
      emit(_mapSearchToState(event.search));
    });
    on<InitEvent>((event, emit) => emit(IdleState()));
    on<NoInternetEvent>((event, emit) => emit(NoInternetState()));
    on<DataFetchingEvent>((event, emit) => emit(DataFetchingState()));
    on<TrackingDataFetchedEvent>((event, emit) {
      deviceStatusImeiMap = getSortedImeiList(_searchText);
      if ((deviceStatusImeiMap['toBeExpiredList']?.length ?? 0) > 0 && !isExpiredVehicleShown) {
        String expiredDeviceNames = '';
        deviceStatusImeiMap['toBeExpiredList']?.forEach((imei) => expiredDeviceNames += (deviceModelMap[imei]?.name ?? '') + ', ');
        emit(DeviceExpiredState(expiredDeviceNames.substring(0, expiredDeviceNames.length - 2)));
        isExpiredVehicleShown = true;
      }
      emit(
        UpdateCountState(
          selectedList: selectItem,
          allCount: deviceStatusImeiMap['all']?.length ?? 0,
          movingCount: deviceStatusImeiMap['moving']?.length ?? 0,
          expiredCount: deviceStatusImeiMap['expired']?.length ?? 0,
          stoppedCount: deviceStatusImeiMap['stopped']?.length ?? 0,
          noDataCount: deviceStatusImeiMap['na']?.length ?? 0,
          idleCount: deviceStatusImeiMap['idle']?.length ?? 0,
          offlineCount: deviceStatusImeiMap['offline']?.length ?? 0,
        ),
      );

      String type = getEventType(selectItem);

      emit(_buildDataFetchedState(deviceStatusImeiMap[type]));
      add(MapSearchEvent(search: searchMapText));
      emit(IdleState());
    });

    on<StartTimerEvent>((event, emit) async {
      writeDebugLogs('StartTimerEvent');
      try {
        deviceModelMap = parseDeviceData();
        if (deviceModelMap.length > 0) {
          await fetchAndSetTrackingData();
          timer?.cancel();
          timer = Timer.periodic(Duration(seconds: 10), (timer) async => await fetchAndSetTrackingData());
        } else {
          add(TrackingDataFetchedEvent());
          Future.delayed(Duration(seconds: 5)).then((value) => add(StartTimerEvent()));
        }
      } catch (e) {
        writeErrorLogs('Timer Error ', e: e);
      }
    });
    on<GetInitialDataEvent>((event, emit) async {
      deviceModelMap = parseDeviceData();
      if (deviceModelMap.length > 0) {
        String trackingData = Repository.getTrackingData();
        trackingDataMap = parseTrackingData(trackingData);
        add(TrackingDataFetchedEvent());
      }
    });
    on<UpdateListTypeEvent>((event, emit) {
      selectItem = event.selectedItem;
      add(TrackingDataFetchedEvent());
      double offset = (selectItem - 1) * 50;
      if (selectItem == 6 || selectItem == 7) {
        offset = MediaQuery.of(navigatorKey.currentState!.context).size.width;
      }
      topItemScrollController.animateTo(offset, duration: Duration(milliseconds: 500), curve: Curves.fastOutSlowIn);
    });
    on<ChangeSearchWidgetEvent>((event, emit) {
      emit(SearchUpdatedState(event.showSearch));
      emit(IdleState());
    });
    on<RefreshDataEvent>((event, emit) async => await fetchAndSetTrackingData());
    on<LogoutEvent>(((event, emit) async {
      isExpiredVehicleShown = false;
      PushNotificationService.unsubscribeFromNotification();
      await Repository.performLogout();
      AllTrackingData().trackingDeviceImeiMap.clear();
      AllDeviceModelData().deviceModelAndImeiMapGpsServer.clear();
      Navigator.of(navigatorKey.currentContext!, rootNavigator: true).popAndPushNamed(LoginPage.page);
      timer?.cancel();
      GPSServerRequests.resetUrl();
      GPSWoxRequests.resetUrl();
    }));

    on<CheckForPopupEvent>(((CheckForPopupEvent event, Emitter<HomeState> emit) async {
      Future<Null> future = Future.delayed(Duration(minutes: 3));

      await future.whenComplete(() {
        String popUpDate = Repository.getPopupDate();
        DateFormat format = DateFormat('dd-MM-yyyy');

        if (popUpDate.isEmpty) {
          emit(ShowTimedDialog());
          Repository.setPopupDate(format.format(DateTime.now()));
        } else {
          DateTime d = format.parse(popUpDate);
          Duration duration = d.difference(DateTime.now());
          if (duration.inDays.abs() >= 15) {
            emit(ShowTimedDialog());
            Repository.setPopupDate(format.format(DateTime.now()));
          }
        }
      });
    }));
  }

  HomeState _mapSearchToState(String search) {
    if (search.isEmpty) {
      return MapDataFetchedState(deviceList: deviceModelMap, trackingList: trackingDataMap);
    }

    Map<String, DeviceModel> searchedDeviceList = Map<String, DeviceModel>();
    Map<String, TrackingDevice> searchedTrackingList = Map<String, TrackingDevice>();
    List<String> imeiList = deviceModelMap.keys.toList();
    for (int i = 0; i < imeiList.length; i++) {
      String imei = imeiList.elementAt(i);
      if (imei.contains(search) || (deviceModelMap[imei]!.name).toLowerCase().contains((search).toLowerCase())) {
        searchedDeviceList.putIfAbsent(imei, () => deviceModelMap[imei]!);
        searchedTrackingList.putIfAbsent(imei, () => trackingDataMap[imei]!);
      }
    }
    return MapDataFetchedState(deviceList: searchedDeviceList, trackingList: searchedTrackingList);
  }

  DataFetchedState _buildDataFetchedState(List<String>? imeiList) {
    Map<String, DeviceModel> deviceList = {};
    Map<String, TrackingDevice?> trackingDeviceList = {};
    if (imeiList != null) {
      for (String imei in imeiList) {
        deviceList.putIfAbsent(imei, () => deviceModelMap[imei]!);
        trackingDeviceList.putIfAbsent(imei, () => trackingDataMap[imei]);
      }
    }
    return DataFetchedState(deviceList: deviceList, trackingList: trackingDataMap);
  }

  Future<void> fetchAndSetTrackingData() async {
    add(DataFetchingEvent());

    String? trackingData;
    if (Repository.getServerType() == GPSServerConstants.type) {
      trackingData = await GPSServerRequests.fetchFnObjects();
    } else if (Repository.getServerType() == GPSWoxConstants.type) {
      add(TrackingDataFetchedEvent());
    }

    if (trackingData != null) {
      trackingDataMap = parseTrackingData(trackingData);
      add(TrackingDataFetchedEvent());
      Repository.setTrackingData(trackingData);
    } else {
      bool isConnected = await isInternetConnected();
      if (!isConnected) {
        add(NoInternetEvent());
      }
    }

    await GPSServerRequests.fetchFnSettings();
  }

  String getEventType(int selectItem) {
    String type = 'all';
    switch (selectItem) {
      case 1:
        type = 'all';
        break;
      case 2:
        type = 'moving';
        break;
      case 3:
        type = 'idle';
        break;
      case 4:
        type = 'stopped';
        break;
      case 5:
        type = 'offline';
        break;
      case 6:
        type = 'expired';
        break;
      case 7:
        type = 'na';
        break;
    }
    return type;
  }
}
