class AlertSetting{
  String alertId, alertName, type;
  bool alertStatus;
  Map<String, dynamic> map;
  
  AlertSetting(this.alertId, this.alertName, this.alertStatus,this.type, this.map);
  factory AlertSetting.fromGPSWoxData(Map<String, dynamic> json){
    return AlertSetting(json['id'].toString(), json['name'],json['active']==1,'', json);
  }

  factory AlertSetting.fromGPSServerData(Map<String, dynamic> json, String id){
    return AlertSetting(id, json['name'], json['active'].toString()=='true', json['type'],json);
  }
}