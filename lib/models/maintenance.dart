class Maintenance {
  final String objectName, name, odometer, odometerLeft, engineHours, enginHoursLeft, days, daysLeft, id, imei;
  final bool isExpired;
  Maintenance(this.days, this.daysLeft, this.engineHours, this.enginHoursLeft, this.id, this.name, this.odometer, this.odometerLeft, this.objectName, this.isExpired, this.imei);
}
