import 'dart:convert';

import 'package:autonemogps/config/string_constants.dart';
import 'package:autonemogps/models/device_model.dart';
import 'package:autonemogps/models/tracking_device_model.dart';
import 'package:autonemogps/storage/repository.dart';
import 'package:autonemogps/utils/logs.dart';

class AllDeviceModelData {
  Map<String, DeviceModel> deviceModelAndImeiMapGpsServer = {};
  Map<int, DeviceModel> deviceListByDeviceId = {};
  static final AllDeviceModelData _singleton = new AllDeviceModelData._internal();

  factory AllDeviceModelData() {
    return _singleton;
  }

  AllDeviceModelData._internal() {
    deviceModelAndImeiMapGpsServer = Map<String, DeviceModel>();
    deviceListByDeviceId = Map<int, DeviceModel>();
  }
}

Map<String, DeviceModel> parseDeviceData() {
  String str = Repository.getAllDevicesData();
  AllDeviceModelData device = AllDeviceModelData();
  if (str.isNotEmpty) {
    if (Repository.getServerType() == GPSWoxConstants.type) {
      List<dynamic> list = json.decode(str) as List<dynamic>;
      list.forEach((element) {
        Map<String, dynamic> groupMap = element as Map<String, dynamic>;
        List itemsList = groupMap['items'] as List;
        itemsList.forEach((element) {
          DeviceModel deviceModel = DeviceModel.fromGPSWoxServerJson(element as Map<String, dynamic>);
          device.deviceModelAndImeiMapGpsServer.putIfAbsent(deviceModel.imei, () => deviceModel);
          device.deviceListByDeviceId.putIfAbsent(deviceModel.deviceId, () => deviceModel);
          AllTrackingData().trackingDeviceImeiMap[deviceModel.imei] = TrackingDevice.fromGPSWoxServerJson(element);
        });
      });
    } else {
      try {
        Map<String, dynamic> map = json.decode(str) as Map<String, dynamic>;

        if (map['data'].runtimeType == List) {
          return device.deviceModelAndImeiMapGpsServer;
        }

        Map<String, dynamic> data = map['data'] as Map<String, dynamic>;

        data.forEach((imei, singleDeviceData) {
          device.deviceModelAndImeiMapGpsServer.putIfAbsent(imei, () => DeviceModel.fromGpsServerJson(singleDeviceData, imei));
        });
      } on Exception catch (e) {
        writeErrorLogs('parseDeviceData ', e: e);
      }
    }
  }

  return device.deviceModelAndImeiMapGpsServer;
}
